-- MySQL Script generated by MySQL Workbench
-- 07/04/15 09:32:51
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dbjuegos
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dbjuegos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbjuegos` DEFAULT CHARACTER SET utf8 ;
USE `dbjuegos` ;

-- -----------------------------------------------------
-- Table `dbjuegos`.`tipo_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`tipo_usuario` (
  `idtipo_usuario` INT NOT NULL AUTO_INCREMENT,
  `rol` VARCHAR(45) NULL,
  PRIMARY KEY (`idtipo_usuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`institucion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`institucion` (
  `idinstitucion` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idinstitucion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`grupo` (
  `idgrupo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idgrupo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`usuario` (
  `idusuario` INT NOT NULL AUTO_INCREMENT,
  `nombres` VARCHAR(45) NULL,
  `ap` VARCHAR(45) NULL DEFAULT NULL,
  `am` VARCHAR(45) NULL DEFAULT NULL,
  `fecha_nacimiento` VARCHAR(15) NULL DEFAULT NULL,
  `idtipo_usuario` INT NULL,
  `idinstitucion` INT NULL,
  `idgrupo` INT NULL,
  PRIMARY KEY (`idusuario`),
  INDEX `fk_usuario_tipo_usuario_idx` (`idtipo_usuario` ASC),
  INDEX `fk_usuario_institucion1_idx` (`idinstitucion` ASC),
  INDEX `fk_usuario_grupo1_idx` (`idgrupo` ASC),
  CONSTRAINT `fk_usuario_tipo_usuario`
    FOREIGN KEY (`idtipo_usuario`)
    REFERENCES `dbjuegos`.`tipo_usuario` (`idtipo_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1`
    FOREIGN KEY (`idinstitucion`)
    REFERENCES `dbjuegos`.`institucion` (`idinstitucion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_grupo1`
    FOREIGN KEY (`idgrupo`)
    REFERENCES `dbjuegos`.`grupo` (`idgrupo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`sesion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`sesion` (
  `idsesion` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`idsesion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`juego`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`juego` (
  `idjuego` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL DEFAULT NULL,
  `desccripcion` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idjuego`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`juego_sesion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`juego_sesion` (
  `idjuego_sesion` INT NOT NULL AUTO_INCREMENT,
  `fecha` VARCHAR(15) NULL DEFAULT NULL,
  `sesion_idsesion` INT NOT NULL,
  `juego_idjuego` INT NOT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  INDEX `fk_juego_sesion_sesion1_idx` (`sesion_idsesion` ASC),
  INDEX `fk_juego_sesion_juego1_idx` (`juego_idjuego` ASC),
  CONSTRAINT `fk_juego_sesion_sesion1`
    FOREIGN KEY (`sesion_idsesion`)
    REFERENCES `dbjuegos`.`sesion` (`idsesion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_juego1`
    FOREIGN KEY (`juego_idjuego`)
    REFERENCES `dbjuegos`.`juego` (`idjuego`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`partida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`partida` (
  `idpartida` INT NOT NULL AUTO_INCREMENT,
  `duracion` INT NULL,
  `partidacol` VARCHAR(45) NULL,
  `idusuario` INT NOT NULL,
  `idjuego_sesion` INT NOT NULL,
  PRIMARY KEY (`idpartida`),
  INDEX `fk_partida_usuario1_idx` (`idusuario` ASC),
  INDEX `fk_partida_juego_sesion1_idx` (`idjuego_sesion` ASC),
  CONSTRAINT `fk_partida_usuario1`
    FOREIGN KEY (`idusuario`)
    REFERENCES `dbjuegos`.`usuario` (`idusuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_juego_sesion1`
    FOREIGN KEY (`idjuego_sesion`)
    REFERENCES `dbjuegos`.`juego_sesion` (`idjuego_sesion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`sesion_grupo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`sesion_grupo` (
  `idsesion_grupo` INT NOT NULL AUTO_INCREMENT,
  `idsesion` INT NOT NULL,
  `idgrupo` INT NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  INDEX `fk_sesion_grupo_sesion1_idx` (`idsesion` ASC),
  INDEX `fk_sesion_grupo_grupo1_idx` (`idgrupo` ASC),
  CONSTRAINT `fk_sesion_grupo_sesion1`
    FOREIGN KEY (`idsesion`)
    REFERENCES `dbjuegos`.`sesion` (`idsesion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_grupo1`
    FOREIGN KEY (`idgrupo`)
    REFERENCES `dbjuegos`.`grupo` (`idgrupo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbjuegos`.`partida_repeticion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`partida_repeticion` (
  `idpartida_repeticion` INT NOT NULL AUTO_INCREMENT,
  `idpartida` INT NOT NULL,
  `fecha` VARCHAR(15) NULL,
  `duracion` INT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  INDEX `fk_partida_repeticion_partida1_idx` (`idpartida` ASC),
  CONSTRAINT `fk_partida_repeticion_partida1`
    FOREIGN KEY (`idpartida`)
    REFERENCES `dbjuegos`.`partida` (`idpartida`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `dbjuegos` ;

-- -----------------------------------------------------
-- Placeholder table for view `dbjuegos`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbjuegos`.`view1` (`id` INT);

-- -----------------------------------------------------
-- View `dbjuegos`.`view1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbjuegos`.`view1`;
USE `dbjuegos`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
