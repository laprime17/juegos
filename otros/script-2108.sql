/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 5.6.17 : Database - dbjuegos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbjuegos` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbjuegos`;

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

LOCK TABLES `grupo` WRITE;

UNLOCK TABLES;

/*Table structure for table `institucion` */

DROP TABLE IF EXISTS `institucion`;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `institucion` */

LOCK TABLES `institucion` WRITE;

UNLOCK TABLES;

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `idjuego` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `desccripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idjuego`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego` */

LOCK TABLES `juego` WRITE;

UNLOCK TABLES;

/*Table structure for table `juego_sesion` */

DROP TABLE IF EXISTS `juego_sesion`;

CREATE TABLE `juego_sesion` (
  `idjuego_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(15) DEFAULT NULL,
  `sesion_idsesion` int(11) NOT NULL,
  `juego_idjuego` int(11) NOT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  KEY `fk_juego_sesion_sesion1_idx` (`sesion_idsesion`),
  KEY `fk_juego_sesion_juego1_idx` (`juego_idjuego`),
  CONSTRAINT `fk_juego_sesion_juego1` FOREIGN KEY (`juego_idjuego`) REFERENCES `juego` (`idjuego`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_sesion1` FOREIGN KEY (`sesion_idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego_sesion` */

LOCK TABLES `juego_sesion` WRITE;

UNLOCK TABLES;

/*Table structure for table `mapa` */

DROP TABLE IF EXISTS `mapa`;

CREATE TABLE `mapa` (
  `idmapa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `imagen` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`idmapa`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

/*Data for the table `mapa` */

LOCK TABLES `mapa` WRITE;

insert  into `mapa`(`idmapa`,`nombre`,`descripcion`,`imagen`) values (1,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(2,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(3,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(4,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(5,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(6,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(7,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(8,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(9,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(10,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(11,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(12,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(13,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(14,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(15,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(16,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(17,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(18,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(19,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(20,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(21,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(22,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(23,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(24,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(25,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(26,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(27,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(28,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(29,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(30,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(31,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(32,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(33,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(34,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(35,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(36,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(37,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(38,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(39,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(40,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(41,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(42,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(43,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(44,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(45,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(46,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(47,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(48,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(49,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(50,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(51,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(52,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(53,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(54,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(55,'','','http://localhost/juegos/assets/uploads/MK.jpg'),(56,'','','http://localhost/juegos/assets/uploads/ros.JPG'),(57,'','','http://localhost/juegos/assets/uploads/ros.JPG'),(58,'','','http://localhost/juegos/assets/uploads/ros.JPG');

UNLOCK TABLES;

/*Table structure for table `mapa_punto` */

DROP TABLE IF EXISTS `mapa_punto`;

CREATE TABLE `mapa_punto` (
  `idmapa_punto` int(11) NOT NULL AUTO_INCREMENT,
  `px` int(11) DEFAULT NULL,
  `py` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `idmapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa_punto`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

/*Data for the table `mapa_punto` */

LOCK TABLES `mapa_punto` WRITE;

insert  into `mapa_punto`(`idmapa_punto`,`px`,`py`,`descripcion`,`idmapa`) values (8,406,191,'undefined',0),(9,300,300,'undefined',32),(10,365,166,'dejalo',33),(11,343,170,'jjaj',34),(12,417,151,'123',35),(13,436,129,'lakjskasj',36),(14,300,300,'64654',37),(15,300,300,'897',38),(16,427,110,'879',39),(17,391,64,'54545644',40),(18,397,174,'546454',41),(19,436,137,'79798797',42),(20,436,74,'8979798',43),(21,300,300,'545646',44),(22,300,300,'hello',46),(23,246,160,'hheheheh',47),(24,289,142,'hello',48),(25,436,227,'lala',48),(26,436,107,'pepe',48),(27,60,183,'nose',48),(28,206,192,'kakaka',49),(29,433,192,'jello',49),(30,78,216,'jajaja',49),(31,436,129,'jello',50),(32,193,194,'jjjJ',50),(33,436,231,'JAJAJAJ',50),(34,158,115,'JKLJJK',50),(35,436,97,'hello',51),(36,297,247,'pepe',51),(37,75,124,'jajaja',51),(38,198,234,'89',51),(39,379,115,'hello',52),(40,131,249,'jajjajaj',52),(41,129,152,'lolololo',52),(42,420,157,'hello',53),(43,176,156,'jja',53),(44,378,258,'lele',53),(45,421,125,'hello',54),(46,136,191,'nno',54),(47,436,216,'4444',54),(48,395,220,'hello',55),(49,154,117,'lal',55),(50,436,211,'jajjaja',56),(51,108,205,'punto2',56),(52,359,232,'p1',57);

UNLOCK TABLES;

/*Table structure for table `partida` */

DROP TABLE IF EXISTS `partida`;

CREATE TABLE `partida` (
  `idpartida` int(11) NOT NULL AUTO_INCREMENT,
  `duracion` int(11) DEFAULT NULL,
  `partidacol` varchar(45) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idjuego_sesion` int(11) NOT NULL,
  PRIMARY KEY (`idpartida`),
  KEY `fk_partida_usuario1_idx` (`idusuario`),
  KEY `fk_partida_juego_sesion1_idx` (`idjuego_sesion`),
  CONSTRAINT `fk_partida_juego_sesion1` FOREIGN KEY (`idjuego_sesion`) REFERENCES `juego_sesion` (`idjuego_sesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `persona` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida` */

LOCK TABLES `partida` WRITE;

UNLOCK TABLES;

/*Table structure for table `partida_repeticion` */

DROP TABLE IF EXISTS `partida_repeticion`;

CREATE TABLE `partida_repeticion` (
  `idpartida_repeticion` int(11) NOT NULL AUTO_INCREMENT,
  `idpartida` int(11) NOT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  KEY `fk_partida_repeticion_partida1_idx` (`idpartida`),
  CONSTRAINT `fk_partida_repeticion_partida1` FOREIGN KEY (`idpartida`) REFERENCES `partida` (`idpartida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida_repeticion` */

LOCK TABLES `partida_repeticion` WRITE;

UNLOCK TABLES;

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `ap` varchar(45) DEFAULT NULL,
  `am` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(15) DEFAULT NULL,
  `idtipo_usuario` int(11) DEFAULT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `usuario` varchar(6) DEFAULT NULL,
  `clave` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_tipo_usuario_idx` (`idtipo_usuario`),
  KEY `fk_usuario_institucion1_idx` (`idinstitucion`),
  KEY `fk_usuario_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipo_usuario` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `persona` */

LOCK TABLES `persona` WRITE;

insert  into `persona`(`idusuario`,`nombres`,`ap`,`am`,`fecha_nacimiento`,`idtipo_usuario`,`idinstitucion`,`idgrupo`,`usuario`,`clave`) values (1,'Rosa','Silva',NULL,'1234654564',1,NULL,NULL,'rsilva','123');

UNLOCK TABLES;

/*Table structure for table `sesion` */

DROP TABLE IF EXISTS `sesion`;

CREATE TABLE `sesion` (
  `idsesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sesion` */

LOCK TABLES `sesion` WRITE;

UNLOCK TABLES;

/*Table structure for table `sesion_grupo` */

DROP TABLE IF EXISTS `sesion_grupo`;

CREATE TABLE `sesion_grupo` (
  `idsesion_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idsesion` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  KEY `fk_sesion_grupo_sesion1_idx` (`idsesion`),
  KEY `fk_sesion_grupo_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_sesion_grupo_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_sesion1` FOREIGN KEY (`idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sesion_grupo` */

LOCK TABLES `sesion_grupo` WRITE;

UNLOCK TABLES;

/*Table structure for table `tipo_usuario` */

DROP TABLE IF EXISTS `tipo_usuario`;

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_usuario` */

LOCK TABLES `tipo_usuario` WRITE;

insert  into `tipo_usuario`(`idtipo_usuario`,`rol`) values (1,'administrador'),(2,'profesor'),(3,'alumno');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
