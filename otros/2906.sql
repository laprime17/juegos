/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 5.6.17 : Database - dbjuegos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbjuegos` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbjuegos`;

/*Table structure for table `abeja` */

DROP TABLE IF EXISTS `abeja`;

CREATE TABLE `abeja` (
  `idabeja` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idabeja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `abeja` */

LOCK TABLES `abeja` WRITE;

UNLOCK TABLES;

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` int(11) DEFAULT NULL,
  `edad_maxima` int(11) DEFAULT NULL,
  `stsreg` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

LOCK TABLES `grupo` WRITE;

insert  into `grupo`(`idgrupo`,`nombre`,`idpersona_creador`,`fecha_creacion`,`edad_minima`,`edad_maxima`,`stsreg`) values (15,'1ro de Primario \"Estrellitas\"',32,'2016-03-19',6,7,'ACT'),(16,'Abejitas A',32,'2016-05-30',6,9,'ACT'),(17,'Grupo 1',50,'2016-06-07',6,9,'ACT'),(18,'Grupo 1',50,'2016-06-07',6,9,'ACT'),(19,'Grupo 1',50,'2016-06-07',6,9,'ACT');

UNLOCK TABLES;

/*Table structure for table `institucion` */

DROP TABLE IF EXISTS `institucion`;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `institucion` */

LOCK TABLES `institucion` WRITE;

insert  into `institucion`(`idinstitucion`,`nombre`) values (1,'Organización General');

UNLOCK TABLES;

/*Table structure for table `invasores` */

DROP TABLE IF EXISTS `invasores`;

CREATE TABLE `invasores` (
  `idinvasores` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` smallint(6) DEFAULT NULL,
  `texto_comprension` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`idinvasores`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

/*Data for the table `invasores` */

LOCK TABLES `invasores` WRITE;

insert  into `invasores`(`idinvasores`,`puntaje`,`tiempo`,`texto_comprension`) values (80,6.000,6,'<p>lallalalallalallalalala</p>\n'),(81,50.000,2,'<p>,lslalslasasas</p>\n'),(82,99.999,5,'<p><strong>orem Ipsum</strong>&nbsp;es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) descon</p>\n');

UNLOCK TABLES;

/*Table structure for table `invasores_detalles` */

DROP TABLE IF EXISTS `invasores_detalles`;

CREATE TABLE `invasores_detalles` (
  `idinvasores_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idinvasores` int(11) DEFAULT NULL,
  `tipo` varchar(11) DEFAULT NULL,
  `soles` int(11) DEFAULT NULL,
  `centimos` int(11) DEFAULT NULL,
  `enunciado` varchar(200) DEFAULT NULL,
  `nro_monedabillete` int(11) DEFAULT NULL,
  `tipofigura` varchar(45) DEFAULT NULL,
  `enunciadofig` varchar(200) DEFAULT NULL,
  `nro_figEsperadas` int(11) DEFAULT NULL,
  `pregunta` varchar(200) DEFAULT NULL,
  `respuesta` varchar(200) DEFAULT NULL,
  `primerDistractor` varchar(200) DEFAULT NULL,
  `segundoDistractor` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idinvasores_detalle`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

/*Data for the table `invasores_detalles` */

LOCK TABLES `invasores_detalles` WRITE;

insert  into `invasores_detalles`(`idinvasores_detalle`,`fk_idinvasores`,`tipo`,`soles`,`centimos`,`enunciado`,`nro_monedabillete`,`tipofigura`,`enunciadofig`,`nro_figEsperadas`,`pregunta`,`respuesta`,`primerDistractor`,`segundoDistractor`) values (111,80,'comprension',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'como s ellama el lobo','juanito','pedro','pepe'),(112,81,'comprension',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Como se llmaa?','lobo','mlo','pepe'),(113,82,'comprension',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'lobo','lobo','lala','lala');

UNLOCK TABLES;

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `idjuego` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `curso_dirigido` varchar(45) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` varchar(2) DEFAULT NULL,
  `edad_maxima` varchar(2) DEFAULT NULL,
  `estado` varchar(25) DEFAULT 'activo',
  `fk_idpersona` int(11) DEFAULT NULL,
  `fk_idjuegoespecifico` int(11) DEFAULT NULL,
  `ruta_imagen` varchar(100) DEFAULT NULL,
  `fk_juego_especifico` int(11) DEFAULT NULL,
  `fk_idtipo` int(10) DEFAULT NULL,
  PRIMARY KEY (`idjuego`),
  KEY `fk_idpersona` (`fk_idpersona`),
  CONSTRAINT `juego_ibfk_4` FOREIGN KEY (`fk_idpersona`) REFERENCES `persona` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8;

/*Data for the table `juego` */

LOCK TABLES `juego` WRITE;

insert  into `juego`(`idjuego`,`nombre`,`descripcion`,`curso_dirigido`,`tipo`,`fecha_creacion`,`edad_minima`,`edad_maxima`,`estado`,`fk_idpersona`,`fk_idjuegoespecifico`,`ruta_imagen`,`fk_juego_especifico`,`fk_idtipo`) values (236,'Partes de la Planta','',NULL,'mapa',NULL,'','','activo',32,29,NULL,NULL,NULL),(237,'Departamentos de la selva','',NULL,'mapa',NULL,'','','activo',32,30,NULL,NULL,NULL),(238,'Nùmeros en ingles','',NULL,'mapa',NULL,'','','activo',32,31,NULL,NULL,NULL),(239,'Corazón','lala',NULL,'snake',NULL,'06','09','activo',32,NULL,NULL,7,NULL),(240,'Corzon','llllll',NULL,'snake',NULL,'02','09','activo',32,8,NULL,NULL,2),(241,'dinero','asasass','Matematica','invasores',NULL,'','','activo',32,NULL,NULL,1,4),(242,'dinero','asasass','Matematica','invasores',NULL,'','','activo',32,2,NULL,NULL,4),(243,'dinero','asasass','Matematica','invasores',NULL,'','','activo',32,3,NULL,NULL,4),(244,',memoria','NNNN','Matematica','memoria',NULL,'12','12','activo',32,1,NULL,NULL,3),(245,'LLALLA','ASASAS','Comunicacion','recolector',NULL,'','','activo',32,14,NULL,NULL,6),(246,'','',NULL,'mapa',NULL,'','','activo',32,32,NULL,NULL,NULL),(247,'Plantaxxxxxx','',NULL,'mapa',NULL,'06','09','activo',32,33,NULL,NULL,NULL),(248,'Plantazzz','asas',NULL,'mapa',NULL,'6','9','activo',32,34,NULL,NULL,NULL),(249,'allslals','asasasasas',NULL,'snake',NULL,'6','6','activo',32,9,NULL,NULL,NULL),(250,'llallalal','asasa','Matematica','invasores',NULL,'','','activo',32,NULL,NULL,NULL,NULL),(251,'lobo','askaks','Matematica','invasores',NULL,'6','9','activo',32,NULL,NULL,NULL,NULL),(252,'recolectot','sasasas','Comunicacion','recolector',NULL,'6','6','activo',32,15,NULL,NULL,NULL),(253,'memoria','ssd','Matematica','memoria',NULL,'6','6','activo',32,2,NULL,NULL,NULL),(254,'Juego 1','asa',NULL,'mapa',NULL,'6','9','activo',50,35,NULL,NULL,NULL),(255,'Juego 2','lalaa',NULL,'mapa',NULL,'6','9','activo',50,36,NULL,NULL,NULL),(256,'Juego 3','lalalla','Matematica','invasores',NULL,'6','9','activo',50,NULL,NULL,NULL,NULL),(257,'MAPA 001','',NULL,'mapa',NULL,'','','activo',32,37,NULL,NULL,NULL),(258,'MAPA 002','',NULL,'mapa',NULL,'','','activo',32,38,NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `juego_imagen` */

DROP TABLE IF EXISTS `juego_imagen`;

CREATE TABLE `juego_imagen` (
  `idjuego_imagen` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(400) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_imagen`),
  KEY `fk_idjuego` (`fk_idjuego`),
  CONSTRAINT `juego_imagen_ibfk_1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `juego_imagen` */

LOCK TABLES `juego_imagen` WRITE;

insert  into `juego_imagen`(`idjuego_imagen`,`ruta`,`fk_idjuego`,`observaciones`) values (1,'http://localhost/juegos/assets/uploads/0541239001459783458.png',239,'corazón'),(2,'http://localhost/juegos/assets/uploads/0540069001459783458.jpg',239,'planta'),(3,'http://localhost/juegos/assets/uploads/0577390001459784022.jpg',240,'hojita'),(4,'http://localhost/juegos/assets/uploads/0576136001459784022.png',240,'corazpon'),(5,'http://localhost/juegos/assets/uploads/0217298001459784843.png',244,NULL),(6,'http://localhost/juegos/assets/uploads/0805212001459785175.png',245,'¿Quien persigue a la liebre????'),(7,'http://localhost/juegos/assets/uploads/0378651001464570013.jpg',249,'planta'),(8,'http://localhost/juegos/assets/uploads/0712587001464571114.jpg',252,'Como s ellama?'),(9,'http://localhost/juegos/assets/uploads/0378319001464571261.png',253,NULL);

UNLOCK TABLES;

/*Table structure for table `juego_sesion` */

DROP TABLE IF EXISTS `juego_sesion`;

CREATE TABLE `juego_sesion` (
  `idjuego_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(15) DEFAULT NULL,
  `fk_idsesion` int(11) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `estado` varchar(25) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  KEY `fk_juego_sesion_sesion1_idx` (`fk_idsesion`),
  KEY `fk_juego_sesion_juego1_idx` (`fk_idjuego`),
  CONSTRAINT `fk_juego_sesion_juego1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_sesion1` FOREIGN KEY (`fk_idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

/*Data for the table `juego_sesion` */

LOCK TABLES `juego_sesion` WRITE;

insert  into `juego_sesion`(`idjuego_sesion`,`fecha`,`fk_idsesion`,`fk_idjuego`,`estado`,`orden`) values (21,'19/03/2016',17,238,NULL,1),(22,'19/03/2016',17,237,NULL,2),(23,'19/03/2016',17,236,NULL,3),(24,'29/05/2016',18,244,NULL,NULL),(25,'29/05/2016',18,243,NULL,NULL),(26,'29/05/2016',19,245,NULL,NULL),(27,'29/05/2016',19,244,NULL,NULL),(28,'29/05/2016',20,245,NULL,NULL),(29,'29/05/2016',20,244,NULL,NULL),(30,'29/05/2016',21,245,NULL,NULL),(31,'29/05/2016',21,244,NULL,NULL),(32,'29/05/2016',22,245,NULL,NULL),(33,'29/05/2016',22,244,NULL,NULL),(34,'29/05/2016',23,245,NULL,NULL),(35,'29/05/2016',23,244,NULL,NULL),(36,'29/05/2016',24,240,NULL,NULL),(37,'29/05/2016',24,242,NULL,NULL),(38,'29/05/2016',24,244,NULL,NULL),(39,'29/05/2016',25,240,NULL,NULL),(40,'29/05/2016',25,242,NULL,NULL),(41,'29/05/2016',25,244,NULL,NULL),(42,'29/05/2016',26,245,NULL,NULL),(43,'29/05/2016',26,245,NULL,NULL),(44,'29/05/2016',26,244,NULL,NULL),(45,'29/05/2016',27,243,NULL,NULL),(46,'29/05/2016',28,242,NULL,NULL),(47,'29/05/2016',28,244,NULL,NULL),(48,'29/05/2016',28,245,NULL,NULL),(49,'30/05/2016',29,245,NULL,NULL),(50,'30/05/2016',29,244,NULL,NULL),(51,'30/05/2016',29,243,NULL,NULL),(52,'30/05/2016',30,238,NULL,NULL),(53,'30/05/2016',31,236,NULL,NULL),(54,'30/05/2016',32,248,NULL,1),(55,'07/06/2016',33,253,NULL,1),(56,'07/06/2016',33,253,NULL,2),(57,'07/06/2016',33,252,NULL,3),(58,'07/06/2016',34,255,NULL,1),(59,'07/06/2016',34,255,NULL,2),(60,'07/06/2016',34,254,NULL,3),(61,'21/06/2016',35,258,NULL,1),(62,'27/06/2016',36,258,NULL,1),(63,'27/06/2016',37,258,NULL,1),(64,'27/06/2016',37,257,NULL,2),(65,'27/06/2016',38,258,NULL,1),(66,'27/06/2016',38,257,NULL,2),(67,'27/06/2016',38,253,NULL,3),(68,'27/06/2016',39,258,NULL,1),(69,'27/06/2016',39,258,NULL,2),(70,'27/06/2016',39,258,NULL,3),(71,'27/06/2016',39,258,NULL,4),(72,'27/06/2016',40,258,NULL,1),(73,'27/06/2016',40,258,NULL,2);

UNLOCK TABLES;

/*Table structure for table `juego_tipo` */

DROP TABLE IF EXISTS `juego_tipo`;

CREATE TABLE `juego_tipo` (
  `idjuego_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `juego_tipo` */

LOCK TABLES `juego_tipo` WRITE;

insert  into `juego_tipo`(`idjuego_tipo`,`descripcion`) values (1,'Mapa');

UNLOCK TABLES;

/*Table structure for table `mapa` */

DROP TABLE IF EXISTS `mapa`;

CREATE TABLE `mapa` (
  `idmapa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `imagen` varchar(400) DEFAULT NULL,
  `tipo` int(2) DEFAULT NULL,
  `escala` float(5,3) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `mapa` */

LOCK TABLES `mapa` WRITE;

insert  into `mapa`(`idmapa`,`nombre`,`descripcion`,`imagen`,`tipo`,`escala`,`puntaje`,`tiempo`) values (29,'Partes de la Planta','','http://localhost/juegos/assets/uploads/0205708001458416645.jpg',NULL,0.774,100,5),(30,'Departamentos de la selva','','http://localhost/juegos/assets/uploads/0633170001458416832.png',NULL,1.044,100,5),(31,'Nùmeros en ingles','','http://localhost/juegos/assets/uploads/0916633001458416919.png',NULL,1.978,100,5),(32,'','','http://localhost/juegos/assets/uploads/0846082001464566639.jpg',NULL,0.335,0,0),(33,'Plantaxxxxxx','','http://localhost/juegos/assets/uploads/0835194001464566735.jpg',NULL,NULL,100,2),(34,'Plantazzz','asas','http://localhost/juegos/assets/uploads/0962502001464566873.jpg',NULL,0.335,1000,6),(35,'Juego 1','asa','http://localhost/juegos/assets/uploads/0428151001465259075.jpg',NULL,1.850,9,6),(36,'Juego 2','lalaa','http://localhost/juegos/assets/uploads/0484916001465259126.jpg',NULL,NULL,100,6),(37,'MAPA 001','','http://localhost/juegos/assets/uploads/0555099001466467780.jpg',NULL,0.357,0,0),(38,'MAPA 002','','http://localhost/juegos/assets/uploads/0934000001466467823.jpg',NULL,0.357,100,0);

UNLOCK TABLES;

/*Table structure for table `mapa_punto` */

DROP TABLE IF EXISTS `mapa_punto`;

CREATE TABLE `mapa_punto` (
  `idmapa_punto` int(11) NOT NULL AUTO_INCREMENT,
  `px` int(11) DEFAULT NULL,
  `py` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `idmapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa_punto`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

/*Data for the table `mapa_punto` */

LOCK TABLES `mapa_punto` WRITE;

insert  into `mapa_punto`(`idmapa_punto`,`px`,`py`,`descripcion`,`idmapa`) values (37,123,83,'hoja',29),(38,176,207,'talllo',29),(39,177,334,'raiz',29),(40,167,108,'Loreto',30),(41,198,215,'Ucayali',30),(42,242,247,'Madre%20de%20Dios',30),(43,242,295,'Puno',30),(44,87,103,'1',31),(45,291,66,'2',31),(46,422,243,'6',31),(47,284,231,'5',31),(48,258,98,'pacla',32),(49,128,316,'nombre',32),(50,212,185,'mano',32),(51,250,89,'pacla',33),(52,151,317,'nombre',33),(53,323,202,'cuerpo',33),(54,235,82,'pacla',34),(55,146,322,'nombre',34),(56,290,122,'higado',35),(57,286,292,'vena',35),(58,94,126,'pum%C3%B2n',36),(59,250,179,'vena',36),(60,251,303,'VERDE',37),(61,309,179,'BLANCO',37),(62,243,307,'VERDE',38),(63,312,185,'BLANCO',38);

UNLOCK TABLES;

/*Table structure for table `memoria` */

DROP TABLE IF EXISTS `memoria`;

CREATE TABLE `memoria` (
  `idmemoria` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmemoria`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `memoria` */

LOCK TABLES `memoria` WRITE;

insert  into `memoria`(`idmemoria`,`puntaje`,`tiempo`,`rpta_tiempo`) values (1,50.000,2,NULL),(2,20.000,6,NULL);

UNLOCK TABLES;

/*Table structure for table `memoria_detalles` */

DROP TABLE IF EXISTS `memoria_detalles`;

CREATE TABLE `memoria_detalles` (
  `idmemoria_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idmemoria` int(11) NOT NULL,
  `nombre_imgfraccion` varchar(45) DEFAULT NULL,
  `distractor1` varchar(45) DEFAULT NULL,
  `distractor2` varchar(45) DEFAULT NULL,
  `rpta_imgfraccion` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  PRIMARY KEY (`idmemoria_detalles`),
  KEY `fk_idmemoria` (`fk_idmemoria`),
  CONSTRAINT `memoria_detalles_ibfk_1` FOREIGN KEY (`fk_idmemoria`) REFERENCES `memoria` (`idmemoria`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

/*Data for the table `memoria_detalles` */

LOCK TABLES `memoria_detalles` WRITE;

insert  into `memoria_detalles`(`idmemoria_detalles`,`fk_idmemoria`,`nombre_imgfraccion`,`distractor1`,`distractor2`,`rpta_imgfraccion`,`rpta_puntos`) values (1,1,'CAJAMARCA','KLKLK','LKL',NULL,NULL),(2,1,'SANMARTIN','546','NBNMB',NULL,NULL),(3,1,'CUSCO','KJKJJKH','LKL',NULL,NULL),(4,1,'LKL','IOUIU','OIUOIUOIU',NULL,NULL),(5,1,'KLKLK','CAJAMARCA','IUOUIOIU',NULL,NULL),(6,1,'KJKJJKH','CAJAMARCA','IOUIU',NULL,NULL),(7,1,'JKHJK','HJK','J',NULL,NULL),(8,1,'HJK','LKL','NMNBNM',NULL,NULL),(9,1,'546','KLKLK','LKL',NULL,NULL),(10,1,'J','K','KJKJJKH',NULL,NULL),(11,1,'K','CAJAMARCA','546',NULL,NULL),(12,1,'NMNBNM','K','IOUIU',NULL,NULL),(13,1,'NBNMB','546','CUSCO',NULL,NULL),(14,1,'NMBMNB','JKHJK','NMNBNM',NULL,NULL),(15,1,'IOUIU','NBNMB','KJKJJKH',NULL,NULL),(16,1,'IUOUIOIU','CAJAMARCA','LKL',NULL,NULL),(17,1,'OIUOIUOIU','IOUIU','NMNBNM',NULL,NULL),(18,1,'OIUOIU','546','K',NULL,NULL),(19,2,'k','hj','nb',NULL,NULL),(20,2,'klk','n','io',NULL,NULL),(21,2,'io','nb','i',NULL,NULL),(22,2,'io','hj','n',NULL,NULL),(23,2,'ji','k','nb',NULL,NULL),(24,2,'i','n','jn',NULL,NULL),(25,2,'i','nb','hj',NULL,NULL),(26,2,'hj','ji','n',NULL,NULL),(27,2,'jn','ji','n',NULL,NULL),(28,2,'nb','hj','i',NULL,NULL),(29,2,'n','n','n',NULL,NULL),(30,2,'n','nb','k',NULL,NULL),(31,2,'n','klk','i',NULL,NULL),(32,2,'nb','n','n',NULL,NULL),(33,2,'nb','i','k',NULL,NULL),(34,2,'nb','klk','k',NULL,NULL),(35,2,'n','hj','io',NULL,NULL),(36,2,'n','ji','i',NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `partida` */

DROP TABLE IF EXISTS `partida`;

CREATE TABLE `partida` (
  `idpartida` int(11) NOT NULL AUTO_INCREMENT,
  `duracion` int(11) DEFAULT NULL,
  `partidacol` varchar(45) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idjuego_sesion` int(11) NOT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `stsreg` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idpartida`),
  KEY `fk_partida_usuario1_idx` (`idusuario`),
  KEY `fk_partida_juego_sesion1_idx` (`idjuego_sesion`),
  CONSTRAINT `fk_partida_juego_sesion1` FOREIGN KEY (`idjuego_sesion`) REFERENCES `juego_sesion` (`idjuego_sesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `persona` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `partida` */

LOCK TABLES `partida` WRITE;

insert  into `partida`(`idpartida`,`duracion`,`partidacol`,`idusuario`,`idjuego_sesion`,`puntaje`,`stsreg`) values (1,235,NULL,32,61,100,'REG');

UNLOCK TABLES;

/*Table structure for table `partida_repeticion` */

DROP TABLE IF EXISTS `partida_repeticion`;

CREATE TABLE `partida_repeticion` (
  `idpartida_repeticion` int(11) NOT NULL AUTO_INCREMENT,
  `idpartida` int(11) NOT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  KEY `fk_partida_repeticion_partida1_idx` (`idpartida`),
  CONSTRAINT `fk_partida_repeticion_partida1` FOREIGN KEY (`idpartida`) REFERENCES `partida` (`idpartida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida_repeticion` */

LOCK TABLES `partida_repeticion` WRITE;

UNLOCK TABLES;

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `ap` varchar(45) DEFAULT NULL,
  `am` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(15) DEFAULT NULL,
  `idtipo_usuario` int(11) DEFAULT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `usuario` varchar(10) DEFAULT NULL,
  `clave` varchar(32) DEFAULT NULL,
  `foto_ruta` varchar(100) DEFAULT NULL,
  `rol` varchar(20) DEFAULT NULL,
  `idusrcrea` int(11) NOT NULL,
  `fullnombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_tipo_usuario_idx` (`idtipo_usuario`),
  KEY `fk_usuario_institucion1_idx` (`idinstitucion`),
  KEY `fk_usuario_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipo_usuario` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

/*Data for the table `persona` */

LOCK TABLES `persona` WRITE;

insert  into `persona`(`idusuario`,`nombres`,`ap`,`am`,`fecha_nacimiento`,`idtipo_usuario`,`idinstitucion`,`idgrupo`,`usuario`,`clave`,`foto_ruta`,`rol`,`idusrcrea`,`fullnombre`) values (32,'Rosa ','Silva','Sosa','1992-11-17',1,1,NULL,'rsilva','900150983cd24fb0d6963f7d28e17f72','rosita.jpg ','administrador',32,'Rosa  Silva Sosa'),(33,'Valeria','Salluca','Casas','2006-11-17',2,1,NULL,'vsalluca','8a1f04406a9d45349ead3d1666b0e0ab','profe1.jpg','profesor',32,'Valeria Salluca Casas'),(34,'Raymundo','Márquez','Casas','2006-11-17',2,1,NULL,'rmarqu','d41d8cd98f00b204e9800998ecf8427e','','profesor',33,'Raymundo Márquez Casas'),(35,'Lucas','Huaman','Casas','2006-11-17',3,1,NULL,'lhuama','900150983cd24fb0d6963f7d28e17f72','lucas.jpg','alumno',32,'Lucas Huaman Casas'),(38,'Mario','Mayor','Perez','2006-11-17',3,1,NULL,'mmayor','1cc39ffd758234422e1f75beadfc5fb2','','alumno',33,'Mario Mayor Perez'),(39,'José','Abanto','Valdez','2006-11-17',3,1,NULL,'jabant','1cc39ffd758234422e1f75beadfc5fb2','','alumno',33,'José Abanto Valdez'),(40,'Maria','Huaman','Supo','2006-11-17',3,1,NULL,'mhuaman','900150983cd24fb0d6963f7d28e17f72','maria.jpg','alumno',33,'Maria Huaman Supo'),(41,'Paty','Quispe','Casas','2006-11-17',3,1,NULL,'pquisp','900150983cd24fb0d6963f7d28e17f72','paty.jpg','alumno',33,'Paty Quispe Casas'),(42,'Ana','Lopez','Casas','2006-11-17',3,1,NULL,'alopez','1cc39ffd758234422e1f75beadfc5fb2','','alumno',32,'Ana Lopez Casas'),(43,'Angel','Estrada','Ttito','2006-11-17',3,1,NULL,'aestra','1cc39ffd758234422e1f75beadfc5fb2','','alumno',32,'Angel Estrada Ttito'),(44,'Carlos','Tupac','Casas','2006-11-17',3,1,NULL,'ctupac','1cc39ffd758234422e1f75beadfc5fb2','','alumno',32,'Carlos Tupac Casas'),(45,'Daniela','Amaru','Lopez','2006-11-17',3,1,NULL,'damaru','1cc39ffd758234422e1f75beadfc5fb2','','alumno',32,'Daniela Amaru Lopez'),(46,'Pedro','Holgado','Casas','2006-11-17',3,1,NULL,'pholga','1cc39ffd758234422e1f75beadfc5fb2','','alumno',32,'Pedro Holgado Casas'),(48,'Azul','blue','lala','2000-12-12',3,NULL,NULL,'azul','900150983cd24fb0d6963f7d28e17f72','0204196001464552867.jpg',NULL,32,'Azul blue lala'),(49,'Martha','Meier','Ñlala','1980-12-27',2,NULL,NULL,'martha','202cb962ac59075b964b07152d234b70','','profesor',32,'Martha Meier Ñlala'),(50,'profe','p','p','2015-12-12',2,NULL,NULL,'pro','202cb962ac59075b964b07152d234b70','0139319001465258885.jpg','profesor',32,'profe p p'),(51,'pruebis','pruebis','pruebis','2015-12-12',2,NULL,NULL,'pruebis','123456','0138821001466977141.jpg','profesor',0,'pruebis pruebis pruebis'),(52,'Firma','firma','firma','2015-12-12',2,NULL,NULL,'firma','202cb962ac59075b964b07152d234b70','0076002001466977331.png','profesor',0,'Firma firma firma'),(73,'kkk','kkk','kk','2015-12-12',3,NULL,NULL,'rsilva1','777b7abfeb226860b85730780b3d2905','','alumno',0,'kkk kkk kk'),(74,'kkjkj','jkkjkjj','4','0005-04-04',3,NULL,NULL,'rsilva2','a87ff679a2f3e71d9181a67b7542122c','','alumno',0,'kkjkj jkkjkjj 4'),(75,'pepep','ppp','p','0002-01-01',3,NULL,NULL,'rsilva3','113ea93302ad1014b3772560d10f0498','','alumno',0,'pepep ppp p');

UNLOCK TABLES;

/*Table structure for table `persona_grupo` */

DROP TABLE IF EXISTS `persona_grupo`;

CREATE TABLE `persona_grupo` (
  `idpersona_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idgrupo` int(11) DEFAULT NULL,
  `idpersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpersona_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

/*Data for the table `persona_grupo` */

LOCK TABLES `persona_grupo` WRITE;

insert  into `persona_grupo`(`idpersona_grupo`,`idgrupo`,`idpersona`) values (18,15,32),(19,15,33),(20,15,34),(21,15,35),(22,15,38),(23,15,39),(24,15,42),(25,15,43),(26,16,32),(27,16,39),(28,16,41),(29,16,42),(30,17,41),(31,17,40),(32,17,39),(33,17,42),(34,17,50),(35,18,41),(36,18,40),(37,18,39),(38,18,42),(39,18,50),(40,19,41),(41,19,40),(42,19,39),(43,19,42),(44,19,50);

UNLOCK TABLES;

/*Table structure for table `recolector` */

DROP TABLE IF EXISTS `recolector`;

CREATE TABLE `recolector` (
  `idrecolector` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `texto_principal` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`idrecolector`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `recolector` */

LOCK TABLES `recolector` WRITE;

insert  into `recolector`(`idrecolector`,`puntaje`,`tiempo`,`texto_principal`) values (14,2.000,15,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem id ex et voluptate doloremque inventore eligendi esse incidunt beatae repellat blanditiis ducimus, vero. Cumque aperiam sint laudantium, vitae deleniti similique?'),(15,20.000,5,'<p>Laslansnasnmasmansmans</p>\n');

UNLOCK TABLES;

/*Table structure for table `recolector_detalles` */

DROP TABLE IF EXISTS `recolector_detalles`;

CREATE TABLE `recolector_detalles` (
  `idrecolector_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idrecolector` int(11) NOT NULL,
  `pregunta` varchar(200) DEFAULT NULL,
  `respuesta` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idrecolector_detalles`),
  KEY `fk_idrecolector` (`fk_idrecolector`),
  CONSTRAINT `recolector_detalles_ibfk_1` FOREIGN KEY (`fk_idrecolector`) REFERENCES `recolector` (`idrecolector`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `recolector_detalles` */

LOCK TABLES `recolector_detalles` WRITE;

insert  into `recolector_detalles`(`idrecolector_detalles`,`fk_idrecolector`,`pregunta`,`respuesta`) values (1,14,'¿Quien persigue a la liebre????','El corazón'),(2,15,'Como s ellama?','planta');

UNLOCK TABLES;

/*Table structure for table `sesion` */

DROP TABLE IF EXISTS `sesion`;

CREATE TABLE `sesion` (
  `idsesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  `fecha_programada` date DEFAULT NULL,
  `ruta_imagen` varchar(50) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

/*Data for the table `sesion` */

LOCK TABLES `sesion` WRITE;

insert  into `sesion`(`idsesion`,`nombre`,`fecha_creacion`,`idpersona_creador`,`idgrupo`,`estado`,`fecha_programada`,`ruta_imagen`,`duracion`,`puntaje`) values (17,'Primera sesiòn','0000-00-00',32,15,'programado','0000-00-00',NULL,20,100),(18,'Sesiòn ùltima','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(19,'Sesión Ultimita 001','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(20,'Sesión Ultimita 001','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(21,'Sesión Ultimita 001','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(22,'Sesión Ultimita 001','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(23,'Sesión Ultimita 001','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(24,'llalala','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(25,'llalala','0000-00-00',32,15,'programado','0000-00-00',NULL,NULL,NULL),(26,'kkkkkk','2016-05-29',32,15,'programado','2017-01-30',NULL,NULL,NULL),(27,'El renio veegtal','2016-05-29',32,15,'programado','2016-12-12',NULL,NULL,NULL),(28,'Pppp','2016-05-29',32,15,'programado','0000-00-00',NULL,NULL,NULL),(29,'Snake     ','2016-05-30',32,16,'programado','2016-05-30',NULL,NULL,NULL),(30,'Sna     ke','2016-05-30',32,16,'programado','2016-05-30',NULL,NULL,NULL),(31,'Abeja 1254 a','2016-05-30',32,16,'programado','2016-05-30',NULL,NULL,NULL),(32,'Plata zzzzzz','2016-05-30',32,16,'programado','2016-05-30',NULL,NULL,NULL),(33,'Sesion X','2016-06-07',32,15,'programado','2016-06-27',NULL,NULL,NULL),(34,'Sesion 1','2016-06-07',50,17,'programado','0000-00-00',NULL,NULL,NULL),(35,'0000000','2016-06-21',32,15,'programado','2016-06-21',NULL,NULL,NULL),(36,'','2016-06-27',32,15,'programado','0000-00-00',NULL,NULL,NULL),(37,'kakakkak','2016-06-27',32,15,'programado','2015-12-12',NULL,NULL,NULL),(38,'kkk','2016-06-27',32,15,'programado','2015-12-12',NULL,NULL,NULL),(39,'jjj','2016-06-27',32,15,'programado','2015-12-12',NULL,NULL,NULL),(40,'jjjj','2016-06-27',32,15,'programado','2015-12-12',NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `sesion_grupo` */

DROP TABLE IF EXISTS `sesion_grupo`;

CREATE TABLE `sesion_grupo` (
  `idsesion_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idsesion` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  KEY `fk_sesion_grupo_sesion1_idx` (`idsesion`),
  KEY `fk_sesion_grupo_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_sesion_grupo_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_sesion1` FOREIGN KEY (`idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

/*Data for the table `sesion_grupo` */

LOCK TABLES `sesion_grupo` WRITE;

insert  into `sesion_grupo`(`idsesion_grupo`,`idsesion`,`idgrupo`) values (9,17,15),(10,17,15),(11,17,15),(12,18,15),(13,18,15),(14,19,15),(15,19,15),(16,20,15),(17,20,15),(18,21,15),(19,21,15),(20,22,15),(21,22,15),(22,23,15),(23,23,15),(24,24,15),(25,24,15),(26,24,15),(27,25,15),(28,25,15),(29,25,15),(30,26,15),(31,26,15),(32,26,15),(33,27,15),(34,28,15),(35,28,15),(36,28,15),(37,29,16),(38,29,16),(39,29,16),(40,30,16),(41,31,16),(42,32,16),(43,33,15),(44,33,15),(45,33,15),(46,34,17),(47,34,17),(48,34,17),(49,35,15),(50,36,15),(51,37,15),(52,37,15),(53,38,15),(54,38,15),(55,38,15),(56,39,15),(57,39,15),(58,39,15),(59,39,15),(60,40,15),(61,40,15);

UNLOCK TABLES;

/*Table structure for table `snake` */

DROP TABLE IF EXISTS `snake`;

CREATE TABLE `snake` (
  `idsnake` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `snake` */

LOCK TABLES `snake` WRITE;

insert  into `snake`(`idsnake`,`puntaje`,`tiempo`) values (1,12.000,12),(2,99.999,12),(3,99.999,12),(4,99.999,12),(5,99.999,5),(6,99.999,5),(7,99.999,5),(8,2.000,2),(9,5.000,2);

UNLOCK TABLES;

/*Table structure for table `snake_detalles` */

DROP TABLE IF EXISTS `snake_detalles`;

CREATE TABLE `snake_detalles` (
  `idsnake_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idsnake` int(11) NOT NULL,
  `nombre_imagen` varchar(45) DEFAULT NULL,
  `texto_ayuda` varchar(45) DEFAULT NULL,
  `rpta_imagen` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake_detalles`),
  KEY `fk_idsnake` (`fk_idsnake`),
  CONSTRAINT `snake_detalles_ibfk_1` FOREIGN KEY (`fk_idsnake`) REFERENCES `snake` (`idsnake`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `snake_detalles` */

LOCK TABLES `snake_detalles` WRITE;

insert  into `snake_detalles`(`idsnake_detalles`,`fk_idsnake`,`nombre_imagen`,`texto_ayuda`,`rpta_imagen`,`rpta_puntos`,`rpta_tiempo`) values (1,7,'corazón','órgano principal',NULL,NULL,NULL),(2,7,'planta','tiene vida',NULL,NULL,NULL),(3,8,'hojita','lal',NULL,NULL,NULL),(4,8,'corazpon','lalaaaa',NULL,NULL,NULL),(5,9,'planta','que es eso?',NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `tipo_juego` */

DROP TABLE IF EXISTS `tipo_juego`;

CREATE TABLE `tipo_juego` (
  `idtipo_juego` tinyint(4) NOT NULL,
  `nombre_tabla` varchar(20) NOT NULL,
  `ruta_imagen` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_juego`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tipo_juego` */

LOCK TABLES `tipo_juego` WRITE;

UNLOCK TABLES;

/*Table structure for table `tipo_usuario` */

DROP TABLE IF EXISTS `tipo_usuario`;

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_usuario` */

LOCK TABLES `tipo_usuario` WRITE;

insert  into `tipo_usuario`(`idtipo_usuario`,`rol`) values (1,'administrador'),(2,'profesor'),(3,'alumno');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
