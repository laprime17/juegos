/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 5.6.17 : Database - dbjuegos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbjuegos` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbjuegos`;

/*Table structure for table `abeja` */

DROP TABLE IF EXISTS `abeja`;

CREATE TABLE `abeja` (
  `idabeja` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idabeja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `abeja` */

LOCK TABLES `abeja` WRITE;

UNLOCK TABLES;

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` int(11) DEFAULT NULL,
  `edad_maxima` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

LOCK TABLES `grupo` WRITE;

insert  into `grupo`(`idgrupo`,`nombre`,`idpersona_creador`,`fecha_creacion`,`edad_minima`,`edad_maxima`) values (15,'1ro de Primario \"Estrellitas\"',32,'2016-03-19',6,7);

UNLOCK TABLES;

/*Table structure for table `institucion` */

DROP TABLE IF EXISTS `institucion`;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `institucion` */

LOCK TABLES `institucion` WRITE;

insert  into `institucion`(`idinstitucion`,`nombre`) values (1,'Organización General');

UNLOCK TABLES;

/*Table structure for table `invasores` */

DROP TABLE IF EXISTS `invasores`;

CREATE TABLE `invasores` (
  `idinvasores` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` smallint(6) DEFAULT NULL,
  `rpta_tiempo` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`idinvasores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `invasores` */

LOCK TABLES `invasores` WRITE;

UNLOCK TABLES;

/*Table structure for table `invasores_detalle` */

DROP TABLE IF EXISTS `invasores_detalle`;

CREATE TABLE `invasores_detalle` (
  `idinvasores_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idinvasores` int(11) DEFAULT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `soles` int(11) DEFAULT NULL,
  `centimos` int(11) DEFAULT NULL,
  `enunciado` varchar(200) DEFAULT NULL,
  `nro_monedabillete` int(11) DEFAULT NULL,
  `rpta_soles` int(11) DEFAULT NULL,
  `rpta_centimos` int(11) DEFAULT NULL,
  `rpta_nromonedas` int(11) DEFAULT NULL,
  `tipofigura` varchar(45) DEFAULT NULL,
  `enunciadofig` varchar(200) DEFAULT NULL,
  `nro_figEsperadas` int(11) DEFAULT NULL,
  `rpta_figEncontradas` int(11) DEFAULT NULL,
  PRIMARY KEY (`idinvasores_detalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `invasores_detalle` */

LOCK TABLES `invasores_detalle` WRITE;

UNLOCK TABLES;

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `idjuego` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `curso_dirigido` varchar(45) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` varchar(2) DEFAULT NULL,
  `edad_maxima` varchar(2) DEFAULT NULL,
  `estado` varchar(25) DEFAULT 'activo',
  `fk_idpersona` int(11) DEFAULT NULL,
  `fk_idjuegoespecifico` int(11) DEFAULT NULL,
  `ruta_imagen` varchar(100) DEFAULT NULL,
  `fk_juego_especifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjuego`),
  KEY `fk_idpersona` (`fk_idpersona`),
  CONSTRAINT `juego_ibfk_4` FOREIGN KEY (`fk_idpersona`) REFERENCES `persona` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

/*Data for the table `juego` */

LOCK TABLES `juego` WRITE;

insert  into `juego`(`idjuego`,`nombre`,`descripcion`,`curso_dirigido`,`tipo`,`fecha_creacion`,`edad_minima`,`edad_maxima`,`estado`,`fk_idpersona`,`fk_idjuegoespecifico`,`ruta_imagen`,`fk_juego_especifico`) values (236,'Partes de la Planta','',NULL,'mapa',NULL,'','','activo',32,29,NULL,NULL),(237,'Departamentos de la selva','',NULL,'mapa',NULL,'','','activo',32,30,NULL,NULL),(238,'Nùmeros en ingles','',NULL,'mapa',NULL,'','','activo',32,31,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `juego_imagen` */

DROP TABLE IF EXISTS `juego_imagen`;

CREATE TABLE `juego_imagen` (
  `idjuego_imagen` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(400) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_imagen`),
  KEY `fk_idjuego` (`fk_idjuego`),
  CONSTRAINT `juego_imagen_ibfk_1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego_imagen` */

LOCK TABLES `juego_imagen` WRITE;

UNLOCK TABLES;

/*Table structure for table `juego_sesion` */

DROP TABLE IF EXISTS `juego_sesion`;

CREATE TABLE `juego_sesion` (
  `idjuego_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(15) DEFAULT NULL,
  `fk_idsesion` int(11) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `estado` varchar(25) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  KEY `fk_juego_sesion_sesion1_idx` (`fk_idsesion`),
  KEY `fk_juego_sesion_juego1_idx` (`fk_idjuego`),
  CONSTRAINT `fk_juego_sesion_juego1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_sesion1` FOREIGN KEY (`fk_idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `juego_sesion` */

LOCK TABLES `juego_sesion` WRITE;

insert  into `juego_sesion`(`idjuego_sesion`,`fecha`,`fk_idsesion`,`fk_idjuego`,`estado`,`orden`) values (21,'19/03/2016',17,238,NULL,1),(22,'19/03/2016',17,237,NULL,2),(23,'19/03/2016',17,236,NULL,3);

UNLOCK TABLES;

/*Table structure for table `juego_tipo` */

DROP TABLE IF EXISTS `juego_tipo`;

CREATE TABLE `juego_tipo` (
  `idjuego_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `juego_tipo` */

LOCK TABLES `juego_tipo` WRITE;

insert  into `juego_tipo`(`idjuego_tipo`,`descripcion`) values (1,'Mapa');

UNLOCK TABLES;

/*Table structure for table `mapa` */

DROP TABLE IF EXISTS `mapa`;

CREATE TABLE `mapa` (
  `idmapa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `imagen` varchar(400) DEFAULT NULL,
  `tipo` int(2) DEFAULT NULL,
  `escala` float(5,3) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `mapa` */

LOCK TABLES `mapa` WRITE;

insert  into `mapa`(`idmapa`,`nombre`,`descripcion`,`imagen`,`tipo`,`escala`,`puntaje`,`tiempo`) values (29,'Partes de la Planta','','http://localhost/juegos/assets/uploads/0205708001458416645.jpg',NULL,0.774,100,5),(30,'Departamentos de la selva','','http://localhost/juegos/assets/uploads/0633170001458416832.png',NULL,1.044,100,5),(31,'Nùmeros en ingles','','http://localhost/juegos/assets/uploads/0916633001458416919.png',NULL,1.978,100,5);

UNLOCK TABLES;

/*Table structure for table `mapa_punto` */

DROP TABLE IF EXISTS `mapa_punto`;

CREATE TABLE `mapa_punto` (
  `idmapa_punto` int(11) NOT NULL AUTO_INCREMENT,
  `px` int(11) DEFAULT NULL,
  `py` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `idmapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa_punto`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Data for the table `mapa_punto` */

LOCK TABLES `mapa_punto` WRITE;

insert  into `mapa_punto`(`idmapa_punto`,`px`,`py`,`descripcion`,`idmapa`) values (37,123,83,'hoja',29),(38,176,207,'talllo',29),(39,177,334,'raiz',29),(40,167,108,'Loreto',30),(41,198,215,'Ucayali',30),(42,242,247,'Madre%20de%20Dios',30),(43,242,295,'Puno',30),(44,87,103,'1',31),(45,291,66,'2',31),(46,422,243,'6',31),(47,284,231,'5',31);

UNLOCK TABLES;

/*Table structure for table `memoria` */

DROP TABLE IF EXISTS `memoria`;

CREATE TABLE `memoria` (
  `idmemoria` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmemoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memoria` */

LOCK TABLES `memoria` WRITE;

UNLOCK TABLES;

/*Table structure for table `memoria_detalles` */

DROP TABLE IF EXISTS `memoria_detalles`;

CREATE TABLE `memoria_detalles` (
  `idmemoria_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idmemoria` int(11) NOT NULL,
  `nombre_imgfraccion` varchar(45) DEFAULT NULL,
  `distractor1` varchar(45) DEFAULT NULL,
  `distractor2` varchar(45) DEFAULT NULL,
  `rpta_imgfraccion` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  PRIMARY KEY (`idmemoria_detalles`),
  KEY `fk_idmemoria` (`fk_idmemoria`),
  CONSTRAINT `memoria_detalles_ibfk_1` FOREIGN KEY (`fk_idmemoria`) REFERENCES `memoria` (`idmemoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memoria_detalles` */

LOCK TABLES `memoria_detalles` WRITE;

UNLOCK TABLES;

/*Table structure for table `partida` */

DROP TABLE IF EXISTS `partida`;

CREATE TABLE `partida` (
  `idpartida` int(11) NOT NULL AUTO_INCREMENT,
  `duracion` int(11) DEFAULT NULL,
  `partidacol` varchar(45) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idjuego_sesion` int(11) NOT NULL,
  PRIMARY KEY (`idpartida`),
  KEY `fk_partida_usuario1_idx` (`idusuario`),
  KEY `fk_partida_juego_sesion1_idx` (`idjuego_sesion`),
  CONSTRAINT `fk_partida_juego_sesion1` FOREIGN KEY (`idjuego_sesion`) REFERENCES `juego_sesion` (`idjuego_sesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `persona` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida` */

LOCK TABLES `partida` WRITE;

UNLOCK TABLES;

/*Table structure for table `partida_repeticion` */

DROP TABLE IF EXISTS `partida_repeticion`;

CREATE TABLE `partida_repeticion` (
  `idpartida_repeticion` int(11) NOT NULL AUTO_INCREMENT,
  `idpartida` int(11) NOT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  KEY `fk_partida_repeticion_partida1_idx` (`idpartida`),
  CONSTRAINT `fk_partida_repeticion_partida1` FOREIGN KEY (`idpartida`) REFERENCES `partida` (`idpartida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida_repeticion` */

LOCK TABLES `partida_repeticion` WRITE;

UNLOCK TABLES;

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `ap` varchar(45) DEFAULT NULL,
  `am` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(15) DEFAULT NULL,
  `idtipo_usuario` int(11) DEFAULT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `usuario` varchar(6) DEFAULT NULL,
  `clave` varchar(32) DEFAULT NULL,
  `foto_ruta` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_tipo_usuario_idx` (`idtipo_usuario`),
  KEY `fk_usuario_institucion1_idx` (`idinstitucion`),
  KEY `fk_usuario_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipo_usuario` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

/*Data for the table `persona` */

LOCK TABLES `persona` WRITE;

insert  into `persona`(`idusuario`,`nombres`,`ap`,`am`,`fecha_nacimiento`,`idtipo_usuario`,`idinstitucion`,`idgrupo`,`usuario`,`clave`,`foto_ruta`) values (32,'Rosa','Silva','Sosa','1990-12-17',1,1,NULL,'rsilva','202cb962ac59075b964b07152d234b70','rosita.jpg'),(33,'Pepito','Perez','Casas','2006-11-17',3,1,NULL,'pperez','202cb962ac59075b964b07152d234b70','pepito.jpg'),(34,'Lunita','Chaparro','DÃ­az','2008-03-15',3,1,NULL,'lchapa','202cb962ac59075b964b07152d234b70','lunita.jpg'),(35,'Lucero','PeÃ±a','Luna','2007-11-15',3,1,NULL,'lucero','202cb962ac59075b964b07152d234b70','lucero.jpg'),(38,'Martita','Coyo','Pinto','2008-06-15',3,1,NULL,'martit','202cb962ac59075b964b07152d234b70',''),(39,'Juancito','Pelaez','Urbano','2008-03-12',3,1,NULL,'jpelae','202cb962ac59075b964b07152d234b70','juancito.jpg'),(40,'Joelangel','Motta','Marin','1990-09-09',1,1,NULL,'100pre','202cb962ac59075b964b07152d234b70',NULL),(41,'Claudio','Pizarro','Noriega','1980-02-01',2,1,NULL,'pizza','202cb962ac59075b964b07152d234b70','profe2.jpg'),(42,'pepillo','Lala','peres','1995-12-12',3,1,NULL,'jeje','202cb962ac59075b964b07152d234b70',NULL),(43,'Valeria','Perez','Lopez','1989-12-01',2,1,NULL,'vale12','202cb962ac59075b964b07152d234b70','profe1.jpg'),(44,'Dorotea','Perez','Perez','2010-12-17',3,NULL,NULL,'dorote','e10adc3949ba59abbe56e057f20f883e','http://localhost/juegos/assets/uploads/0378594001459750508.jpg'),(45,'lalal','lal','lll','2015-12-12',3,NULL,NULL,'lalal','2f3e9eccc22ee583cf7bad86c751d865',''),(46,'lkk','lklkl','klk','2015-12-12',3,NULL,NULL,'12121','ef80af910fa07870e25b1a4c86d10402','0006658001459751048.jpg');

UNLOCK TABLES;

/*Table structure for table `persona_grupo` */

DROP TABLE IF EXISTS `persona_grupo`;

CREATE TABLE `persona_grupo` (
  `idpersona_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idgrupo` int(11) DEFAULT NULL,
  `idpersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpersona_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `persona_grupo` */

LOCK TABLES `persona_grupo` WRITE;

insert  into `persona_grupo`(`idpersona_grupo`,`idgrupo`,`idpersona`) values (18,15,32),(19,15,33),(20,15,34),(21,15,35),(22,15,38),(23,15,39),(24,15,42),(25,15,43);

UNLOCK TABLES;

/*Table structure for table `sesion` */

DROP TABLE IF EXISTS `sesion`;

CREATE TABLE `sesion` (
  `idsesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  `fecha_programada` date DEFAULT NULL,
  `ruta_imagen` varchar(50) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `sesion` */

LOCK TABLES `sesion` WRITE;

insert  into `sesion`(`idsesion`,`nombre`,`fecha_creacion`,`idpersona_creador`,`idgrupo`,`estado`,`fecha_programada`,`ruta_imagen`,`duracion`,`puntaje`) values (17,'Primera sesiòn','0000-00-00',32,15,'programado','0000-00-00',NULL,20,100);

UNLOCK TABLES;

/*Table structure for table `sesion_grupo` */

DROP TABLE IF EXISTS `sesion_grupo`;

CREATE TABLE `sesion_grupo` (
  `idsesion_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idsesion` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  KEY `fk_sesion_grupo_sesion1_idx` (`idsesion`),
  KEY `fk_sesion_grupo_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_sesion_grupo_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_sesion1` FOREIGN KEY (`idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `sesion_grupo` */

LOCK TABLES `sesion_grupo` WRITE;

insert  into `sesion_grupo`(`idsesion_grupo`,`idsesion`,`idgrupo`) values (9,17,15),(10,17,15),(11,17,15);

UNLOCK TABLES;

/*Table structure for table `snake` */

DROP TABLE IF EXISTS `snake`;

CREATE TABLE `snake` (
  `idsnake` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `snake` */

LOCK TABLES `snake` WRITE;

insert  into `snake`(`idsnake`,`puntaje`,`tiempo`) values (1,12.000,12),(2,99.999,12),(3,99.999,12),(4,99.999,12);

UNLOCK TABLES;

/*Table structure for table `snake_detalles` */

DROP TABLE IF EXISTS `snake_detalles`;

CREATE TABLE `snake_detalles` (
  `idsnake_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idsnake` int(11) NOT NULL,
  `nombre_imagen` varchar(45) DEFAULT NULL,
  `texto_ayuda` varchar(45) DEFAULT NULL,
  `rpta_imagen` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake_detalles`),
  KEY `fk_idsnake` (`fk_idsnake`),
  CONSTRAINT `snake_detalles_ibfk_1` FOREIGN KEY (`fk_idsnake`) REFERENCES `snake` (`idsnake`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `snake_detalles` */

LOCK TABLES `snake_detalles` WRITE;

UNLOCK TABLES;

/*Table structure for table `tipo_juego` */

DROP TABLE IF EXISTS `tipo_juego`;

CREATE TABLE `tipo_juego` (
  `idtipo_juego` tinyint(4) NOT NULL,
  `nombre_tabla` varchar(20) NOT NULL,
  `ruta_imagen` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_juego`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tipo_juego` */

LOCK TABLES `tipo_juego` WRITE;

UNLOCK TABLES;

/*Table structure for table `tipo_usuario` */

DROP TABLE IF EXISTS `tipo_usuario`;

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_usuario` */

LOCK TABLES `tipo_usuario` WRITE;

insert  into `tipo_usuario`(`idtipo_usuario`,`rol`) values (1,'administrador'),(2,'profesor'),(3,'alumno');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
