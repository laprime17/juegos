/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 5.6.17 : Database - dbjuegos
*********************************************************************
*/ 
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbjuegos` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbjuegos`;

/*Table structure for table `abeja` */

DROP TABLE IF EXISTS `abeja`;

CREATE TABLE `abeja` (
  `idabeja` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idabeja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `abeja` */

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` int(11) DEFAULT NULL,
  `edad_maxima` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

insert  into `grupo`(`idgrupo`,`nombre`,`idpersona_creador`,`fecha_creacion`,`edad_minima`,`edad_maxima`) values (7,'2do de primario',32,'2015-11-04',6,8),(8,'2do de primario',32,'2015-11-04',6,8),(9,'2do de primario',32,'2015-11-04',6,8),(10,'2do de primario',32,'2015-11-04',6,8);

/*Table structure for table `institucion` */

DROP TABLE IF EXISTS `institucion`;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `institucion` */

insert  into `institucion`(`idinstitucion`,`nombre`) values (1,NULL);

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `idjuego` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `curso_dirigido` varchar(45) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` varchar(2) DEFAULT NULL,
  `edad_maxima` varchar(2) DEFAULT NULL,
  `estado` varchar(25) DEFAULT 'activo',
  `fk_idmapa` int(11) DEFAULT NULL,
  `fk_idsnake` int(11) DEFAULT NULL,
  `fk_idabeja` int(11) DEFAULT NULL,
  `fk_idpersona` int(11) DEFAULT NULL,
  `fk_idmemoria` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjuego`),
  KEY `fk_idmapa` (`fk_idmapa`),
  KEY `fk_idsnake` (`fk_idsnake`),
  KEY `fk_idabeja` (`fk_idabeja`),
  KEY `fk_idpersona` (`fk_idpersona`),
  KEY `fk_idmemoria` (`fk_idmemoria`),
  CONSTRAINT `juego_ibfk_1` FOREIGN KEY (`fk_idmapa`) REFERENCES `mapa` (`idmapa`),
  CONSTRAINT `juego_ibfk_2` FOREIGN KEY (`fk_idsnake`) REFERENCES `snake` (`idsnake`),
  CONSTRAINT `juego_ibfk_3` FOREIGN KEY (`fk_idabeja`) REFERENCES `abeja` (`idabeja`),
  CONSTRAINT `juego_ibfk_4` FOREIGN KEY (`fk_idpersona`) REFERENCES `persona` (`idusuario`),
  CONSTRAINT `juego_ibfk_5` FOREIGN KEY (`fk_idmemoria`) REFERENCES `memoria` (`idmemoria`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

/*Data for the table `juego` */

/*Table structure for table `juego_imagen` */

DROP TABLE IF EXISTS `juego_imagen`;

CREATE TABLE `juego_imagen` (
  `idjuego_imagen` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(400) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_imagen`),
  KEY `fk_idjuego` (`fk_idjuego`),
  CONSTRAINT `juego_imagen_ibfk_1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

/*Data for the table `juego_imagen` */

/*Table structure for table `juego_sesion` */

DROP TABLE IF EXISTS `juego_sesion`;

CREATE TABLE `juego_sesion` (
  `idjuego_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(15) DEFAULT NULL,
  `sesion_idsesion` int(11) NOT NULL,
  `juego_idjuego` int(11) NOT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  KEY `fk_juego_sesion_sesion1_idx` (`sesion_idsesion`),
  KEY `fk_juego_sesion_juego1_idx` (`juego_idjuego`),
  CONSTRAINT `fk_juego_sesion_juego1` FOREIGN KEY (`juego_idjuego`) REFERENCES `juego` (`idjuego`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_sesion1` FOREIGN KEY (`sesion_idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego_sesion` */

/*Table structure for table `mapa` */

DROP TABLE IF EXISTS `mapa`;

CREATE TABLE `mapa` (
  `idmapa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `imagen` varchar(400) DEFAULT NULL,
  `tipo` int(2) DEFAULT NULL,
  `escala` float(5,3) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `mapa` */

/*Table structure for table `mapa_punto` */

DROP TABLE IF EXISTS `mapa_punto`;

CREATE TABLE `mapa_punto` (
  `idmapa_punto` int(11) NOT NULL AUTO_INCREMENT,
  `px` int(11) DEFAULT NULL,
  `py` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `idmapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa_punto`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `mapa_punto` */


/*Table structure for table `memoria` */

DROP TABLE IF EXISTS `memoria`;

CREATE TABLE `memoria` (
  `idmemoria` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmemoria`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `memoria` */

/*Table structure for table `memoria_detalles` */

DROP TABLE IF EXISTS `memoria_detalles`;

CREATE TABLE `memoria_detalles` (
  `idmemoria_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idmemoria` int(11) NOT NULL,
  `nombre_imgfraccion` varchar(45) DEFAULT NULL,
  `distractor1` varchar(45) DEFAULT NULL,
  `distractor2` varchar(45) DEFAULT NULL,
  `rpta_imgfraccion` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  PRIMARY KEY (`idmemoria_detalles`),
  KEY `fk_idmemoria` (`fk_idmemoria`),
  CONSTRAINT `memoria_detalles_ibfk_1` FOREIGN KEY (`fk_idmemoria`) REFERENCES `memoria` (`idmemoria`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

/*Data for the table `memoria_detalles` */

/*Table structure for table `partida` */

DROP TABLE IF EXISTS `partida`;

CREATE TABLE `partida` (
  `idpartida` int(11) NOT NULL AUTO_INCREMENT,
  `duracion` int(11) DEFAULT NULL,
  `partidacol` varchar(45) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idjuego_sesion` int(11) NOT NULL,
  PRIMARY KEY (`idpartida`),
  KEY `fk_partida_usuario1_idx` (`idusuario`),
  KEY `fk_partida_juego_sesion1_idx` (`idjuego_sesion`),
  CONSTRAINT `fk_partida_juego_sesion1` FOREIGN KEY (`idjuego_sesion`) REFERENCES `juego_sesion` (`idjuego_sesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `persona` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida` */

/*Table structure for table `partida_repeticion` */

DROP TABLE IF EXISTS `partida_repeticion`;

CREATE TABLE `partida_repeticion` (
  `idpartida_repeticion` int(11) NOT NULL AUTO_INCREMENT,
  `idpartida` int(11) NOT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  KEY `fk_partida_repeticion_partida1_idx` (`idpartida`),
  CONSTRAINT `fk_partida_repeticion_partida1` FOREIGN KEY (`idpartida`) REFERENCES `partida` (`idpartida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida_repeticion` */

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `ap` varchar(45) DEFAULT NULL,
  `am` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(15) DEFAULT NULL,
  `idtipo_usuario` int(11) DEFAULT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `usuario` varchar(6) DEFAULT NULL,
  `clave` varchar(6) DEFAULT NULL,
  `foto_ruta` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_tipo_usuario_idx` (`idtipo_usuario`),
  KEY `fk_usuario_institucion1_idx` (`idinstitucion`),
  KEY `fk_usuario_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipo_usuario` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

/*Data for the table `persona` */

insert  into `persona`(`idusuario`,`nombres`,`ap`,`am`,`fecha_nacimiento`,`idtipo_usuario`,`idinstitucion`,`idgrupo`,`usuario`,`clave`,`foto_ruta`) values (32,'Rosa','Silva','Sosa','1990-12-17',1,1,NULL,'rsilva','123','rosita.jpg'),(33,'Pepito','Perez','Casas','2006-11-17',3,NULL,NULL,'pperez','123456',NULL),(34,'Lunita','Chaparro','DÃ­az','2008-03-15',3,NULL,NULL,'lchapa','123456',NULL),(35,'Lucero','PeÃ±a','Luna','2007-11-15',3,NULL,NULL,'lucero','123456',NULL),(38,'Martita','Coyo','Pinto','2008-06-15',3,NULL,NULL,'martit','123456',NULL),(39,'Juancito','Pelaez','Urbano','2008-03-12',3,NULL,NULL,'jpelae','123456',NULL),(40,'Joelangel','Motta','Marin','1990-09-09',1,NULL,NULL,'100pre','123',NULL),(41,'Claudio','Pizarro','Noriega','1980-02-01',2,NULL,NULL,'pizza','123',NULL);

/*Table structure for table `persona_grupo` */

DROP TABLE IF EXISTS `persona_grupo`;

CREATE TABLE `persona_grupo` (
  `idpersona_grupo` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idpersona` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persona_grupo` */

insert  into `persona_grupo`(`idpersona_grupo`,`idgrupo`,`idpersona`) values (NULL,NULL,33),(NULL,NULL,34),(NULL,NULL,33),(NULL,NULL,34),(NULL,NULL,33),(NULL,NULL,34),(NULL,10,33),(NULL,10,34);

/*Table structure for table `sesion` */

DROP TABLE IF EXISTS `sesion`;

CREATE TABLE `sesion` (
  `idsesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sesion` */

/*Table structure for table `sesion_grupo` */

DROP TABLE IF EXISTS `sesion_grupo`;

CREATE TABLE `sesion_grupo` (
  `idsesion_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idsesion` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  KEY `fk_sesion_grupo_sesion1_idx` (`idsesion`),
  KEY `fk_sesion_grupo_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_sesion_grupo_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_sesion1` FOREIGN KEY (`idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sesion_grupo` */

/*Table structure for table `snake` */

DROP TABLE IF EXISTS `snake`;

CREATE TABLE `snake` (
  `idsnake` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;

/*Data for the table `snake` */

/*Table structure for table `snake_detalles` */

DROP TABLE IF EXISTS `snake_detalles`;

CREATE TABLE `snake_detalles` (
  `idsnake_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idsnake` int(11) NOT NULL,
  `nombre_imagen` varchar(45) DEFAULT NULL,
  `texto_ayuda` varchar(45) DEFAULT NULL,
  `rpta_imagen` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake_detalles`),
  KEY `fk_idsnake` (`fk_idsnake`),
  CONSTRAINT `snake_detalles_ibfk_1` FOREIGN KEY (`fk_idsnake`) REFERENCES `snake` (`idsnake`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;

/*Data for the table `snake_detalles` */

/*Table structure for table `tipo_usuario` */

DROP TABLE IF EXISTS `tipo_usuario`;

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_usuario` */

insert  into `tipo_usuario`(`idtipo_usuario`,`rol`) values (1,'administrador'),(2,'profesor'),(3,'alumno');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
