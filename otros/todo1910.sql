/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 5.5.41-MariaDB : Database - dbjuegos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbjuegos` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbjuegos`;

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

LOCK TABLES `grupo` WRITE;

UNLOCK TABLES;

/*Table structure for table `institucion` */

DROP TABLE IF EXISTS `institucion`;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `institucion` */

LOCK TABLES `institucion` WRITE;

UNLOCK TABLES;

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `idjuego` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `desccripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idjuego`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego` */

LOCK TABLES `juego` WRITE;

UNLOCK TABLES;

/*Table structure for table `juego_sesion` */

DROP TABLE IF EXISTS `juego_sesion`;

CREATE TABLE `juego_sesion` (
  `idjuego_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(15) DEFAULT NULL,
  `sesion_idsesion` int(11) NOT NULL,
  `juego_idjuego` int(11) NOT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  KEY `fk_juego_sesion_sesion1_idx` (`sesion_idsesion`),
  KEY `fk_juego_sesion_juego1_idx` (`juego_idjuego`),
  CONSTRAINT `fk_juego_sesion_juego1` FOREIGN KEY (`juego_idjuego`) REFERENCES `juego` (`idjuego`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_sesion1` FOREIGN KEY (`sesion_idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego_sesion` */

LOCK TABLES `juego_sesion` WRITE;

UNLOCK TABLES;

/*Table structure for table `mapa` */

DROP TABLE IF EXISTS `mapa`;

CREATE TABLE `mapa` (
  `idmapa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `imagen` varchar(400) DEFAULT NULL,
  `tipo` int(2) DEFAULT NULL,
  PRIMARY KEY (`idmapa`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

/*Data for the table `mapa` */

LOCK TABLES `mapa` WRITE;

UNLOCK TABLES;

/*Table structure for table `mapa_punto` */

DROP TABLE IF EXISTS `mapa_punto`;

CREATE TABLE `mapa_punto` (
  `idmapa_punto` int(11) NOT NULL AUTO_INCREMENT,
  `px` int(11) DEFAULT NULL,
  `py` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `idmapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa_punto`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

/*Data for the table `mapa_punto` */

LOCK TABLES `mapa_punto` WRITE;

insert  into `mapa_punto`(`idmapa_punto`,`px`,`py`,`descripcion`,`idmapa`) values (8,406,191,'undefined',0),(9,300,300,'undefined',32),(10,365,166,'dejalo',33),(11,343,170,'jjaj',34),(12,417,151,'123',35),(13,436,129,'lakjskasj',36),(14,300,300,'64654',37),(15,300,300,'897',38),(16,427,110,'879',39),(17,391,64,'54545644',40),(18,397,174,'546454',41),(19,436,137,'79798797',42),(20,436,74,'8979798',43),(21,300,300,'545646',44),(22,300,300,'hello',46),(23,246,160,'hheheheh',47),(24,289,142,'hello',48),(25,436,227,'lala',48),(26,436,107,'pepe',48),(27,60,183,'nose',48),(28,206,192,'kakaka',49),(29,433,192,'jello',49),(30,78,216,'jajaja',49),(31,436,129,'jello',50),(32,193,194,'jjjJ',50),(33,436,231,'JAJAJAJ',50),(34,158,115,'JKLJJK',50),(35,436,97,'hello',51),(36,297,247,'pepe',51),(37,75,124,'jajaja',51),(38,198,234,'89',51),(39,379,115,'hello',52),(40,131,249,'jajjajaj',52),(41,129,152,'lolololo',52),(42,420,157,'hello',53),(43,176,156,'jja',53),(44,378,258,'lele',53),(45,421,125,'hello',54),(46,136,191,'nno',54),(47,436,216,'4444',54),(48,395,220,'hello',55),(49,154,117,'lal',55),(50,436,211,'jajjaja',56),(51,108,205,'punto2',56),(52,359,232,'p1',57),(53,257,101,NULL,60),(54,257,101,'hola',60),(55,155,53,'hola',61),(56,276,101,'puno1',64),(57,436,65,'punto2',64),(58,397,77,'ojo',65),(59,304,128,'ojo',66),(60,287,104,'ojo',67),(61,319,80,'1',68),(62,289,100,'lallla',69),(63,300,123,'lala',70),(64,300,83,'lala',71),(65,436,70,'jejejej',71),(66,300,300,'jejejej',75),(67,313,112,'lalal',75),(68,408,81,'kakka',75),(69,304,132,'hola',76),(70,288,117,'helo',77),(71,110,44,'hola',78),(72,314,74,'banner',79),(73,314,74,'no',79),(74,371,58,'ni%C3%B1a',80),(75,436,146,'vaso',80),(76,351,79,'ni%C3%B1a',81),(77,179,117,'verde',81),(78,128,96,'ojo',82),(79,128,96,'ojo',82),(80,128,96,'ojo',82),(81,108,102,'ojo',83),(82,212,129,'ojo',84),(83,436,138,'oreja',84),(84,436,138,'oreja',84),(85,267,263,'boca',84),(86,141,256,'pie',84),(87,263,149,'cecnoaso',85),(88,185,246,'hola',85),(89,263,149,'cecnoaso',85),(90,300,300,'asasas',87),(91,300,300,'asasas',87),(92,362,157,'lala',91),(93,362,157,'lala',91),(94,362,157,NULL,91),(95,362,157,'open',91),(96,294,241,'lwllw',91),(97,232,93,'ejem',93),(98,617,380,'jaja',98),(99,297,108,'jajaja',175),(100,238,328,'jejeje',175),(101,417,221,'hola',175),(102,274,265,'nono',175),(103,247,226,'okok',175),(104,215,199,'nooo',175),(105,215,199,'nooo',175),(106,266,196,'hoja',176),(107,189,32,'jajaj',177),(108,211,38,'jejeje',179),(109,288,145,'hola',179),(110,211,38,'jejeje',179),(111,187,31,'holaaa',180),(112,198,38,'hoaaa',-1),(113,393,143,'hoja',181),(114,268,199,'hello',190),(115,363,363,'raiz',190),(116,189,31,'punto1',-1),(117,462,31,'punto%202',192),(118,189,376,'punto%203',192),(119,462,375,'punto%204',192),(120,14,31,'punto1',194),(121,14,31,'holiiii',195);

UNLOCK TABLES;

/*Table structure for table `partida` */

DROP TABLE IF EXISTS `partida`;

CREATE TABLE `partida` (
  `idpartida` int(11) NOT NULL AUTO_INCREMENT,
  `duracion` int(11) DEFAULT NULL,
  `partidacol` varchar(45) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idjuego_sesion` int(11) NOT NULL,
  PRIMARY KEY (`idpartida`),
  KEY `fk_partida_usuario1_idx` (`idusuario`),
  KEY `fk_partida_juego_sesion1_idx` (`idjuego_sesion`),
  CONSTRAINT `fk_partida_juego_sesion1` FOREIGN KEY (`idjuego_sesion`) REFERENCES `juego_sesion` (`idjuego_sesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `persona` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida` */

LOCK TABLES `partida` WRITE;

UNLOCK TABLES;

/*Table structure for table `partida_repeticion` */

DROP TABLE IF EXISTS `partida_repeticion`;

CREATE TABLE `partida_repeticion` (
  `idpartida_repeticion` int(11) NOT NULL AUTO_INCREMENT,
  `idpartida` int(11) NOT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  KEY `fk_partida_repeticion_partida1_idx` (`idpartida`),
  CONSTRAINT `fk_partida_repeticion_partida1` FOREIGN KEY (`idpartida`) REFERENCES `partida` (`idpartida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida_repeticion` */

LOCK TABLES `partida_repeticion` WRITE;

UNLOCK TABLES;

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `ap` varchar(45) DEFAULT NULL,
  `am` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(15) DEFAULT NULL,
  `idtipo_usuario` int(11) DEFAULT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `usuario` varchar(6) DEFAULT NULL,
  `clave` varchar(6) DEFAULT NULL,
  `foto_ruta` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_tipo_usuario_idx` (`idtipo_usuario`),
  KEY `fk_usuario_institucion1_idx` (`idinstitucion`),
  KEY `fk_usuario_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipo_usuario` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `persona` */

LOCK TABLES `persona` WRITE;

insert  into `persona`(`idusuario`,`nombres`,`ap`,`am`,`fecha_nacimiento`,`idtipo_usuario`,`idinstitucion`,`idgrupo`,`usuario`,`clave`,`foto_ruta`) values (1,'Rosa','Silva','Sosa','1234654564',1,NULL,NULL,'rsilva','123','rosita.jpg'),(2,'Manuel','Ortega','Perez','1234567898',3,NULL,NULL,'manuel','123','ma.jpg'),(3,'Esther','Costas','Lopez','7418529636',2,NULL,NULL,'profe','123','profe.jpg'),(4,'Rosita','lñklk','lkñ','275760-05-06',NULL,NULL,NULL,'7898-0',',ansa',NULL),(5,'rosit','kkkjl','lklñk','7897-08-09',NULL,NULL,NULL,'9789-0','ass',NULL),(6,'Rosai','lkjkl','kljkl','4654-06-05',NULL,NULL,NULL,'8455-0','sasasa',NULL),(7,'Rosai','lkjkl','kljkl','4654-06-05',NULL,NULL,NULL,'8455-0','sasasa',NULL),(8,'','','','',NULL,NULL,NULL,'','',NULL),(9,'rosita','ppo','popo','0056-04-05',NULL,NULL,NULL,'275760','lkjklj',NULL),(10,'','','','',NULL,NULL,NULL,'','',NULL),(11,'rooisaopi','oipopi','opiopi','0089-07-08',NULL,NULL,NULL,'0008-0','sasas',NULL),(12,'rosia','65456','454','21321-12-23',NULL,NULL,NULL,'0456-0','asas',NULL),(13,'rosi','ñlñlklñ','lñkñlk','275760-04-05',NULL,NULL,NULL,'275760','as',NULL),(14,'','','','',NULL,NULL,NULL,'','',NULL),(15,'rois','poopi','opi','45456-04-05',NULL,NULL,NULL,'89798-','asas',NULL),(16,'ñlkl','ljjklj','kljklj','275760-05-06',NULL,NULL,NULL,'5564-0','jhjh',NULL),(17,'asas','poiopi','poiopi','4654-06-05',NULL,NULL,NULL,'65456-','aaa',NULL),(18,'Ros','ñlñlk','lñkñlk','4654-06-05',NULL,NULL,NULL,'1231-0','akkk7',NULL),(19,'riosiao','opiopi','opiop','4564-05-06',NULL,NULL,NULL,'56445-','sas',NULL),(20,'rosi','popoio','5645646','0044-05-04',NULL,NULL,NULL,'0078-0','aa',NULL),(21,'Roasia','poioiop','opipip','0054-04-05',NULL,NULL,NULL,'78987-','aaaa',NULL),(22,'roias','54654654','545465','275760-06-05',NULL,NULL,NULL,'5646-0','aaa',NULL),(23,'oiopoip','oiopipi','poiopip','0789-08-07',NULL,NULL,NULL,'0008-0','kj',NULL),(24,'ropiopi','oiopip','poipo','0787-08-05',NULL,NULL,NULL,'275760','alk',NULL),(25,'p`pop','	opòop','	`po`po','0897-08-07',NULL,NULL,NULL,'0897-0','aaa',NULL),(26,'`popo','	opoò','pò`po','7987-07-08',NULL,NULL,NULL,'7879-0','kkjjkj',NULL),(27,'rosi','oioip','opio','0079-08-04',NULL,NULL,NULL,'98789-','a',NULL),(28,'Rosita','p`pop','opiopi','0787-09-08',NULL,NULL,NULL,'9879-0','8789',NULL),(29,'Pepe','oioi','iioi','46545-06-05',NULL,NULL,NULL,'4564-0','aaaa',NULL);

UNLOCK TABLES;

/*Table structure for table `sesion` */

DROP TABLE IF EXISTS `sesion`;

CREATE TABLE `sesion` (
  `idsesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sesion` */

LOCK TABLES `sesion` WRITE;

UNLOCK TABLES;

/*Table structure for table `sesion_grupo` */

DROP TABLE IF EXISTS `sesion_grupo`;

CREATE TABLE `sesion_grupo` (
  `idsesion_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idsesion` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  KEY `fk_sesion_grupo_sesion1_idx` (`idsesion`),
  KEY `fk_sesion_grupo_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_sesion_grupo_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_sesion1` FOREIGN KEY (`idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sesion_grupo` */

LOCK TABLES `sesion_grupo` WRITE;

UNLOCK TABLES;

/*Table structure for table `tipo_usuario` */

DROP TABLE IF EXISTS `tipo_usuario`;

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_usuario` */

LOCK TABLES `tipo_usuario` WRITE;

insert  into `tipo_usuario`(`idtipo_usuario`,`rol`) values (1,'administrador'),(2,'profesor'),(3,'alumno');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
