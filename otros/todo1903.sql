/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 5.5.5-10.1.9-MariaDB : Database - dbjuegos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbjuegos` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dbjuegos`;

/*Table structure for table `abeja` */

DROP TABLE IF EXISTS `abeja`;

CREATE TABLE `abeja` (
  `idabeja` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idabeja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `abeja` */

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` int(11) DEFAULT NULL,
  `edad_maxima` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

insert  into `grupo`(`idgrupo`,`nombre`,`idpersona_creador`,`fecha_creacion`,`edad_minima`,`edad_maxima`) values (7,'2do de primaria',32,'2015-11-04',6,8),(11,'3ro de Primaria',32,'2016-01-10',6,9),(12,'Prueba',32,'2016-01-14',6,9),(13,'Primero de Primaria \"abejitas\"',32,'2016-02-12',6,9),(14,'1ro de Primaria - Estrellitas',32,'2016-03-19',6,9);

/*Table structure for table `institucion` */

DROP TABLE IF EXISTS `institucion`;

CREATE TABLE `institucion` (
  `idinstitucion` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idinstitucion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `institucion` */

insert  into `institucion`(`idinstitucion`,`nombre`) values (1,'Organización General');

/*Table structure for table `invasores` */

DROP TABLE IF EXISTS `invasores`;

CREATE TABLE `invasores` (
  `idinvasores` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` smallint(6) DEFAULT NULL,
  `rpta_tiempo` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`idinvasores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `invasores` */

/*Table structure for table `invasores_detalle` */

DROP TABLE IF EXISTS `invasores_detalle`;

CREATE TABLE `invasores_detalle` (
  `idinvasores_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idinvasores` int(11) DEFAULT NULL,
  `tipo` varchar(10) DEFAULT NULL,
  `soles` int(11) DEFAULT NULL,
  `centimos` int(11) DEFAULT NULL,
  `enunciado` varchar(200) DEFAULT NULL,
  `nro_monedabillete` int(11) DEFAULT NULL,
  `rpta_soles` int(11) DEFAULT NULL,
  `rpta_centimos` int(11) DEFAULT NULL,
  `rpta_nromonedas` int(11) DEFAULT NULL,
  `tipofigura` varchar(45) DEFAULT NULL,
  `enunciadofig` varchar(200) DEFAULT NULL,
  `nro_figEsperadas` int(11) DEFAULT NULL,
  `rpta_figEncontradas` int(11) DEFAULT NULL,
  PRIMARY KEY (`idinvasores_detalle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `invasores_detalle` */

/*Table structure for table `juego` */

DROP TABLE IF EXISTS `juego`;

CREATE TABLE `juego` (
  `idjuego` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `curso_dirigido` varchar(45) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `edad_minima` varchar(2) DEFAULT NULL,
  `edad_maxima` varchar(2) DEFAULT NULL,
  `estado` varchar(25) DEFAULT 'activo',
  `fk_idpersona` int(11) DEFAULT NULL,
  `fk_idjuegoespecifico` int(11) DEFAULT NULL,
  `ruta_imagen` varchar(100) DEFAULT NULL,
  `fk_juego_especifico` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjuego`),
  KEY `fk_idpersona` (`fk_idpersona`),
  CONSTRAINT `juego_ibfk_4` FOREIGN KEY (`fk_idpersona`) REFERENCES `persona` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8;

/*Data for the table `juego` */

insert  into `juego`(`idjuego`,`nombre`,`descripcion`,`curso_dirigido`,`tipo`,`fecha_creacion`,`edad_minima`,`edad_maxima`,`estado`,`fk_idpersona`,`fk_idjuegoespecifico`,`ruta_imagen`,`fk_juego_especifico`) values (231,'Partes de la planta','Haz click en las partes  indicadas',NULL,'mapa',NULL,'06','09','activo',32,1,'plantas.jpg',NULL),(232,'Nùmeros en Inglès','Haz click en las traducciones correctas',NULL,'mapa',NULL,'06','09','activo',32,1,'ingles.jpg',NULL),(233,'lalal','',NULL,'mapa',NULL,'','','activo',32,26,NULL,NULL),(234,'Geografia 1','Haz click en las ciudades indicadas',NULL,'mapa',NULL,'','','activo',32,27,NULL,NULL),(235,'Partes de la planta','',NULL,'mapa',NULL,'','','activo',32,28,NULL,NULL);

/*Table structure for table `juego_imagen` */

DROP TABLE IF EXISTS `juego_imagen`;

CREATE TABLE `juego_imagen` (
  `idjuego_imagen` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(400) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `observaciones` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_imagen`),
  KEY `fk_idjuego` (`fk_idjuego`),
  CONSTRAINT `juego_imagen_ibfk_1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `juego_imagen` */

/*Table structure for table `juego_sesion` */

DROP TABLE IF EXISTS `juego_sesion`;

CREATE TABLE `juego_sesion` (
  `idjuego_sesion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(15) DEFAULT NULL,
  `fk_idsesion` int(11) NOT NULL,
  `fk_idjuego` int(11) NOT NULL,
  `estado` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idjuego_sesion`),
  KEY `fk_juego_sesion_sesion1_idx` (`fk_idsesion`),
  KEY `fk_juego_sesion_juego1_idx` (`fk_idjuego`),
  CONSTRAINT `fk_juego_sesion_juego1` FOREIGN KEY (`fk_idjuego`) REFERENCES `juego` (`idjuego`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_juego_sesion_sesion1` FOREIGN KEY (`fk_idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `juego_sesion` */

insert  into `juego_sesion`(`idjuego_sesion`,`fecha`,`fk_idsesion`,`fk_idjuego`,`estado`) values (13,'12/02/2016',13,232,NULL),(14,'12/02/2016',13,231,NULL),(15,'12/02/2016',14,232,NULL),(16,'12/02/2016',14,231,NULL),(17,'12/02/2016',15,232,NULL),(18,'12/02/2016',15,231,NULL),(19,'19/03/2016',16,234,NULL),(20,'19/03/2016',16,235,NULL);

/*Table structure for table `juego_tipo` */

DROP TABLE IF EXISTS `juego_tipo`;

CREATE TABLE `juego_tipo` (
  `idjuego_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idjuego_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `juego_tipo` */

insert  into `juego_tipo`(`idjuego_tipo`,`descripcion`) values (1,'Mapa');

/*Table structure for table `mapa` */

DROP TABLE IF EXISTS `mapa`;

CREATE TABLE `mapa` (
  `idmapa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `imagen` varchar(400) DEFAULT NULL,
  `tipo` int(2) DEFAULT NULL,
  `escala` float(5,3) DEFAULT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `mapa` */

insert  into `mapa`(`idmapa`,`nombre`,`descripcion`,`imagen`,`tipo`,`escala`,`puntaje`,`tiempo`) values (10,'','','http://localhost/juegos/assets/uploads/0123849001450652095.jpg',NULL,NULL,0,0),(11,'','','http://localhost/juegos/assets/uploads/0589080001450652309.jpg',NULL,NULL,0,0),(12,'','','http://localhost/juegos/assets/uploads/0558544001450652362.jpg',NULL,NULL,0,0),(13,'','','http://localhost/juegos/assets/uploads/0088700001450652620.jpg',NULL,NULL,0,0),(14,'','','http://localhost/juegos/assets/uploads/0348085001450652706.jpg',NULL,NULL,0,0),(15,'','','http://localhost/juegos/assets/uploads/0788228001450652728.jpg',NULL,NULL,0,0),(16,'','','http://localhost/juegos/assets/uploads/0210461001450653007.jpg',NULL,1.575,0,0),(17,'','','http://localhost/juegos/assets/uploads/0716144001450653119.jpg',NULL,NULL,0,5),(18,'','','http://localhost/juegos/assets/uploads/0761743001450653163.jpg',NULL,1.575,0,6),(19,'','','http://localhost/juegos/assets/uploads/0386329001450653316.jpg',NULL,1.575,0,6),(20,'','','http://localhost/juegos/assets/uploads/0509744001450653358.jpg',NULL,1.575,0,0),(21,'Partes de la planta','HAz click en las partes señaladas','http://localhost/juegos/assets/uploads/0528231001454295410.png',NULL,1.983,120,6),(22,'Partes de la planta','Haz click en las partes  indicadas','http://localhost/juegos/assets/uploads/0189226001454295784.jpg',NULL,0.776,100,5),(23,'Nùmeros en Inglès','Haz click en las traducciones correctas','http://localhost/juegos/assets/uploads/0628746001454295878.png',NULL,1.983,5,4),(24,'Partes de la planta 2016','Señala las partes de la planta','http://localhost:8448/juegos/assets/uploads/0634989001458410784.jpg',NULL,NULL,100,20),(25,'Plantitas','Haz click en las partes de la planta','http://localhost:8448/juegos/assets/uploads/0981784001458410908.jpg',NULL,NULL,100,5),(26,'lalal','','http://localhost:8448/juegos/assets/uploads/0357066001458411219.png',NULL,1.044,0,0),(27,'Geografia 1','Haz click en las ciudades indicadas','http://localhost:8448/juegos/assets/uploads/0450585001458411263.png',NULL,1.044,100,20),(28,'Partes de la planta','','http://localhost:8448/juegos/assets/uploads/0504768001458412384.jpg',NULL,0.774,0,0);

/*Table structure for table `mapa_punto` */

DROP TABLE IF EXISTS `mapa_punto`;

CREATE TABLE `mapa_punto` (
  `idmapa_punto` int(11) NOT NULL AUTO_INCREMENT,
  `px` int(11) DEFAULT NULL,
  `py` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `idmapa` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmapa_punto`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

/*Data for the table `mapa_punto` */

insert  into `mapa_punto`(`idmapa_punto`,`px`,`py`,`descripcion`,`idmapa`) values (7,220,68,'jpslala',-1),(8,99,207,'lelellel',-1),(9,235,131,'jajja',-1),(10,356,169,'jajajja',15),(11,167,103,'jajaj',16),(12,191,79,'lll',17),(13,161,93,'paalala',18),(14,186,79,'malditooo',19),(15,155,100,'bolitaaa',20),(16,92,87,'Uno',21),(17,279,85,'Dos',21),(18,474,74,'tres',21),(19,84,264,'Cuatro',21),(20,291,247,'Cinco',21),(21,445,264,'Seis',21),(22,123,99,'Hoja',22),(23,177,207,'Talla',22),(24,178,337,'Raiz',22),(25,80,77,'Uno',23),(26,296,266,'Dos',23),(27,460,257,'Seis',23),(28,124,92,'hoja',-1),(29,177,208,'tallo',-1),(30,185,336,'raiz',-1),(31,180,92,'Loreto',27),(32,188,213,'Ucayali',27),(33,244,255,'Madre%20de%20Dios',27),(34,126,97,'Hoja',28),(35,189,296,'tallo',28),(36,191,324,'raiz',28);

/*Table structure for table `memoria` */

DROP TABLE IF EXISTS `memoria`;

CREATE TABLE `memoria` (
  `idmemoria` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmemoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memoria` */

/*Table structure for table `memoria_detalles` */

DROP TABLE IF EXISTS `memoria_detalles`;

CREATE TABLE `memoria_detalles` (
  `idmemoria_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idmemoria` int(11) NOT NULL,
  `nombre_imgfraccion` varchar(45) DEFAULT NULL,
  `distractor1` varchar(45) DEFAULT NULL,
  `distractor2` varchar(45) DEFAULT NULL,
  `rpta_imgfraccion` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  PRIMARY KEY (`idmemoria_detalles`),
  KEY `fk_idmemoria` (`fk_idmemoria`),
  CONSTRAINT `memoria_detalles_ibfk_1` FOREIGN KEY (`fk_idmemoria`) REFERENCES `memoria` (`idmemoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memoria_detalles` */

/*Table structure for table `partida` */

DROP TABLE IF EXISTS `partida`;

CREATE TABLE `partida` (
  `idpartida` int(11) NOT NULL AUTO_INCREMENT,
  `duracion` int(11) DEFAULT NULL,
  `partidacol` varchar(45) DEFAULT NULL,
  `idusuario` int(11) NOT NULL,
  `idjuego_sesion` int(11) NOT NULL,
  PRIMARY KEY (`idpartida`),
  KEY `fk_partida_usuario1_idx` (`idusuario`),
  KEY `fk_partida_juego_sesion1_idx` (`idjuego_sesion`),
  CONSTRAINT `fk_partida_juego_sesion1` FOREIGN KEY (`idjuego_sesion`) REFERENCES `juego_sesion` (`idjuego_sesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partida_usuario1` FOREIGN KEY (`idusuario`) REFERENCES `persona` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida` */

/*Table structure for table `partida_repeticion` */

DROP TABLE IF EXISTS `partida_repeticion`;

CREATE TABLE `partida_repeticion` (
  `idpartida_repeticion` int(11) NOT NULL AUTO_INCREMENT,
  `idpartida` int(11) NOT NULL,
  `fecha` varchar(15) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartida_repeticion`),
  KEY `fk_partida_repeticion_partida1_idx` (`idpartida`),
  CONSTRAINT `fk_partida_repeticion_partida1` FOREIGN KEY (`idpartida`) REFERENCES `partida` (`idpartida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `partida_repeticion` */

/*Table structure for table `persona` */

DROP TABLE IF EXISTS `persona`;

CREATE TABLE `persona` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) DEFAULT NULL,
  `ap` varchar(45) DEFAULT NULL,
  `am` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` varchar(15) DEFAULT NULL,
  `idtipo_usuario` int(11) DEFAULT NULL,
  `idinstitucion` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `usuario` varchar(6) DEFAULT NULL,
  `clave` varchar(32) DEFAULT NULL,
  `foto_ruta` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `fk_usuario_tipo_usuario_idx` (`idtipo_usuario`),
  KEY `fk_usuario_institucion1_idx` (`idinstitucion`),
  KEY `fk_usuario_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_institucion1` FOREIGN KEY (`idinstitucion`) REFERENCES `institucion` (`idinstitucion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_tipo_usuario` FOREIGN KEY (`idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `persona` */

insert  into `persona`(`idusuario`,`nombres`,`ap`,`am`,`fecha_nacimiento`,`idtipo_usuario`,`idinstitucion`,`idgrupo`,`usuario`,`clave`,`foto_ruta`) values (32,'Rosa','Silva','Sosa','1990-12-17',1,1,NULL,'rsilva','202cb962ac59075b964b07152d234b70','rosita.jpg'),(33,'Pepito','Perez','Casas','2006-11-17',3,1,NULL,'pperez','202cb962ac59075b964b07152d234b70','pepito.jpg'),(34,'Lunita','Chaparro','DÃ­az','2008-03-15',3,1,NULL,'lchapa','202cb962ac59075b964b07152d234b70','lunita.jpg'),(35,'Lucero','PeÃ±a','Luna','2007-11-15',3,1,NULL,'lucero','202cb962ac59075b964b07152d234b70','lucero.jpg'),(38,'Martita','Coyo','Pinto','2008-06-15',3,1,NULL,'martit','202cb962ac59075b964b07152d234b70',''),(39,'Juancito','Pelaez','Urbano','2008-03-12',3,1,NULL,'jpelae','202cb962ac59075b964b07152d234b70','juancito.jpg'),(40,'Joelangel','Motta','Marin','1990-09-09',1,1,NULL,'100pre','202cb962ac59075b964b07152d234b70',NULL),(41,'Claudio','Pizarro','Noriega','1980-02-01',2,1,NULL,'pizza','202cb962ac59075b964b07152d234b70','profe2.jpg'),(42,'pepillo','Lala','peres','1995-12-12',3,1,NULL,'jeje','202cb962ac59075b964b07152d234b70',NULL),(43,'Valeria','Perez','Lopez','1989-12-01',2,1,NULL,'vale12','202cb962ac59075b964b07152d234b70','profe1.jpg');

/*Table structure for table `persona_grupo` */

DROP TABLE IF EXISTS `persona_grupo`;

CREATE TABLE `persona_grupo` (
  `idpersona_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idgrupo` int(11) DEFAULT NULL,
  `idpersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpersona_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `persona_grupo` */

insert  into `persona_grupo`(`idpersona_grupo`,`idgrupo`,`idpersona`) values (7,13,32),(8,13,33),(9,13,34),(10,13,35),(11,13,38),(12,13,39),(13,13,43),(14,14,35),(15,14,38),(16,14,39),(17,14,41);

/*Table structure for table `sesion` */

DROP TABLE IF EXISTS `sesion`;

CREATE TABLE `sesion` (
  `idsesion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `idpersona_creador` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  `fecha_programada` date DEFAULT NULL,
  `ruta_imagen` varchar(50) DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsesion`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `sesion` */

insert  into `sesion`(`idsesion`,`nombre`,`fecha_creacion`,`idpersona_creador`,`idgrupo`,`estado`,`fecha_programada`,`ruta_imagen`,`duracion`) values (1,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(2,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(3,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(4,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(5,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(6,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(7,NULL,'0000-00-00',32,NULL,NULL,NULL,NULL,NULL),(8,NULL,'0000-00-00',32,7,NULL,'0000-00-00',NULL,NULL),(9,NULL,'0000-00-00',32,7,NULL,'0000-00-00',NULL,NULL),(10,'El reino vegetal','0000-00-00',32,7,'programado','0000-00-00',NULL,NULL),(11,'kkakkaka','0000-00-00',32,7,'programado','0000-00-00',NULL,NULL),(12,'El reino vegetal','0000-00-00',32,7,NULL,'0000-00-00',NULL,NULL),(13,'Rosaliooo','0000-00-00',32,7,'programado','0000-00-00',NULL,NULL),(14,'el Reino Vegetal','0000-00-00',32,13,'programado','2016-03-19',NULL,NULL),(15,'','0000-00-00',32,7,'programado','0000-00-00',NULL,NULL),(16,'Sesión de prueba','0000-00-00',32,14,'programado','0000-00-00',NULL,NULL);

/*Table structure for table `sesion_grupo` */

DROP TABLE IF EXISTS `sesion_grupo`;

CREATE TABLE `sesion_grupo` (
  `idsesion_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `idsesion` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`idsesion_grupo`),
  KEY `fk_sesion_grupo_sesion1_idx` (`idsesion`),
  KEY `fk_sesion_grupo_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_sesion_grupo_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sesion_grupo_sesion1` FOREIGN KEY (`idsesion`) REFERENCES `sesion` (`idsesion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `sesion_grupo` */

insert  into `sesion_grupo`(`idsesion_grupo`,`idsesion`,`idgrupo`) values (1,13,7),(2,13,7),(3,14,13),(4,14,13),(5,15,7),(6,15,7),(7,16,14),(8,16,14);

/*Table structure for table `snake` */

DROP TABLE IF EXISTS `snake`;

CREATE TABLE `snake` (
  `idsnake` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` float(5,3) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `snake` */

/*Table structure for table `snake_detalles` */

DROP TABLE IF EXISTS `snake_detalles`;

CREATE TABLE `snake_detalles` (
  `idsnake_detalles` int(11) NOT NULL AUTO_INCREMENT,
  `fk_idsnake` int(11) NOT NULL,
  `nombre_imagen` varchar(45) DEFAULT NULL,
  `texto_ayuda` varchar(45) DEFAULT NULL,
  `rpta_imagen` varchar(45) DEFAULT NULL,
  `rpta_puntos` float(5,3) DEFAULT NULL,
  `rpta_tiempo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsnake_detalles`),
  KEY `fk_idsnake` (`fk_idsnake`),
  CONSTRAINT `snake_detalles_ibfk_1` FOREIGN KEY (`fk_idsnake`) REFERENCES `snake` (`idsnake`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `snake_detalles` */

/*Table structure for table `tipo_juego` */

DROP TABLE IF EXISTS `tipo_juego`;

CREATE TABLE `tipo_juego` (
  `idtipo_juego` tinyint(4) NOT NULL,
  `nombre_tabla` varchar(20) NOT NULL,
  `ruta_imagen` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipo_juego`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `tipo_juego` */

/*Table structure for table `tipo_usuario` */

DROP TABLE IF EXISTS `tipo_usuario`;

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tipo_usuario` */

insert  into `tipo_usuario`(`idtipo_usuario`,`rol`) values (1,'administrador'),(2,'profesor'),(3,'alumno');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
