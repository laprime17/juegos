<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_inicio extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'usuario';
        $this->primary_key = 'idusuario';
    }
 
    public function default_select() {
        $this->db->select('usuario.*');
    }

    public function default_order_by() {
        if ($this->order_by && $this->order) {
            $this->db->order_by($this->order_by, $this->order);
        }
        else {
            $this->db->order_by($this->primary_key);
        }
    }
    public function validation_rules() {
        return array(
            'descripcion' => array(
                    'field' => 'descripcion',
                    'label' => 'descripcion',
                    'rules' => 'required|trim|xss_clean'
                    )
        );
    }
    public   function participantes($idusuario)
    {
        //mostrar a participantes del mismo grupo  en el panel de inicio
        $personas = $this->db->query('select * from persona where idusuario in
            (select idpersona from persona_grupo where idgrupo in (select idgrupo from persona_grupo where idpersona='.$idusuario.'))')->result();
        
        
        return $personas;
    }
    public function juegos($idusuario, $fecha_prog = null)
    {
        $cadena = '';
        if ($fecha_prog!= null) {
            $cadena = ' where  fecha_programada="'.$fecha_prog.'"';
        }

        $juegos = $this->db->query('select T.* from (select b.*, g.nombre as nombre_grupo, 
                                        (select fecha_programada 
                                            from sesion 
                                            where idsesion = b.fk_idsesion)as fecha_programada 
                                        from (select a.*, sg.idgrupo 
                                              from 
                                             (select juego.*, juego_sesion.fk_idsesion 
                                              from juego,juego_sesion 
                                              where juego.idjuego = juego_sesion.fk_idjuego)a, 
                                        (select * 
                                            from sesion_grupo 
                                            where idgrupo in 
                                                        (select idgrupo 
                                                         from persona_grupo 
                                                         where idpersona ='.$idusuario.')) sg 
                                        where a.fk_idsesion = sg.idsesion)b,grupo g 
                                    where b.idgrupo = g.idgrupo)T '.$cadena)->result();
        
        
        return $juegos;
    }
    public function sesiones($idusuario, $fecha_prog = null)
    {
        $cadena = '';
        if ($fecha_prog!= null) {
            $cadena = ' and  fecha_programada="'.$fecha_prog.'"';
        }
        $sesiones = $this->db->query('select * from sesion where idsesion in
                                    (select distinct(idsesion) from 
                                        (select idgrupo 
                                         from persona_grupo 
                                         where idpersona = '.$idusuario.')G,
                                    sesion_grupo S
                                    where G.idgrupo =S.idgrupo)
                                    and estado="programado" '.$cadena.' order by idsesion desc')->result();
        
        
        return $sesiones;
    }
    public function get_by_pk($tabla,$pk)
    {
        return $this->db->query('select * from '.$tabla.' where id'.$tabla.'='.$pk)->row();
    }
   
}
?>