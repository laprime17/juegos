<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_administrador extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'usuario';
        $this->primary_key = 'idusuario';
    }
 
    public function get_juegos_x_creador($idpersona=null){
        

        if ($idpersona==null) {
            return null;
        }
        $result = $this->db->query('select * from juego where fk_idpersona ='.$idpersona.' order by idjuego desc limit 10');
        return $result->result();
        
    }
    public function by_id($tabla,$id)
    {

        $row =  $this->db->query('select * from '.$tabla.' where id'.$tabla.' ='.$id);

        if ($row->num_rows==1) {
            return $row->row();
        }
        return null;
    }
    public function get_detalle_juego($idjuego)
    {

        $query_juego = 'select  *  from juego where idjuego ='.$idjuego;
        $num_rows =$this->db->query($query_juego)->num_rows();

        if ($num_rows==0 || $num_rows >1) {
            return null;
        }
        $Juego = $this->db->query($query_juego)->row();        
        
        $query = 'select  * from '.$Juego->tipo.' where id'.$Juego->tipo .'='. $Juego->fk_idjuegoespecifico;
        $num_rows =$this->db->query($query_juego)->num_rows();
        if ($num_rows==0 || $num_rows >1) {
            return null;
        }
        
        $Juego_derivado = $this->db->query($query)->row();

        $A = array('juego' => $Juego ,
                   'juego_derivado' => $Juego_derivado);
        
        return $A;
    }
    public function grupos_x_creador($idpersona = null)
    {
        if ($idpersona==null) {
            return null;
        }
        $query ='select g.*,
                (select count(idsesion_grupo)nro_sesiones 
                from sesion_grupo where idgrupo=g.idgrupo )nro_sesiones,
                (select fullnombre from persona where idusuario =g.idpersona_creador)usuario_propietario
                from grupo g where g.stsreg="ACT" and idpersona_creador = '.$idpersona;

        if ($this->session->userdata('rol')=='administrador') {
            'select g.*,
            (select count(idsesion_grupo)nro_sesiones 
            from sesion_grupo where idgrupo=g.idgrupo )nro_sesiones,
(select fullnombre from persona where idusuario =g.idusuario_creador)usuario_propietario
            from grupo g where g.stsreg="ACT"';
        }

        $result = $this->db->query($query)->result();

        return $result;

    }
    public function sesiones_x_creador($idpersona = null)
    {
        if ($idpersona==null) {
            return null;
        }

        $result = $this->db->query('select * from sesion where idpersona_creador = '.$idpersona)->result();
        return $result;
        

    }
    public function participantes_x_grupo($idgrupo)
    {
        $query='select p.*,
(select rol from tipo_usuario where idtipo_usuario = p.idtipo_usuario)rol

from persona p inner join persona_grupo pg on pg.idpersona = p.idusuario
where pg.idgrupo = '.$idgrupo;
        $result = $this->db->query($query)->result();
        return $result;
    }
    public function guardar_grupo($data)
    {
        $this->db->insert('grupo',$data);
        $id = $this->db->insert_id();
        return $id;
    }
    public function asignar_alumnos_grupo($alumnos,$idgrupo)
    {
        
        if(!in_array($this->session->userdata('id'), $alumnos))
        {
            array_push($alumnos,$this->session->userdata('id'));
        }
        $data = array('idgrupo' => $idgrupo);
        $i = 0;

        foreach ($alumnos as $alumno) {
            $data['idpersona'] = $alumno;
            $this->db->insert('persona_grupo',$data);
            $i++;
        }
        return $i;
    }
    public function sesiones_x_grupo($idgrupo,$estado=null)
    {
        $p_estado = '= "'.$estado.'"';
        if ($estado==null) {
            $p_estado = " like '%'";
        }
   
        $result = $this->db->query('select sesion.idsesion, sesion.nombre, sesion.fecha_creacion,sesion.fecha_programada, count(juego_sesion.fk_idjuego) as 
            nro from sesion left join juego_sesion on sesion.idsesion 
            = juego_sesion.fk_idsesion where sesion.estado '.$p_estado .'and idgrupo ='.$idgrupo.'
group by sesion.idsesion,sesion.nombre, sesion.fecha_creacion,sesion.fecha_programada '.$p_estado.' and idgrupo ='.$idgrupo.' order by idsesion desc limit 10 ')->result();

        
        
        return $result;
        // actividades por sesion

    }
    public function reset_clave($idusuario,$data1)
    {
        $data = array('clave' => md5($data1['clave']));
        $this->db->where('idusuario',$idusuario);
        $this->db->update('persona',$data);
        return $this->db->affected_rows();
        
    }
    public function eliminar_grupo($idgrupo)
    {
        $this->db->set('stsreg', 'ELIM');
        $this->db->where('idgrupo', $idgrupo);
        $this->db->update('grupo');
        return $this->db->affected_rows();
    }

}
?>