<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_persona extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'usuario';
        $this->primary_key = 'idusuario';
    }
 
    public function default_select() {
        $this->db->select('usuario.*');
    }

    public function default_order_by() {
        if ($this->order_by && $this->order) {
            $this->db->order_by($this->order_by, $this->order);
        }
        else {
            $this->db->order_by($this->primary_key);
        }
    }
    public function validation_rules() {
        return array(
            'descripcion' => array(
                    'field' => 'descripcion',
                    'label' => 'descripcion',
                    'rules' => 'required|trim|xss_clean'
                    )
        );
    }

    // funciones propias
    public function get_by_usuario($usuario)
    {
        $recurso = $this->db->query('select * from persona where usuario ="'.$usuario.'"');
        return $recurso;
    }
    public function obtener_persona_usuario($row)
    {
        $result = $this->db->query('select * from  persona where usuario ="'.$row->usuario.'"')->row();
        return $result; 

    }
    public function lista_usuario_coincidencia($texto)
    {
        $texto = str_replace(' ', '%', $texto);
        // var_dump($texto);   
        $texto = '%'.$texto.'%';
        // var_dump($texto);   
        $result = $this->db->query("select p.*,i.nombre as organizacion,(select rol from tipo_usuario where idtipo_usuario = p.idtipo_usuario)rol  from (select * from persona  where concat(nombres,ap,am) like '".$texto."')p left join institucion i on p.idinstitucion=i.idinstitucion")->result();
        return $result;
    }
    public function lista($rol='alumno')
    {
        $result = $this->db->query("select * from persona where rol='".$rol."'")->result();
        return $result;

    }
    public function get_usuarios_by_tipo($tipo)
    {
        $texto = '';
        if ($tipo == 'all') {
            $texto = ' like "%" ';
        }
        else
        {
            $texto = ' = "'.$tipo.'" ' ;
        }
        if ($this->session->userdata('rol')=='administrador') {
            $result =  $this->db->query('select * from persona where idtipo_usuario '.$texto)->result();
        }
        else
        {
            $result =  $this->db->query('select * from persona where idusrcrea='.$this->session->userdata('id').' and idtipo_usuario '.$texto)->result();

        }
       return $result;
    }  
    public function buscar_coincidencia_usuario($cadena)
    {

        if ($this->session->userdata('administrador')) {
            $result =  $this->db->query('select * from persona where concat(nombres,ap,am) like "%'.$cadena.'%"')->result();
        }
        else
        {
            $result =  $this->db->query('select * from persona where idusrcrea = '.$this->session->userdata('id').' and concat(nombres,ap,am) like "%'.$cadena.'%"')->result();
        }        
       return $result;
    }
    public function existe_usuario($usuario,$idusuario=null)
    {
        $query = 'select count(idusuario) as nro from persona  where usuario = "'.$usuario.'"';
        if ($idusuario != null) {
            $query = 'select count(idusuario) as nro from persona  where usuario = "'.$usuario.'" and idusuario <> '.$idusuario;
        }
        $result = $this->db->query($query)->result()[0]->nro;        
        if (intval($result)==0) {
             return 'N';
        }
             return 'S';


    }
    public function guardar_usuario($data,$idusuario){

        if ($idusuario == 0 ) {
            if ($this->existe_usuario($data['usuario']) == 'N')
            {
                $data['idusrcrea'] = $this->session->userdata('id');
                $result = $this->db->insert('persona',$data);
                if ($result) {
                    return $this->db->insert_id();
                }
            }
            else
            {
                return 'Error. El usuario ya existe.';
            }
        }
        else //ediciòn
        {
            // verificar que no exist otra persona  con el mismo nombre de usuario
            if ($this->existe_usuario($data['usuario'],$idusuario)=='N') {
                $this->db->where('idusuario',$idusuario);
                $this->db->update('persona',$data);
                return 1;
            }
            else
            {

                return 'Error. El usuario ya existe.';
            }
           
                
        }
        


        return false;
    }
    public function get_todos()
    {
        $idusrcrea = $this->session->userdata('id');
        $cad_prof = ' like "%"';
        if ($this->session->userdata('rol') == 'profesor') {
            $cad_prof = ' = '. $idusrcrea;
        }
        $result = $this->db->query("select  p.*,i.nombre as organizacion,(select rol from tipo_usuario where idtipo_usuario = p.idtipo_usuario)rol from persona p left join institucion i on p.idinstitucion=i.idinstitucion where p.idusrcrea ".$cad_prof)->result();
        return $result;
        
    }
    public function get_alumnos()
    {
        $result = $this->db->query("select * from persona where idtipo_usuario = 3")->result();
        return $result;
    }
    public function grupos_x_persona($idpersona)
    {
        $A = array();
        $result = $this->db->query('select grupo.* 
                                  from  grupo
                                  where idgrupo in(select idgrupo  
                                                   from persona_grupo 
                                                   where idpersona = '.$this->session->userdata('id').')')->result();

            foreach ($result as $grupo ) {
                    //profesores por grupo
                    $docentes = $this->db->query('select persona.* 
                              from (select * 
                                    from persona_grupo 
                                    where idgrupo = '.$grupo->idgrupo.' 
                                    and idpersona in(select idusuario 
                                                     from persona 
                                                     where idtipo_usuario = 2))PG 
                              inner join persona 
                              on PG.idpersona = persona.idusuario')->result();

                    $alumnos = $this->db->query('select persona.* 
                              from (select * 
                                    from persona_grupo 
                                    where idgrupo = '.$grupo->idgrupo.' 
                                    and idpersona in(select idusuario 
                                                     from persona 
                                                     where idtipo_usuario = 3))PG 
                              inner join persona 
                              on PG.idpersona = persona.idusuario')->result();

                    $agrupo = array('grupo' => $grupo,
                                    'docentes'=> $docentes,
                                    'alumnos' => $alumnos);

                    array_push($A, $agrupo);
            }
        return $A;
    }
    public function resultados_x_persona($idpersona)
    {
        $A = array();

        $sesiones = $this->db->query('select * from sesion where idsesion in
                                    (select distinct(idsesion) from 
                                        (select idgrupo 
                                         from persona_grupo 
                                         where idpersona = '.$this->session->userdata('id').')G,
                                    sesion_grupo S
                                    where G.idgrupo =S.idgrupo)
                                    and estado="programado" order by idsesion desc')->result();

            foreach ($sesiones as $sesion ) {

                    $ajuegoespecifico =array();
                    $juegos = $this->db->query('select * from (select j.idjuego, j.nombre, j.tipo, j.estado,j.fk_idjuegoespecifico, js.idjuego_sesion,pa.idpartida, pa.duracion, pa.puntaje 
                                         from juego j inner join 
                                            (select idjuego_sesion,fk_idjuego,orden  
                                             from juego_sesion
                                             where fk_idsesion ='.$sesion->idsesion.')js 
                                             on js.fk_idjuego = j.idjuego inner join 
                                            (select idpartida, idjuego_sesion, duracion, puntaje 
                                             from partida where stsreg = "FIN" and idusuario='.$this->session->userdata('id').')pa 
                                             on pa.idjuego_sesion = js.idjuego_sesion )t')->result();

                     foreach ($juegos as $juego ) {
                        $juegoespecifico = $this->db->query('select * from '.$juego->tipo.' where id'.$juego->tipo.'='.$juego->fk_idjuegoespecifico)->row();
                        
                        array_push($ajuegoespecifico, $juegoespecifico->puntaje);
                     }
                    

                    $asesion = array('sesion' => $sesion,
                                     'juegos' => $juegos,
                                     'juegoespecifico'=>$ajuegoespecifico
                                    );

                    array_push($A, $asesion);
            }
        return $A;
    }
}
?>