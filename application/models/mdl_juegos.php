<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_juegos extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'usuario';
        $this->primary_key = 'idusuario';
    }
 
    public function Guardar_mapa($datos,$idmapa=null) {


        if ($idmapa > -1) { //edicion
            # code...
            return $datos['idmapa'];
        }
        else
        {
            
            $edad_maxima = $datos['edad_maxima'];
            $edad_minima = $datos['edad_minima'];
            unset($datos['idmapa']);
            unset($datos['edad_minima']);
            unset($datos['edad_maxima']);
            // $datos['tiempo']=60;
            $this->db->insert('mapa',$datos);
            $id = $this->db->insert_id();
            

            // insertar en tabla juegos
            $data_juego = array('descripcion' =>$datos['descripcion'],
                                'nombre'=>$datos['nombre'],
                                'tipo'=>'mapa',
                                'edad_minima'=>$edad_minima,
                                'edad_maxima'=>$edad_maxima,
                                'fk_idjuegoespecifico' => $id,
                                'fk_idpersona'=>$this->session->userdata('id'));
            $this->db->insert('juego',$data_juego);
            
            
            return $id;
        }

    }
     public function Guardar_punto($data)
     {


       $this->db->insert('mapa_punto',$data);
       $id = $this->db->insert_id();   
       return $id;

     }
     public function  mapa_puntosxmapa($idmapa)
    {
        $query = 'select * from mapa_punto where idmapa = '.$idmapa;
        $result  = $this->db->query($query)->result();
        return $result;
    }
    public function mapa_getmapa($idmapa)
    {
        $query = 'select * from mapa where idmapa = '.$idmapa;
        $result = $this->db->query($query)->result()[0];
        return $result;
    }
    public function mapa_guardar_escala($escala,$idmapa)
    {
        $data = array('escala' => $escala);
        $this->db->where('idmapa',$idmapa);
        $this->db->update('mapa',$data);
        return $this->db->affected_rows();
    }
       public function mapa_eliminar_registro($idmapa)
       {
            $this->db->delete('mapa', array('idmapa' => $idmapa)); 
       }
       public function mapa_eliminar_puntos($idmapa)
       {
            $this->db->delete('mapa_punto', array('idmapa' => $idmapa)); 

       }
       public function  mapa_datos($idmapa)
       {
        
            $result = $this->db->query('select * from mapa where idmapa='.$idmapa)->row();
            return $result;
       }
       public function mapa_puntos($idmapa)
       {
            $result = $this->db->query('select * from mapa_punto where idmapa='.$idmapa)->result();
            return $result;
       }

    //Metodos para el snake
    public function Guardar_snake($datos) {
        
        if ($datos['idsnake'] >= 0) { //edicion; en realidad $datos['idsnake'] es el id de juego, no de snake
            # code...
            $resultid = $this->db->query('select fk_idjuegoespecifico from juego where idjuego = '.$datos['idsnake'])->row();
            
            $snakeid = $resultid->fk_idjuegoespecifico;
            //Modificar la tabla snake
            $datasnake = array('tiempo' => $datos['tiempo'],
                                'puntaje' =>$datos['puntaje']);
            $this->db->where('idsnake',$snakeid);
            $this->db->update('snake',$datasnake);

            //modificar juego
            $datajuego = array('nombre' => $datos['nombre'],
                                'descripcion' =>$datos['descripcion'],
                                'edad_minima' =>$datos['edad_minima'],
                                'edad_maxima' =>$datos['edad_maxima']);
            $this->db->where('idjuego',$datos['idsnake']);
            $this->db->update('juego',$datajuego);


            $this->db->delete('snake_detalles', array('fk_idsnake' => $snakeid));
            $this->db->delete('juego_imagen', array('fk_idjuego' => $datos['idsnake'])); 

            $arreglo=json_decode($datos['detalles'], true);

            $contador = count($arreglo) / 3;
            
            for ($i=1; $i <= $contador; $i++) { 
                // insertar en la tabla juego_imagen
                $data_juegoimagen = array('ruta' =>$arreglo['imagen'.$i],
                                'fk_idjuego'=>$datos['idsnake'],
                                'observaciones'=>$arreglo['palabra'.$i]);
                $this->db->insert('juego_imagen',$data_juegoimagen);

                //insertar en la tabla snake_detalles
                $data_snakedetalles = array('fk_idsnake' =>$snakeid,
                                'nombre_imagen'=>$arreglo['palabra'.$i],
                                'texto_ayuda'=>$arreglo['ayuda'.$i]);
                $this->db->insert('snake_detalles',$data_snakedetalles);
            }
            return $datos['idsnake'];
        }
        else
        {
                      
            $nombre = $datos['nombre'];
            $descripcion = $datos['descripcion'];
            $edad_maxima = $datos['edad_maxima'];
            $edad_minima = $datos['edad_minima'];
            $curso = $datos['curso'];
            $cadena = $datos['detalles'];
            unset($datos['nombre']);
            unset($datos['descripcion']);
            unset($datos['idsnake']);
            unset($datos['edad_minima']);
            unset($datos['edad_maxima']);
            unset($datos['detalles']);
            unset($datos['curso']);
            // $datos['tiempo']=60;

            //insertar en la tabla snake
            $this->db->insert('snake',$datos);
            $id = $this->db->insert_id();
            
            $arreglo=json_decode($cadena, true);


            // insertar en tabla juegos
            $data_juego = array('descripcion' =>$descripcion,
                                'nombre'=>$nombre,
                                'curso_dirigido' =>$curso,
                                'tipo'=>'snake',
                                'edad_minima'=>$edad_minima,
                                'edad_maxima'=>$edad_maxima,        
                                'fk_idpersona'=>$this->session->userdata('id'),
                                'fk_idjuegoespecifico'=>$id);
            $this->db->insert('juego',$data_juego);
            $idjuego = $this->db->insert_id();
            
            $contador = count($arreglo) / 3; //si sirve
            //echo "contador : ".$cont ;


            // insertar en la tabla juego_imagen
            for ($i=1; $i <= $contador; $i++) { 
                $data_juegoimagen = array('ruta' =>$arreglo['imagen'.$i],
                                'fk_idjuego'=>$idjuego,
                                'observaciones'=>$arreglo['palabra'.$i]);
                $this->db->insert('juego_imagen',$data_juegoimagen);

                //insertar en la tabla snake_detalles
                $data_snakedetalles = array('fk_idsnake' =>$id,
                                'nombre_imagen'=>$arreglo['palabra'.$i],
                                'texto_ayuda'=>$arreglo['ayuda'.$i]);
                $this->db->insert('snake_detalles',$data_snakedetalles);
            }

            return $idjuego;
        }

    }
    public function  snake_datos($idjuego)
    {

       $result = $this->db->query('select * from snake s, juego j where s.idsnake = j.fk_idjuegoespecifico and j.tipo="snake" and j.idjuego = '.$idjuego)->row();
        return $result;
    }
    public function  snakedetalles_datos($idjuego)
    {

        $result = $this->db->query('select ji.ruta,sd.* from juego j inner join snake s on j.fk_idjuegoespecifico = s.idsnake inner join snake_detalles sd on s.idsnake = sd.fk_idsnake inner join juego_imagen ji on j.idjuego=ji.fk_idjuego and ji.observaciones = sd.nombre_imagen and j.tipo="snake" and j.idjuego = '.$idjuego)->result();

        return $result;
    }
    public function  snake_datos_byidsnake($idsnake)
    {

       $result = $this->db->query('select * from snake s, juego j where s.idsnake = j.fk_idjuegoespecifico and j.tipo="snake" and s.idsnake ='.$idsnake)->row();
        return $result;
    }
    public function  snakedetalles_datos_byidsnake($idsnake)
    {

        $result = $this->db->query('select ji.ruta,sd.* from juego j inner join snake s on j.fk_idjuegoespecifico = s.idsnake inner join snake_detalles sd on s.idsnake = sd.fk_idsnake inner join juego_imagen ji on j.idjuego=ji.fk_idjuego and ji.observaciones = sd.nombre_imagen and j.tipo="snake" and s.idsnake = '.$idsnake)->result();

        return $result;
    }
    //--------------------------------- JUEGO MEMORIA --------
    public function Guardar_memoria($datos) {
        if ($datos['idmemoria'] >= 0) { //edicion
            
            //recuperar el id de memoria con el id de juego
            $resultid = $this->db->query('select fk_idjuegoespecifico from juego where idjuego = '.$datos['idmemoria'])->row();

            $memoriaid = $resultid->fk_idjuegoespecifico;

            //Modificar la tabla memoria
            $datamemoria = array('tiempo' => $datos['tiempo'],
                                'puntaje' =>$datos['puntaje']);
            $this->db->where('idmemoria',$memoriaid);
            $this->db->update('memoria',$datamemoria);

            //modificar juego
            $datajuego = array('nombre' => $datos['nombre'],
                                'descripcion' =>$datos['descripcion'],
                                'edad_minima' =>$datos['edad_minima'],
                                'edad_maxima' =>$datos['edad_maxima']);
            $this->db->where('idjuego',$datos['idmemoria']);
            $this->db->update('juego',$datajuego);

            $this->db->delete('memoria_detalles', array('fk_idmemoria' => $memoriaid));
            $this->db->delete('juego_imagen', array('fk_idjuego' => $datos['idmemoria'])); 

            $arreglo=json_decode($datos['detallesmemoria'], true);

            // insertar en la tabla juego_imagen
            $data_juegoimagen = array('ruta' =>$datos['imagen'],
                                'fk_idjuego'=>$datos['idmemoria']);
            $this->db->insert('juego_imagen',$data_juegoimagen);


            for ($i=1; $i <= 18; $i++) { 
                //insertar en la tabla memoria_detalles
                $data_memoriadetalles = array('fk_idmemoria' =>$memoriaid,
                                'nombre_imgfraccion'=>$arreglo['nombrefraccion'.$i],
                                'distractor1'=>$arreglo['primerdistractor'.$i],
                                'distractor2'=>$arreglo['segundodistractor'.$i]);
                $this->db->insert('memoria_detalles',$data_memoriadetalles);
            }
            return $datos['idmemoria'];
        }
        else
        {
            
            $nombre = $datos['nombre'];
            $descripcion = $datos['descripcion'];
            $edad_maxima = $datos['edad_maxima'];
            $edad_minima = $datos['edad_minima'];
            $cadena = $datos['detallesmemoria'];
            $imagen = $datos['imagen'];
            $curso = $datos['curso'];
            unset($datos['nombre']);
            unset($datos['descripcion']);
            unset($datos['idmemoria']);
            unset($datos['edad_minima']);
            unset($datos['edad_maxima']);
            unset($datos['detallesmemoria']);
            unset($datos['imagen']);
            unset($datos['curso']);
            // $datos['tiempo']=60;

            //insertar en la tabla memoria
            $this->db->insert('memoria',$datos);
            $id = $this->db->insert_id();
            
            $arreglo=json_decode($cadena, true);

            //$fecha = date();
            // insertar en tabla juegos
            $data_juego = array('nombre'=>$nombre,
                                'descripcion' =>$descripcion,
                                'curso_dirigido' =>$curso,
                                'tipo'=>'memoria',
                                //'fecha_creacion'=>$fecha;
                                'edad_minima'=>$edad_minima,
                                'edad_maxima'=>$edad_maxima,
                                'fk_idpersona'=>$this->session->userdata('id'),
                                'fk_idjuegoespecifico'=>$id);

            $this->db->insert('juego',$data_juego);
            $idjuego = $this->db->insert_id();
            

             // insertar en la tabla juego_imagen
            $data_juegoimagen = array('ruta' =>$imagen,
                                'fk_idjuego'=>$idjuego);
            $this->db->insert('juego_imagen',$data_juegoimagen);

                      
            for ($i=1; $i <= 18; $i++) { 
            
                //insertar en la tabla memoria_detalles
                $data_memoriadetalles = array('fk_idmemoria' =>$id,
                                'nombre_imgfraccion'=>$arreglo['nombrefraccion'.$i],
                                'distractor1'=>$arreglo['primerdistractor'.$i],
                                'distractor2'=>$arreglo['segundodistractor'.$i]);
                $this->db->insert('memoria_detalles',$data_memoriadetalles);
            }

            return $idjuego;
        }

    }
    public function memoria_datos($idjuego)
    {

        $result = $this->db->query('select * from memoria m, juego j, juego_imagen ji where m.idmemoria = j.fk_idjuegoespecifico and j.tipo="memoria" and ji.fk_idjuego= j.idjuego and j.idjuego = '.$idjuego)->row();
        return $result;
    }
    public function  memoriadetalles_datos($idjuego)
    {
        $result = $this->db->query('select md.* from memoria_detalles md, memoria m, juego j where md.fk_idmemoria = m.idmemoria and j.fk_idjuegoespecifico = m.idmemoria and j.tipo="memoria" and j.idjuego = '.$idjuego)->result();
        return $result;
    }
    public function memoria_datos_byidmemoria($idmemoria)
    {

        $result = $this->db->query('select * from memoria m, juego j, juego_imagen ji where m.idmemoria = j.fk_idjuegoespecifico and j.tipo="memoria" and ji.fk_idjuego= j.idjuego and m.idmemoria = '.$idmemoria)->row();
        return $result;
    }
    public function  memoriadetalles_datos_byidmemoria($idmemoria)
    {
        $result = $this->db->query('select md.* from memoria_detalles md, memoria m, juego j where md.fk_idmemoria = m.idmemoria and j.fk_idjuegoespecifico = m.idmemoria and j.tipo="memoria" and m.idmemoria = '.$idmemoria)->result();
        return $result;
    }
    //Juego invasores
    public function Guardar_invasores($datos) {
        if ($datos['idinvasores'] >= 0) { //edicion
            # code...
            return $datos['idinvasores'];
        }
        else
        {            
            $nombre = $datos['nombre'];
            $descripcion = $datos['descripcion'];
            $edad_maxima = $datos['edad_maxima'];
            $edad_minima = $datos['edad_minima'];
            $cadena = $datos['detallesinvasores'];
            $cadenafig = $datos['detallesinvasores_fig'];
            $cadenacomprension = $datos['detallesinvasores_texto'];
            $textoprincipal = $datos['invasores_textoprincipal'];
            //$imagen = $datos['imagen'];
            $curso = $datos['curso'];
            unset($datos['nombre']);
            unset($datos['descripcion']);
            unset($datos['idinvasores']);
            unset($datos['edad_minima']);
            unset($datos['edad_maxima']);
            unset($datos['detallesinvasores']);
            unset($datos['detallesinvasores_fig']);
            unset($datos['detallesinvasores_texto']);
            unset($datos['invasores_textoprincipal']);
            //unset($datos['imagen']);
            unset($datos['curso']);
            // $datos['tiempo']=60;

            $arreglotexto=json_decode($textoprincipal, true);
            $datos['texto_comprension']=$arreglotexto['texto_comprension'];

            //insertar en la tabla invasores
            $this->db->insert('invasores',$datos);
            $id = $this->db->insert_id();

            
            //$fecha = date();
            // insertar en tabla juegos
            $data_juego = array('nombre'=>$nombre,
                                'descripcion' =>$descripcion,
                                'curso_dirigido' =>$curso,
                                'tipo'=>'invasores',
                                //'fecha_creacion'=>$fecha;
                                'edad_minima'=>$edad_minima,
                                'edad_maxima'=>$edad_maxima,
                                'fk_idpersona'=>$this->session->userdata('id'),
                                'fk_idjuegoespecifico'=>$id);                            
            $this->db->insert('juego',$data_juego);
            $idjuego = $this->db->insert_id();
                    

            $arreglo=json_decode($cadena, true);
            $cont = count($arreglo)/3; //si sirve

            $arreglofiguras = json_decode($cadenafig,true);
            $contfig = count($arreglofiguras)/2;
            //echo "contador : ".$cont ." jjj";

            $arreglocomprension = json_decode($cadenacomprension,true);
            $contcomprension = count($arreglocomprension)/4;

            for ($i=1; $i <= $cont; $i++) { 
                //insertar en la tabla invasores_detalles
                $data_invasoresdetalles = array('fk_idinvasores' =>$id,
                                'tipo'=> 'dinero',
                                'soles'=>$arreglo['montosoles'.$i],
                                'centimos'=>$arreglo['montocentimos'.$i],
                                'enunciado'=>$arreglo['enunciado'.$i]);
                $this->db->insert('invasores_detalles',$data_invasoresdetalles);
            }
            for ($i=1; $i <= $contfig; $i++) { 
            
                //insertar en la tabla invasores_detalles (figuras)
                $data_invasoresdetallesfig = array('fk_idinvasores' =>$id,
                                'tipo'=> 'figuras',
                                'tipofigura'=>$arreglofiguras['tipofigura'.$i],
                                'enunciadofig'=>$arreglofiguras['enunciadofig'.$i]);
                $this->db->insert('invasores_detalles',$data_invasoresdetallesfig);
            }
            for ($i=1; $i <= $contcomprension; $i++) { 
            
                //insertar en la tabla invasores_detalles (comprension lectora)
                $data_invasoresdetallescomp = array('fk_idinvasores' =>$id,
                                'tipo'=> 'comprension',
                                'pregunta'=>$arreglocomprension['pregunta'.$i],
                                'respuesta'=>$arreglocomprension['respuesta'.$i],
                                'primerDistractor'=>$arreglocomprension['primerDistractor'.$i],
                                'segundoDistractor'=>$arreglocomprension['segundoDistractor'.$i]);
                $this->db->insert('invasores_detalles',$data_invasoresdetallescomp);
            }

            return $idjuego;
        }

    }
    public function invasores_datos($idjuego)
    {
        $result = $this->db->query('select * from invasores s, juego j where s.idinvasores = j.fk_idjuegoespecifico and j.tipo="invasores" and j.idjuego ='.$idjuego)->row();
        
        return $result;
    }
    public function  invasoresdetalles_datos($idjuego)
    {
        $result = $this->db->query('select vd.* from invasores_detalles vd, invasores i, juego j where vd.fk_idinvasores = i.idinvasores and j.fk_idjuegoespecifico = i.idinvasores and j.tipo="invasores" and j.idjuego ='.$idjuego)->result();
        return $result;
    }
    public function Guardar_invasorescomprension($datos) {
        if ($datos['idinvasores'] >= 0) { //edicion
            # code...
            $resultid = $this->db->query('select fk_idjuegoespecifico from juego where idjuego = '.$datos['idinvasores'])->row();
            
            $invasoresid = $resultid->fk_idjuegoespecifico;
            //Modificar la tabla invasores

            $textoprincipal = $datos['invasores_textoprincipal'];
            $arreglotexto=json_decode($textoprincipal, true);
            $datos['invasores_textoprincipal']=$arreglotexto;

            $datainvasores = array('tiempo' => $datos['tiempo'],
                                'puntaje' =>$datos['puntaje'],
                                'texto_comprension'=>$datos['invasores_textoprincipal']);
            $this->db->where('idinvasores',$invasoresid);
            $this->db->update('invasores',$datainvasores);

            //modificar juego
            $datajuego = array('nombre' => $datos['nombre'],
                                'descripcion' =>$datos['descripcion'],
                                'curso_dirigido' =>$datos['curso'],
                                'edad_minima' =>$datos['edad_minima'],
                                'edad_maxima' =>$datos['edad_maxima']);
            $this->db->where('idjuego',$datos['idinvasores']);
            $this->db->update('juego',$datajuego);


            $this->db->delete('invasores_detalles', array('fk_idinvasores' => $invasoresid));
            //$this->db->delete('juego_imagen', array('fk_idjuego' => $datos['idinvasores'])); 

            $arreglocomprension=json_decode($datos['detallesinvasores_texto'], true);
            $contador = count($arreglocomprension) / 4; //si sirve

            for ($i=1; $i <= $contador; $i++) { 
                //insertar en la tabla invasores_detalles (comprension lectora)
                $data_invasoresdetallescomp = array('fk_idinvasores' =>$invasoresid,
                                'tipo'=> 'comprension',
                                'pregunta'=>$arreglocomprension['pregunta'.$i],
                                'respuesta'=>$arreglocomprension['respuesta'.$i],
                                'primerDistractor'=>$arreglocomprension['primerDistractor'.$i],
                                'segundoDistractor'=>$arreglocomprension['segundoDistractor'.$i]);
                $this->db->insert('invasores_detalles',$data_invasoresdetallescomp);
            }
            //---------------
            return $datos['idinvasores'];

        }
        else
        {            
            $nombre = $datos['nombre'];
            $descripcion = $datos['descripcion'];
            $edad_maxima = $datos['edad_maxima'];
            $edad_minima = $datos['edad_minima'];
            $cadenacomprension = $datos['detallesinvasores_texto'];
            $textoprincipal = $datos['invasores_textoprincipal'];
            $curso = $datos['curso'];
            unset($datos['nombre']);
            unset($datos['descripcion']);
            unset($datos['idinvasores']);
            unset($datos['edad_minima']);
            unset($datos['edad_maxima']);
            unset($datos['detallesinvasores_texto']);
            unset($datos['invasores_textoprincipal']);
            unset($datos['curso']);
            
            $arreglotexto=json_decode($textoprincipal, true);
            $datos['texto_comprension']=$arreglotexto;

            //$arreglotexto=json_decode($textoprincipal, true);
            //$datos['texto_comprension']=$arreglotexto['texto_comprension'];

            //insertar en la tabla invasores
            $this->db->insert('invasores',$datos);
            $id = $this->db->insert_id();

            
            //$fecha = date();
            // insertar en tabla juegos
            $data_juego = array('nombre'=>$nombre,
                                'descripcion' =>$descripcion,
                                'curso_dirigido' =>$curso,
                                'tipo'=>'invasores',
                                //'fecha_creacion'=>$fecha;
                                'edad_minima'=>$edad_minima,
                                'edad_maxima'=>$edad_maxima,
                                'fk_idpersona'=>$this->session->userdata('id'),
                                'fk_idjuegoespecifico'=>$id);                            
            $this->db->insert('juego',$data_juego);
            $idjuego = $this->db->insert_id();

            $arreglocomprension = json_decode($cadenacomprension,true);
            $contcomprension = count($arreglocomprension)/4;
            for ($i=1; $i <= $contcomprension; $i++) { 
            
                //insertar en la tabla invasores_detalles (comprension lectora)
                $data_invasoresdetallescomp = array('fk_idinvasores' =>$id,
                                'tipo'=> 'comprension',
                                'pregunta'=>$arreglocomprension['pregunta'.$i],
                                'respuesta'=>$arreglocomprension['respuesta'.$i],
                                'primerDistractor'=>$arreglocomprension['primerDistractor'.$i],
                                'segundoDistractor'=>$arreglocomprension['segundoDistractor'.$i]);
                $this->db->insert('invasores_detalles',$data_invasoresdetallescomp);
            }

            return $idjuego;
        }

    }
    public function invasores_datos_byidinvasores($idinvasores)
    {
        $result = $this->db->query('select * from invasores s, juego j where s.idinvasores = j.fk_idjuegoespecifico and j.tipo="invasores" and s.idinvasores ='.$idinvasores)->row();
        
        return $result;
    }
    public function  invasoresdetalles_datos_byidinvasores($idinvasores)
    {
        $result = $this->db->query('select vd.* from invasores_detalles vd, invasores i, juego j where vd.fk_idinvasores = i.idinvasores and j.fk_idjuegoespecifico = i.idinvasores and j.tipo="invasores" and i.idinvasores ='.$idinvasores)->result();
        return $result;
    }
    //Metodos para el juego recolector
    public function Guardar_recolector($datos) {
        //var_dump($datos);
        
        if ($datos['idrecolector'] >= 0) { //edicion
            # code...
            $resultid = $this->db->query('select fk_idjuegoespecifico from juego where idjuego = '.$datos['idrecolector'])->row();
            
            $recolectorid = $resultid->fk_idjuegoespecifico;
            //Modificar la tabla recolector

            $textoprincipal = $datos['texto_principal'];
            $arreglotexto=json_decode($textoprincipal, true);
            $datos['texto_principal']=$arreglotexto;

            $datarecolector = array('tiempo' => $datos['tiempo'],
                                'puntaje' =>$datos['puntaje'],
                                'texto_principal'=>$datos['texto_principal']);
            $this->db->where('idrecolector',$recolectorid);
            $this->db->update('recolector',$datarecolector);

            //modificar juego
            $datajuego = array('nombre' => $datos['nombre'],
                                'descripcion' =>$datos['descripcion'],
                                'curso_dirigido' =>$datos['curso'],
                                'edad_minima' =>$datos['edad_minima'],
                                'edad_maxima' =>$datos['edad_maxima']);
            $this->db->where('idjuego',$datos['idrecolector']);
            $this->db->update('juego',$datajuego);


            $this->db->delete('recolector_detalles', array('fk_idrecolector' => $recolectorid));
            $this->db->delete('juego_imagen', array('fk_idjuego' => $datos['idrecolector'])); 

            $arreglo=json_decode($datos['detallesrecolector'], true);
            $contador = count($arreglo) / 3; //si sirve

            for ($i=1; $i <= $contador; $i++) { 
                $data_juegoimagen = array('ruta' =>$arreglo['imagen'.$i],
                                'fk_idjuego'=>$datos['idrecolector'],
                                'observaciones'=>$arreglo['pregunta'.$i]);
                $this->db->insert('juego_imagen',$data_juegoimagen);

                //insertar en la tabla recolector_detalles
                $data_recolectordetalles = array('fk_idrecolector' =>$recolectorid,
                                'pregunta'=>$arreglo['pregunta'.$i],
                                'respuesta'=>$arreglo['respuesta'.$i]);
                $this->db->insert('recolector_detalles',$data_recolectordetalles);
            }

            return $datos['idrecolector'];
        }
        else
        {
                      
            $nombre = $datos['nombre'];
            $descripcion = $datos['descripcion'];
            $edad_maxima = $datos['edad_maxima'];
            $edad_minima = $datos['edad_minima'];
            $cadena = $datos['detallesrecolector'];
            $textoprincipal = $datos['texto_principal'];
            $curso = $datos['curso'];
            unset($datos['nombre']);
            unset($datos['descripcion']);
            unset($datos['idrecolector']);
            unset($datos['edad_minima']);
            unset($datos['edad_maxima']);
            unset($datos['detallesrecolector']);
            unset($datos['texto_principal']);
            unset($datos['curso']);

            $arreglotexto=json_decode($textoprincipal, true);
            $datos['texto_principal']=$arreglotexto;

            //insertar en la tabla recolector
            $this->db->insert('recolector',$datos);
            $id = $this->db->insert_id();
            
            $arreglo=json_decode($cadena, true);

            // insertar en tabla juegos
            $data_juego = array('descripcion' =>$descripcion,
                                'nombre'=>$nombre,
                                'curso_dirigido' =>$curso,
                                'tipo'=>'recolector',
                                'edad_minima'=>$edad_minima,
                                'edad_maxima'=>$edad_maxima,
                                'fk_idpersona'=>$this->session->userdata('id'),
                                'fk_idjuegoespecifico'=>$id);
            $this->db->insert('juego',$data_juego);
            $idjuego = $this->db->insert_id();
            
            $contador = count($arreglo) / 3; //si sirve
            //echo "contador : ".$cont ;

            // insertar en la tabla juego_imagen
            for ($i=1; $i <= $contador; $i++) { 
                $data_juegoimagen = array('ruta' =>$arreglo['imagen'.$i],
                                'fk_idjuego'=>$idjuego,
                                'observaciones'=>$arreglo['pregunta'.$i]);
                $this->db->insert('juego_imagen',$data_juegoimagen);

                //insertar en la tabla recolector_detalles
                $data_recolectordetalles = array('fk_idrecolector' =>$id,
                                'pregunta'=>$arreglo['pregunta'.$i],
                                'respuesta'=>$arreglo['respuesta'.$i]);
                $this->db->insert('recolector_detalles',$data_recolectordetalles);
            }

            return $idjuego;
        }

    }
    public function recolector_datos($idjuego)
    {
        $result = $this->db->query('select * from recolector s, juego j where s.idrecolector = j.fk_idjuegoespecifico and j.tipo="recolector" and j.idjuego ='.$idjuego)->row();
       
        return $result;
    }
    public function  recolectordetalles_datos($idjuego)
    {

        $result = $this->db->query('select rd.*, ji.ruta from recolector_detalles rd, recolector i, juego j, juego_imagen ji where rd.fk_idrecolector = i.idrecolector and j.fk_idjuegoespecifico = i.idrecolector and j.tipo="recolector" and ji.fk_idjuego = j.idjuego and ji.observaciones=rd.pregunta and j.idjuego ='.$idjuego)->result();
        return $result;
    }
    public function recolector_datos_byidrecolector($idrecolector)
    {
        $result = $this->db->query('select * from recolector s, juego j where s.idrecolector = j.fk_idjuegoespecifico and j.tipo="recolector" and s.idrecolector ='.$idrecolector)->row();
       
        return $result;
    }
    public function  recolectordetalles_datos_byidrecolector($idrecolector)
    {

        $result = $this->db->query('select rd.*, ji.ruta from recolector_detalles rd, recolector i, juego j, juego_imagen ji where rd.fk_idrecolector = i.idrecolector and j.fk_idjuegoespecifico = i.idrecolector and j.tipo="recolector" and ji.fk_idjuego = j.idjuego and ji.observaciones=rd.pregunta and i.idrecolector ='.$idrecolector)->result();
        return $result;
    }
    public function get_sesion($idsesion)
    {
        $sesion = $this->db->query('select * 
                                    from sesion 
                                    where idsesion = '.$idsesion)->result()[0];
        $juegos = $this->db->query('select idjuego,nombre,descripcion,tipo,estado,fk_idjuegoespecifico,ruta_imagen,t.orden 
                                    from (select * 
                                         from juego j inner join 
                                            (select fk_idjuego,orden 
                                             from juego_sesion 
                                             where fk_idsesion = '.$idsesion.')js 
                                         on js.fk_idjuego = j.idjuego)t')->result();


        $Sesion = array('sesion' =>$sesion,
                        'juegos'=>$juegos );
        return $Sesion;
    }
    public function resultados_juegos_sesion($idsesion)
    {

        $ajuegoespecifico =array();
        $juegos = $this->db->query('select * from (select j.idjuego, j.nombre, j.tipo, j.estado,j.fk_idjuegoespecifico, js.idjuego_sesion,pa.idpartidat, pa.duracion, pa.puntaje 
                                         from juego j inner join 
                                            (select idjuego_sesion,fk_idjuego,orden  
                                             from juego_sesion
                                             where fk_idsesion ='.$idsesion.')js 
                                             on js.fk_idjuego = j.idjuego inner join 
                                            (select idpartidat, a.idjuego_sesion, duracion, puntaje from 
(select idjuego_sesion, max(idpartida) as idpartidat from partida where stsreg = "REG" 
group by idjuego_sesion)a inner join partida p on a.idpartidat = p.idpartida)pa 
                                             on pa.idjuego_sesion = js.idjuego_sesion )t')->result();

        foreach ($juegos as $juego ) {
                        $juegoespecifico = $this->db->query('select * from '.$juego->tipo.' where id'.$juego->tipo.'='.$juego->fk_idjuegoespecifico)->row();
                        
                        array_push($ajuegoespecifico, $juegoespecifico->puntaje);
                     }

         $asesion = array('juegos' => $juegos,
                         'juegoespecifico'=>$ajuegoespecifico
                        );


        return $asesion;
    }
    public function Guardar_resultados_sesion($idsesion)
    {
       $this->db->query('update partida set stsreg = "FIN" where idpartida in (select idpartidat from (select j.idjuego, j.nombre, j.tipo, j.estado,j.fk_idjuegoespecifico, js.idjuego_sesion,pa.idpartidat, pa.duracion, pa.puntaje 
                                         from juego j inner join 
                                            (select idjuego_sesion,fk_idjuego,orden  
                                             from juego_sesion
                                             where fk_idsesion ='.$idsesion.')js 
                                             on js.fk_idjuego = j.idjuego inner join 
                                            (select idpartidat, a.idjuego_sesion, duracion, puntaje from 
                                            (select idjuego_sesion, max(idpartida) as idpartidat from partida where stsreg = "REG" 
                                            group by idjuego_sesion)a inner join partida p on a.idpartidat = p.idpartida)pa 
                                             on pa.idjuego_sesion = js.idjuego_sesion )t)');
   }
}
?>