<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Mdl_profesor extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'profesor';
        $this->primary_key = 'idprofesor';
    }

    public function default_select() {
        $this->db->select('profesor.*');
    }

    public function default_order_by() {
        if ($this->order_by && $this->order) {
            $this->db->order_by($this->order_by, $this->order);
        }
        else {
            $this->db->order_by($this->primary_key);
        }
    }
    public function validation_rules() {
        return array(
            'descripcion' => array(
                    'field' => 'descripcion',
                    'label' => 'descripcion',
                    'rules' => 'required|trim|xss_clean'
                    )
        );
    }
    public function lista_total()
    {
       $result = $this->get()->result();
       return $result;
    }
   
   
}
?>