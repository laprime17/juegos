<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<title>Pukllay: Aprende Jugando</title>
<!--  -->
<!-- estilos -->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo public_url();?>css/estilo_pukllay.css">

<!-- js´s -->
<script type="text/javascript" src="<?php echo public_url();?>pukllay/js/jquery1-11-2.min.js"></script>
</head>
<body>
	<div id="boton_login">
		<div class="raya"></div>
		<div class="raya"></div>
		<div class="raya"></div>
	</div>
	<section class="section_login oculto_arriba">
			<h1>PUK<b>LLAY</b> </h1>
			
<form  method="post" action="<?php echo base_url();?>login/verificar">
			<div class="form_login">
				<div class="control">
					Usuario
					<input type="text" name="usuario" />
				</div>
				<div class="control">
					Password
					<input type="password" name="clave"/>
				</div>
				<input type="submit" class="boton_entrar" value="Entrar"/>

			</div> 
		</form>

	</section>
	<section class="cargar_pagina">
		<div class="cargar_c">
			<div class="texto upper rale color_b">
						Puk <b>llay</b>
			</div>
			<div class="loading"></div>

		</div>
		

	<!-- <span class="pasar">pasar</span> -->
	</section>

	<section class="pagina" id="pagina1">

	<h1>PUK <b>LLAY</b> </h1>
	</section>


	<section class="pagina" id="pagina2">

		<div class="ta_center label_aprende_jugando">

				<div>APRENDE</div>
				<div class="jugando">JUGANDO</div>
		</div>
		<img class="compu" src="<?php echo public_url();?>pukllay/img/compu.png">
		<div id="parrafo1" class="parrafo">Pukllay es una plataforma donde puedes personalizar y jugar actividades educativas.</div>
		<div id="parrafo2" class="parrafo"> Aprender nunca fue tan divertido. Si eres Profesor puedes crear actividades para tus alumnos; y si eres alumno podrás jugar cualquier actividad programada para ti. 
		</div>

	</section>


	<section class="pagina" id="pagina3">

		<div class="ta_center label_aprende_jugando">

				<div>¿EN QUÉ</div>
				<div class="jugando">CONSISTE?</div>
		</div>

		
		<div id="cuadros">
			
				<div class="dib">Si eres Profesor, puedes personalizar un juego, esto es bien fácil, selecciona un videojuego de nuestra lista de actividades y completa los datos con imágenes, palabras, frases, etc.</div>
				<div class="dib">Crea un grupo de jugadores y una sesión de juegos donde incluirás las actividades que personalizaste, Así podrás jugar cuando tengas una sesión programada para ti y tus alumnos.</div>
				<div class="db">Pukllay medirá tu desempeño y el de tus alumnos, así podrás ver quienes necesitan más de tu ayuda y quienes progresan con rapidez.</div>
			
		</div>
		
		<!-- <img src="<?php echo public_url();?>pukllay/img/pizarra.png" class="img_pizarra"> -->
	</section>

	
</body>

</html>
<script type="text/javascript">

$(function(){

// $('.pasar').click(function(){
  subir_load();

// })

$('#boton_login').click(function(){

	console.log("hola");
	$('.section_login').toggleClass('oculto_arriba');
	
	$(this).toggleClass('boton_x');
	var f=$('#boton_login .raya')[0];
	$(f).css('border','none');
	$(f).css('height','10px');
	var l=$('#boton_login .raya')[2];
	$(l).addClass('final');
	// $(l).css('height','10px');


});


});

function subir_load()
{
$('.cargar_c').addClass('top_60');

var delay=100;//1 seconds
    setTimeout(function(){
$('.cargar_pagina').addClass('oculto_arriba');
   
    },delay); 


	
}



</script>
