<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>css/countdown.css">
</head>
<body class="juego">
	<header class="" id="header">
	
		<a href="<?php echo base_url();?>inicio"><img class="blubird" src="<?php echo public_url();?>img/logo_pk.png" alt="pukllay"></a>
		<a  class="juego-volver" href="<?php echo base_url();?>inicio">Volver al menú</a>
		<?php $this->load->view('layouts/navbar');?>

		
	</header>


	<div class="row" data-equalizer>
	
		<div class="columns large-12 contenedor ejecutar_sesion" data-equalizer-watch>
			<div class="ejecutar_sesion-columna">
				<div class="ejecutar_sesion-lista">
					<div class="ejecutar_sesion-lista-reloj">Juegos
					</div>
					<!-- <div class="ejecutar_sesion-lista-reloj">
						<div id="counter">
						  <div id="counter_item1" class="counter_item">
						  <div class="front"></div>
						  <div class="digit digit0"></div>
						  </div>
						  <div id="counter_item2" class="counter_item">
						  <div class="front"></div>
						  <div class="digit digit0"></div>
						  </div>
						  <div id="counter_item3" class="counter_item">
						  <div class="front"></div>
						  <div class="digit digit_colon"></div>
						  </div>
						  <div id="counter_item4" class="counter_item">
						  <div class="front"></div>
						  <div class="digit digit0"></div>
						  </div>
						  <div id="counter_item5" class="counter_item">
						  <div class="front"></div>
						  <div class="digit digit0"></div>
						  </div>
						</div>
					</div> -->
					
					<ul>
						<?php 
						$juegos =$sesion['juegos'];
						$cont =1;
							foreach ($juegos as $juego) {
								echo '<span class="puntaje">'.$cont.'</span><li>'. $juego->nombre.'</li>';
								$cont++;
							}
						 ?>
					</ul>		
				</div>
				
				<div class="ejecutar_sesion-contenedor">
					<iframe src="<?php 	echo base_url();?>juegos/inicio_sesion/<?php echo $sesion['sesion']->idsesion;?> " frameborder="0"></iframe>
					<?php //$this->load->view('inicio_sesion'); ?>
					<?php //$this->load->view('administracion/mapa_previsualizacion'); ?>

				</div>
			</div>
			<div class="ejecutar_sesion-footer">
				<!-- <div class="ejecutar_sesion-chat">
					<div class="titulo"> Preguntar al Profesor</div>
					<textarea name="" id=""></textarea>
					<div class="boton amarillo"> Enviar</div>
				</div> -->
				<div class="ejecutar_sesion-flechas">
					<!-- <span class="icon-arrow-left"></span>
									<span class="icon-arrow-right"></span> -->	
				</div>
			</div>
			
		</div>
		
	</div>


</body>


<?php 
echo '<script>
public_url = "'.public_url().'";
idsesion = "'.$sesion['sesion']->idsesion.'";
</script>'; ?>
<script>
	$(document).foundation();

	$(window).load(function(){
      $("#sticker").sticky({ topSpacing: 0, center:true, className:"hey" });
      
      $("#header").sticky({ topSpacing: 0 });
    });
  
                
$(window).scroll(function(){
console.log($(window).scrollTop());
    
    
    if  ($(window).scrollTop() > 130 && $(window).scrollTop() < 150) {
    	
       $("#sticker").addClass('abierto');
       $("#header").addClass('abierto');

    }


    if  ($(window).scrollTop() < 130 ) {
    	
       $("#sticker").removeClass('abierto');
    }

});
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/jquery.timeout.interval.idle.js"></script>
<script src="<?php echo public_url();?>js/jquery.countdown.counter.js"></script>
<script src="<?php echo public_url();?>js/ejecutar_juego.js"></script>

</html>