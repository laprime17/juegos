<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Inka Hack 2016</title>
	<link rel="stylesheet" href="assets/css/materialize.css">
	<link rel="stylesheet" href="assets/css/inkahack.css">
</head>
<body>
	<section class="portada">
		<img src="assets/img/portada.jpg" alt="" class="portada-back">
		<img src="assets/img/logo.png" alt="" class="portada-logo">
		<div class="portada-redes">
			
		</div>
		<nav>
			<ul>
				<li>EL EVENTO</li>
				<li>TEMAS Y EXPOSITORES</li>
				<li>CONTACTO</li>
				<li>BLOG</li>
			</ul>
		</nav>
		<h1>EL EVENTO CUSQUEÑO SOBRE SEGURIDAD INFORMATICA</h1>
		<div class="portada-foot">
			<h2>¿Estás listo?</h2>
			<div class="boton">Inscribirse ahora o nunca</div>
		</div>

	</section>
	
</body>
</html>
<script src="assets/js/jquery1.12.3.min.js"></script>
<script src="assets/js/materialize.min.js"></script>