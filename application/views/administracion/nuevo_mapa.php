<?php $this->load->view('administracion/layouts/header.php'); ?>

<?php //$this->load->view('layouts/navbar_administradidor');?>

<div class="row">
	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
	<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_mapa row" data-equalizer>
	<div class="lbl2">Nuevo Mapa Interactivo</div>
	
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
		<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/mapa_guardarmapa" method="post"class="form_datos">
			
			<div class="row">
				<div class="columns large-12">
					<div class=""><label for="">Titulo de la Actividad:</label></div>
					<div class=""> <input type="text" name="nombre" id=""/></div>
				</div>
			</div>
			<div class="row">
				<div class="column large-6">
					<div class=""><label for="">Duración(min):</label></div>
					<div class=""><input type="number" name="tiempo" placeholder="minutos"/></div>
				</div>
				<div class="column large-6">
					<div class=""><label for="">Puntaje total</label></div>
					<div class=""><input type ="number" name="puntaje"/></div>
				</div>
			</div>
			<div class="row">
				<div class="column large-6">
					<div class=""><label for="">Materia:</label></div>
					<div class=""><select name="" id="">
						<option value="">Matemáticas</option>
					</select></div>
				</div>
				<div class="column large-6">
					<div class=""><label for="">Rango de edad:</label></div>
					<div class="row">
						<div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima"/></div>
						<div class="columns large-2">-</div>
						<div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima"/></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="columns large-12">
					<div class=""><label for="">Descripción:</label></div>
					<div class=""><textarea name="descripcion" id="" cols="30" rows="s"></textarea></div>
				</div>
			</div>
			
			<input type="hidden" name="imagen" id="mapa_src_imagen">
			<input type="hidden" name="idmapa" id="mapa_id_mapa" value="-1">
		</form>
		
		<label for="">Puntos</label>
		<div class="nuevo_mapa-contenedor_respuestas"></div>
	</div>
	<div class="dib nuevo_juego-vista_previa columns large-8" data-equalizer-watch>
		<!-- <h2 class="nuevo_juego-subtitulo">Visualización</h2>
		<span>Sitúa el puntero para establecer un nuevo punto</span> -->
		<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/subir_archivo" method="post" class="form_imagen">
			<div class="btn_default dib  small file_wcss">
                <div class="content"><span class="icon-images"></span> Imágen principal</div>
                <input type="file" name="userfile"  class="input_file dib" id="userfile">
            </div>
			<!-- <input type="file" name="userfile[]" multiple id="userfile" > -->
			
		</form>
		<div class="mapa-prev_img" id="mapa-contenedorimg"> 
 		</div>
		<section class="nuevo_mapa-botones">
		<div   class="button primary " id="btn_guardar_mapa"> Guardar </div>
		<div   class="button  alert " id="cancelar_todo"> Cancelar </div>
		<a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
		</section>
		
	</div>
	
</div>
	

</div>
</div>
	

<div id="modal_prev" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" src="" frameborder="0"></iframe>
</div>
		




<?php $this->load->view('administracion/layouts/footer.php'); ?>



<script src="<?php echo public_url();?>js/administracion/nuevo_mapa.js"></script>