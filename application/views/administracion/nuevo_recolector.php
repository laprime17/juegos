<?php $this->load->view('administracion/layouts/header.php'); ?>

<div class="row">
	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_memoria row" style= "padding-bottom: 5px;" data-equalizer>
	<div class="lbl2">Personalizar juego "Recolector"</div>
	<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/recolector_guardarrecolector" method="post"class="form_datos">
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text"  placeholder="título" name="nombre" id=""/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo" placeholder="minutos"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje de actividad</label></div>
                    <div class=""><input type ="number" name="puntaje"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="curso" id="curso">
                        <option value="Comunicacion">Comunicación</option>
                        <option value="Matematica">Matemática</option>
                        <option value="CTA">Ciencia y Ambiente</option>
                        <option value="Ciencias sociales">Personal Social</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Edad sugerida:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima"/></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" id="" cols="30" rows="s"></textarea></div>
                </div>
            </div>
		
		<label for="">Explicacion del juego</label>
		<div class="recolector_explicacion">
           <span class="gris">Este juego consiste en recolectar figuras que son respuestas a las preguntas de un texto en cuestion. En este juego un hombrecito (dominado por el usaurio) se mueve por un mapa (teclas de direccion y la tecla de espacio para saltar) para ir recolectando figuras y ponerlos en una caja, así irá reuniendo las respuestas. </span> 
           <label class="gris" for="">Pasos</label>
           <span class="gris"> - Redactar o copiar un texto en el panel del lado derecho de esta ventana. <br/>
                               - Cargar imagenes de su computador que será las respuestas a las preguntas. <br/>
                               - Al seleccionar una imagen escribir una pregunta y escribir la respuesta. <br/> 
                               - Guardar la edición. <br/>
                               - previsualizar la edición. <br/>
                           </span>
           
        </div>

            <input type="hidden" name="idrecolector" id="recolector_id_recolector" value="-1">
            <input type="hidden" name="texto_principal" id="idrecolectortexto" value="-1">
            <input type="hidden" name="detallesrecolector" id="iddetallesrecolector" value="-1">
            
        </form>
	</div>
    
    <div class="dib nuevo_juego-vista_previa columns large-8" >
        <form enctype="multipart/form-data"  class="form_imgs recolector-form_imgs">
        <div class="label db" id="recolector_imagen_titulo">Texto</div>
        <div class="columns large-12" style="margin-bottom: 15px;">
            <!-- <div class=""><label for="">Texto:</label></div> -->
            <div class=""><textarea  name="textoprincipal" id="editor"></textarea></div>
        </div>
        <div class="dib">
            <label class="gris" for="">Elija una colección de imágenes:</label>
            <span class="icon-images gris"></span>  
            <span class="gris">Tamaño recomendable 250X250 cuadrado</span>
        </div>
        <div class="btn_default dib  small file_wcss">
            <div class="content"><span class="icon-images"></span> Imágenes</div>
            <input type="file" name="recolector_img[]" multiple class="input_file dib" id="file_imgs">
        </div>
        
        
        <label class="gris" for="">Seleccione las imágenes</label>
        <span class="gris">Haz click en cada imagen, aparecerá un formulario donde podrás plantear una pregunta y su respectiva respuesta
                            escrita; esta imagen es la respuesta a la pregunta que estas planteando.</span>
        <span class="gris">El juego permite recolectar la respuesta mediante una imagen o en forma de texto escrito.</span>

        <div class="recolector-galeria">
            <section class="recolector-galeria-todos">
                <div class="label db">Imágenes incorrectas</div>
            
            </section>
             <section class="recolector-galeria-correctos">
                <div class="label db">Imágenes Correctas</div>
            </section>
        </div>
        </form>
    </div>
    
    </div>
</div>
    <div id="aviso_guardado"></div>
    <section class="nuevo_recolector-botones">
        <div   class="button  guardar " id="guardardatos"> Guardar </div>
        <a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
        <div   class="button  alert " id="cancelar_todo"> Cancelar </div>
    </section>        
</div>
</div>

<div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
</div>
<script>
    var contando=1;
    localStorage.clear();
</script>
		
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/nuevo_recolector.js"></script> 

