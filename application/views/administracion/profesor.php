<?php $this->load->view('layouts/head_html'); ?>
<?php $this->load->view('layouts/navbar_administrador');?>
<h1>Profesores</h1>

<a href="<?php echo base_url();?>mantenimiento_profesor/form"><div class="btn btn-primary">Nuevo</div></a>
<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
    Opciones
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ordenar</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Agregar</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Reportes</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Exportar</a></li>
  </ul>
</div>
<?php 


foreach ($profesores as $profesor ) {

	echo '<div class="item_persona">
	<figure><img class="foto" src="'.$this->config->item('public_url').'uploads/fotos/'.$profesor->idprofesor.'.jpg"/></figure> <div class="descripcion">'.$profesor->nombres.'<br>'.$profesor->apellidos.'</br>'.$profesor->usuario.'</br>';
	echo '</div></div>';

    

	
}

 ?>
