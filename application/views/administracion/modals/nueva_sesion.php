<?php $this->load->view('administracion/layouts/header_modal.php'); ?>

<div class="lbl">Grupo: <?php echo $grupo->nombre;?></div>
<div class="db"><div class="label">Nueva Sesión</div></div>
<input type="hidden" id="idgrupo" value="<?php echo $grupo->idgrupo;?>">
<div class="subpanel2 w100">
	<form action="<?php echo base_url();?>administracion/guardar_sesion" enctype="multipart/form-data" method="post" class="">
		<div class="row">
			<div class="columns large-12"><label for="nombres">Nombre de sesión</label></div>
				<div class="columns large-12"><input type="text" id="nombre" placeholder="El Reino Vegetal" id="nombre"></div></div>
		<div class="row ">
			<div class="columns large-6">
				<label for="nombres">Fecha de Creación</label>
				<input type="text" readonly name="fecha_creacion" placeholder="2do de primaria sección B " id="fecha_creacion" value="<?php echo hoy_d_m_a(); ?> ">
			</div>
			<div class="columns large-6">
				<label for="nombres">Fecha programada</label>
				<input type="date" name="fecha_programada" placeholder="2do de primaria sección B " id="fecha_programada">
			</div>
		</div>

		<div class="row"><div class="columns large-12"><div class="label success">Actividades incluidas</div></div>
				<div class="columns large-12"><table class="tabla_small w100">
							<tbody class="tabla_incluidos">
								
							</tbody>
						</table>
			</div>
		</div>
		<div class="row">
			<div class="columns large-12"><div class="label success">Agregar Actividades </div></div>
			<div class="columns large-12"><input type="text" placeholder="Buscar..."> </div>
				<div class="columns large-12"><table class="tabla_small w100">
							<thead>
								<tr>
									<th style="width:70%;">Nombre</th>
									<th>Creación</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody>	

								<?php foreach ($juegos as $juego) {
									?>
									<tr>
										<td><?php echo $juego->nombre;?></td>
										<td><?php echo $juego->fecha_creacion;?> </td>
										<td><div class="btn_default incluir_actividad" data-idact="<?php echo $juego->idjuego;?>"><span  class="icon-plus"></span> Incluir</div> </td>
									</tr>
									<?php 
				
								} ?>
							</tbody>
						</table></div></div>		
	


		<input type="hidden" name="idpersona_creador" value="<?php echo $this->session->userdata('id');?>">
		<div class="ta-right">
			<div class="button  guardar_sesion" >Guardar</div>
			<div class="button  alert close-button" aria-label="Close modal" data-close >Regresar</div>
		</div>
		



</form>
</div>
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script>
	$(document).ready(function(){
		orden = 1;
		Lista_actividades =  new Array();
		$('.guardar_sesion').click(guardar_sesion);
		$('.incluir_actividad').click(function(){
			idjuego = $(this).data('idact');
			Incluir_juego(idjuego);

		});

	});
	function  Incluir_juego(idjuego)
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/detalle_juego_ajax/'+idjuego,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		       	console.log(data);
		           if (data) 
		           	{
		           		var cadena = '<tr><td>'+ orden++ +'</td><td>'+data.juego.nombre+'</td></tr>';
		           		$('.tabla_incluidos').append(cadena);
		           		Lista_actividades.push(data.juego.idjuego);
		           		Obtener_juegos();
		           	};
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax	
	}
	function guardar_sesion()
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/guardar_sesion/'+idjuego,
		       data: "nombre="+$('#nombre').val()+"&idgrupo="+$('#idgrupo').val()+"&juegos="+ JSON.stringify(Lista_actividades)+"&nombre"+$('#nombre').val()+"&fec="+$('#fecha_creacion').val(),
		       dataType:"json",
		      
		       success: function(data)
		       {
		       	
		           if (data) 
		           	{
		           		$('.guardar_sesion').html('<span class="icon-pencil2"></span> Editar');
		           		$('.guardar_sesion').addClass('success');
		           	};
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax
	}
	function Obtener_juegos()
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/nueva_sesion_ajax/'+idjuego,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		       	
		           if (data) 
		           	{
		           		var cadena = '';
		           		$.each(data,function(id,juego){
		           			if (!Lista_actividades.contains(juego.idjuego) ) {

		           				cadena +='<tr><td>'+juego.nombre+'</td><td>'+juego.fecha_creacion+'</td><td><div class="btn_default incluir_actividad" data-idact="'+juego.idjuego+'"><span  class="icon-plus"></span> Incluir</div> </td></tr>';
							}
		           		});
		           	};
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax	
	}
	Array.prototype.contains = function(element) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == element) {
            return true;
        }
    }
    return false;
}
</script>