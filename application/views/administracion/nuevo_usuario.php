<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>
			<?php
			if ($this->session->userdata('rol') ==null) {
			echo "Error. Por favor debe 
			<a href='".public_url()."'>iniciar sesión.</a>";
			die;
		}
			$side = 'administracion/layouts/side_'.$this->session->userdata('rol').'.php';

			$this->load->view($side); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<div class="adm-contenedor_alertas">
			  	<span class="success label dis_none alert_correcto">Usuario guardado correctamente.</span>
			    <span class="alert label dis_none alert_incorrecto">Error al guardar usuario. Por favor comuníquese con el administrador.</span>
			  </div>
				<h5>Nuevo Usuario</h5>
				<?php $this->load->view('administracion/paneles/form_nuevo_usuario');?>	
			</div>
		</div>

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/general.js"></script>
<script src="<?php echo public_url();?>js/administracion/inicio.js"></script>



