<?php $this->load->view('administracion/layouts/header.php'); ?>

<?php //$this->load->view('layouts/navbar_administradidor');?>

<div class="row">
	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
	<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_mapa row" >
	<div class="lbl2">Nueva xxxxx</div>
	
	<div class="dib nuevo_juego-form columns large-4" >
		<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/mapa_guardarmapa" method="post"class="form_datos">
			
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text" name="nombre" id="titulo" class="requerido"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo_min" id="tiempo" class="requerido" placeholder="minutos"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje total</label></div>
                    <div class=""><input type ="number" name="puntaje" id="puntaje" class="requerido"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="" id="">
                        <option value="">Matemáticas</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Rango de edad:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima"/></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" id="" class="requerido" cols="30" rows="s"></textarea></div>
                </div>
            </div>
		</form>
		
		
	</div>
	<div class="dib nuevo_juego-vista_previa columns large-8" >
		

		<div class="abeja-prev_img" id="abeja-contenedorimg">
            
         <form enctype="multipart/form-data"  class="form_imgs abeja-form_imgs">
            
            <div class="dib">
                <label class="gris" for="">Elija una colección de imágenes:</label><span class="icon-images gris"></span>  <span class="gris">Tamaño recomendable 250X250 cuadrado</span>
            </div>
            <div class="btn_default dib  small file_wcss">
                <div class="content"><span class="icon-images"></span> Imágenes</div>
                <input type="file" name="abeja_img[]" multiple class="input_file dib" id="file_imgs">
            </div>
            <input type="hidden" name="premisa" id="premisa">
        </form>
            
            <label class="gris" for="">Premisa</label>
            <input type="text" class="requerido" id="premisa_img" placeholder="¿Que verduras son tubérculos?">

            <label class="gris" for="">Seleccione las imágenes correctas</label>
            <span class="gris">Haz click en cada imagen para seleccionarla y nombrarla</span>

            <div class="abeja-galeria">
                <section class="abeja-galeria-todos">
                    <div class="label db">Imágenes incorrectas</div>
                
                </section>
                 <section class="abeja-galeria-correctos">
                    <div class="label db">Imágenes Correctas</div>

                
                </section>
            </div>
        </div>

		</div>

		<section class="nuevo_mapa-botones">
        <div   class="button" id="guardar"> Guardar </div>
		<div   class="button  alert " id="cancelar_todo"> Cancelar </div>
		<div  href="#"   class="button success" id="btn_previsualizar" > Previsualizar </div>
		</section>
		
	</div>
	
</div>
	

</div>
</div>
<!-- modal de previsualización -->
	
<a href="#" id="lanzar_modal_prev" data-reveal-id="modal_prev"></a>
<div id="modal_prev" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" src="" frameborder="0"></iframe>
</div>
<!-- fin modal de previsualización -->
		
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/nueva_abeja.js "></script>

