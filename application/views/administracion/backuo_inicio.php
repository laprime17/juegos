<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>

			<?php
			if ($this->session->userdata('rol') ==null) {
			echo "Error. Por favor debe 
			<a href='".public_url()."'>iniciar sesión.</a>";
			die;
		}
			$side = 'administracion/layouts/side_'.$this->session->userdata('rol').'.php';

			$this->load->view($side); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				
				
					<div class="subpanel1"> 	
						
							<h4>Gestión de Usuarios</h4>
								<a  href="<?php echo base_url();?>administrador" class="btn_default small" id="btn_nuevogrupo"><span class="icon-plus"></span> Nuevo usuario</a>
								<a  href="<?php echo base_url();?>administrador" class="btn_default small" id="btn_nuevogrupo"><span class="icon-search"></span> Búsqueda</a>
							<ul class="m0">
								
								<li class="subpanel1-item" data-function="get_usuarios" data-parameter = "3">Lista de Alumnos</li>
								<li data-function="get_usuarios" data-parameter = "2" class="subpanel1-item">Lista de Profesores</li>
								<li data-function="get_usuarios" data-parameter = "1" class="subpanel1-item">Lista de Administradores</li>
								<li><a href="<?php echo base_url();?>administrador/gruposysesiones">Grupos</a></li>
							</ul>
							<div class="separador">
							<div class="flecha"></div>
						</div>
					
					</div>
					
					<div class="subpanel2">
						<div class="panel_desplazar">
							<div class="paneln actual">
								<!-- panel por defecto->panel  formulario de nuevo usuario -->
								<h5>Nuevo Usuario</h5>
								<?php $this->load->view('administracion/paneles/form_nuevo_usuario');?>
							</div>
							<div class="paneln nuevo">

							</div>
						</div>
						
					</div>
		

			</div>
		</div>
<!-- <p><a data-open="modal_resetearclave">Click me for a modal</a></p> -->

<!-- <div class="reveal" id="modal_resetearclave" data-reveal>
  <h4 class="nombre"></h4>
  <div class="row">
  	<div class="columns large-6">Nueva Clave</div>
  	<div class="columns large-6"><input type="password" placeholder="Nueva Clave"> </div>
  </div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
 -->

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/inicio.js"></script>



