<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>

</head>
<body class="administrador">
	
		