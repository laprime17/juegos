<div class="columns large-3 administrador-side" data-equalizer-watch>
				<div class="sticky">
				<div class="administrador-side-datosusuario">
					
					<div class="cont-foto" style="background:url(<?php echo public_url().'fotos/'.$uusuario['foto_ruta'];?>">
						
					</div>
					<div class="desc-foto">
						<div class="nombre"><?php echo $uusuario['nombre_completo'];?> </div>
						<i><?php echo  $uusuario['rol']; ?> </i>
					</div>
				</div>
			
				<div class="administrador-side-item" id="usuarios">
					<div class="sombrear">
					<div class="icono"><span class="icon-users"></span></div>
					<a href="<?php echo base_url();?>administrador"><div class="texto"> Gestión de usuarios</div></div></a>
				<div class="submenu abierto">
					<div class="submenu-item"><a href="<?php echo base_url();?>administrador/nuevo_usuario "><span class="icon-plus fz9"></span> Agregar usuario</a></div>
					<div class="submenu-item"><a href="<?php echo base_url();?>administrador">Listar</a></div>
					
					<div class="submenu-item"><a href="<?php echo base_url();?>administrador">Instituciones<a></div>
				</div>
				</div>
				<div class="administrador-side-item">
					<div class="sombrear">
					<div class="icono"><span class="icon-images"></span></div>
					<a href="<?php echo base_url();?>administrador/gruposysesiones ">
						<div class="icono"><span class="icon-sitemap"></span></div><div class="texto"> Grupos y Sesiones</div></a>
					</div>
				</div>
				<div class="administrador-side-item">
					<div class="sombrear">
					<div class="icono"><span class="icon-smile"></span></div>
					<a href="<?php echo base_url();?>administrador/juegos"><div class="icono"><span class="icon-home"></span></div><div class="texto"> Juegos predefinidos</div></a>
					</div>
				</div>
				
				<div class="administrador-side-item">
					<div class="sombrear">
					<div class="icono"><span class="icon-pie-chart"></span></div><div class="texto"> Reportes</div></div>
					</div>
				</div>

			</div>