<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>js/timer.jquery.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>

	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	<script src="<?php echo public_url();?>ckeditor/ckeditor.js"></script>
	<script src="<?php echo public_url();?>ckeditor/js/sample.js"></script>
	<link rel="stylesheet" href="<?php echo public_url();?>ckeditor/css/samples.css">
	<link rel="stylesheet" href="<?php echo public_url();?>ckeditor/toolbarconfigurator/lib/codemirror/neo.css">

</head>
<body class="administrador">
	
		<header class="row">
			<figure class="columns large-3 administrador-logo">
				<a href="<?php echo base_url();?>inicio"><img  src="<?php echo  public_url();?>img/logo_pk_inv2.png" alt="Administracion"></a>
			</figure>
			<div class="administrador-menuprincipal columns large-9">
				<a href="<?php echo base_url();?>administrador">Principal</a>
				<a href="<?php echo base_url();?>administrador/juegos">Juegos</a>
				<a href="<?php echo base_url();?>administrador/gruposysesiones">Grupos y sesiones</a>
				<a href="#">Reportes</a>
				<a href="<?php echo base_url()?>login/cerrar_sesion">Cerrar Sesión</a>
			</div>
		</header>