

<div class="prev">

	<div class="prev-content">
		<div class="prev-content-titulo row">
			<div class="mt03m prev-content-titulo-titulo columns large-5">Partes de la Planta</div >
			<div class="mt03m prev-content-titulo-otros columns large-2">0 intentos</div>
			<div class="mt03m prev-content-titulo-otros columns large-2" id="tiempo_restante">06:25</div>
			<div class="prev-content-titulo-otros columns large-3 btn_siguiente_juego"><div class="boton amarillo dib"><span class="icon-arrow-right"></span> Siguiente</div></div>
		</div>
		<div class="prev-content-datos ">
			<div class="dib prev-content-datos-enunciado">Haz click en la hoja</div>
			
			<div class="prev-content-datos-puntos"><div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> </div>
			
		</div>
		<figure class="prev-img">
		<?php echo $imagen;?>
		<div class="mapa_prev_final"></div>
		</figure>
		<div class="prev-footer">
			
				<span class="icon-arrow-left"></span>
				<span class="icon-arrow-right"></span>
			
			
		</div>	
		

	</div>
	
</div>
<?php echo '<script> Puntos = '.json_encode($puntos).';
					pu = '.$puntaje_unitario.';
					tiempo_total = '.$mapa->tiempo.';</script>'; ?>

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script>
	
	$(document).on('ready',ini);
	function ini()
	{
		
		punto_actual = 0;
		boton_actual = null;
		puntaje = 0;

		siguiente_pregunta();
		dibujar_puntos();
		updateReloj();
	}
	function dibujar_puntos()
	{
		for (var i = Puntos.length - 1; i >= 0; i--) {

			py_ = parseInt(Puntos[i].py);
			px_ = parseInt(Puntos[i].px);
			px_-=36;
			py_-=36;

			boton_actual = document.createElement('div');
			$(boton_actual).addClass("prev-boton");
			$(boton_actual).attr("data-nrodepunto",i);
			$(boton_actual).css('position','absolute');
			$(boton_actual).css('top',py_+'px');
			$(boton_actual).css('left',px_+'px');
			$(boton_actual).attr('onclick','Procesar_respuesta(this)');
			$('.prev-img').append(boton_actual);
		};
		
	}
	function Procesar_respuesta(boton)
	{
		punto_elegido = $(boton).data('nrodepunto');
		if (punto_elegido==punto_actual) {
			// es correcto
			console.log('correcto');
			// transformar boton
			$(boton).addClass('correcto');
			$(boton).html('<img src="'+public_url+'img/mapa_correcto.png"/>');
			// aumentar puntaje

			puntaje+=pu;
			
			$('#puntaje_acumulado').html(Math.round(puntaje));
			// continuar
			punto_actual++;
			siguiente_pregunta('Correcto');
		}
		else
		{
			console.log('No es correcto');
		}
	}
	function siguiente_pregunta()
	{
		if (punto_actual<Puntos.length) 
		{
			var texto = 'Haz click en '+Puntos[punto_actual].descripcion;
			$('.prev-content-datos-enunciado').html(texto);
		}else
		{
			$('.mapa_prev_final').addClass('show');
			$('.mapa_prev_final').html('<div class="content">Felicidades</div>');
		}
	}
	 function updateReloj()
    {
        if(tiempo_total==1)
        {
            console.log('Se terminó');
            $('#tiempo_restante').html('00:00');
        }
        else{
        	tiempo_total-=1;
            // convertir el tiempo total a min y segundos
            trestante_sec = tiempo_total % 60;
            trestante_min = tiempo_total/60;
            trestante_min = Math.floor(trestante_min);
            $('#tiempo_restante').html(trestante_min+':'+ajustar('1',trestante_sec));
            //
            setTimeout("updateReloj()",1000);
        }
    }
    function ajustar(tam, num) {

	    if (num.toString().length <= tam) return ajustar(tam, "0" + num)
	    else return num;
    }

</script>

<!--<script src="<?php echo public_url();?>js/ejecutar_juego.js"></script>-->
<!--<script src="<?php echo public_url();?>js/administracion/mapa_prev.js"></script>-->
