<form action="" enctype="multipart/form-data" method="post" id="form_nuevogrupo">

	<label for="nombres">Nombre del Grupo</label>
	<input type="text" name="nombre" placeholder="2do de primaria sección B " id="nombres">
	
	<div class="row">
		<div class="columns large-6">
			<label for="">Rango de Edad</label>
			<div class="row">
				<div class="columns large-5"><input type="text" placeholder="e mínima" name="edad_minima"></div>
				<div class="columns large-2">-</div>
				<div class="columns large-5"><input type="text" placeholder="e máxima" name="edad_maxima"></div>
			</div>
			
		</div>
		
	</div>
	<label for="">Asignar Alumnos</label>

	<div class="btn_default"><span class="icon-checkmark"></span> Limpiar</div>
	<table class="w100">
		<thead>
			<tr>
				<th style="width:8%;"></th>
				<th style="width:8%;">N°</th>
				<th style="width:84%;">Nombre completo</th>
			</tr>
		</thead>
		<tbody>

			<?php $i =1;foreach ($alumnos as $alumno) {
				
				?>
				<tr>
					<td><input type="checkbox" value="<?php echo $alumno->idusuario;?>" name="alumnos[]"></td>
					<td><?php echo $i;$i++; ?></td>
					<td><?php echo $alumno->nombres.' '.$alumno->ap.' '.$alumno->am;?></td>
				</tr>
				<?php 
			} ?>
		</tbody>
	</table>
	<div class="button  small" id="btn_guardargrupo">Guardar Grupo</div>
	
</form>

<script>
	
	$(document).on('ready',ini);
	function ini()
	{

	$('#btn_guardargrupo').click(guardar_grupo);
	}
	function guardar_grupo()
	{
		console.log("hola");
		formData = new FormData($(".form_nuevogrupo")[0]);
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/guardar_grupo',
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
               async:false,
	     	   processData: false,
		       success: function(data)
		       {
					$(".subpanel2").html('Éxito');
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 
	}
</script>