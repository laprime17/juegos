<div class="row" data-equalizer>
	<div class=" columns large-4 nuevo_usuario-contfoto" data-equalizer-watch>
			<figure>
			<img id="nuevo_usuario-foto" src="<?php echo public_url();?>fotos/default.jpg" alt="">
				</figure>
			<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/subir_archivo" method="post" class="form_imagen">
				<div class="btn_default dib  small file_wcss">
	                <div class="content"><span class="icon-images"></span><span id="label_subir_foto">Subir foto</span> </div>
	                <input type="file" name="userfile"  class="input_file dib" id="userfile">
	            </div>
	        </form>
	</div>
<div class="columns large-8" data-equalizer-watch>
	<form action="" enctype="multipart/form-data" method="post" id="form_nuevousuario">
		<input type="hidden" id="idusr_edicion" value="0" >	
			<input type="hidden" id="ruta_foto" name="foto_ruta">
			<div class="row">
				<div class="columns large-6"><label for="nombres">Nombre</label>
														<input type="text" name="nombres" placeholder="Nombres" class="obligatorio" id="nombres"></div>
			</div>
			<div class="row">
				<div class="columns large-6">
					<label for="">Apellido paterno</label>
					<input type="text" placeholder="Apellido Paterno" class="obligatorio" name="ap">
				</div>
				<div class="columns large-6">
					<label for="">Apellido Materno</label>
					<input type="text" placeholder="Apellido Materno" class="obligatorio" name="am">
				</div>
			</div>
			<div class="row">
				<div class="columns large-6">
					<label for="">Fecha de nacimiento</label>
					<input type="date" placeholder="Apellido Paterno" class="obligatorio" name="fecha_nacimiento">
				</div>
				<div class="columns large-6">
					<label for="">Tipo de Usuario</label>
					<select id="" name="idtipo_usuario">
						<option value="3">Alumno</option>
						<option value="2">Profesor</option>
						<option value="1">Administrador</option>

					</select>
				</div>
			</div>
			<div class="row">
				<div class="columns large-6">
					<label for="">Nombre de usuario (sin espacios ni mayúsculas)</label>
					<span class="alert label dis_none alert_incorrecto_user">El usuario ya existe.</span>
					<input type="text" placeholder="usuario" id="usuario" class="obligatorio" name="usuario">
				</div>
				<div class="columns large-6">
					<label for="">Clave</label>
					<input type="password" placeholder="" class="obligatorio" name="clave">
				</div>
			</div>
			<div class="button submit" >Guardar</div>										
		</form>
	</div>
</div>



