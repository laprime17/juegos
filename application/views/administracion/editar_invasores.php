<?php $this->load->view('administracion/layouts/header.php'); ?>

<div class="row">
	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_memoria row" style= "padding-bottom: 5px;" data-equalizer>
	<div class="lbl2">Editar "Invasores comprensión"</div>
	<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/memoria_guardarmemoria" method="post"class="form_datos">
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
		
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text"  placeholder="título" name="nombre" id="titulo"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo" placeholder="minutos" id="tiempo"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje de actividad</label></div>
                    <div class=""><input type ="number" name="puntaje" id="puntaje"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="curso" id="curso">
                        <option value="Comunicacion">Comunicación</option>
                        <option value="Matematica">Matemática</option>
                        <option value="Ciencia y Ambiente">Ciencias y Ambiente</option>
                        <option value="Personal social">Personal Social</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Edad sugerida:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima" id="edad_minima"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima" id="edad_maxima"/></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" cols="30" rows="s" id="descripcion"></textarea></div>
                </div>
            </div>
		
		<label for="">Explicación del juego</label>
		<div class="invasores_explicacion">
           <span class="gris">Este juego consiste en recolectar ciertos elementos (monedas, billetes, figuras, números, letras); los adecuados para llegar a un determinado objetivo. En el juego mismo, una nave (dominada por el usuario) se enfrentará a invasores que rodean los elementos a buscar; esta nave se mueve a ambos costador, derecha-izquierda, (usar la teclas de dirección) y dispara dos tipos de balas; la primera es de destruccion (tecla D) y la segunda sirve para atrapar el objetivo (tecla A). </span> 
           <label class="gris" for="">Pasos</label>
           <span class="gris"> - Selecccionar la opcion Dinero</span>
           <span class="gris">   Completar el monto que será recolectado</span>
        </div>

            <input type="hidden" name="idinvasores" id="invasores_id_invasores" value="-1">
            <!-- <input type="hidden" name="detallesinvasores" id="iddetallesinvasores" value="-1">
            <input type="hidden" name="detallesinvasores_fig" id="iddetallesinvasores_fig" value="-1"> -->
            <input type="hidden" name="detallesinvasores_texto" id="iddetallesinvasores_texto" value="-1">
            <input type="hidden" name="invasores_textoprincipal" id="idinvasores_textoprincipal" value="-1">
        </form>
	</div>
    
    <div class="dib nuevo_juego-vista_previa columns large-8" >
    <form enctype="multipart/form-data"  class="form_imgs invasores-form_imgs">
        <div id ="texto" style="display:none;">
                    <?php echo $juegoinvasores->texto_comprension;?>
        </div>
        <div class="label db" id="invasores_imagen_titulo">Comprensión lectora</div> 
        <div class="columns large-12" style="margin-bottom: 15px;">
            <div class=""><textarea  name="textoprincipal" id="editor">
                <?php echo str_replace('\n','',$juegoinvasores->texto_comprension) ?>
            </textarea></div>
        </div>
        <div class="row" style="margin: 3px;">
            <div class="column large-4" style="margin-top: 7px;">
             <div class=""><label for="">Número de preguntas</label></div>
             <div class=""><input type ="number" name="nroejercicios_comprension" id="nroejercicios_comprension1"/></div>
            </div>
            <div class="column large-4" style="margin-top: 7px;">
               <div   class="button" style= "background: #E64A30;" id="nroejercicios_comprension"> Cargar </div>
            </div>
        </div>
        <div class="label db" id="invasores_imagen_titulo">Completa las preguntas</div>
        <div id="actividad_comprension" style ="margin: 20px;"></div>
    </form>
    </div>
</div>
    <div id="aviso_guardado"></div>
    <section class="nuevo_invasores-botones">
        <div   class="button  guardar " id="guardardatos"> Guardar </div>
        <a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
        <div   class="button  alert " id="cancelar_todo"> Cancelar </div>
    </section>        
</div>
</div>

<div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
</div>

<script>

    //completar el formulario de edicion
    $('#invasores_id_invasores').val('<?php echo $juegoinvasores->idjuego;?>');
    $('#titulo').val('<?php echo $juegoinvasores->nombre;?>');
    $('#tiempo').val('<?php echo $juegoinvasores->tiempo;?>');
    $('#puntaje').val('<?php echo $juegoinvasores->puntaje;?>');
    $('#edad_minima').val(' <?php echo $juegoinvasores->edad_minima;?>');
    $('#edad_maxima').val('<?php echo $juegoinvasores->edad_maxima;?>');
    $('#descripcion').val('<?php echo $juegoinvasores->descripcion;?>');
    $('#iframe_prev').attr('src','<?php echo base_url();?>juegos/invasores_previsualizacion/<?php echo $juegoinvasores->idjuego;?>');
      var cont=1;
      $('#actividad_comprension').empty();
        </script>
        <?php foreach ($invasoresdetalles as $detalles ) { ?>
        <script>
        var pregunt='<?php echo $detalles->pregunta;?>';
        var rpta = '<?php echo $detalles->respuesta;?>';
        var dist1 = '<?php echo $detalles->primerDistractor;?>';
        var dist2 = '<?php echo $detalles->segundoDistractor;?>';
        var divpreg = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+cont+"</div><div class='column large-11'><input type='text' name='pregunta_"+cont+"' id='pregunta_"+cont+"' placeholder='Pregunta' value='"+pregunt+"'/></div><div class='column large-2'>Respuesta:</div><div class='column large-10'><input type='text' name='respuesta_"+cont+"' id='respuesta_"+cont+"' placeholder='Respuesta' value='"+rpta+"'/></div><div class='column large-2'>Distractor1:</div><div class='column large-10'><input type='text' name='primerDistractor_"+cont+"' id='primerDistractor_"+cont+"' placeholder='Primer distractor' value='"+dist1+"'/></div><div class='column large-2'>Distractor2:</div><div class='column large-10'><input type='text' name='segundoDistractor_"+cont+"' id='segundoDistractor_"+cont+"' placeholder='Segundo distractor' value='"+dist2+"'/></div></div>";
        $( "#actividad_comprension").append(divpreg);
        cont++;
        </script>
        <?php } ?>
        <script>


    CKEDITOR.instances['editor'].setData($('#texto').html());

</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/nuevo_invasorescomprension.js"></script>
