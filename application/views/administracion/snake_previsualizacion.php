<div class="prev">

	<div class="prev-content">
		<div class="prev-content-titulo row" style="background: #2B7692;">
            <div class="mt03m prev-content-titulo-otros columns large-2" id="tiempo_actividad">
                <div id="hour">00</div>
                <div class="divideruno">:</div>
                <div id="minute">00</div>
                <div class="dividerdos">:</div>
                <div id="second">00</div>
            </div>
			<div class="mt03m prev-content-titulo-titulo columns large-6" style= "text-align: center;"><?php echo $juegosnake->nombre;?> </div >
			<div class="prev-content-titulo-otros columns large-3"><div class="boton amarillo dib"><span class="icon-arrow-right"></span> Siguiente Actividad</div></div>
		</div>
		<div class="prev-imagen-fraccionado ">
			<div class="prev-imagen-fraccionado-img"><img id="img_guia" style= "width: 127px; height: 127px;" src=""></div> 
            <div class ="prev-imagen-fraccionado-textoayuda">Mira atentamente la imagen.¿Qué cosa ves?; con el gusanito recolecta las letras que forman la palabra.</div>
            <div class ="prev-imagen-fraccionado-textoayuda" id ="txtayuda"></div>
            <div><img id="flecha_img" src="<?=public_url()?>img/flecha_izquierda.png"></div>
            <div class="letras"></div> 
			<div class=" prev-imagen-fraccionado-puntos"><div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> </div>
            
			
		</div>
		<figure class="prev-img" id="prev-img">
		<div class="snake_prev_final"></div>
		</figure>
		<div class="prev-footer">
				<span class="icon-arrow-left"></span>
				<span class="icon-arrow-right"></span>
		</div>	
	</div>

	
</div>
<script >
    var puntaje_actividad = <?php echo $juegosnake->puntaje;?>;
    var tiempo_actividad = <?php echo $juegosnake->tiempo;?>;
    var palabrasclave = new Array();
    var textoayuda = new Array();
    var imagenguia = new Array();
    var nroejercicio = 0;
</script>
<?php foreach ($snakedetalles as $detalles ) { ?>
    <script>palabrasclave.push('<?php echo $detalles->nombre_imagen;?>');
            textoayuda.push('<?php echo $detalles->texto_ayuda;?>');
            imagenguia.push('<?php echo $detalles->ruta;?>');
    </script> <?php } ?>
<script>
    var puntajeximagen = puntaje_actividad / palabrasclave.length;
    cargar_imagenguia(nroejercicio);
    cargar_txtayuda(nroejercicio);

    var tamanio_tmp =  palabrasclave[nroejercicio].trim();
    var tamanio_palabra1 = tamanio_tmp.length;
    Agregar_cuadroletra(tamanio_palabra1);
    

    function Agregar_cuadroletra(tamanio_palabra1)
    {
        for (var i = 1; i <= tamanio_palabra1; i++) {
            div = '<div class=" prev-imagen-fraccionado-letras" id = "letra_'+i+'"></div>';
            $('.letras').append(div);
        };  
    }
    function cargar_imagenguia(nroimagen)
    {
        var imagentemp =  imagenguia[nroimagen]; 
        $('#img_guia').attr('src',imagentemp);
    }
    function cargar_txtayuda(nrotexto)
    {   
        $('#txtayuda').text('Ayuda: '+textoayuda[nrotexto]);
    }
    function Agregar_puntaje()
    {
        console.log('entró agregar puntos');
        if ($.isNumeric($('#puntaje_acumulado').html()))
        {
            var tmp = parseInt($('#puntaje_acumulado').html()); 
            tmp = tmp + puntajeximagen; 
            $('#puntaje_acumulado').html(tmp);
        }
        
    }
    var tiempo = {hora:0, minuto:0, segundo:0};
    var tiempo_corriendo = null;
    var texto_tiempo;
    correr_tiempo();
    function correr_tiempo()
    {
        
            tiempo_corriendo = setInterval(function(){

                // Segundos
                tiempo.segundo++;
                if(tiempo.segundo >= 60)
                {
                    tiempo.segundo = 0;
                    tiempo.minuto++;
                }

                if(tiempo.minuto>=tiempo_actividad)// se acabó el teimpo
                {
                    texto_tiempo = game.add.text(400, 250, '¡Se terminó el tiempo! \n pasa a la siguiente actividad' , {fill: '#fff' });
                    texto_tiempo.fontWeight = 'bold';
                    texto_tiempo.fontSize = 40;
                    texto_tiempo.font = 'Arial Black';
                    texto_tiempo.anchor.setTo(0.5, 0.5);
                    texto_tiempo.stroke = "#008CBA";
                    texto_tiempo.strokeThickness = 16;
                    texto_tiempo.setShadow(2, 2, "#333333", 2, true, false);

                    //parar_tiempo();
                    clearInterval(tiempo_corriendo);

                    game.paused=true; 

                }
                // Minutos
                if(tiempo.minuto >= 60)
                {
                    tiempo.minuto = 0;
                    tiempo.hora++;
                }

                $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
                $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
                $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);

            },1000);
        
    }
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>

<script>


	var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'prev-img', { preload: preload, create: create, update: update,render : render });

function preload() {

    game.load.image('ball',public_url+'sprites/cuerpo.png');
    game.load.image('cabeza',public_url+'sprites/cabeza.png');
    game.load.image('background', public_url+'sprites/fondo1.jpg');
    game.load.image('menu', public_url+'sprites/opcionessnake.png', 270, 180);
    game.load.image('siguienteimg', public_url+'sprites/sigt_imagen.png', 270, 180);
    game.load.image('menu_terminar', public_url+'sprites/opcionessnake_terminar.png', 270, 180);

    //cargar las imagenes de las letras:
    game.load.image('letraA',public_url+'sprites/snake/A.png');
    game.load.image('letraB',public_url+'sprites/snake/B.png');
    game.load.image('letraC',public_url+'sprites/snake/C.png');
    game.load.image('letraD',public_url+'sprites/snake/D.png');
    game.load.image('letraE',public_url+'sprites/snake/E.png');
    game.load.image('letraF',public_url+'sprites/snake/F.png');
    game.load.image('letraG',public_url+'sprites/snake/G.png');
    game.load.image('letraH',public_url+'sprites/snake/H.png');
    game.load.image('letraI',public_url+'sprites/snake/I.png');
    game.load.image('letraJ',public_url+'sprites/snake/J.png');
    game.load.image('letraK',public_url+'sprites/snake/K.png');
    game.load.image('letraL',public_url+'sprites/snake/L.png');
    game.load.image('letraM',public_url+'sprites/snake/M.png');
    game.load.image('letraN',public_url+'sprites/snake/N.png');
    game.load.image('letraÑ',public_url+'sprites/snake/Ñ.png');
    game.load.image('letraO',public_url+'sprites/snake/O.png');
    game.load.image('letraP',public_url+'sprites/snake/P.png');
    game.load.image('letraQ',public_url+'sprites/snake/Q.png');
    game.load.image('letraR',public_url+'sprites/snake/R.png');
    game.load.image('letraS',public_url+'sprites/snake/S.png');
    game.load.image('letraT',public_url+'sprites/snake/T.png');
    game.load.image('letraU',public_url+'sprites/snake/U.png');
    game.load.image('letraV',public_url+'sprites/snake/V.png');
    game.load.image('letraW',public_url+'sprites/snake/W.png');
    game.load.image('letraX',public_url+'sprites/snake/X.png');
    game.load.image('letraY',public_url+'sprites/snake/Y.png');
    game.load.image('letraZ',public_url+'sprites/snake/Z.png');
    game.load.image('letraAtilde',public_url+'sprites/snake/Atilde.png');
    game.load.image('letraEtilde',public_url+'sprites/snake/Etilde.png');
    game.load.image('letraItilde',public_url+'sprites/snake/Itilde.png');
    game.load.image('letraOtilde',public_url+'sprites/snake/Otilde.png');
    game.load.image('letraUtilde',public_url+'sprites/snake/Utilde.png');

    //sonido
    game.load.audio('sonidocomida', public_url+'audio/snake/swoosh.mp3');


}

var snakeHead; //head of snake sprite
var snakeSection = new Array(); //array of sprites that make the snake body sections
var snakePath = new Array(); //arrary of positions(points) that have to be stored for the path the sections follow
var numSnakeSections = 5; //number of snake body sections
//el siguiente paramentro: sankespacer, aumentar si se disminuye la velocidad del gusano 
var snakeSpacer = 12; //parameter that sets the spacing between sections, este varaible junto con velocidad en X determinan la velocidad del gusano
var nuevoElemento;
var elemx;
var elemy;
var distractorx;
var distractory;
var ditractor;
var elementoDistractor;
var letraDistractor;
var posiblesDistractores = new Array();
var numpalabra = 0;
var secuenciaPalabra = palabrasclave; //['entrevista','Piano',<?php echo "artista";?>];
var palabra = secuenciaPalabra[numpalabra].trim();
var contador=1;
var temp;
var teclas;
var spaceKey;
var opcionesYN, colisionYN;
var completadoYN = 'N';
var todocorrectoYN='N';
var w = 800, h = 600;
var juegoterminadoYN='N';
var sonidocomida;
var menu_terminar;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    //Background de fondo
    game.add.tileSprite(0, 0, 800, 600, 'background');

    //Tamaño del mundo visible
    game.world.setBounds(0, 0, 800, 600);

    cursors = game.input.keyboard.createCursorKeys();
    //teclas = game.input.keyboard.createCursorKeys();
    //spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);


    snakeHead = game.add.sprite(400, 300, 'cabeza');
    snakeHead.anchor.setTo(0.5, 0.5);

    game.physics.enable(snakeHead, Phaser.Physics.ARCADE);

    //  Init snakeSection array
    for (var i = 1; i <= numSnakeSections-1; i++)
    {
        snakeSection[i] = game.add.sprite(400, 300, 'ball');
        snakeSection[i].anchor.setTo(0.5, 0.5);
        game.physics.enable(snakeSection[i], Phaser.Physics.ARCADE);
    }
    
    //  Init snakePath array
    for (var i = 0; i <= numSnakeSections * snakeSpacer; i++)
    {
        snakePath[i] = new Phaser.Point(400, 300);
    }

    
    //agregar la primera letra y el distractor que comerá el gusanito
    nuevas_letras(palabra[0]);
    todocorrectoYN = 'Y';

    //sonido por comida 
    sonidocomida = game.add.audio('sonidocomida',0.6);

    //---------------------- boton opciones
    // Create a label to use as a button
    pause_label = game.add.text(w - 140, 20, 'Opciones', { font: '26px Arial Black', fill: '#fff'});
    pause_label.inputEnabled = true;
    pause_label.events.onInputUp.add(opciones);

    // Add a input listener that can help us return from being paused
    game.input.onDown.add(unpause_opciones, self);
        
}
function opciones()
    {
    // When the paus button is pressed, we pause the game
        game.paused = true;

        // Then add the menu
        menu = game.add.sprite(w/2, h/2, 'menu');
        menu.anchor.setTo(0.5, 0.5);

        // And a label to illustrate which menu item was chosen. (This is not necessary)
        choiseLabel = game.add.text(w/2, h-150, 'Haz click fuera del cuadro para continuar', { font: '30px Arial', fill: '#fff' });
        choiseLabel.anchor.setTo(0.5, 0.5);

        opcionesYN = 'Y';
    }
function unpause_opciones(event){
        // Only act if paused
        if(game.paused){

            // Calculate the corners of the menu
            var x1 = w/2 - 270/2, x2 = w/2 + 270/2,
                y1 = h/2 - 180/2, y2 = h/2 + 180/2;

            // Check if the click was inside the menu
            if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
                // The choicemap is an array that will help us see which item was clicked
                var choisemap = ['Uno', 'Dossss'];

                // Get menu local coordinates for the click
                var x = event.x - x1,
                    y = event.y - y1;

                // Calculate the choice 
                var choise = Math.floor(x / 270) + Math.floor(y / 90);

                // Display the choice
                if(choise == 0)
                {
                    $('.letras').empty();
                    // reiniciar cuadro de respuestas
                    Agregar_cuadroletra(tamanio_palabra1);
                    //reiniciar contador
                    contador = 0;

                    //eliminar las letras actuales
                    nuevoElemento.destroy();
                    elementoDistractor.destroy();

                    //reiniciar letras
                    nuevas_letras(palabra[contador]);
                    contador++;

                    //destruir el gusanito
                    snakeHead.destroy();
                    for (var i = 1; i < numSnakeSections; i++) {
                        snakeSection[i].destroy();
                    };
                    

                    // Remove the menu and the label
                    menu.destroy();
                    choiseLabel.destroy();

                    numSnakeSections = 5;
                    //crear un nuevo gusanito en la posicion inicial
                    create();

                    // Unpause the game
                    game.paused = false;
                }
                else
                {
                    if(juegoterminadoYN == 'Y')
                    {
                        menu_terminar.destroy();
                        if (todocorrectoYN == 'Y' && completadoYN=='Y')
                        {
                            Agregar_puntaje();
                        }
                        //parar_tiempo porque terminó de jugar;
                        clearInterval(tiempo_corriendo);
                        //choiseLabel.destroy();
                    }
                    else
                    {
                        //
                        $('.letras').empty();

                        //cambiar imagen
                        nroejercicio++;
                        numpalabra++;
                        cargar_imagenguia(nroejercicio);
                        cargar_txtayuda(nroejercicio);

                        // reiniciar cuadro de respuestas
                        var tama = palabrasclave[nroejercicio].trim();
                        tamanio_palabra1 = tama.length;
                        Agregar_cuadroletra(tamanio_palabra1);

                        palabra = secuenciaPalabra[numpalabra].trim();
                        //reiniciar contador
                        contador = 0;

                        //eliminar las letras actuales
                        nuevoElemento.destroy();
                        elementoDistractor.destroy();

                        //reiniciar letras
                        nuevas_letras(palabra[contador]);
                        contador++;

                        //destruir el gusanito
                        snakeHead.destroy();
                        for (var i = 1; i < numSnakeSections; i++) {
                            snakeSection[i].destroy();
                        };

                        if (todocorrectoYN == 'Y' && completadoYN=='Y')
                        {
                            Agregar_puntaje();
                        }

                         // Remove the menu and the label
                        menu.destroy();
                        choiseLabel.destroy();

                        numSnakeSections = 5;
                        //crear un nuevo gusanito en la posicion inicial
                        create();

                        // Unpause the game
                        game.paused = false;
                    }
                }
                
            }
            else{ // cuando el click es fuera del cuadro

                if(opcionesYN == 'Y')
                {
                    // Remove the menu and the label
                    menu.destroy();
                    choiseLabel.destroy();

                    opcionesYN='N';

                    // Unpause the game
                    game.paused = false;
                }
            }
        
    }
};
// And finally the method that handels the pause menu

function update() {

    
    //snakeHead.body.velocity.setTo(0, 0);
    snakeHead.body.velocity.copyFrom(game.physics.arcade.velocityFromAngle(snakeHead.angle, 150)); //agregado
    snakeHead.body.angularVelocity = 0;

    // Everytime the snake head moves, insert the new location at the start of the array, 
    // and knock the last position off the end
    var part = snakePath.pop();

        part.setTo(snakeHead.x, snakeHead.y);

        snakePath.unshift(part);

        for (var i = 1; i <= numSnakeSections - 1; i++)
        {
            snakeSection[i].x = (snakePath[i * snakeSpacer]).x;
            snakeSection[i].y = (snakePath[i * snakeSpacer]).y;
        }


    if(snakeHead.x > 800)
    {
        snakeHead.x=0;
    }
    else if(snakeHead.x < 0)
    {
        snakeHead.x=800;
    }
    else if(snakeHead.y < 0)
    {
        snakeHead.y=600;
    }
    else if(snakeHead.y > 600)
    {
        snakeHead.y=0;
    }


    //Acciones cuando se aprieten las flechas
    if (cursors.up.isDown)
    {
        //Rotar hacia arriba, excepto si está yendo hacia abajo
        if(snakeHead.body.rotation  != 90)
            snakeHead.body.rotation = -90;

    }
    else if (cursors.left.isDown)
    {
        //Rotar hacia la izquierda, excepto si está yendo hacia la derecha
        if(snakeHead.body.rotation  != 0)
            snakeHead.body.rotation = 180;
        
    }
    else if (cursors.right.isDown)
    {
        //Rotar hacia la derecha, excepto si está yendo hacia la izquierda
        if(snakeHead.body.rotation  != -180 )
            snakeHead.body.rotation = 0;
    }
    else if (cursors.down.isDown) 
    {
        //Rotar hacia abajo, excepto si está yendo hacia arriba
        if(snakeHead.body.rotation  != -90)
        snakeHead.body.rotation = 90;
    }

        

    //Cuando la serpiente impacta con una imagen
    game.physics.arcade.overlap(snakeHead, nuevoElemento, Colision_elementos, null, this);

    game.physics.arcade.overlap(snakeHead, elementoDistractor, Colision_distractor, null, this);
    
    if(game.physics.arcade.collide(snakeHead, snakeSection, null, null, this))
    {
        colision_cola(snakeHead, snakeSection);
    }    

}
function nuevas_letras(palabra)
{
    //Elegir aleatoriamente las posiciones  del nuevo elemento
    elemx = Math.floor((Math.random() * 750) + 50);
    elemy = Math.floor((Math.random() * 540) + 10);

    //Elegir aleatoriamente las posiciones  del elemento distractor
    distractorx = Math.floor((Math.random() * 750) + 50);
    distractory = Math.floor((Math.random() * 540) + 10);

    distractor = Math.floor((Math.random() * 4) + 1);


    
    if(palabra == 'A' || palabra == 'a')
    {
        nuevoElemento = game.add.sprite(elemx,elemy, 'letraA');
        posiblesDistractores = ['R','E','O','Á','Á']; 
    }
    else if(palabra == 'B' || palabra == 'b')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraB');
        posiblesDistractores = ['R','V','V','P','V']; }
    else if(palabra == 'C' || palabra == 'c')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraC');
        posiblesDistractores = ['B','S','Z','K','Q']; }
    else if(palabra == 'D' || palabra == 'd')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraD');
        posiblesDistractores = ['J','P','G','K','H']; }
    else if(palabra == 'E' || palabra == 'e')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraE');
        posiblesDistractores = ['Y','A','É','I','É']; }
    else if(palabra == 'F' || palabra == 'f')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraF');
        posiblesDistractores = ['P','E','J','G','H']; }
    else if(palabra == 'G' || palabra == 'g')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraG');
        posiblesDistractores = ['H','J','F','T','B']; }
    else if(palabra == 'H' || palabra == 'h')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraH');
        posiblesDistractores = ['K','J','G','C','L']; }
    else if(palabra == 'I' || palabra == 'i')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraI');
        posiblesDistractores = ['A','Y','Y','Í','Y']; }
    else if(palabra == 'J' || palabra == 'j')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraJ');
        posiblesDistractores = ['K','G','G','H','F']; }
    else if(palabra == 'K' || palabra == 'k')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraK');
        posiblesDistractores = ['N','C','Q','C','Q']; }
    else if(palabra == 'L' || palabra == 'l')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraL');
        posiblesDistractores = ['P','J','H','K','Y']; }
    else if(palabra == 'M' || palabra == 'm')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraM');
        posiblesDistractores = ['U','N','Ñ','N','W']; }
    else if(palabra == 'N' || palabra == 'n')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraN');
        posiblesDistractores = ['D','M','M','H','Ñ']; }
    else if(palabra == 'Ñ' || palabra == 'ñ')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraÑ');
        posiblesDistractores = ['D','M','N','H','N']; }
    else if(palabra == 'O' || palabra == 'o')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraO');
        posiblesDistractores = ['Y','A','Ó','Ó','U']; }
    else if(palabra == 'P' || palabra == 'p')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraP');
        posiblesDistractores = ['Q','T','B','D','T']; }
    else if(palabra == 'Q' || palabra == 'q')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraQ');
        posiblesDistractores = ['U','C','P','C','K']; }
    else if(palabra == 'R' || palabra == 'r')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraR');
        posiblesDistractores = ['M','S','L','D','P']; }
    else if(palabra == 'S' || palabra == 's')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraS');
        posiblesDistractores = ['Y','C','X','Z','Z']; }
    else if(palabra == 'T' || palabra == 't')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraT');
        posiblesDistractores = ['C','P','P','C','D']; }
    else if(palabra == 'U' || palabra == 'u')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraU');
        posiblesDistractores = ['Y','Ú','Ú','I','O']; }
    else if(palabra == 'V' || palabra == 'v')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraV');
        posiblesDistractores = ['R','B','B','W','B']; }
    else if(palabra == 'W' || palabra == 'w')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraW');
        posiblesDistractores = ['D','V','H','H','U']; }
    else if(palabra == 'X' || palabra == 'x')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraX');
        posiblesDistractores = ['L','S','S','Z','S']; }
    else if(palabra == 'Y' || palabra == 'y')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraY');
        posiblesDistractores = ['W','I','U','L','L']; }
    else if(palabra == 'Z' || palabra == 'z')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraZ');
        posiblesDistractores = ['Y','S','S','C','X']; }
    else if(palabra == 'Á' || palabra == 'á')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraAtilde');
        posiblesDistractores = ['A','A','A','A','A']; }
    else if(palabra == 'É' || palabra == 'é')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraEtilde');
        posiblesDistractores = ['E','E','E','E','E']; }
    else if(palabra == 'Í' || palabra == 'í')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraItilde');
        posiblesDistractores = ['I','I','I','I','I']; }
    else if(palabra == 'Ó' || palabra == 'ó')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraOtilde');
        posiblesDistractores = ['O','O','O','O','O']; }
    else if(palabra == 'Ú' || palabra == 'ú')
    {   nuevoElemento = game.add.sprite(elemx,elemy, 'letraUtilde');
        posiblesDistractores = ['U','U','U','U','U']; }

    nuevoElemento.anchor.setTo(0.5, 0.5); 
    game.physics.enable(nuevoElemento, Phaser.Physics.ARCADE);

    //Crear distractores
    letraDistractor = posiblesDistractores[distractor];

    if(letraDistractor == 'A')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraA'); }
    else if(letraDistractor == 'B')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraB'); }
    else if(letraDistractor == 'C')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraC'); }
    else if(letraDistractor == 'D')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraD'); }
    else if(letraDistractor == 'E' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraE'); }
    else if(letraDistractor == 'F')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraF'); }
    else if(letraDistractor == 'G' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraG'); }
    else if(letraDistractor == 'H')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraH'); }
    else if(letraDistractor == 'I')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraI'); }
    else if(letraDistractor == 'J')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraJ'); }
    else if(letraDistractor == 'K')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraK'); }
    else if(letraDistractor == 'L' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraL'); }
    else if(letraDistractor == 'M')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraM'); }
    else if(letraDistractor == 'N')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraN'); }
    else if(letraDistractor == 'Ñ')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraÑ'); }
    else if(letraDistractor == 'O')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraO'); }
    else if(letraDistractor == 'P')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraP'); }
    else if(letraDistractor == 'Q' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraQ'); }
    else if(letraDistractor == 'R')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraR'); }
    else if(letraDistractor == 'S' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraS'); }
    else if(letraDistractor == 'T')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraT'); }
    else if(letraDistractor == 'U')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraU'); }
    else if(letraDistractor == 'V')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraV'); }
    else if(letraDistractor == 'W')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraW'); }
    else if(letraDistractor == 'X')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraX'); }
    else if(letraDistractor == 'Y' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraY'); }
    else if(letraDistractor == 'Z' )
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraZ'); }
    else if(letraDistractor == 'Á')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraAtilde'); }
    else if(letraDistractor == 'É')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraEtilde'); }
    else if(letraDistractor == 'Í')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraItilde'); }
    else if(letraDistractor == 'Ó')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraOtilde'); }
    else if(letraDistractor == 'Ú')
    { elementoDistractor = game.add.sprite(distractorx,distractory, 'letraUtilde'); }

    elementoDistractor.anchor.setTo(0.5, 0.5); 
    game.physics.enable(elementoDistractor, Phaser.Physics.ARCADE);


}
function Colision_elementos(snakeHead, nuevoElemento)
{
    nuevoElemento.kill();
    elementoDistractor.kill();
    numSnakeSections = numSnakeSections + 1;

    //sonido
    sonidocomida.play();
    
    //  agregar una seccion a la serpierte
    snakeSection[numSnakeSections-1] = game.add.sprite((snakeHead.x), (snakeHead.y), 'ball');
    snakeSection[numSnakeSections-1].anchor.setTo(0.5, 0.5);
    game.physics.enable(snakeSection[numSnakeSections-1], Phaser.Physics.ARCADE);

    //  Aumentar el snakePath array
    for (var i = ((numSnakeSections-1) * snakeSpacer)+1; i <= numSnakeSections * snakeSpacer; i++)
    {
        snakePath[i] = new Phaser.Point(snakeHead.x, snakeHead.y);
    }
    
    $('#letra_'+contador).text(palabra[contador-1].toUpperCase());

    //si la palabra sigue teniendo letras, agregar las siguiente 
    if(contador<palabra.length)
    {
        nuevas_letras(palabra[contador]);
        contador++;
    }
    else
    {
        //completó la palabra, ya no hay mas letras
        game.paused =  true;
        completadoYN = 'Y';

        if((nroejercicio+1)==palabrasclave.length)//completó la actividad de snake.
        {
            texto_terminaste();

        }
        else
        {
            fin_de_una_imagen();
        }
    }
            
}
function Colision_distractor(snakeHead, elementoDistractor)
{
    //El jugador eligió mal el elemento, sin embargo no parar hasta que se de cuenta o acabe.
    nuevoElemento.kill();
    elementoDistractor.kill();
    numSnakeSections = numSnakeSections + 1;

    sonidocomida.play();
    
    //  agregar una seccion a la serpierte
    snakeSection[numSnakeSections-1] = game.add.sprite((snakeHead.x), (snakeHead.y), 'ball');
    snakeSection[numSnakeSections-1].anchor.setTo(0.5, 0.5);
    game.physics.enable(snakeSection[numSnakeSections-1], Phaser.Physics.ARCADE);

    // aumentar el snakePath array
    for (var i = ((numSnakeSections-1) * snakeSpacer)+1; i <= numSnakeSections * snakeSpacer; i++)
    {
        snakePath[i] = new Phaser.Point(snakeHead.x, snakeHead.y);
    }

    $('#letra_'+contador).text(letraDistractor.toUpperCase());

    todocorrectoYN = 'N';

    if(contador<palabra.length)
    {
        nuevas_letras(palabra[contador]);
        contador++;
    }
    else
    {
        game.paused =  true;
        completadoYN = 'Y';

        if((nroejercicio+1)==palabrasclave.length)
        {
            
            texto_terminaste();
 
        }
        else
        {
            fin_de_una_imagen();
        }
    }            
}
function fin_de_una_imagen()
{
    choiseLabel = game.add.text(w/2, 150, '¡Completado!', { fill: '#fff' });
    choiseLabel.fontWeight = 'bold';
    choiseLabel.fontSize = 70;
    choiseLabel.anchor.setTo(0.5, 0.5);
    choiseLabel.stroke = "#008CBA";
    choiseLabel.strokeThickness = 16;
    choiseLabel.setShadow(2, 2, "#333333", 2, true, false);

    // Then add the menu
    menu = game.add.sprite(w/2, h/2, 'siguienteimg');
    menu.anchor.setTo(0.5, 0.5);

    game.input.onDown.add(unpause_opciones, self);
}

function texto_terminaste()
{
    choiseLabel = game.add.text(w/2, 120, '¡TERMINASTE DE JUGAR \n            GUSANITO!', { fill: '#008CBA' });
    choiseLabel.anchor.setTo(0.5, 0.5);
    choiseLabel.font = 'Arial Black';
    choiseLabel.fontSize = 40;
    choiseLabel.fontWeight = 'bold';
    choiseLabel.stroke = "#fff";
    choiseLabel.strokeThickness = 16;
    choiseLabel.setShadow(2, 2, "#333333", 2, true, false);
    opcionesYN='N';
    juegoterminadoYN ='Y';

    menu_terminar = game.add.sprite(w/2, h/2, 'menu_terminar');
    menu_terminar.anchor.setTo(0.5, 0.5);
}

function colision_cola(snakeHead, snakeSection){

    //console.log('chocó con la cola, perdiste');
    game.paused = true;
    
    // And a label to illustrate which menu item was chosen. (This is not necessary)
    choiseLabel = game.add.text(w/2, 150, '¡Chocaste! Vuelve a intentarlo', { fill: '#fff' });
    choiseLabel.fontWeight = 'bold';
    choiseLabel.fontSize = 40;
    choiseLabel.anchor.setTo(0.5, 0.5);
    
    // Then add the menu
    menu = game.add.sprite(w/2, h/2, 'menu');
    menu.anchor.setTo(0.5, 0.5);

    // Add a input listener that can help us return from being paused
    game.input.onDown.add(unpause_opciones, self);

    
}
function render() {

    //game.debug.spriteInfo(snakeHead, 32, 32);
}
//-----------------------------------------------------------------------------------	
	
	 // function updateReloj()
  //   {
  //       if(tiempo_total==1)
  //       {
  //           console.log('Se terminó');
  //           $('#tiempo_restante').html('00:00');

  //       }
  //       else{
            
  //       	tiempo_total-=1;
  //           // convertir el tiempo total a min y segundos
  //           trestante_sec = tiempo_total % 60;

  //           trestante_min = tiempo_total/60;
  //           trestante_min = Math.floor(trestante_min);

  //           $('#tiempo_restante').html(trestante_min+':'+ajustar('1',trestante_sec));
  //           //
  //           setTimeout("updateReloj()",1000);
  //       }
  //   }
  //   function ajustar(tam, num) {

	 //    if (num.toString().length <= tam) return ajustar(tam, "0" + num)
	 //    else return num;
  //   }


</script>
<!--<script src="<?php echo public_url();?>js/administracion/mapa_prev.js"></script>-->
