<div class="prev">

	<div class="prev-content">
		<div class="prev-content-titulo row">
			<div class="mt03m prev-content-titulo-titulo columns large-5">Partes de la Planta</div >
			<div class="mt03m prev-content-titulo-otros columns large-2">0 intentos</div>
			<div class="mt03m prev-content-titulo-otros columns large-2" id="tiempo_restante">06:25</div>
			<div class="prev-content-titulo-otros columns large-3"><div class="boton amarillo dib"><span class="icon-arrow-right"></span> Siguiente</div></div>
		</div>
		<div class="prev-content-datos ">
			<div class="dib prev-content-datos-enunciado"></div>
			
			<div class="prev-content-datos-puntos"><div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> </div>
			
		</div>
		<figure class="prev-img" id="prev-img">
            <div class="abeja-prev-mensaje">
                
                <figure class ="abeja-prev-mensaje-fig"><img  class ="abeja-prev-mensaje-img" src=""></figure>
                <div class="abeja-prev-mensaje-des"></div>

            </div>
		<!--previsualización -->



		<!--fin previsualización -->
		<div class="mapa_prev_final"></div>
		</figure>
		<div class="prev-footer">
			
				<span class="icon-arrow-left"></span>
				<span class="icon-arrow-right"></span>
			
			
		</div>	
		

	</div>

	
</div>
<script>
$(document).ready(ini);

var game = new Phaser.Game(739, 380, Phaser.CANVAS, 'prev-img', { preload: preload, create: create, update: update, render: render });
function ini()
{
    imgs_atrapadas = 0;
    prev_imagenes_json = localStorage.getItem('abeja_imagenes');
    prev_correctos_json = localStorage.getItem('abeja_correctos');
    prev_imagenes =  JSON.parse(prev_imagenes_json);
    prev_correctos =  JSON.parse(prev_correctos_json);
    puntaje=0;
    puntaje_x_correcto = localStorage.getItem('abeja_puntaje')/prev_correctos.length;
    
    $('.prev-content-datos-enunciado').html(localStorage.getItem('abeja_premisa'));
}


function preload() {

    
    game.load.spritesheet('abejita', public_url+'sprites/abeja.png', 88, 60);
    game.load.image('background', public_url+'sprites/abeja_cielo.jpg');

     

    for (var i = prev_imagenes.length - 1; i >= 0; i--) {
        game.load.image(prev_imagenes[i].valor.nombre, prev_imagenes[i].valor.ruta);
        
    };

}
var player;
var facing = 'left';
var jumpTimer = 0;
var cursors;
var jumpButton;
var bg;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.add.tileSprite(0, 0, 2832, 1200, 'background');
    game.world.setBounds(0, 0, 1900, 500);


    game.physics.arcade.gravity.y = 300;

    player = game.add.sprite(50, 10, 'abejita');
    // añadir las imagens
    var posX=200;
    prev_sprites = Array();
    for (var i = prev_imagenes.length - 1; i >= 0; i--) {

        game.physics.enable(prev_imagenes[i].valor.nombre, Phaser.Physics.ARCADE);
        prev_img = game.add.sprite(posX, 300, prev_imagenes[i].valor.nombre);
        game.physics.enable(prev_img, Phaser.Physics.ARCADE);
        prev_img.body.immovable = true;
        prev_img.body.allowGravity = false;
        // prev_img.body.setZeroVelocity(); 
        //  
        // prev_img.
        posX+=300;
        prev_img.width = 120;
        prev_img.height = 120;
        prev_sprites.push(prev_img);
    };
    // fin imàgenes

    // camaara
    game.physics.startSystem(Phaser.Physics.P2JS);

    game.physics.enable(player, Phaser.Physics.ARCADE);

    player.body.collideWorldBounds = true;
    player.body.gravity.y = 1000;
    player.body.maxVelocity.y = 1000;
    player.body.setSize(20, 32, 5, 16);

    // player.animations.add('left', [0, 1, 2, 3], 10, true);
    // player.animations.add('turn', [4], 20, true);
    // player.animations.add('right', [5, 6, 7, 8], 10, true);
      player.animations.add('left', [3,4,5], 8, true);
    // player.animations.add('left', [0, 1, 2, 3], 8"VELOCIDAD DE CAMBIO", true "alternar con actual");
    player.animations.add('turn', [3], 8, true);
    player.animations.add('right', [0, 1, 2], 8, true);

    cursors = game.input.keyboard.createCursorKeys();
    jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    // camara
    game.physics.p2.enable(player);
    game.camera.follow(player);

}

function update() {

    

    player.body.velocity.x = 0;

    if (cursors.left.isDown)
    {
        player.body.velocity.x = -150;

        if (facing != 'left')
        {
            player.animations.play('left');
            facing = 'left';
        }
    }
    else if (cursors.right.isDown)
    {
        player.body.velocity.x = 150;

        if (facing != 'right')
        {
            player.animations.play('right');
            facing = 'right';
        }
    }
    else
    {
        if (facing != 'idle')
        {
            player.animations.stop();

            if (facing == 'left')
            {
                player.frame = 0;
            }
            else
            {
                player.frame = 5;
            }

            facing = 'idle';
        }
    }
    
    if (jumpButton.isDown && player.body.onFloor() && game.time.now > jumpTimer)
    {
        player.body.velocity.y = -500;
        jumpTimer = game.time.now + 750;
    }

    // añadir los spirtes y colisiones
    for (var i = prev_sprites.length - 1; i >= 0; i--) {

        game.physics.arcade.collide(player, prev_sprites[i], collisionHandler, null, this);

        
    };

}
function collisionHandler (abeja, img) {

    $('.abeja-prev-mensaje').css('opacity','1');
    
    img.kill();
    imgs_atrapadas++;

    var nombre_img = img.key;
    // verificar si es una imagen correcta o incorrecta
    var prev_actual='';

    prev_actual = es_correcto(nombre_img);
    console.log("CURRENT",prev_actual);
    if (!prev_actual) {
        prev_actual = Elemento_by_nombre(nombre_img);
        $('.abeja-prev-mensaje-img').attr('src', public_url+'img/Cross.png');
        $('.abeja-prev-mensaje-des').html('Incorrecto :(');
    }
    else
    {
        $('.abeja-prev-mensaje-img').attr('src', public_url+'img/Tick.png');
        $('.abeja-prev-mensaje-des').html('Muy  bien :)');

        // actualizar puntaje
        puntaje+=puntaje+puntaje_x_correcto;
        $('#puntaje_acumulado').html(Math.round(puntaje));

    }
    // $('.abeja-prev-mensaje').css('opacity','0');
    
    // construir objeto dom 
    var mensaje = '';
    
    
    // if (imgs_atrapadas==prev_imagenes.length) {
    //     $('.mapa_prev_final').html('<div class="content">Felicidades</div>');
    // };

}

function render () {

	
    // game.debug.text(game.time.physicsElapsed, 32, 32);
    // game.debug.body(player);
    // game.debug.cameraInfo(game.camera, 32, 32);
    // game.debug.bodyInfo(player, 16, 24);

}
function es_correcto(nombre)
{
    for (var i = prev_correctos.length - 1; i >= 0; i--) {
            
            if(prev_correctos[i].valor.nombre==nombre)
            {
                return prev_correctos[i];
            }
           
        }
    return false;
}
function Elemento_by_nombre(nombre)
{
        
        for (var i = prev_imagenes.length - 1; i >= 0; i--) {
            
            if(prev_imagenes[i].valor.nombre==nombre)
            {
                return prev_imagenes[i];
            }
           
        }
        return null;
}

</script>
