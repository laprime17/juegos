	<?php $this->load->view('administracion/layouts/header.php'); ?>
		<div class="row" data-equalizer>
			<?php $this->load->view('administracion/layouts/side_administrador.php'); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<div class="administrador-panelgruposysesiones">
					<div class="lbl2 ">Grupos y Sesiones</div>
					<div class="contenedor_gris ">
					<ul class="tabs" data-tab>
					    <li class="tab-title active"><a href="#panel2" class="traer_usuarios"> Nuevo Grupo</a></li>
					    <li class="tab-title"><a href="#panel1" class="traer_usuarios">Todos los grupos</a></li>
					</ul>
					<hr>
					<div class="tabs-content">
					   <div class="content " id="panel1">
					      	<table class="w100">
					      		<thead>
					      			<tr>
					      				<th>N°</th>
					      				<th>Nombre del grupo</th>
					      				<th>Propietario</th>
					      				<th>Total Sesiones </th>
					      				<th>Opciones</th>
					      			</tr>
					      		</thead>
					      		<tbody></tbody>
								<?php $i =1;
								
								 foreach ($grupos as $grupo) {
								?>
									<tr>
									<?php  
									echo '<td>'. $i++ .'</td><td>'.$grupo->nombre.'</td><td>'.$grupo->usuario_propietario.'</td><td>'.$grupo->nro_sesiones.'</td><td><a href="'.base_url().'administrador/gruposysesiones/'.$grupo->idgrupo.'" class="secondary xsmall button">Ver detalle</a><a data-reveal-id="modal_eliminar_grupo" class="button alert btn_eliminar_grupo xsmall" onclick="set_idgrupo_elim('.$grupo->idgrupo.')"><span class="icon-cross"></span> Eliminar</a></td>';
									?>
									</tr>
									<?php } ?>
												
							
							</table>
					   </div>
					    <div class="content active" id="panel2">
					      
					      <!-- inicio subpanel -->
					      <form action="" enctype="multipart/form-data" method="post" class="form_nuevogrupo">
					      	<input type="hidden" name="lista_alumnos" id="lista_alumnos" value="">
					      	<div class="row">
					      	<div class="columns large-5">
					      		<label for="nombres">Nombre del Grupo</label>
					      			<input type="text" name="nombre" placeholder="2do de primaria sección B " id="nombres">
					      	</div>
					      	<div class="columns large-4">
					      		<label for="">Rango de Edad</label>
					      		<div class="row">
					      			<div class="columns large-5"><input type="text" placeholder="e mínima" name="edad_minima"></div>
					      			<div class="columns large-2">-</div>
					      			<div class="columns large-5"><input type="text" placeholder="e máxima" name="edad_maxima"></div>
					      		</div>
					      	</div>
					      	<div class="columns large-3 ta_right">
					      		<div class="button  small" id="btn_guardargrupo">Guardar Grupo</div>
					      	</div>
					      	</div>
					      	
					      	
					      	<div class="columns large-12"><label for="">Docente</label>
					      						      	<input type="text" id="responsable" readonly value="<?php echo $this->session->userdata('nombre_completo'); ?> "></div>

					      	<!-- Administrador -->


					      	<div class="columns large-12">
					      		<label for="">Asignar Alumnos</label>
					      			<input type="text" id="bnombres"  placeholder="Nombres Apellidos - PRESIONA ENTER" class="buscar_persona" ></div>
					      	
					      	<div class="columns large-12">
					      		<table class="w100 tabla_small">
					      			<thead>
					      				<tr>
					      					<th style="width:8%;"></th>
					      					<th style="width:8%;">N°</th>
					      					<th style="width:51%;">Nombre completo</th>
					      					<th style="width:15%;">Usuario</th>
					      					<th style="width:19%;">Organización</th>
					      				</tr>
					      			</thead>
					      			<tbody id="tabla_usuarios">

					      				<?php $i =1;foreach ($alumnos as $alumno) {
					      					
					      					?>
					      					<tr>
					      						<td><input class='usuario_elegido' data-id='<?php echo $alumno->idusuario;?>' type="checkbox" value="<?php echo $alumno->idusuario;?>" name="alumnos[]" onchange="Check_usuario(this);"></td>
					      						<td><?php echo $i;$i++; ?></td>
					      						<td><?php 
					      						if ($alumno->idtipo_usuario<3) {
					      							echo '<div class="label success">'.substr($alumno->rol,0,3).' </div>';
					      						}
					      						echo $alumno->nombres.' '.$alumno->ap.' '.$alumno->am;?></td>
					      						<td><?php echo $alumno->usuario;?> </td>
					      						<td>-</td>
					      					</tr>
					      					<?php 
					      				} ?>
					      			</tbody>
					      		</table>
					      	</div>
					      	
					      	
					      </form>
					   </div>
					  
					</div>
					</div>
						
							
						
					
					
						
						
				
				
			
			</div>
		</div>

<!-- modales -->
<div id="modal_eliminar_grupo" class="reveal-modal tiny" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<div class="modal_label">Presione [Esc] para salir</div>
  	<div class="lbl2 ">El grupo y las sesiones asociadas se eliminarán permanentemente</div>
	
  <button  id="btn_confirmar_elim_grupo" aria-label="Close modal" type="button">
    Confirmar
  </button>
  <div>
  	<span class="success label dis_none elimg_alert_correcto">Grupo eliminado correctamente.</span>
    <span class="alert label dis_none elimg_alert_incorrecto">Error al eliminar grupo.</span>
  </div>
   
  
    
  
</div>


<!-- fin modales -->

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/general.js"></script>
<?php echo "<script>
	ruta_formnuevogrupo = '".$ruta_formnuevogrupo."';
</script>"; ?>


<script>

	$(document).on('ready',ini);
	function ini()
	{
		idgrupo_elim = -1;
		$('.btn_eliminar_grupo').click(function(){

		});
		$('#btn_confirmar_elim_grupo').click(Eliminar_grupo);


		Elegidos = Array();
	$('#btn_guardargrupo').click(guardar_grupo);
	
			Pintar_elegidos();
		
		$('#btn_nuevogrupo').click(nuevo_grupo);
		$('.lista_grupos-item').click(function(){
			$.ajax({
			       type: "POST",
			       url: base_url+'administrador/get_sesiones_x_grupo/'+$(this).data('idgrupo'),
			       data: formData,
			       dataType:"json", 
			       cache: false,
	               contentType: false,
		     	   processData: false,
			       success: function(data)
			       {
			       		var cadena ='';
			          	$.each(data,function(index,sesion){

			          	cadena +='<div>'+sesion.nombre+'</div>';

			          });
			          	$(this).children('.sesiones').append(cadena);


			       }, //fin success
			    	 error: function(data) {

	                    console.log(data);
	                }
			    });	
		});
		
		$('.buscar_persona').keypress(function(e){
			if (!e) e = window.event;
		    var keyCode = e.keyCode || e.which;
		    if (keyCode == '13'){
		      	
		      	Traer_Conincidencias();

		    }
		});
		    
		  
	}
	function set_idgrupo_elim(idgrupo)
	{
		idgrupo_elim = idgrupo;
	}
	function Eliminar_grupo()
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/eliminar_grupo/'+idgrupo_elim,
		       dataType:"json",
		       success: function(data)
		       {
		       	if (data['result']==1) {
		       		$('.elimg_alert_correcto').addClass('dis_block');
		       	}
		       	else
		       	{
		       		$('.elimg_alert_correcto').removeClass('dis_block');
		       		$('.elimg_alert_incorrecto').addClass('dis_block');

		       	}
		       		
		       }, 
		    	 error: function(data) {

                    console.log(data['error']);
                    $('.elimg_alert_correcto').removeClass('dis_block');
		       		$('.elimg_alert_incorrecto').addClass('dis_block');
                }
		    });	
	}
	function Check_usuario(objeto)
		{
			var idusuario = $(objeto).data('id');
				if($(objeto).is(':checked'))
				{
					console.log('sjsjs');
					console.log(idusuario);
					Elegidos.push(idusuario);
				}
				else
				{
					var index = Elegidos.indexOf(idusuario);
					Elegidos.splice(index,1);
				}
			
		}
	function Pintar_elegidos()
	{
		$('.usuario_elegido').each(function(){
						var idu = $(this).attr('data-idusuario');
			if(Elegidos.includes(idu))
			{
				$(this).parent().addClass('usuario_seleccionado');
				$(this).checked();
			}
		});
	
	}
	function elegir_usuario($id)
	{

	}
	function Traer_Conincidencias()
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/get_usuarios',
		       data: 'nombres='+$('#bnombres').val(),		       
		       dataType:"json",
		       success: function(data)
		       {
		       	var i = 0;
		       	var cadena = '';
		       	$.each(data,function(index,user){

		       		var tag ='';
		       		if (user.idtipo_usuario<3) {
		       			tag = '<div class="label success">'+user.rol+' </div>';
		       		}


		       		var checked = '';
		       		if (Elegidos.includes(parseInt(user.idusuario))) {
		       			checked = ' checked ';
		       			console.log('entro');
		       		};


		       		 cadena += "<tr><td class='usuario_elegido' > <input "+checked+" type='checkbox' name='alumnos[]' data-id='"+user.idusuario+"' onchange='Check_usuario(this);'></td><td>"+ ++i +"</td><td>"+tag+user.nombres+" "+user.ap+" "+user.am+"</td><td>"+user.usuario+"</td><td>"+user.organizacion+"</td></tr>";
		       	});
		       		$('#tabla_usuarios').html(cadena);
		       }, 
		    	 error: function(data) {

                    console.log(data);
                }
		    });	
	}
	 function nuevo_grupo()
	{
		$(".subpanel2").load(ruta_formnuevogrupo);
	}
	function guardar_grupo()
	{
		$('#lista_alumnos').val(JSON.stringify(Elegidos));
		formData = new FormData($(".form_nuevogrupo")[0]);
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/guardar_grupo',
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
               async:false,
	     	   processData: false,
		       success: function(data)
		       {
					$(".subpanel2").html('<div class="panel-mensaje-adm success"><span class="icon-checkmark"></span> El grupo se ha agregado con éxito</div>');
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 
	}
</script>
