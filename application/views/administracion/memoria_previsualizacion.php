<div class="prev">

	<div class="prev-content" style="background: #065D79;">
		<div class="prev-content-titulo row" style="background: #2B7692;">
            <div class="mt03m prev-content-titulo-otros columns large-2" id="tiempo_actividad">
                <div id="hour">00</div>
                <div class="divideruno">:</div>
                <div id="minute">00</div>
                <div class="dividerdos">:</div>
                <div id="second">00</div>
            </div>
			<div class="mt03m prev-content-titulo-titulo columns large-6" style= "text-align: center;"><?php echo $juegomemoria->nombre;?> </div >
			<div class="prev-content-titulo-otros columns large-3"><div class="boton amarillo dib"><span class="icon-arrow-right"></span> Siguiente Actividad</div></div>
		</div>
        <div class="prev-content-datos " style="background: #065D79;">
            <div class="dib prev-content-datos-enunciado"><?php echo $juegomemoria->descripcion;?></div>
            
            <div class="prev-content-datos-puntos" style=" position: relative; top: -75px; margin-bottom: -70px;">
                <div class="puntaje_memoria">
                <div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> 
                </div>
            </div>
            
        </div>
		<figure class="prev-img" id="prev-img">
		  <div class="memoria_prev_final">
            <div class="memoria_prev_final_derecha" id="lateral"></div>
          </div>
		</figure>
		<div class="prev-footer">
				<span class="icon-arrow-left"></span>
				<span class="icon-arrow-right"></span>
		</div>	
	</div>

	
</div>
<script >
    var puntaje_actividad = <?php echo $juegomemoria->puntaje;?>;
    var tiempo_actividad = <?php echo $juegomemoria->tiempo;?>;
    var nombrecorrecto = new Array();
    var distractorimg1 = new Array();
    var distractorimg2 = new Array();
    var correctoYN='N';
    var distractor1YN='N';
    var distractor2YN='N';

</script>
<?php foreach ($memoriadetalles as $detalles ) { ?>
    <script>nombrecorrecto.push('<?php echo $detalles->nombre_imgfraccion;?>');
            distractorimg1.push('<?php echo $detalles->distractor1;?>');
            distractorimg2.push('<?php echo $detalles->distractor2;?>');
    </script> <?php } ?>
<script >
agregar_resultados_lateral();
function agregar_resultados_lateral()
{
    var divlateral; 
    for (var i = 0; i < 18; i++) {

        divlateral = '<div class ="memoria_prev_final_derecha_elemento" id="elemento_'+i+'">'+nombrecorrecto[i]+'</div>';
        $('#lateral').append(divlateral);
    };
    
}
var puntaje_individual = puntaje_actividad / 18;
function Agregar_puntaje()
{
        console.log('entró a agregar puntos');
        if ($.isNumeric($('#puntaje_acumulado').html()))
        {
            var tmp = parseFloat($('#puntaje_acumulado').html()).toFixed(1);
            //var twoPlacedFloat = parseFloat(yourString).toFixed(2)
            console.log('valor del div ',tmp);
            console.log('puntaje_individual ',puntaje_individual); 
            tmp = parseFloat(tmp) + parseFloat(puntaje_individual);
            tmp = parseFloat(tmp).toFixed(1);
            console.log('suma ',tmp); 
            $('#puntaje_acumulado').html(tmp);
        }
        
}
var tiempo = {hora:0, minuto:0, segundo:0};
var tiempo_corriendo = null;
var texto_tiempo;
correr_tiempo();
function correr_tiempo()
{
    
        tiempo_corriendo = setInterval(function(){

            // Segundos
            tiempo.segundo++;
            if(tiempo.segundo >= 60)
            {
                tiempo.segundo = 0;
                tiempo.minuto++;
            }

            if(tiempo.minuto>=tiempo_actividad)
            {
                console.log('se acabó el tiempo');
                texto_tiempo = game.add.text(300, 250, '¡Se terminó el tiempo! \n  siguiente actividad' , {fill: '#fff' });
                texto_tiempo.fontWeight = 'bold';
                texto_tiempo.fontSize = 40;
                texto_tiempo.font = 'Arial Black';
                texto_tiempo.anchor.setTo(0.5, 0.5);
                texto_tiempo.stroke = "#008CBA";
                texto_tiempo.strokeThickness = 16;
                texto_tiempo.setShadow(2, 2, "#333333", 2, true, false);

                //parar_tiempo();
                clearInterval(tiempo_corriendo);

                game.paused=true; 

            }
            // Minutos
            if(tiempo.minuto >= 60)
            {
                tiempo.minuto = 0;
                tiempo.hora++;
            }

            $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
            $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
            $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);

        },1000); 
}

var memoriaimagen = '<?php echo $juegomemoria->ruta;?>';

var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'prev-img', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.tilemap('matching', public_url+'json/phaser_tiles.json', null, Phaser.Tilemap.TILED_JSON);
    //game.load.image('tiles', public_url+'sprites/memoria/mapa_peru.png');//, 100, 100, -1, 1, 1);    
    game.load.image('tiles', memoriaimagen);//, 100, 100, -1, 1, 1);
    game.load.image('opciones', public_url+'sprites/memoria/opcionesmemoria.png', 270, 180);
}

var timeCheck = 0;
var flipFlag = false;

var startList = new Array();
var squareList = new Array();

var masterCounter = 0;
var squareCounter = 0;
var square1Num;
var square2Num;
var savedSquareX1;
var savedSquareY1;
var savedSquareX2;
var savedSquareY2;

var map;
var tileset;
var layer;

var marker;
var currentTile;
var currentTilePosition;

var tileBack = 25;
var timesUp = '+';
var youWin = '+';

var myCountdownSeconds;
var w = 800, h = 600;
var pos1, pos2, pos3; //Desocupado, Ocupado
var D1establecidoYN = 'N';
var aleatorio;

var ejemploparaborrar;

function create() {

        map = game.add.tilemap('matching');

        map.addTilesetImage('Desert', 'tiles');

        //tileset = game.add.tileset('tiles');
    
        layer = map.createLayer('Ground');//.tilemapLayer(0, 0, 600, 600, tileset, map, 0);

        //layer.resizeWorld();

        marker = game.add.graphics();
        marker.lineStyle(2, 0x00FF00, 1);
        marker.drawRect(0, 0, 100, 100);
    
    randomizeTiles();
}

function update() {
    
    countDownTimer();
    
    if (layer.getTileX(game.input.activePointer.worldX) <= 5) // to prevent the marker from going out of bounds
    {
        marker.x = layer.getTileX(game.input.activePointer.worldX) * 100;
        marker.y = layer.getTileY(game.input.activePointer.worldY) * 100;
    }

    if (flipFlag == true) 
    {
        if (game.time.totalElapsedSeconds() - timeCheck > 0.5)
        {
            flipBack();
        }
    }
    else
    {
        processClick();
    }
}
    
function countDownTimer() {
  
    var timeLimit = 120;
  
    mySeconds = game.time.totalElapsedSeconds();
    myCountdownSeconds = timeLimit - mySeconds;
    
    if (myCountdownSeconds <= 0) 
        {
        // time is up
        timesUp = 'Time is up!';    
    }
}

function processClick() {
   
    currentTile = map.getTile(layer.getTileX(marker.x), layer.getTileY(marker.y));
    currentTilePosition = ((layer.getTileY(game.input.activePointer.worldY)+1)*6)-(6-(layer.getTileX(game.input.activePointer.worldX)+1));
        
    if (game.input.mousePointer.isDown)
        {
        // check to make sure the tile is not already flipped
        if (currentTile.index == tileBack)
        {
            // get the corresponding item out of squareList
                currentNum = squareList[currentTilePosition-1];
            flipOver();
                squareCounter++;
            // is the second tile of pair flipped?
            if  (squareCounter == 2) 
            {
                // reset squareCounter
                squareCounter = 0;
                square2Num = currentNum;
                // check for match
                if (square1Num == square2Num)
                {
                    masterCounter++;    
                    var nroimg = getHiddenTile();

                    pos1 = 'D';
                    pos2 = 'D';
                    pos3 = 'D';

                    aleatorio = Math.floor((Math.random() * 3) + 1);
                    console.log('aleatorio ',aleatorio)
                    if(aleatorio==1)
                    {
                        posy= h/2-60;
                        pos1='O'; //ocupado
                    }
                    else if (aleatorio==2) 
                    {
                        posy= h/2;
                        pos2='O';
                    }
                    else if (aleatorio==3) {
                        posy= h/2+60;
                        pos3='O';
                    }
                    game.paused = true;
                    opcionimg = game.add.sprite(w/2, h/2, 'opciones');
                    opcionimg.anchor.setTo(0.5, 0.5);

                    nombreimg = game.add.text(w/2, posy, nombrecorrecto[nroimg-1], { font: '20px Arial', fill: '#fff' });
                    nombreimg.anchor.setTo(0.5, 0.5);
                   
                    if(pos1=='D'){
                        distract1 = game.add.text(w/2, h/2-60, distractorimg1[nroimg-1], { font: '20px Arial', fill: '#fff' });
                        distract1.anchor.setTo(0.5, 0.5);
                        pos1 = 'O';
                    }
                    else
                    {
                        distract1 = game.add.text(w/2, h/2, distractorimg1[nroimg-1], { font: '20px Arial', fill: '#fff' });
                        distract1.anchor.setTo(0.5, 0.5);
                        pos2 = 'O';
                    }
                    
                    if (pos2=='D') //posicion 2 desocupado?
                    {
                        distract2 = game.add.text(w/2, h/2, distractorimg2[nroimg-1], { font: '20px Arial', fill: '#fff' });
                        distract2.anchor.setTo(0.5, 0.5);
                        pos2 = 'O';        
                    }
                    else if (pos3 =='D') //posicion 3 desocupado?
                    {
                        distract2 = game.add.text(w/2, h/2 +60, distractorimg2[nroimg-1], { font: '20px Arial', fill: '#fff' });
                        distract2.anchor.setTo(0.5, 0.5);
                        pos3 = 'O';
                    };                     
                    
                    game.input.onDown.add(opciones, self); 

                    if (masterCounter == 18) 
                    {
                        // go "win"
                        youWin = 'Got them all!';
                    }                       
                }
                else
                {
                    //console.log('cuando es diferente');
                    savedSquareX2 = layer.getTileX(marker.x);
                    savedSquareY2 = layer.getTileY(marker.y);
                        flipFlag = true;
                        timeCheck = game.time.totalElapsedSeconds();
                }   
            }   
            else
            {
                savedSquareX1 = layer.getTileX(marker.x);
                savedSquareY1 = layer.getTileY(marker.y);
                    square1Num = currentNum;
            }           
        }           
    }    
}
function opciones(event)
{
    if(game.paused){
    var x1 = w/2 - 270/2, x2 = w/2 + 270/2;
    var y1 = h/2 - 180/2, y2 = h/2 + 180/2;
    if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
        // The choicemap is an array that will help us see which item was clicked
        var choisemap = ['Uno', 'Dossss','tres'];
        
        var x = event.x - x1,
            y = event.y - y1;

        // Calculate the choice 
        var choise = Math.floor(x / 270) + Math.floor(y / 60);

        //var choise = Math.floor(x / 90) + 3*Math.floor(y / 90);
        //console.log('opcion ', choisemap[choise]);
        // Display the choice
        if(choise == (aleatorio-1))
        {
            console.log('Respuesta correcta');
            Agregar_puntaje();
            //console.log('nombre de la imagen ',nombreimg.text);
            //pintar barra lateral
            // for (var i = 0; i < 18; i++) {
            //     if(nombrecorrecto[i]==nombreimg.text)
            //     {
            //         console.log('pintar');
            //         $('#elemento_'+i).css('background','#fff');
            //     }
            // };

        }
                
    }

    
        if(correctoYN=='Y'){
            correctoYN='N';
            
        }
        else if (distractor1YN == 'Y')
        {
            //este valor debe ser guardado en BD como respuesta(equivocada) FALTA
            distractor1YN = 'N';
        }
        else if (distractor2YN =='Y')
        {
            //este valor debe ser guardado en BD como respuesta(equivocada) FALTA
            distractor2YN = 'N';
        }

        opcionimg.destroy();
        nombreimg.destroy();
        distract1.destroy();
        distract2.destroy();

        // Unpause the game
        var pares = getHiddenTile();
        //console.log('pintar',pares-1);
        $('#elemento_'+(pares-1)).css('background','#E0E0DC');

        game.paused = false;


        if (masterCounter == 18) 
        {
            // go "win"
            //youWin = 'Got them all!';
        }

    }
}
function flipOver() {
 
    map.putTile(currentNum, layer.getTileX(marker.x), layer.getTileY(marker.y));
}
 
function flipBack() {
        
    flipFlag = false;
    
    map.putTile(tileBack, savedSquareX1, savedSquareY1);
    map.putTile(tileBack, savedSquareX2, savedSquareY2);
 
}
 
function randomizeTiles() {

    for (num = 1; num <= 18; num++)
    {
        startList.push(num);
    }
    for (num = 1; num <= 18; num++)
    {
        startList.push(num);
    }

    // for debugging
    myString1 = startList.toString();

    console.log('myString1 ',myString1);
  
    // randomize squareList
    for (i = 1; i <=36; i++)
    {
        var randomPosition = game.rnd.integerInRange(0,startList.length - 1);

        var thisNumber = startList[ randomPosition ];

        squareList.push(thisNumber);
        var a = startList.indexOf(thisNumber);

        startList.splice( a, 1);
    }
    
    // for debugging
    myString2 = squareList.toString();
  
    for (col = 0; col < 6; col++)
    {
        for (row = 0; row < 6; row++)
        {
            map.putTile(tileBack, col, row);
        }
    }
}

function getHiddenTile() {
        
    thisTile = squareList[currentTilePosition-1];
    return thisTile;
}

function render() {

    //game.debug.text(timesUp, 620, 208, 'rgb(0,255,0)');
    //game.debug.text(youWin, 620, 240, 'rgb(0,255,0)');

    //game.debug.text('Time: ' + myCountdownSeconds, 620, 15, 'rgb(0,255,0)');

    //game.debug.text('squareCounter: ' + squareCounter, 620, 272, 'rgb(0,0,255)');
    //game.debug.text('Matched Pairs: ' + masterCounter, 620, 304, 'rgb(0,0,255)');

    //game.debug.text('startList: ' + myString1, 620, 208, 'rgb(255,0,0)');
    //game.debug.text('squareList: ' + myString2, 620, 240, 'rgb(255,0,0)');


    //game.debug.text('Tile: ' + map.getTile(layer.getTileX(marker.x), layer.getTileY(marker.y)).index, 620, 48, 'rgb(255,0,0)');

    // game.debug.text('LayerX: ' + layer.getTileX(marker.x), 620, 80, 'rgb(255,0,0)');
    // game.debug.text('LayerY: ' + layer.getTileY(marker.y), 620, 112, 'rgb(255,0,0)');

    // game.debug.text('Tile Position: ' + currentTilePosition, 620, 144, 'rgb(255,0,0)');
    // game.debug.text('Hidden Tile: ' + getHiddenTile(), 620, 176, 'rgb(255,0,0)');
}
</script>

