<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>

			<?php
			if ($this->session->userdata('rol') ==null) {
			echo "Error. Por favor debe 
			<a href='".public_url()."'>iniciar sesión.</a>";
			die;
			}
			

			$side = 'administracion/layouts/side_'.$this->session->userdata('rol').'.php';

			$this->load->view($side); ?>
			
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<div class="lbl2 pr" >Gestión de Usuarios <a  href="<?php echo base_url();?>administrador/nuevo_usuario" class="btn_default small pa r0" id="btn_nuevogrupo"><span class="icon-plus"></span> Nuevo usuario</a></div>
				
				
				<form action="" enctype="multipart/form-data" method="post" id="form_buscar_usuario">
					<input type="text" class="text" id="buscar_nombres" name="nombres" placeholder="Buscar: Nombres ...presione enter ">
				</form>
				<div class="contenedor_gris  cerrado" id="cont_coincidencias">
					<table class="tabla_small w100"><thead><tr><th>N°</th><th>Nombre Completo</th><th>Usuario</th><th>Opciones</th></tr></thead>
						<tbody id="t_user_found">

						</tbody>
					</table>
					
				</div>
				<?php if ($this->session->userdata('rol')=='administrador') {
					echo "<span>Todos los usuarios</span>";
				}
				else if ($this->session->userdata('rol')=='profesor') 
				echo "<span>Usuarios registrados por ".$uusuario['nombre_completo']."</span>";
				 ?>
				
				


				<div class="contenedor_gris ">
				<ul class="tabs" data-tab>
					<?php if($this->session->userdata('rol')=='administrador')
					{?>

					<li class="tab-title active"><a href="#panel1" class="traer_usuarios" onclick="get_usuarios(1);">Administradores</a></li>
					<?php 
					} ?>
				    
				    <li class="tab-title"><a href="#panel2" class="traer_usuarios" onclick="get_usuarios(2);" >Profesores</a></li>
				    <li class="tab-title"><a href="#panel3" class="traer_usuarios" onclick="get_usuarios(3);">Alumnos</a></li>
				</ul>

				<div class="tabs-content">
				   <div class="content active" id="panel1">
				      
				   </div>
				    <div class="content active" id="panel2">
				      
				   </div>
				    <div class="content active" id="panel3">
				      
				   </div>
				</div>
				</div>
			

				
				
					
			</div>
		</div>
<div id="modal_resetear_clave" class="reveal-modal tiny" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<div class="modal_label">Presione [Esc] para salir</div>
  	<div class="lbl2 ">Resetear clave</div>
  <form action="" enctype="multipart/form-data" method="post" class="form_reset">
  	Nueva Clave
  	<input type="password" name="clave" >
  </form>
  <button  id="btn_confirmar_cambio_clave" aria-label="Close modal" type="button">
    Confirmar
  </button>
  <div>
  	<span class="success label dis_none alert_correcto">Clave reseteada correctamente.</span>
    <span class="alert label dis_none alert_incorrecto">Error al resetear clave.</span>
  </div>
   
  
    
  
</div>


<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/general.js"></script>
<script src="<?php echo public_url();?>js/administracion/inicio.js"></script>



