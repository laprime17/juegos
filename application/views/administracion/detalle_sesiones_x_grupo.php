<?php $this->load->view('administracion/layouts/header.php'); ?>
	<div class="row" data-equalizer>
		<?php $this->load->view('administracion/layouts/side_'. $this->session->userdata('rol') .'.php'); ?>
		<div class="columns large-9 administrador-panel" data-equalizer-watch>
			<?php echo $breadcumb;?>
			<div class="administrador-panelgruposysesiones">
				<div class="lbl2 db">Grupo: <?php echo $grupo_actual->nombre;?></div>
			    <div class="contenedor_gris">
			    	    <ul class="tabs" data-tab>
			    		    <li class="tab-title"><a href="#panel_nueva_sesion">Nueva Sesión</a></li>
			    		    <li class="tab-title active"><a href="#panel_sesiones">Sesiones</a></li>
			    		    <li class="tab-title"><a href="#panel_participanes">Participantes</a></li>
			    		    
			    		</ul>
			    		<div class="tabs-content">
			    		  <div class="content active" id="panel_sesiones">
			    		  	<div>Se muestran las últimas 10 sesiones por estado.</div>
			    	          <div class="label">Sesiones Programadas</div>
			    	          <table class="w100 tabla_small ">
			    	          	<thead class="ta_center">
			    	          		<tr>
			    	          			<th width="4%" class="ta_center">N°</th>
			    	          			<th width="40%">Nombre</th>
			    	          			<th width="11%">Actividades</th>
			    	          			<th width="11%">Fecha Programada</th>
			    	          			<th width="34%">Opciones</th>
			    	          		</tr>
			    	          		<tbody id="tsesionesp">
			    	          			<?php $i=1;foreach ($sesionesp as $sesion) {
			    	          				?>
			    	          				<tr>
			    	          					<?php if ($sesion->idsesion==null) {
			    	          						echo '<td colspan="5">No hay actividades</td>';
			    	          					}
			    	          					else{ ?>

			    	          					<td><?php echo $i;$i++; ?></td>
			    	          					<td><?php echo $sesion->nombre; ?></td>
			    	          					<td><?php echo $sesion->nro. ' actividades';?> </td>
			    	          					<td><?php echo $sesion->fecha_programada;?></td>
			    	          					<td>
			    	          						<div class="button secondary  xsmall"><span class="icon-pencil2"></span> Cambiar nombre</div>

			    	          						<div class="button secondary  xsmall"><span class="icon-pencil2"></span> Reprogramar</div>

			    	          						<div class="button alert xsmall"><span class="icon-cross"></span></div>

			    	          					</td>
			    	          					<?php } ?>
			    	          				</tr>
			    	          				<?php 
			    	          			} ?>
			    	          		</tbody>
			    	          	</thead>
			    	          </table>
			    	          <div class="label">Sesiones Ejecutadas</div>
			    	          <table class="w100 tabla_small">
			    	          	<thead>
			    	          		<tr>
			    	          			<th>N°</th>
			    	          			<th>Nombre</th>
			    	          			<th>Actividades</th>
			    	          			<th>Fecha Programada</th>
			    	          			<th>Opciones</th>
			    	          		</tr>
			    	          		<tbody>
			    	          			<?php $i=1;foreach ($sesionesj as $sesion) {
			    	          				?>
			    	          				<tr>
			    	          					<?php if ($sesion->idsesion==null) {
			    	          						echo '<td colspan="5">No hay actividades</td>';
			    	          					}
			    	          					else{ ?>

			    	          					<td><?php echo $i;$i++; ?></td>
			    	          					<td><?php echo $sesion->nombre; ?></td>
			    	          					<td><?php echo $sesion->nro. ' actividades';?> </td>
			    	          					<td><?php echo $sesion->fecha_programada;?></td>
			    	          					<td><div class="button success xsmall"><span class="icon-pencil2"></span> Editar</div></td>
			    	          					<?php 
			    	          					} ?>
			    	          				</tr>
			    	          				<?php 
			    	          			} ?>
			    	          		</tbody>
			    	          	</thead>
			    	          </table>
			    	          <!-- ********************** -->
			    	          <div class="alert label">Sesiones Eliminadas</div>
			    	           <table class="w100 tabla_small">
			    	          	<thead>
			    	          		<tr>
			    	          			<th>N°</th>
			    	          			<th>Nombre</th>
			    	          			<th>Actividades</th>
			    	          			<th>Fecha Programada</th>
			    	          			<th>Opciones</th>
			    	          		</tr>
			    	          		<tbody id="tsesionesa">
			    	          			<?php $i=1;foreach ($sesionesa as $sesion) {
			    	          				?>
			    	          				<tr>
			    	          					<?php if ($sesion->idsesion==null) {
			    	          						echo '<td colspan="5">No hay actividades</td>';
			    	          					}
			    	          					else{ ?>

			    	          					<td><?php echo $i;$i++; ?></td>
			    	          					<td><?php echo $sesion->nombre; ?></td>
			    	          					<td><?php echo $sesion->nro. ' actividades';?> </td>
			    	          					<td><?php echo $sesion->fecha_programada;?></td>
			    	          					<td><div class="button success xsmall"><span class="icon-pencil2"></span> Editar</div></td>
			    	          					<?php } ?>
			    	          				</tr>
			    	          				<?php 
			    	          			} ?>
			    	          		</tbody>
			    	          	</thead>
			    	          </table>
			    		  </div>
			    		  <div class="content" id="panel_participanes">
			    		  	Agregar participante
			    		  	<form action="" enctype="multipart/form-data" method="post" id="form_agregar_user_grupo">
			    		  	<input type="text" placeholder="Nombre...presione enter" name="nombres">
			    		  	</form>

			    		  	<table class="w100 tabla_small">
			    						<thead>
			    							<tr>
			    								
			    								<th style="width:8%;">N°</th>
			    								<th style="width:51%;">Nombre completo</th>
			    								<th style="width:15%;">Usuario</th>
			    								<th style="width:19%;">Opciones</th>
			    							</tr>
			    						</thead>
			    						<tbody id="tabla_agregarnuevo">
			    						</tbody>
							</table>
							<hr>
			    			<table class="w100 tabla_small">
			    						<thead>
			    							<tr>
			    								
			    								<th style="width:8%;">N°</th>
			    								<th style="width:51%;">Nombre completo</th>
			    								<th style="width:15%;">Usuario</th>
			    								<th style="width:19%;">Opciones</th>
			    							</tr>
			    						</thead>
			    						<tbody id="tabla_usuarios">
			    							<?php $i =1;foreach ($participantes as $alumno) {
			    								?>
			    								<tr>
			    									
			    									<td><?php echo $i;$i++; ?></td>
			    									<td><?php 
			    									if ($alumno->idtipo_usuario<3) {
			    										echo '<div class="label success">'.substr($alumno->rol,0,3).' </div>';
			    									}
			    									echo $alumno->nombres.' '.$alumno->ap.' '.$alumno->am;?></td>
			    									<td><?php echo $alumno->usuario;?> </td>
			    									<td><a class=" small">Eliminar</div> </td>
			    								</tr>
			    								<?php 
			    							} ?>
			    						</tbody>
			    			</table>
			    		  </div>
			    		  
			    		  <div class="content " id="panel_nueva_sesion">
			    		  	
			    		  	
			    		  	<input type="hidden" id="idgrupo" value="<?php echo $grupo->idgrupo;?>">
			    		  	<div class="subpanel2 w100">
			    		  		<form action="<?php echo base_url();?>administracion/guardar_sesion" enctype="multipart/form-data" method="post" class="form_nueva_sesion">
			    		  			<div class="row">
			    		  				<div class="adm-contenedor_alertas">
									  	<span class="success label dis_none alert_nueva_sesion_correcto">Sesión guardada correctamente.</span>
									    <span class="alert label dis_none alert_nueva_sesion_incorrecto">Error al guardar sesión. Por favor comuníquese con el administrador.</span>
									  </div>
			    		  				<div class="columns large-6">
			    		  					
			    		  					<label for="nombres">Nombre de sesión</label>
				    		  				
				    		  				
				    		  					<input type="text" id="nombre" placeholder="El Reino Vegetal" id="nombre" class="obligatorio">
				    		  				
			    		  				</div>
			    		  				<div class="columns large-3">
			    		  					<label for="nombres">Fecha de Creación</label>
			    		  					<input type="text" readonly name="fecha_creacion" placeholder="2do de primaria sección B " id="fecha_creacion" value="<?php echo hoy_d_m_a(); ?> ">
			    		  				</div>
			    		  				<div class="columns large-3">
			    		  					<label for="nombres">Fecha programada</label>
			    		  					<input type="date" name="fecha_programada" placeholder="2do de primaria sección B " id="fecha_programada" class="obligatorio">
			    		  				</div>
			    		  				
			    		  			</div>
			    		  			
									<div class="row">
										<div class="columns large-6">
					    		  			<div class="row">
					    		  				<div class="columns large-12">
					    		  					<div class="label primary">Agregar Actividades </div>
					    		  				</div>
					    		  				<div class="columns large-12">
					    		  					<input type="text" placeholder="Buscar..."> 
					    		  				</div>
					    		  				<div class="columns large-12">
													<span>Se muestran las últimas 10 actividades registradas</span>
					    		  					<table class="tabla_small w100">
			    		  								<thead>
			    		  									<tr>
			    		  										<th style="width:70%;">Nombre</th>
			    		  										<th>Creación</th>
			    		  										<th>Opciones</th>
			    		  									</tr>
			    		  								</thead>
					    		  						<tbody class="todas_act">	
						    		  						<?php foreach ($juegos as $juego) {?>
					  										<tr>
					  											<td><?php echo $juego->nombre;?></td>
					  											<td><?php echo $juego->fecha_creacion;?> </td>
					  											<td><div class="btn_default incluir_actividad" data-idact="<?php echo $juego->idjuego;?>"><span  class="icon-plus"></span> Incluir</div> </td>
					  										</tr>
					  										<?php 
					  										} ?>
					    		  						</tbody>
					    		  					</table>
					    		  				</div>
					    		  			</div>	
										</div>
										<div class="columns large-6">
					    		  			<div class="row">
					    		  				<div class="columns large-12">
					    		  					<div class="label primary">Actividades incluidas</div>
									    			<span class="alert label dis_none alert_juegos_vacio">Debe incluir por lo menos 1 juego.</span>

					    		  				</div>
					    		  				<div class="columns large-12">
				    		  						<table class="tabla_small w100">
				    		  							<tbody class="tabla_incluidos">
				    		  							</tbody>
				    		  						</table>
					    		  				</div>
					    		  			</div>
				    		  			</div>
			    		  			</div>

			    		  			<input type="hidden" name="idpersona_creador" value="<?php echo $this->session->userdata('id');?>">
			    		  			<div class="ta-right columns large-12">
			    		  				<div class="button  guardar_sesion" >Guardar</div>
			    		  				
			    		  			</div>
			    		  			



			    		  	</form>
			    		  	</div>
			    		  	
			    		  	
			    		  </div>
			    		</div>
			    </div>
			</div>				
		</div>
	</div>

<!-- modales -->
<div id="modal_nuevasesion" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" src="" frameborder="0"></iframe>
</div>
<!-- fin modales -->
<?php echo "<script>
	idgrupo = '".$grupo_actual->idgrupo."';
	
</script>"; ?>
<script src="<?php echo public_url();?>js/administracion/general.js"></script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script>
	$(document).foundation();

	$(document).ready(function(){



		orden = 1;
		Lista_actividades =  new Array();
		$('.guardar_sesion').click(guardar_sesion);


		$('.incluir_actividad').click(function(){
			idjuego = $(this).data('idact');
			Incluir_juego(idjuego);

		});


	});
	function validar_nuev_sesion()
{
	var A= $('.form_nueva_sesion .obligatorio');
	var b=true;
	$.each(A,function(i,item)
	{
		if ($(item).val()=='') {
			$(item).css('border-color','salmon');
			b &=false;
		}
		else
		{
			$(item).css('border-color','#cccccc');
			
		}
	});
	// verificar si el nombre de usuario ya existe

	
	
	return b;
}
	function  Incluir_juego(idjuego)
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/detalle_juego_ajax/'+idjuego,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {

		       	console.log(data);
		       	
		           if (data) 
		           	{

		           		var cadena = '<tr class="juego_agregado-'+data.juego.idjuego+'"><td width="5%">'+ orden++ +'</td><td width="80%">'+data.juego.nombre+'</td><td width="15%"><div href="#" onclick= "quitar_juego('+data.juego.idjuego+')" class="btn_default"><span class="icon-cross"></span> quitar</div> </td></tr>';
		           		$('.tabla_incluidos').append(cadena);
		           		$('.todas_act').html('');
		           		Lista_actividades.push(data.juego.idjuego);
		           		Obtener_juegos();
		           	};

		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax	
	}
	function quitar_juego(idjuego)
	{
		$('.juego_agregado-'+idjuego).remove();
		Lista_actividades.splice(Lista_actividades.indexOf(idjuego),1);
		Obtener_juegos();
	}
	function Get_sesionesp()
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/get_sesionesp_ajax/'+idgrupo,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		       	 var cadena = '';
		       	 var i =1;
          			$.each(data,function(id,sesionp){
		           		cadena += '<tr><td>'+ i++ +'</td><td>'+sesionp.nombre+'</td><td>'+sesionp.nro+' actividades</td><td>'+sesionp.fecha_programada+'</td><td><div class="button success xsmall"><span class="icon-pencil2"></span> Editar</div></td></tr>';
		           	});
          		$('#tsesionesp').html(cadena);
		       }, 
		    	 error: function(data) {console.log(data);}
		    });	
	}
	function Obtener_juegos()
	{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/get_juegos_x_creador/',
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		       	
		           if (data) 
		           	{
		           		var cadena = '';
		           		$.each(data,function(id,juego){
		           			if (!Lista_actividades.contains(juego.idjuego) ) {

		           				cadena +='<tr><td>'+juego.nombre+'</td><td>'+juego.fecha_creacion+'</td><td><div class="btn_default incluir_actividad" data-idact="'+juego.idjuego+'" onclick="Incluir_juego('+juego.idjuego+');"><span  class="icon-plus"></span> Incluir</div> </td></tr>';

							}
		           		});

		           		$('.todas_act').html(cadena);
		           	};
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax	
	}
	function guardar_sesion()
	{
   		$('.alert_nueva_sesion_correcto').removeClass('dis_block');
   		$('.alert_nueva_sesion_incorrecto').removeClass('dis_block');
		$('.alert_juegos_vacio').removeClass('dis_block');

		if (!validar_nuev_sesion()) {
			return;
		}
		else 
		{
			if (Lista_actividades.length == 0)
			{

			$('.alert_juegos_vacio').addClass('dis_block');
			}
			
		
		else
		{
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/guardar_sesion/'+idjuego,
		       data: "nombre="+$('#nombre').val()+"&idgrupo="+$('#idgrupo').val()+"&juegos="+ JSON.stringify(Lista_actividades)+"&nombre"+$('#nombre').val()+"&fec="+$('#fecha_creacion').val()+"&fecprog="+$('#fecha_programada').val(),
		       dataType:"json",
		      
		       success: function(data)
		       {
		           if (data) 
		           	{
		           		$('.alert_nueva_sesion_correcto').addClass('dis_block');
		           		$('.alert_nueva_sesion_incorrecto').removeClass('dis_block');
						$('.alert_juegos_vacio').removeClass('dis_block');
						$('.form_nueva_sesion')[0].reset();
						$('.tabla_incluidos').html('');
						Lista_actividades= new Array;

		           		Get_sesionesp();
		           	};
		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax
		}
	}
	}
	
	Array.prototype.contains = function(element) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == element) {
            return true;
        }
    }
    return false;
}
</script>

