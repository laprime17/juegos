<?php $this->load->view('administracion/layouts/header.php'); ?>

<div class="row">
	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_memoria row" style= "padding-bottom: 5px;" data-equalizer>
	<div class="lbl2">Juego "Invasores comprensión"</div>
	<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/memoria_guardarmemoria" method="post"class="form_datos">
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
		
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text"  placeholder="título" name="nombre" id=""/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo" placeholder="minutos"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje de actividad</label></div>
                    <div class=""><input type ="number" name="puntaje"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="curso" id="curso">
                        <option value="Comunicacion">Comunicación</option>
                        <option value="Matematica">Matemática</option>
                        <option value="Ciencia y Ambiente">Ciencias y Ambiente</option>
                        <option value="Personal social">Personal Social</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Edad sugerida:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima"/></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" id="" cols="30" rows="s"></textarea></div>
                </div>
            </div>
		
		<label for="">Explicación del juego</label>
		<div class="invasores_explicacion">
           <span class="gris">Este juego es de comprensión lectora. En este juego una nave (dominada por el usuario) se enfrentará a invasores que rodean las respuestas y demás alternativas de las preguntas del texto en cuestión.  <br/> Esta nave se mueve en ambos sentidos, derecha-izquierda, (usar la teclas de dirección) y dispara dos tipos de balas; la primera es de destruccion (tecla D) y la segunda sirve para atrapar el objetivo (tecla A). </span> 
           <label class="gris" for="">Pasos</label>
           <span class="gris"> - Redactar o copiar un texto en el panel del lado derecho de esta ventana. <br/>
                               - Escribir el número de preguntas que tendrá la actividad y hacer click en Cargar. <br/>
                               - Por cada pregunta existe una respuesta y dos alternativas como distractores, completarlas.  <br/>
                               - Guardar la edición.  <br/>
                               - Previsualizar la edición.</span>
        </div>

            <input type="hidden" name="idinvasores" id="invasores_id_invasores" value="-1">
            <!-- <input type="hidden" name="detallesinvasores" id="iddetallesinvasores" value="-1">
            <input type="hidden" name="detallesinvasores_fig" id="iddetallesinvasores_fig" value="-1"> -->
            <input type="hidden" name="detallesinvasores_texto" id="iddetallesinvasores_texto" value="-1">
            <input type="hidden" name="invasores_textoprincipal" id="idinvasores_textoprincipal" value="-1">
        </form>
	</div>
    
    <div class="dib nuevo_juego-vista_previa columns large-8" >
    <form enctype="multipart/form-data"  class="form_imgs recolector-form_imgs">
    
        <div class="label db" id="invasores_imagen_titulo">Comprensión lectora</div> 
        <div class="columns large-12" style="margin-bottom: 15px;">
            <div class=""><textarea  name="textoprincipal" id="editor"></textarea></div>
        </div>
        <div class="row" style="margin: 3px;">
            <div class="column large-4" style="margin-top: 7px;">
             <div class=""><label for="">Número de preguntas</label></div>
             <div class=""><input type ="number" name="nroejercicios_comprension" id="nroejercicios_comprension1"/></div>
            </div>
            <div class="column large-4" style="margin-top: 7px;">
               <div   class="button" style= "background: #E64A30;" id="nroejercicios_comprension"> Cargar </div>
            </div>
        </div>
        <div class="label db" id="invasores_imagen_titulo">Completa las preguntas</div>
        <div id="actividad_comprension" style ="margin: 20px;"></div>
    </form>
    </div>
</div>
    <div id="aviso_guardado"></div>
    <section class="nuevo_invasores-botones">
        <div   class="button  guardar " id="guardardatos"> Guardar </div>
        <a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
        <div   class="button  alert " id="cancelar_todo"> Cancelar </div>
    </section>        
</div>
</div>

<div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
</div>

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/nuevo_invasorescomprension.js"></script>
<script>

</script>