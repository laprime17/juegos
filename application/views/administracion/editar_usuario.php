<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>
			<?php $this->load->view('administracion/layouts/side_'.$this->session->userdata('rol').'.php'); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<h4><?php echo $usuario->nombres.' '.$usuario->ap.' '.$usuario->am; ?> </h4>
<div class="row">
	<div>
  	<span class="success label dis_none alert_correcto">Los datos fueron guardados correctamente.</span>
    <span class="alert label dis_none alert_incorrecto">Error al guardar usuario.</span>
  </div>
				<div class=" nuevo_usuario-contfoto columns large-3">
			
					<figure>
					<img id="nuevo_usuario-foto" src="
					<?php if ($usuario->foto_ruta != '')
						{echo public_url().'fotos/'.$usuario->foto_ruta;}
						else
						{echo public_url().'fotos/default.jpg';}
					 ?>
					

					" alt="">
					</figure>
					<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/subir_archivo" method="post" class="form_imagen">
						<div class="btn_default dib  small file_wcss">
			                <div class="content"><span class="icon-images"></span><span id="label_subir_foto">Cambiar </span> </div>
			                <input type="file" name="userfile"  class="input_file dib" id="userfile">
			            </div>
			        </form>
				</div>
				
				<form action="" enctype="multipart/form-data" method="post" id="form_nuevousuario" class="columns large-9">

					<input type="hidden" id="idusr_edicion" value="<?php echo $usuario->idusuario;?>" >
						<input type="hidden" id="ruta_foto" name="foto_ruta" value="<?php echo $usuario->foto_ruta; ?> ">
						<label for="nombres">Nombre</label>
						<input type="text" name="nombres" placeholder="Nombres" class="obligatorio" id="nombres" value="<?php echo $usuario->nombres;?>">
						<div class="row">
							<div class="columns large-6">
								<label for="">Apellido paterno</label>
								<input type="text" placeholder="Apellido Paterno" class="obligatorio" name="ap" value="<?php echo $usuario->ap;?>">
							</div>
							<div class="columns large-6">
								<label for="">Apellido Materno</label>
								<input type="text" placeholder="Apellido Materno" class="obligatorio" name="am" value="<?php echo $usuario->am;?>">
							</div>
						</div>
						<div class="row">
							<div class="columns large-6">
								<label for="">Fecha de nacimiento</label>
								<input type="date" placeholder="Apellido Paterno" class="obligatorio" name="fecha_nacimiento" value="<?php echo $usuario->fecha_nacimiento;?>">
							</div>
							<div class="columns large-6">
								<label for="">Tipo de Usuario</label>
								<select id="" name="idtipo_usuario">
									<option value="3" <?php if ($usuario->idtipo_usuario ==3) {
										echo " selected ";
									} ?> >Alumno</option>
									<option value="2" <?php if ($usuario->idtipo_usuario ==2) {
										echo " selected ";
									} ?>>Profesor</option>
									<option value="1" <?php if ($usuario->idtipo_usuario ==1) {
										echo " selected ";
									} ?>>Administrador</option>

								</select>
							</div>
						</div>
						<div class="row">
							<div class="columns large-6">
								<label for="">Nombre de usuario</label>
									<span class="alert label dis_none alert_incorrecto_user">El usuario ya existe.</span>
								<input type="text" placeholder="usuario" class="obligatorio" name="usuario" value="<?php echo $usuario->usuario;?>">
							</div>
						</div>
						<div class="button submit" >Guardar cambios</div>
											
				</form>
	
	</div>

			</div>
		</div>

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/inicio.js"></script>
<script src="<?php echo public_url();?>js/administracion/general.js"></script>



