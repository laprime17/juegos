<?php $this->load->view('layouts/head_html'); ?>
<?php $this->load->view('layouts/navbar_administrador');?>
<h1>Nuevo Profesor</h1>

<a href="<?php echo base_url();?>mantenimiento_profesor/form"><div class="btn btn-primary">Nuevo</div></a>
<div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
    Opciones
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ordenar</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Agregar</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Reportes</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Exportar</a></li>
  </ul>
</div>

<form method="post" action="<?php echo base_url();?>mantenimiento_profesor/form">
   <div class="form_control">
      <label>Nombres</label>
      <input type="text" name="nombres">
   </div>

   <div class="form_control">
      <label>Apellidos</label>
      <input type="text" name="apellidos">
   </div>
    <div class="form_control">
      <label>Usuario</label>
      <input type="text" name="apellidos">
   </div>
    <div class="form_control">
      <label>Clave</label>
      <input type="text" name="apellidos">
   </div>
  <div class="form_control">
      <label>Correo</label>
      <input type="text" name="apellidos">
   </div>

    <div class="form_control">
      <label>Foto</label>
      <input id="foto" type="file" name="foto">
   </div>
 <input type="submit" class="btn btn-success" value="Enviar">
</form>

<script>
    $("#file-3").fileinput({
        initialPreview: ["<img src='Desert.jpg' class='file-preview-image'>", "<img src='Jellyfish.jpg' class='file-preview-image'>"],
        overwriteInitial: false,
        maxFileSize: 1000,
        maxFilesNum: 10,
        allowedFileTypes: ['image', 'video', 'flash']
  });
  $("#foto").fileinput({
    showUpload: false,
    showCaption: false,
    browseClass: "btn btn-primary btn-lg",
    fileType: "any"
  });
    $(".btn-warning").on('click', function() {
        if ($('#file-4').attr('disabled')) {
            $('#file-4').fileinput('enable');
        } else {
            $('#file-4').fileinput('disable');
        }
    });
    $('#file-4').on('fileselectnone', function() {
        alert('Huh! You selected no files.');
    });
    $('#file-4').on('filebrowse', function() {
        alert('File browse clicked for #file-4');
    });
    $(document).ready(function() {
        $("#test-upload").fileinput({
            'showPreview' : false,
            'allowedFileExtensions' : ['jpg', 'png','gif'],
            'elErrorContainer': '#errorBlock'
        });
        /*
        $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
            alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
        });
        */
    });
  </script>

