<?php $this->load->view('layouts/head_html'); ?>
<?php $this->load->view('layouts/navbar_administrador');?>

<div class="pad">
	<h2>Administración</h2>
	<div class="side_menu dib">

		<select class="ty_select">
			<option> Alumnos</option>
			<option> Profesores</option>
			<option> Administradores</option>
			<option> Colegios</option>
			<option> Juegos</option>

		</select>
		<ul>
			<li class="mant_li opcion_mant_selected" id="lista_alumnos">Lista</li>
			<li class="mant_li " id="form_alumnos">Nuevo alumno</li>
			<li class="mant_li">Asignar Alumnos</li>
			<li class="mant_li">Reportes</li>

		</ul>

	</div>
	<div class="barra_flecha dib">
		<div class="linea"></div>
		<img src="<?php echo public_url()?>iconos/flechita.png">

	</div>
	<div class="content_menu dib" id="cont_lista_alumnos">
	
		<div class="lista">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Colegio</th>
						<th>Edad</th>
						<th>Género</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>

					<?php foreach ($alumnos as $alumno ) {
					?>

					<tr>
						<td><?php echo $alumno->nombres;?> </td>
						<td><?php echo $alumno->apaterno;?> </td>
						<td><?php echo $alumno->edad;?> </td>
						<td class="<?php 
						if ($alumno->genero=='f') {
							echo 'bg_nina';
						}
						else
						{
							echo 'bg_nino';
						}
						?>" >

						 </td>
						 <td>Opciones</td>
					</tr>

					<?php  
					} ?>
				</tbody>


			</table>	

		</div>

	</div>
	<div class="content_menu dib cont_menu_out" id="cont_form_alumnos">

		<label>Nombres</label>
		 <input type="text">
	</div>

</div>

<script type="text/javascript">
$(function(){
	$('.mant_li').click(function(){

console.log("hola");
		var id_li = $(this).attr('id');
		console.log(id_li);
		var id_section = '#cont_'+id_li;

		console.log($(id_section));
		$(id_section).toggleClass('content_menu_out');
	});


});

</script>