<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>
			<?php $this->load->view('administracion/layouts/side.php'); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<div class="administrador-panelusuario">
					<?php echo $breadcumb; ?>
					<h5><?php echo $persona->nombres.' '.$persona->ap.' '.$persona->am;?> </h5>
					<div class="row">
						<div class="columns large-3">
							<figure>
								<img src="<?php if (!$persona->foto_ruta) {
									echo public_url().'img/foto_default.jpg';} else {
										echo $persona->foto_ruta;} ?> " alt="">
							</figure>
						</div>
						<div class="columns large-9	">
								

						</div>
					</div>
	

				</div>

			</div>
		</div>



<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/obligatorio.js"></script>
