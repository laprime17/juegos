<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>
			<?php $this->load->view('administracion/layouts/side_'.$this->session->userdata('rol').'.php'); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<div class="btn_default" id="btn_nuevaactividad"><span class="icon-plus"></span>  Nueva Actividad</div>
					<div class="lista_juegos">
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearsnake"><img src="<?php echo public_url();?>img/ico_mediano_snake.jpg" alt="Gusanito"></a>
							</figure>
							<figcaption>
								Gusanos
							</figcaption>
						</div>
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearinvasores"><img src="<?php echo public_url();?>img/ico_mediano_invasores.jpg" alt="Invasores"></a>
							</figure>
							<figcaption>
								Invasores
							</figcaption>
						</div>
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearmapa"><img src="<?php echo public_url();?>img/ico_mediano_mapa.jpg" alt="mapa interactivo"></a>
							</figure>
							<figcaption>
								Mapa Interactivo
							</figcaption>
						</div>
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearrecolector"><img src="<?php echo public_url();?>img/ico_mediano_recolector.jpg" alt="recolector"></a>
							</figure>
							<figcaption>
								Recolector
							</figcaption>
						</div>
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearmemoria"><img src="<?php echo public_url();?>img/ico_mediano_memoria.jpg" alt="Memoria"></a>
							</figure>
							<figcaption>
								Memoria
							</figcaption>
						</div>
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearinvasorescomprension"><img src="<?php echo public_url();?>img/ico_mediano_comprension.jpg" alt="Invasores comprensión"></a>
							</figure>
							<figcaption>
								Invasores comprensión
							</figcaption>
						</div>
						<div class="lista_juegos-item">
							<figure>
								<a class="lista_juegos-item-link" href="<?php echo base_url();?>administrador/crearabeja"><img src="<?php echo public_url();?>img/ico_mediano_abeja.jpg" alt="Agrupaciones"></a>
							</figure>
							<figcaption>
								Agrupaciones
							</figcaption>
						</div>
					</div>

				<table class="administrador-paneljuegos-tabla">
					<thead>
						<tr>
							<th class="tipo">Tipo</th>
							<th>Título</th>
							<th class="descripcion">Descripción:</th>
							<th>Detalles</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($juegos as $juego) {
							?>
								<tr>		
									<td><?php echo $juego->tipo;?></td>
									<td style="color:#008cba;"><?php echo $juego->nombre;?></a></td>
									<td><?php echo $juego->descripcion;?></td>
									<td class="ver_actividad"><a href="<?php echo base_url();?>administrador/detalles<?php echo $juego->tipo;?>/<?php echo $juego->idjuego;?>" id="ver_detalles"> Detalles </a></td>		
								</tr>
								<?php
							}
							// foreach ($juegos as $juego) {
							// }
						?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- <div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		  	<iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
		</div> -->

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/general.js"></script>
<script src="<?php echo public_url();?>js/administracion/inicio.js"></script>
<script>

	$(document).on('ready',ini);
	function ini()
	{
		$('#btn_nuevaactividad').click(abrir_lista);
		//idjuego=384;
		//$('.boton_previsualizacion').click(prev);
	}
	function abrir_lista()
	{
		$('.lista_juegos').toggleClass('abierto');
	}
	function prev()
	{
		//$('#iframe_prev').attr('src',base_url+'juegos/invasores_previsualizacion/'+idjuego);
	    //$('#modal_prev').foundation('reveal', 'open', base_url+'juegos/invasores_previsualizacion/'+idjuego);
	}

</script>
