<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>
			<?php $this->load->view('administracion/layouts/side.php'); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<div class="administrador-paneldetalle_grupo">
					<div class="lbl2">
						Asignar alumno
					</div>
					<div class="row">
					    <div class="large-12 columns">
					      <div class="row collapse">
					        <div class="small-10 columns">
					          <input type="text" placeholder="Apellidos ">
					        </div>
					        <div class="small-2 columns">
					          <a href="#" class="button postfix"><span class="icon-search mr_03"></span>Búscar </a>
					        </div>
					      </div>
					    </div>
					  </div>

					  <table>
					  	<thead>
					  		<tr>
					  			<th>Nº</th>
					  			<th>Nombre Completo</th>
					  			<th>Opciones</th>
					  		</tr>
					  	</thead>
					  	<tbody>
					  		<?php $i=0;foreach ($alumnos as $alumno) {
					  			?>
									<tr>
										<td><?php echo $i; $i++;?></td>
										<td><?php echo $alumno->nombres.' '.$alumno->ap.' '.$alumno->am;?></td>
										<td><span class="icon-pencil2"></span></td>
									</tr>

					  			<?php 
					  		} ?>
					  	</tbody>
					  </table>

				</div>

				
			</div>
		</div>

<!-- modales -->

<!-- fin modales -->

<?php $this->load->view('administracion/layouts/footer.php'); ?>

<script src="<?php echo public_url();?>js/administracion/general.js"></script>
<script>

	$(document).on('ready',ini);
	function ini()
	{
		$('#btn_nuevaactividad').click(abrir_lista);
		$('.boton_previsualizacion').click(prev);
	}

</script>
