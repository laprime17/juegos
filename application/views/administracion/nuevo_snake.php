<?php $this->load->view('administracion/layouts/header.php'); ?>

<?php //$this->load->view('layouts/navbar_administradidor');?>

<div class="row">

	<?php $this->load->view('administracion/layouts/side_juegos.php');?>

<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_snake row" style="padding-bottom: 1em;" data-equalizer >
	<div class="lbl2">Nuevo Gusanito</div>
	<form enctype="multipart/form-data" action="#" method="post"class="form_datos">
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
		
			
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text"  placeholder="título" name="nombre" id=""/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo" placeholder="minutos" id="tiempoactividad"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje de actividad</label></div>
                    <div class=""><input type ="number" name="puntaje"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="curso" id="curso">
                        <option value="Comunicacion">Comunicación</option>
                        <option value="CTA">Ciencia y Ambiente</option>
                        <option value="Ciencias sociales">Personal Social</option>
                        <option value="Matematica">Matemática</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Edad sugerida:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima"/></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" id="" cols="30" rows="s"></textarea></div>
                </div>
            </div>
		  <!-- <input type="hidden" name="imagen" id="mapa_src_imagen"> -->
		
		<label for="">Explicación del juego</label>
		<div class="snake_explicacion">
           <span class="gris">En este juego, un gusanito irá 
            comiendo letras; estos caracteres comidos o atrapados por el gusanito
            armarán una "palabra" que identifica a una imagen; esta imagen debe cargarlo usted y asigarle un nombre(el nombre será la "palabra") y una descripción.  <br/>

            El jugador moverá el gusanito con las flechas de dirección del teclado e irá comiendo las letras que se le presenten en el camino.

            </span> 
           <label class="gris" for="">Pasos</label>
           <span class="gris"> - Complete los datos generales del juego (Nombre de la actividad, Duración, etc) <br/>
                               - Cargue una o varias imagenes desde su computador en la parte derecha de esta ventana.  <br/>
                               - Seleccione las imagenes que usará, asignandole un nombre y una descripción. <br/>
                               - Guarde la edición.  <br/>
                               - Previsaulice su edición. </span>
        </div>

<!--         <input type="hidden" name="imagen" id="snake_src_imagen">
 -->        <input type="hidden" name="idsnake" id="snake_id_snake" value="-1">
            <input type="hidden" name="detalles" id="iddetalles" value="-1">
            
        
        </form>
        <!-- <form enctype="multipart/form-data" action="#" method="post"class="form_detalles">
            <input type="hidden" name="detalles" id="iddetalles" value="-1">
        </form> -->
	</div>
    
	<div class="dib nuevo_juego-vista_previa columns large-8" >
        

        <div class="snake-prev_img" id="snake-contenedorimg">
            
        <form enctype="multipart/form-data"  class="form_imgs snake-form_imgs">
            
            <div class="dib">
                <label class="gris" for="">Elija una colección de imágenes:</label>
                <span class="icon-images gris"></span>  
                <span class="gris">Tamaño recomendable 250X250 cuadrado</span>
            </div>
            <div class="btn_default dib  small file_wcss">
                <div class="content"><span class="icon-images"></span> Imágenes</div>
                <input type="file" name="snake_img[]" multiple class="input_file dib" id="file_imgs">
            </div>
        </form>
            
            <label class="gris" for="">Seleccione las imágenes correctas</label>
            <span class="gris">Haz click en cada imagen para seleccionarla y nombrarla</span>

            <div class="snake-galeria">
                <section class="snake-galeria-todos">
                    <div class="label db">Imágenes incorrectas</div>
                
                </section>
                 <section class="snake-galeria-correctos">
                    <div class="label db">Imágenes Correctas</div>
                </section>
            </div>
        </div>
        
    </div>
    <!--</form>-->	
</div>
    <div class="row" data-equalizer>    
    
    </div>
    <div id="aviso_guardado"></div>
        <section class="nuevo_snake-botones">
            <div   class="button  guardar " id="guardardatos"> Guardar </div>
            <a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
            <div   class="button  alert " id="cancelar_todo"> Cancelar </div>
        </section>


</div>
</div>

<!-- <div class="mapa-prev_img" id="mapa-contenedorimg">
</div> -->

<div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
</div>
<script>
    var cont=1;
    localStorage.clear();
</script>		        
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/nuevo_snake.js"></script> 

