<?php $this->load->view('administracion/layouts/header.php'); ?>

<div class="row">
    <?php $this->load->view('administracion/layouts/side_juegos.php');?>
<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_memoria row" style= "padding-bottom: 5px;" data-equalizer>
    <div class="lbl2">Editar juego "Recolector"</div>
    <form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/recolector_guardarrecolector" method="post"class="form_datos">
    <div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text"  placeholder="título" name="nombre" id="titulo"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo" placeholder="minutos" id="tiempo"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje de actividad</label></div>
                    <div class=""><input type ="number" name="puntaje" id="puntaje"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="curso" id="curso">
                        <option value="Comunicacion">Comunicación</option>
                        <option value="Matematica">Matemática</option>
                        <option value="CTA">Ciencias Naturales</option>
                        <option value="Ciencias sociales">Ciencias sociales</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Edad sugerida:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima" id="edad_minima"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima" id="edad_maxima"/></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" cols="30" rows="s" id="descripcion"></textarea></div>
                </div>
            </div>
        
        <label for="">Explicacion del juego</label>
        <div class="recolector_explicacion">
           <span class="gris">Este juego consiste en recolectar ciertos elementos (monedas, billetes, figuras, números, letras); los adecuados para llegar a un determinado objetivo. En el juego mismo, una nave (dominada por el usuario) se enfrentará a invasores que rodean los elementos a buscar; esta nave se mueve a ambos costador, derecha-izquierda, (usar la teclas de dirección) y dispara dos tipos de balas; la primera es de destruccion (tecla D) y la segunda sirve para atrapar el objetivo (tecla A). </span> 
           <label class="gris" for="">Pasos</label>
           <span class="gris"> - Selecccionar la opcion Dinero</span>
           <span class="gris">   Completar el monto que será recolectado</span>
        </div>

            <input type="hidden" name="idrecolector" id="recolector_id_recolector" value="-1">
            <input type="hidden" name="texto_principal" id="idrecolectortexto" value="-1">
            <input type="hidden" name="detallesrecolector" id="iddetallesrecolector" value="-1">
            
        </form>
    </div>
    
    <div class="dib nuevo_juego-vista_previa columns large-8" >
        <form enctype="multipart/form-data"  class="form_imgs recolector-form_imgs">
            <div id ="texto" style="display:none;">
                    <?php echo $juegorecolector->texto_principal;?>
            </div>
        <div class="label db" id="recolector_imagen_titulo">Texto</div>
        <div class="columns large-12" style="margin-bottom: 15px;">
            <!-- <div class=""><label for="">Texto:</label></div> -->
            <div class=""><textarea  name="textoprincipal" id="editor">
            <?php echo str_replace('\n','',$juegorecolector->texto_principal) ?>

            </textarea></div>
        </div>
        <div class="dib">
            <label class="gris" for="">Elija una colección de imágenes:</label>
            <span class="icon-images gris"></span>  
            <span class="gris">Tamaño recomendable 250X250 cuadrado</span>
        </div>
        <div class="btn_default dib  small file_wcss">
            <div class="content"><span class="icon-images"></span> Imágenes</div>
            <input type="file" name="recolector_img[]" multiple class="input_file dib" id="file_imgs">
        </div>
        
        
        <label class="gris" for="">Seleccione las imágenes</label>
        <span class="gris">Haz click en cada imagen, aparecerá un formulario donde podrás plantear una pregunta y su respectiva respuesta
                            escrita; esta imagen es la respuesta a la pregunta que estas planteando.</span>
        <span class="gris">El juego permite recolectar la respuesta mediante una imagen o en forma de texto escrito.</span>

        <div class="recolector-galeria">
            <section class="recolector-galeria-todos">
                <div class="label db">Imágenes incorrectas</div>
            
            </section>
             <section class="recolector-galeria-correctos">
                <div class="label db">Imágenes Correctas</div>
            </section>
        </div>
        </form>
    </div>
    
    </div>
</div>
    <div id="aviso_guardado"></div>
    <section class="nuevo_recolector-botones">
        <div   class="button  guardar " id="guardardatos"> Guardar </div>
        <a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
        <div   class="button  alert " id="cancelar_todo"> Cancelar </div>
    </section>        
</div>
</div>


<div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
</div>

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/nuevo_recolector.js"></script> 
<script>
// $(document).ready(function(){
//         initSample();
        //completar el formulario de edicion 
        $('#recolector_id_recolector').val('<?php echo $juegorecolector->idjuego;?>');
        $('#titulo').val('<?php echo $juegorecolector->nombre;?>');
        $('#tiempo').val('<?php echo $juegorecolector->tiempo;?>');
        $('#puntaje').val('<?php echo $juegorecolector->puntaje;?>');
        $('#edad_minima').val(' <?php echo $juegorecolector->edad_minima;?>');
        $('#edad_maxima').val('<?php echo $juegorecolector->edad_maxima;?>');
        $('#descripcion').val('<?php echo $juegorecolector->descripcion;?>');
        $('#curso').val('<?php echo $juegorecolector->curso_dirigido;?>');

        
        $('#iframe_prev').attr('src','<?php echo base_url();?>juegos/recolector_previsualizacion/<?php echo $juegorecolector->idjuego;?>');

    
    var imagenes_correctas_abierto = false;
    localStorage.clear();
// });
    var cont=1;
</script>
    <?php foreach ($recolectordetalles as $detalles ) { ?>
        <script> 
        
        localStorage.setItem('pregunta'+cont, '<?php echo $detalles->pregunta;?>');
        localStorage.setItem('respuesta'+cont, '<?php echo $detalles->respuesta;?>');
        localStorage.setItem('imagen'+cont,'<?php echo $detalles->ruta;?>');

        var tmppregunta =  localStorage.getItem('pregunta'+cont);  
        var tmprespuesta = localStorage.getItem('respuesta'+cont);
        var tmpimg = localStorage.getItem('imagen'+cont); 

        var cont_modelo = '<figure  id="item-'+cont+'" data-id="'+cont+'" class="recolector-galeria-todos-item"><img alt="imagen_'+cont+'" src="'+tmpimg+'"><figcaption><input type="text" value="" class="recolector-galeria-itemdes"></figcaption><article class="sombra"> <span class="icon-arrow-right" data-item="'+cont+'" onclick="seleccionar_imagen(this);" ></span></article></figure>';

        $('.recolector-galeria-todos').append(cont_modelo);

        var id = '#item-'+cont;
        $(id).css('border-color','#037fa2');
        
        cont++;

        // trasladar a panel derecho 

        if (!imagenes_correctas_abierto) {

            $('.recolector-galeria-correctos').addClass('ampliado');
            $('.recolector-galeria-todos').addClass('reducido');
            imagenes_correctas_abierto = true;

            // mover de padre
        }
        var figure = $(id).detach();
        $('.recolector-galeria-correctos').append(figure);

        var divdatosimagen = '<div class = "recolector_explicacion_imagen"><label class ="gris" for>'+tmppregunta+'</label><span class ="negro">'+tmprespuesta+'</span></div>';
        $('.recolector-galeria-correctos').append(divdatosimagen);
        $(figure).addClass('datosimagen');

    </script> <?php } ?>
    <script>
        CKEDITOR.instances['editor'].setData($('#texto').html());
    </script>
    

        
