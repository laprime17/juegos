<div class="prev">

	<div class="prev-content" style="background: #065D79;">
		<div class="prev-content-titulo row" style="background: #2B7692;">
            <div class="mt03m prev-content-titulo-otros columns large-2" id="tiempo_actividad">
                <div id="hour">00</div>
                <div class="divideruno">:</div>
                <div id="minute">00</div>
                <div class="dividerdos">:</div>
                <div id="second">00</div>
            </div>
			<div class="mt03m prev-content-titulo-titulo columns large-6" style= "text-align: center;"><?php echo $juegorecolector->nombre;?> </div >
			<div class="prev-content-titulo-otros columns large-3"><div class="boton amarillo dib"><span class="icon-arrow-right"></span> Siguiente Actividad</div></div>
		</div>
        <div class="prev-imagen-fraccionado " style="background: #065D79;">
            <div class="prev-imagen-fraccionado-textoayuda" id="pre_enunciado">Para ganar en este juego primero lee el texto y luego responde a las preguntas</div>
            <div class="barra_vida">
                <div class="alert progress" role="progressbar" tabindex="0" aria-valuenow="50" aria-valuemin="0" aria-valuetext="50 percent" aria-valuemax="100">
                  <div class="progress meter" id="vida_jugador" ></div>
                </div>
            </div> 
            <div id="textocompleto">Texto</div>
            <div id="texto_expandido" ><?php echo $juegorecolector->texto_principal;?></div>
            <div class="prev-content-datos-puntos" style=" position: relative; top: -85px;">
                <div class="puntaje_memoria">
                <div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> 
                </div>
            </div>
            <div class="prev-imagen-fraccionado-textoayuda" id ="enunciado"></div>
        </div>
		<figure class="prev-img" id="prev-img">
		  <div class="recolector_prev_final"></div>
		</figure>
		<div class="prev-footer">
				<span class="icon-arrow-left"></span>
				<span class="icon-arrow-right"></span>
		</div>	
	</div>	
</div>
<script >
    var puntaje_actividad = <?php echo $juegorecolector->puntaje;?>;
    var tiempo_actividad = <?php echo $juegorecolector->tiempo;?>;
    var preguntaarray = new Array();
    var respuestaarray = new Array();
    var imagenrpta = new Array();
    var posicion = new Array();
    var nroejercicio = 0;
    var nroaleatorio;
    $("#textocompleto").click(function(){

        var te_nro_caracteres = $('#texto_expandido').html().length;
        var te_height = (te_nro_caracteres/110)*70;
        $('#texto_expandido').css('height',te_height+'px');
        $("#texto_expandido").toggleClass('abierto');
        if($('#texto_expandido').hasClass('abierto')==false)
        {
           $('#texto_expandido').css('height','0px');
        }
   /*     $("#texto_expandido").show();
        }, function(){
            $("#texto_expandido").hide();*/
        });

</script>
<?php foreach ($recolectordetalles as $detalles ) { ?>
    <script>preguntaarray.push('<?php echo $detalles->pregunta;?>');
            respuestaarray.push('<?php echo $detalles->respuesta;?>');
            imagenrpta.push('<?php echo $detalles->ruta;?>');
    </script> <?php } ?>
<script>
    var puntajeximagen = puntaje_actividad / preguntaarray.length;
    //console.log('puntaje unitario', puntajeximagen);
    cargar_pregunta(nroejercicio);
    //correr_reloj();       
    asignar_posicion_imagenes();

    function asignar_posicion_imagenes()
    {
        for (var i = 0; i <imagenrpta.length; i++) {
            
            nroaleatorio = Math.floor((Math.random() * imagenrpta.length) + 0); 
            while(esusado(nroaleatorio))
            {
                nroaleatorio = Math.floor((Math.random() * imagenrpta.length) + 0);
            }
            posicion[i]=nroaleatorio;
            //console.log('posicion'+i+': ',posicion[i]);
        };
    }

    function esusado(nroaleatorio)
    {
        var usado=false;
        for (var i = 0; i <imagenrpta.length; i++) {
               if (posicion[i]==nroaleatorio) 
                {
                    usado=true;
                };
           };
        return usado;   

    }
    function cargar_pregunta(nrotexto)
    {   
        $('#enunciado').text('Pregunta '+(nroejercicio+1)+': '+preguntaarray[nrotexto]);
            nroejercicio++;
    }
    function correr_reloj()
    {   
        //$('#tiempo').Timer('start');
    }
    function Agregar_puntaje()
    {
        console.log('entró agregar puntos');
        if ($.isNumeric($('#puntaje_acumulado').html()))
        {
            var tmp = parseInt($('#puntaje_acumulado').html()); 
            tmp = tmp + puntajeximagen; 
            $('#puntaje_acumulado').html(tmp);
        }   
    }
    var tiempo = {hora:0, minuto:0, segundo:0};
    var tiempo_corriendo = null;
    var texto_tiempo;
    var tiempo_pausadoYN='N';
    correr_tiempo();
    function correr_tiempo()
    {
        if(tiempo_pausadoYN == 'N')
        {
            tiempo_pausadoYN='Y';
            tiempo_corriendo = setInterval(function(){

                // Segundos
                tiempo.segundo++;
                if(tiempo.segundo >= 60)
                {
                    tiempo.segundo = 0;
                    tiempo.minuto++;
                }

                if(tiempo.minuto>=tiempo_actividad)
                {
                    console.log('se acabó el tiempo');
                    texto_tiempo = game.add.text(400, 250, '¡Se terminó el tiempo! \n pasa la siguiente actividad' , {fill: '#fff' });
                    texto_tiempo.fontWeight = 'bold';
                    texto_tiempo.fontSize = 40;
                    texto_tiempo.font = 'Arial Black';
                    texto_tiempo.anchor.setTo(0.5, 0.5);
                    texto_tiempo.stroke = "#008CBA";
                    texto_tiempo.strokeThickness = 16;
                    texto_tiempo.setShadow(2, 2, "#333333", 2, true, false);

                    //parar_tiempo();
                    clearInterval(tiempo_corriendo);

                    game.paused=true; 

                }
                // Minutos
                if(tiempo.minuto >= 60)
                {
                    tiempo.minuto = 0;
                    tiempo.hora++;
                }

                $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
                $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
                $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);

            },1000);
        }
        else
        {
            tiempo_pausadoYN = 'N'
            clearInterval(tiempo_corriendo);
        }   
            
        
    }
    // function parar_tiempo()
    // {
    //     console.log('parar');
    //     clearInterval(tiempo_corriendo);
    // }
</script>
<script >
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'prev-img', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.tilemap('map',public_url+'json/mapa_recolector.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('ground_1x1', public_url+'sprites/recolector/piso2_1x1.png');
    game.load.spritesheet('coin', public_url+'sprites/recolector/coin.png', 32, 32);
    game.load.spritesheet('phaser', public_url+'sprites/recolector/dude.png', 32, 48);
    game.load.spritesheet('dude_carga', public_url+'sprites/recolector/dudecarga.png', 32, 48);
    game.load.image('fondo', public_url+'sprites/recolector/fondo2.png');
    game.load.spritesheet('barrera',public_url+'sprites/recolector/barrera2.png',8,64);
    game.load.spritesheet('barreralarga',public_url+'sprites/recolector/barreralarga.png',8,96);

    game.load.spritesheet('nro1', public_url+'sprites/recolector/nrouno.png', 32, 32);
    game.load.spritesheet('nro2', public_url+'sprites/recolector/nrodos.png', 32, 32);
    game.load.spritesheet('nro3', public_url+'sprites/recolector/nrotres.png', 32, 32);
    game.load.spritesheet('nro4', public_url+'sprites/recolector/nrocuatro.png', 32, 32);
    game.load.spritesheet('nro5', public_url+'sprites/recolector/nrocinco.png', 32, 32);
    game.load.spritesheet('nro6', public_url+'sprites/recolector/nroseis.png', 32, 32);

    game.load.spritesheet('moneda', public_url+'sprites/recolector/coin.png', 32, 32);
    game.load.image('menu', public_url+'sprites/recolector/opcionesrecolector.png', 270, 180);
    game.load.image('jugar_nuevo', public_url+'sprites/recolector/jugar_nuevo.png', 270, 90);
    game.load.image('menu_terminado', public_url+'sprites/recolector/opcionesrecolector.png', 270, 180);

    for (var i=0; i<imagenrpta.length; i++) {
        game.load.image('imgrpta'+i, imagenrpta[i], 50, 50); 
    };

}

var map;
var layer;

var sprite, spritecargado;
var carga_yn = 'N';
var cursors;
var scoreText;
var texto1, texto2;
var imagenejemplo, fondo;
var jumpButton;
var facing = 'left';
var facingcargado = 'left';
var jumpTimer = 0;
var bg;
var descargarButton;
var imagenrespuesta0, imagenrespuesta1, imagenrespuesta2, imagenrespuesta3, imagenrespuesta4, imagenrespuesta5;
var obstaculo1A, obstaculo2A, obstaculo3A, obstaculo4A, obstaculo5A, obstaculo6A, obstaculo7A, obstaculo8A;
var obstaculo1B, obstaculo2B, obstaculo3B, obstaculo4B, obstaculo5B, obstaculo6B, obstaculo7A;
var respuestajugador0, respuestajugador1, respuestajugador2, respuestajugador3, respuestajugador4, respuestajugador5; 
var timer;
var imgcapturadaX, imgcapturadaY;
var imagencapturada;
var nro1, nro2, nro3, nro4, nro5, nro6, posnum;
var contador = 0;
var vidajugador=100;
var monedas, moneda;
var w=800;
var h=600;
var opcionesYN;
var juegoterminadoYN='N';
var textoinformativo;
var reiniciar_juego;
var perdioYN = 'N';
var menu_terminado;

function create() {

    game.stage.backgroundColor = '#292924';

    game.physics.startSystem(Phaser.Physics.ARCADE);

    descargarButton = game.input.keyboard.addKey(Phaser.Keyboard.D);
    

    map = game.add.tilemap('map');

    map.addTilesetImage('ground_1x1');
    map.addTilesetImage('coin');

    map.setCollisionBetween(1, 12);

    //  This will set Tile ID 26 (the coin) to call the hitCoin function when collided with
    //map.setTileIndexCallback(26, hitCoin, this);

    //  This will set the map location 2, 0 to call the function
    //map.setTileLocationCallback(2, 0, 1, 1, hitCoin, this);

    // game.device.canvasBitBltShift = false;

    layer = map.createLayer('Tile Layer 1');


    layer.resizeWorld();

    // Here we create our coins group
    // crear nuevas monedas
    crear_monedas();

    var t = game.add.text(660, 0, "Opciones", { font: "26px Arial Black", fill: "#ffffff", align: "center" });
    t.fixedToCamera = true;
    t.cameraOffset.setTo(660, 0);
    t.inputEnabled = true;
    t.events.onInputUp.add(opciones);

    // Add a input listener that can help us return from being paused
    game.input.onDown.add(unpause_opciones, self);

    poner_imagenes();
    poner_obstaculosA();
    poner_obstaculosB();

    cargar_numeros_rpta();

    game.physics.arcade.gravity.y = 200;

    poner_jugador();

    cursors = game.input.keyboard.createCursorKeys();
    jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

}
function crear_monedas()
{
    monedas = game.add.group();
    monedas.enableBody = true;
    monedas.physicsBodyType = Phaser.Physics.ARCADE;

    //moneda = monedas.create(288, 96, 'moneda');
    moneda = monedas.create(448, 64, 'moneda');
    moneda = monedas.create(672, 128, 'moneda');
    moneda = monedas.create(1376, 32, 'moneda');


    //  Add animations to all of the coin sprites
    monedas.callAll('animations.add', 'animations', 'spin', [0, 1, 2, 3, 4, 5], 10, true);
    monedas.callAll('animations.play', 'animations', 'spin');
}
function poner_imagenes()
{
    for (var i=0; i<imagenrpta.length; i++) {
        if(i==0)
        {
            imagenrespuesta0 = game.add.sprite(110, 490, 'imgrpta'+posicion[i]);
            imagenrespuesta0.anchor.setTo(0.5, 0.5);
            imagenrespuesta0.width=140;
            imagenrespuesta0.height=120;
            game.physics.enable(imagenrespuesta0, Phaser.Physics.ARCADE);
            //imagenrespuesta0.body.collideWorldBounds = true;
        }
        if(i==1)
        {
            imagenrespuesta1 = game.add.sprite(470, 490, 'imgrpta'+posicion[i]);
            imagenrespuesta1.anchor.setTo(0.5, 0.5);
            imagenrespuesta1.width=140;
            imagenrespuesta1.height=120;
            game.physics.enable(imagenrespuesta1, Phaser.Physics.ARCADE);
            //imagenrespuesta1.body.collideWorldBounds = true;
        }
        if(i==2)
        {
            imagenrespuesta2 = game.add.sprite(1170, 480, 'imgrpta'+posicion[i]);
            imagenrespuesta2.anchor.setTo(0.5, 0.5);
            imagenrespuesta2.width=140;
            imagenrespuesta2.height=120;
            game.physics.enable(imagenrespuesta2, Phaser.Physics.ARCADE);
            //imagenrespuesta2.body.collideWorldBounds = true;
        }
        if(i==3)
        {
            imagenrespuesta3 = game.add.sprite(1500, 80, 'imgrpta'+posicion[i]);
            imagenrespuesta3.anchor.setTo(0.5, 0.5);
            imagenrespuesta3.width=140;
            imagenrespuesta3.height=120;
            game.physics.enable(imagenrespuesta3, Phaser.Physics.ARCADE);
            //imagenrespuesta3.body.collideWorldBounds = true;
        }
        if(i==4)
        {
            imagenrespuesta4 = game.add.sprite(1500, 480, 'imgrpta'+posicion[i]);
            imagenrespuesta4.anchor.setTo(0.5, 0.5);
            imagenrespuesta4.width=140;
            imagenrespuesta4.height=120;
            game.physics.enable(imagenrespuesta4, Phaser.Physics.ARCADE);
            //imagenrespuesta4.body.collideWorldBounds = true;
        }
        if(i==5)
        {
            imagenrespuesta5 = game.add.sprite(450, 270, 'imgrpta'+posicion[i]);
            imagenrespuesta5.anchor.setTo(0.5, 0.5);
            imagenrespuesta5.width=140;
            imagenrespuesta5.height=120;
            game.physics.enable(imagenrespuesta5, Phaser.Physics.ARCADE);
            //imagenrespuesta4.body.collideWorldBounds = true;
        }
    };
}
function poner_jugador()
{

    sprite = game.add.sprite(32, 32, 'phaser');
    game.physics.enable(sprite, Phaser.Physics.ARCADE);

    console.log('sprite x',sprite.x);
    console.log('sprite y',sprite.y);

    sprite.body.bounce.y = 0.2;
    sprite.body.collideWorldBounds = true;
    sprite.body.setSize(20, 32, 5, 16);

    sprite.animations.add('left', [0, 1, 2, 3], 10, true);
    sprite.animations.add('turn', [4], 20, true);
    sprite.animations.add('right', [5, 6, 7, 8], 10, true);

    game.camera.follow(sprite);
}
function opciones()
{
    // When the paus button is pressed, we pause the game
    game.paused = true;
    var mitad;
    // Then add the menu
    if (carga_yn=='Y') 
    {
        mitad = spritecargado.x;
        poner_menu(mitad);        
    }
    else
    {
        mitad = sprite.x;
        poner_menu(mitad);
    }

    //menu = game.add.sprite(sprite.x, h/2, 'menu');
    menu.anchor.setTo(0.5, 0.5);
    menu.fixedToCamera = true;
    menu.cameraOffset.setTo(w/2, h/2);

    // And a label to illustrate which menu item was chosen. (This is not necessary)
    //choiseLabel = game.add.text(w/2, h-150, 'Haz click fuera del cuadro para continuar', { font: '30px Arial', fill: '#fff' });
    //choiseLabel.anchor.setTo(0.5, 0.5);

    opcionesYN = 'Y';
}
function poner_menu(medio)
{
    if(medio < w/2)
        {
            menu = game.add.sprite(w/2, h/2, 'menu');
        }
        else if (medio > 1200)
        {
            menu = game.add.sprite(1200, h/2, 'menu');
        }
        else
        {
            menu = game.add.sprite(medio, h/2, 'menu');
        }
}
function unpause_opciones(event){
        // Only act if paused
        if(game.paused){

            // Calculate the corners of the menu
            var x1 = w/2 - 270/2, x2 = w/2 + 270/2,
                y1 = h/2 - 180/2, y2 = h/2 + 180/2;

            // Check if the click was inside the menu
            if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
                // The choicemap is an array that will help us see which item was clicked
                var choisemap = ['Uno', 'Dossss'];

                // Get menu local coordinates for the click
                var x = event.x - x1,
                    y = event.y - y1;

                // Calculate the choice 
                var choise = Math.floor(x / 270) + Math.floor(y / 90);

                // Display the choice
                if(choise == 0)
                {
                    //
                    //iniciar todo de nuevo
                    //eliminar los elementos existentes
                    if(carga_yn=='Y')
                    {
                        spritecargado.kill();
                    }
                    sprite.kill();
                    monedas.removeAll();
                    imagenrespuesta0.destroy();
                    if (imagenrpta.length>1) 
                    {
                        imagenrespuesta1.destroy();
                    }
                    if (imagenrpta.length>2) 
                    {
                        imagenrespuesta2.destroy();
                    }
                    if (imagenrpta.length>3) 
                    {
                        imagenrespuesta3.destroy();
                    }
                    if (imagenrpta.length>4) 
                    {
                        imagenrespuesta4.destroy();
                    }
                    if (imagenrpta.length>5) 
                    {
                        imagenrespuesta5.destroy();
                    }

                    //game.camera.x=0;
                    crear_monedas();
                    poner_imagenes();
                    poner_jugador();

                    if (contador>0)
                    {respuestajugador0.destroy();}
                    if (contador>1)
                    {respuestajugador1.destroy();}
                    if (contador>2)
                    {respuestajugador2.destroy();}
                    if (contador>3)
                    {respuestajugador3.destroy();}
                    if (contador>4)
                    {respuestajugador4.destroy();}
                    if (contador>5)
                    {respuestajugador6.destroy();}
                    
                    contador = 0;
                    vidajugador=100;
                    nroejercicio = 0; 

                    $('#vida_jugador').css('width',vidajugador+'%');
                    $('#puntaje_acumulado').html(0);
                    cargar_pregunta(nroejercicio);      

                    carga_yn='N';

                    if(perdioYN =='Y')
                    {
                        reiniciar_juego.destroy();
                        textoinformativo.destroy();
                    }
                    else if(juegoterminadoYN == 'Y')
                    {
                        menu_terminado.destroy();
                        textoinformativo.destroy();
                    }
                    else
                    {
                        // Remove the menu and the label
                        menu.destroy();
                        //choiseLabel.destroy();
                    }
                        opcionesYN='N';

                        // Unpause the game
                        game.paused = false;
                    
                }
                else
                {
                    console.log('segunda opcion');
                    if(opcionesYN == 'Y')
                    {
                        // Remove the menu and the label
                        menu.destroy();
                        //choiseLabel.destroy();

                        opcionesYN='N';

                        // Unpause the game
                        game.paused = false;
                    }

                }
                
            }
            else{ // cuando el click es fuera del cuadro
                
                console.log('click fuera del cuadro');
            }
        }
};
function poner_obstaculosA()
{
    //Obstaculo 1 tipo A
    obstaculo1A = game.add.sprite(174, 32, 'barrera');
    obstaculo1A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo1A.animations.play('siempre');
    game.physics.enable(obstaculo1A, Phaser.Physics.ARCADE);

    //Obstaculo 2 tipo A
    obstaculo2A = game.add.sprite(429, 32, 'barrera');
    obstaculo2A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo2A.animations.play('siempre');
    game.physics.enable(obstaculo2A, Phaser.Physics.ARCADE);

    //Obstaculo 3 tipo A
    obstaculo3A = game.add.sprite(494, 32, 'barrera');
    obstaculo3A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo3A.animations.play('siempre');
    game.physics.enable(obstaculo3A, Phaser.Physics.ARCADE);

    //Obstaculo 4 tipo A
    obstaculo4A = game.add.sprite(559, 32, 'barrera');
    obstaculo4A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo4A.animations.play('siempre');
    game.physics.enable(obstaculo4A, Phaser.Physics.ARCADE);

    //Obstaculo 5 tipo A
    obstaculo5A = game.add.sprite(621, 192, 'barrera');
    obstaculo5A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo5A.animations.play('siempre');
    game.physics.enable(obstaculo5A, Phaser.Physics.ARCADE);

    //Obstaculo 6 tipo A
    obstaculo6A = game.add.sprite(335, 480, 'barrera');
    obstaculo6A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo6A.animations.play('siempre');
    game.physics.enable(obstaculo6A, Phaser.Physics.ARCADE);

    //Obstaculo 7 tipo A
    obstaculo7A = game.add.sprite(1228, 288, 'barrera');
    obstaculo7A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo7A.animations.play('siempre');
    game.physics.enable(obstaculo7A, Phaser.Physics.ARCADE);

    //Obstaculo 8 tipo A
    obstaculo8A = game.add.sprite(1355, 32, 'barrera');
    obstaculo8A.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo8A.animations.play('siempre');
    game.physics.enable(obstaculo8A, Phaser.Physics.ARCADE);
    //obstaculo1A.body.bounce.y = 0.2;
    //obstaculo1A.body.collideWorldBounds = true;

    game.time.events.add(Phaser.Timer.SECOND * 2, desaparecer, this); 

}
function desaparecer()
{  
    obstaculo1A.kill();
    obstaculo2A.kill();
    obstaculo3A.kill();
    obstaculo4A.kill();
    obstaculo5A.kill();
    obstaculo6A.kill();
    obstaculo7A.kill();
    obstaculo8A.kill();
    game.time.events.add(Phaser.Timer.SECOND * 2, poner_obstaculosA, this);
}

function poner_obstaculosB()
{
    obstaculo1B = game.add.sprite(42, 195, 'barreralarga');
    obstaculo1B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo1B.animations.play('siempre');
    game.physics.enable(obstaculo1B, Phaser.Physics.ARCADE);

    //obstaculo 2 tipo B
    obstaculo2B = game.add.sprite(524, 32, 'barreralarga');
    obstaculo2B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo2B.animations.play('siempre');
    game.physics.enable(obstaculo2B, Phaser.Physics.ARCADE);

     //obstaculo 3 tipo B
    obstaculo3B = game.add.sprite(594, 32, 'barreralarga');
    obstaculo3B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo3B.animations.play('siempre');
    game.physics.enable(obstaculo3B, Phaser.Physics.ARCADE);

     //obstaculo 4 tipo B
    obstaculo4B = game.add.sprite(364, 256, 'barreralarga');
    obstaculo4B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo4B.animations.play('siempre');
    game.physics.enable(obstaculo4B, Phaser.Physics.ARCADE);

     //obstaculo 5 tipo B
    obstaculo5B = game.add.sprite(364, 448, 'barreralarga');
    obstaculo5B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo5B.animations.play('siempre');
    game.physics.enable(obstaculo5B, Phaser.Physics.ARCADE);

     //obstaculo 6 tipo B
    obstaculo6B = game.add.sprite(1354, 224, 'barreralarga');
    obstaculo6B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo6B.animations.play('siempre');
    game.physics.enable(obstaculo6B, Phaser.Physics.ARCADE);

     //obstaculo 7 tipo B
    obstaculo7B = game.add.sprite(1548, 256, 'barreralarga');
    obstaculo7B.animations.add('siempre', [0, 1, 2, 3],20,true);
    obstaculo7B.animations.play('siempre');
    game.physics.enable(obstaculo7B, Phaser.Physics.ARCADE);


    game.time.events.add(Phaser.Timer.SECOND * 4, desaparecerB, this);
}
function desaparecerB()
{
    obstaculo1B.kill();
    obstaculo2B.kill();
    obstaculo3B.kill();
    obstaculo4B.kill();
    obstaculo5B.kill();
    obstaculo6B.kill();
    obstaculo7B.kill();
    game.time.events.add(Phaser.Timer.SECOND * 4, poner_obstaculosB, this);

}
function hitCoin(sprite, tile) {

    tile.kill();
    if (vidajugador<99)
    {
        vidajugador = vidajugador+10;
        $('#vida_jugador').css('width',vidajugador+'%');
    }
}
function cargar_numeros_rpta()
{
    posnum = 0;
    for (var i=1; i<=imagenrpta.length; i++) {
        nro1 = game.add.sprite(700, 320+posnum, 'nro'+i);
        nro1.anchor.setTo(0.5, 0.5);

        posnum = posnum +40;
    }   
}
function update() {

    game.physics.arcade.collide(sprite, layer);
    sprite.body.velocity.x = 0;

    game.physics.arcade.collide(monedas, layer);

    game.physics.arcade.collide(imagenrespuesta0, layer);
    imagenrespuesta0.body.velocity.x = 0;

    if (imagenrpta.length>1) 
    {
        game.physics.arcade.collide(imagenrespuesta1, layer);
        imagenrespuesta1.body.velocity.x = 0;
    }
    if (imagenrpta.length>2) 
    {
        game.physics.arcade.collide(imagenrespuesta2, layer);
        imagenrespuesta1.body.velocity.x = 0;
    }
    if (imagenrpta.length>3) 
    {
        game.physics.arcade.collide(imagenrespuesta3, layer);
        imagenrespuesta1.body.velocity.x = 0;
    }
    if (imagenrpta.length>4) 
    {
        game.physics.arcade.collide(imagenrespuesta4, layer);
        imagenrespuesta1.body.velocity.x = 0;
    }
    if (imagenrpta.length>5) 
    {
        game.physics.arcade.collide(imagenrespuesta5, layer);
        imagenrespuesta1.body.velocity.x = 0;
    }

    game.physics.arcade.collide(obstaculo1A,layer);
    game.physics.arcade.collide(obstaculo2A,layer);
    game.physics.arcade.collide(obstaculo3A,layer);
    game.physics.arcade.collide(obstaculo4A,layer);
    game.physics.arcade.collide(obstaculo5A,layer);
    game.physics.arcade.collide(obstaculo6A,layer);
    game.physics.arcade.collide(obstaculo7A,layer);
    game.physics.arcade.collide(obstaculo8A,layer);

    game.physics.arcade.collide(obstaculo1B,layer);
    game.physics.arcade.collide(obstaculo2B,layer);
    game.physics.arcade.collide(obstaculo3B,layer);
    game.physics.arcade.collide(obstaculo4B,layer);
    game.physics.arcade.collide(obstaculo5B,layer);
    game.physics.arcade.collide(obstaculo6B,layer);
    game.physics.arcade.collide(obstaculo7B,layer);
    
    if (carga_yn == 'Y') 
        {
            game.physics.arcade.collide(spritecargado, layer);
            spritecargado.body.velocity.x = 0;
             if (cursors.left.isDown)
             {
                 spritecargado.body.velocity.x = -150;

                if (facingcargado != 'left')
                {
                    spritecargado.animations.play('left');
                    facingcargado = 'left';
                }
             }
             else if (cursors.right.isDown)
            {
                    spritecargado.body.velocity.x = 150;

                    if (facingcargado != 'left')
                    {
                        spritecargado.animations.play('right');
                        facingcargado = 'right';
                    }
            }
            else
            {
                if (facingcargado != 'idle')
                {
                    spritecargado.animations.stop();

                    if (facingcargado == 'left')
                    {
                        spritecargado.frame = 0;
                    }
                    else
                    {
                        spritecargado.frame = 5;
                    }

                    facingcargado = 'idle';
                }
            }
            if (jumpButton.isDown && spritecargado.body.onFloor() && game.time.now > jumpTimer)
            {
                spritecargado.body.velocity.y = -170;
                jumpTimer = game.time.now + 550;
            }

            if (descargarButton.isDown &&  spritecargado.x > 810 && spritecargado.x < 890 && spritecargado.y==144)
            {
                soltar_dentro_caja();
            }
            
            if (descargarButton.isDown &&  (spritecargado.x < 810 || spritecargado.x > 890 || spritecargado.y!=144))
            {
                soltar_fuera_caja();
            }
            game.physics.arcade.overlap(spritecargado, obstaculo1A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo2A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo3A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo4A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo5A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo6A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo7A, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo8A, colision_obstaculo, null, this);

            game.physics.arcade.overlap(spritecargado, obstaculo1B, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo2B, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo3B, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo4B, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo5B, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo6B, colision_obstaculo, null, this);
            game.physics.arcade.overlap(spritecargado, obstaculo7B, colision_obstaculo, null, this);

            game.physics.arcade.overlap(spritecargado, monedas, hitCoin, null, this);

        }
        else
        {
                if (cursors.left.isDown)
                {
                    sprite.body.velocity.x = -150;

                    if (facing != 'left')
                    {
                        sprite.animations.play('left');
                        facing = 'left';
                    }
                    //console.log("x ",sprite.x);
                    //console.log("y ",sprite.y);
                }
                else if (cursors.right.isDown)
                {
                    sprite.body.velocity.x = 150;

                    if (facing != 'right')
                    {
                        sprite.animations.play('right');
                        facing = 'right';
                    }
                }
                else
                {
                    if (facing != 'idle')
                    {
                        sprite.animations.stop();

                        if (facing == 'left')
                        {
                            sprite.frame = 0;
                        }
                        else
                        {
                            sprite.frame = 5;
                        }

                        facing = 'idle';
                    }
                }
                
                if (jumpButton.isDown && sprite.body.onFloor() && game.time.now > jumpTimer)
                {
                    sprite.body.velocity.y = -170;
                    jumpTimer = game.time.now + 550;
                }
                game.physics.arcade.overlap(sprite, obstaculo1A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo2A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo3A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo4A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo5A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo6A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo7A, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo8A, colision_obstaculo, null, this);

                game.physics.arcade.overlap(sprite, obstaculo1B, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo2B, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo3B, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo4B, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo5B, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo6B, colision_obstaculo, null, this);
                game.physics.arcade.overlap(sprite, obstaculo7B, colision_obstaculo, null, this);

                game.physics.arcade.collide(sprite, imagenrespuesta0, Colision_imagen, null, this);
                game.physics.arcade.collide(sprite, imagenrespuesta1, Colision_imagen, null, this);
                game.physics.arcade.collide(sprite, imagenrespuesta2, Colision_imagen, null, this);
                game.physics.arcade.collide(sprite, imagenrespuesta3, Colision_imagen, null, this);
                game.physics.arcade.collide(sprite, imagenrespuesta4, Colision_imagen, null, this);
                game.physics.arcade.collide(sprite, imagenrespuesta5, Colision_imagen, null, this);

                game.physics.arcade.overlap(sprite, monedas, hitCoin, null, this);
                
        }
}
function Colision_imagen(sprite,imagen)
{
    console.log('imagen: ',imagen.key);
    imgcapturadaX = imagen.x;
    imgcapturadaY = imagen.y;
    imagencapturada = imagen;
    // console.log('x: ',imagen.x);
    // console.log('y: ',imagen.y);
    //imagen.kill();
    imagen.visible =false;
    dude_con_carga();
    sprite.kill();
}
function Colision_texto(sprite,fondo)
{
    fondo.kill();
    texto1.destroy();
    dude_con_carga();
    sprite.kill();
}
function colision_obstaculo(sprite,obstaculo)
{
    if (vidajugador>0)
    {
        vidajugador = vidajugador -0.5;
        $('#vida_jugador').css('width',vidajugador+'%');
    }
    else
    {
        console.log('perdiste');
        game.paused = true;
        // var medio = game.camera.x + w/2;
        // textoinformativo = game.add.text(medio, 150, '¡perdiste! vuelve a intentarlo', {fill: '#fff' });
        // textoinformativo.fontWeight = 'bold';
        // textoinformativo.fontSize = 40;
        // textoinformativo.font = 'Arial Black';
        // textoinformativo.anchor.setTo(0.5, 0.5);
        // textoinformativo.stroke = "#008CBA";
        // textoinformativo.strokeThickness = 16;
        // textoinformativo.setShadow(2, 2, "#333333", 2, true, false);
        var mitad;
        if (carga_yn=='Y') 
        {
            mitad = spritecargado.x;
            poner_aviso(mitad);        
        }
        else
        {
            mitad = sprite.x;
            poner_aviso(mitad);
        }
    }    
}
function poner_aviso(medio)
{
    if(medio < w/2)
    {
        reiniciar_juego = game.add.sprite(w/2-136, (h/2-90), 'jugar_nuevo');
        textoinformativo = game.add.text(w/2, 150, '¡perdiste! vuelve a intentarlo', {fill: '#fff' });
        
    }
    else if (medio > 1200)
    {
        reiniciar_juego = game.add.sprite(1200-136, (h/2-90), 'jugar_nuevo');
        textoinformativo = game.add.text(1200, 150, '¡perdiste! vuelve a intentarlo', {fill: '#fff' });
    }
    else
    {
        reiniciar_juego = game.add.sprite(medio-136, (h/2-90), 'jugar_nuevo');
        textoinformativo = game.add.text(medio, 150, '¡perdiste! vuelve a intentarlo', {fill: '#fff' });
    }
    textoinformativo.fontWeight = 'bold';
    textoinformativo.fontSize = 40;
    textoinformativo.font = 'Arial Black';
    textoinformativo.anchor.setTo(0.5, 0.5);
    textoinformativo.stroke = "#008CBA";
    textoinformativo.strokeThickness = 16;
    textoinformativo.setShadow(2, 2, "#333333", 2, true, false);

    perdioYN = 'Y';

    game.input.onDown.add(unpause_opciones, self);
}
function dude_con_carga()
{
    spritecargado = game.add.sprite(sprite.x, sprite.y, 'dude_carga');
    game.physics.enable(spritecargado, Phaser.Physics.ARCADE);

    spritecargado.body.bounce.y = 0.2;
    spritecargado.body.collideWorldBounds = true;
    spritecargado.body.setSize(20, 32, 5, 16);

    spritecargado.animations.add('left', [0, 1, 2, 3], 10, true);
    spritecargado.animations.add('turn', [4], 20, true);
    spritecargado.animations.add('right', [5, 6, 7, 8], 10, true);

    game.camera.follow(spritecargado);

    carga_yn = 'Y';
}
function soltar_dentro_caja()
{
    //console.log('contador ',contador);
    var cadena = imagencapturada.key;
    var ultimo = cadena.substr(-1);
    switch(contador)
    {
        case 0:
              respuestajugador0 = game.add.text(732, 310, respuestaarray[parseInt(ultimo)], { font: '18px Arial', fill: '#fff' });
              break;
        case 1:
              respuestajugador1 = game.add.text(732, 350, respuestaarray[parseInt(ultimo)], { font: '18px Arial', fill: '#fff' });
              break;
        case 2:
              respuestajugador2 = game.add.text(732, 390, respuestaarray[parseInt(ultimo)], { font: '18px Arial', fill: '#fff' });
              break;
        case 3:
              respuestajugador3 = game.add.text(732, 430, respuestaarray[parseInt(ultimo)], { font: '18px Arial', fill: '#fff' });
              break;
        case 4:
              respuestajugador4 = game.add.text(732, 470, respuestaarray[parseInt(ultimo)], { font: '18px Arial', fill: '#fff' });
              break;
        case 5:
              respuestajugador5 = game.add.text(732, 510, respuestaarray[parseInt(ultimo)], { font: '18px Arial', fill: '#fff' });
              break;
    }
    if(parseInt(ultimo)==contador)
    {
        //console.log('entro a agregar');
        Agregar_puntaje();
    }
    //console.log('ultimo: ',parseInt(ultimo));
    imagencapturada.kill();
    contador++;
    if(contador>(preguntaarray.length-1))
    {
        //console.log('no hay mas preguntas, acabó la actividad');
        game.paused = true;
        var mitad = spritecargado.x;
        menu_terminado = game.add.sprite(mitad-136, (h/2-90), 'menu_terminado');
        textoinformativo = game.add.text(mitad, 150, '     ¡TERMINASTE! \nrevisa tus respuestas', {fill: '#fff' });
        textoinformativo.fontWeight = 'bold';
        textoinformativo.fontSize = 30;
        textoinformativo.font = 'Arial Black';
        textoinformativo.anchor.setTo(0.5, 0.5);
        textoinformativo.stroke = "#008CBA";
        textoinformativo.strokeThickness = 16;
        textoinformativo.setShadow(2, 2, "#333333", 2, true, false);

        juegoterminadoYN = 'Y';

    }
    else
    {
        //cargar la siguiente actividad
        cargar_pregunta(nroejercicio);
    }
    
    // switch(cadena)
    // {
    //     case 'imgrpta0':
    //           respuestajugador0 = game.add.text(732, 310, respuestaarray[0], { font: '18px Arial', fill: '#fff' });
    //           break;
    //     case 'imgrpta1':
    //           respuestajugador1 = game.add.text(732, 350, respuestaarray[1], { font: '18px Arial', fill: '#fff' });
    //           break;
    //     case 'imgrpta2':
    //           respuestajugador2 = game.add.text(732, 390, respuestaarray[2], { font: '18px Arial', fill: '#fff' });
    //           break;
    //     case 'imgrpta3':
    //           respuestajugador3 = game.add.text(732, 430, respuestaarray[3], { font: '18px Arial', fill: '#fff' });
    //           break;
    //     case 'imgrpta4':
    //           respuestajugador4 = game.add.text(732, 470, respuestaarray[4], { font: '18px Arial', fill: '#fff' });
    //           break;
    //     case 'imgrpta5':
    //           respuestajugador5 = game.add.text(732, 510, respuestaarray[5], { font: '18px Arial', fill: '#fff' });
    //           break;
    // }
    
    reemplazar_actor();
    
   
}
function soltar_fuera_caja()
{
    imagencapturada.visible=true;
    imagencapturada.width=140;
    imagencapturada.height=120;
    imagencapturada.x = imgcapturadaX;
    imagencapturada.y = imgcapturadaY;
     
    reemplazar_actor();
}
function reemplazar_actor()
{
    sprite = game.add.sprite(spritecargado.x, spritecargado.y, 'phaser');
    game.physics.enable(sprite, Phaser.Physics.ARCADE);

    sprite.body.bounce.y = 0.2;
    sprite.body.collideWorldBounds = true;
    sprite.body.setSize(20, 32, 5, 16);

    sprite.animations.add('left', [0, 1, 2, 3], 10, true);
    sprite.animations.add('turn', [4], 20, true);
    sprite.animations.add('right', [5, 6, 7, 8], 10, true);

    game.camera.follow(sprite);

    carga_yn = 'N';

    spritecargado.kill();
}
function render() {

    //game.debug.body(sprite);
} 
</script>