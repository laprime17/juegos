<?php $this->load->view('administracion/layouts/header.php'); ?>

		<div class="row" data-equalizer>
			<?php $this->load->view('administracion/layouts/side.php'); ?>
			<div class="columns large-9 administrador-panel" data-equalizer-watch>
				<?php echo $breadcumb;?>
				<div class="administrador-paneldetallejuego">
					
					<div class="paneldetallejuego-header row">
						<figure class="columns large-2">
							<img src="<?php echo public_url();?>icos/<?php echo $detalle['juego']->tipo;?>.jpg" alt="">
						</figure>
						<div class="columns large-6 titulo">
							
							<div class="lbl verde medium "><?php echo $detalle['juego']->tipo;?></div>
							<h1><?php echo $detalle['juego_derivado']->nombre;?></h1>
						</div>
						<div class="columns large-4 pr h100">
							<div class="botones">
								<div class="boton small verde"><span class="icon-pencil2"></span>Editar</div>
								<a data-reveal-id="modal_prev" href="<?php echo base_url();?>juegos/previsualizacion/<?php echo $detalle['juego']->tipo;?>/<?php echo $detalle['juego']->tipo;?>" class="boton small amarillo boton_previsualizacion"><span class="icon-enlarge2"></span>Prev</a>
							</div>
							
						</div>
	
					</div>
					<div class="paneldetallejuego-panel row">
						<div class="label verde medium db">
							Datos Generales
						</div>
						<div class="row">
							<div class="columns large-2"><div class="lbl verde">Tipo</div><?php echo $detalle['juego']->tipo;?></div>
							<div class="columns large-2"><div class="lbl verde">Fecha de creación</div><?php echo $detalle['juego']->fecha_creacion;?></div>
							<div class="columns large-2"><div class="lbl verde">Tiempo</div><?php echo $detalle['juego_derivado']->tiempo;?></div>
							<div class="columns large-2"><div class="lbl verde">Puntaje</div><?php echo $detalle['juego_derivado']->puntaje;?></div>
							<div class="columns large-2"><div class="lbl verde">Dirigido a:</div><?php echo 'Niños de '.$detalle['juego']->edad_minima.' a '.$detalle['juego']->edad_maxima;?></div>
						</div>

					</div>
					<div class="paneldetallejuego-panel row">
						<div class="label verde medium db">
							Opciones de Publicación
						</div>
						<div class="paneldetallejuego-op">
						<div class="dib paneldetallejuego-op-item"><span class="icon-share2"></span>Publicar</div>
						<div class="dib paneldetallejuego-op-item"><span class="icon-share2"></span>Asignar a sesión</div>
					</div>	
						


					</div>
					<div class="paneldetallejuego-panel row">
						<div class="label verde medium">
							Reportes
						</div>

					</div>
					

				</div>				
		

			</div>
		</div>

<!-- modales -->7
<div id="modal_prev" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" src="" frameborder="0"></iframe>
</div>
<!-- fin modales -->

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<?php  echo 
'<script>
	 idtipo = '.$detalle['juego']->idtipo.'; tipo = "'.$detalle['juego']->tipo.'";
</script>';
?>
<script src="<?php echo public_url();?>js/administracion/inicio.js"></script>
<script>

	$(document).on('ready',ini);
	function ini()
	{
		$('#btn_nuevaactividad').click(abrir_lista);
		$('.boton_previsualizacion').click(prev);
	}
	function abrir_lista()
	{
		$('.lista_juegos').toggleClass('abierto');
	}
	function prev()
	{
	    $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/previsualizacion/'+tipo+'/'+idtipo);

	}

</script>
