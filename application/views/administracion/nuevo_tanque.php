<?php $this->load->view('administracion/layouts/header.php'); ?>

<?php //$this->load->view('layouts/navbar_administradidor');?>

<div class="row">
	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
	<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_mapa row" data-equalizer>
	<div class="lbl2">Nueva Guerra de Tanques</div>
	
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
		<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/mapa_guardarmapa" method="post"class="form_datos">
			
		</form>
		
		<label for="">Puntos</label>
		<div class="nuevo_mapa-contenedor_respuestas"></div>
	</div>
	<div class="dib nuevo_juego-vista_previa columns large-8" data-equalizer-watch>
		

		<div class="mapa-prev_img" id="mapa-contenedorimg">
		</div>

		<section class="nuevo_mapa-botones">
		<div   class="button  alert " id="cancelar_todo"> Cancelar </div>
		<a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
		</section>
		
	</div>
	
</div>
	

</div>
</div>
	

<div id="modal_prev" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" src="" frameborder="0"></iframe>
</div>
		




<?php $this->load->view('administracion/layouts/footer.php'); ?>



<script>

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update });

function preload() {

    game.load.image('phaser', public_url+'sprites/dude.png',20,20);
    game.load.spritesheet('veggies', public_url+'sprites/frutas.png', 32, 32);

}

var sprite;
var group;
var cursors;
var facing = 'left';

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.stage.backgroundColor = '#2d2d2d';

    sprite = game.add.sprite(32, 200, 'phaser');
    game.physics.enable(sprite, Phaser.Physics.ARCADE);

   sprite.body.collideWorldBounds = true;
   // sprite.body.gravity.y = 1000;
   sprite.body.maxVelocity.y = 500;
    // otro
    // sprite.body.setSize(20, 32, 5, 16);

    sprite.animations.add('left', [0, 1, 2, 3], 10, true);
    sprite.animations.add('turn', [4], 20, true);
    sprite.animations.add('right', [5, 6, 7, 8], 10, true);
    // fin

    // game.physics.arcade.enable(sprite);
    game.physics.enable(sprite, Phaser.Physics.ARCADE);

    
    group = game.add.physicsGroup();

    for (var i = 0; i < 50; i++)
    {
        var c = group.create(game.rnd.between(100, 770), game.rnd.between(0, 570), 'veggies', game.rnd.between(0, 35));
        c.body.mass = -100;
    }

    for (var i = 0; i < 20; i++)
    {
        var c = group.create(game.rnd.between(100, 770), game.rnd.between(0, 570), 'veggies', 17);
    }

    cursors = game.input.keyboard.createCursorKeys();

}

function update() {

    //if (game.physics.arcade.collide(sprite, group, collisionHandler, processHandler, this))
    //{
    //    console.log('boom');
    //}

     game.physics.arcade.overlap(sprite, group, collisionHandler, null, this);

    sprite.body.velocity.x = 0;
    sprite.body.velocity.y = 0;

    if (cursors.left.isDown)
    {
        sprite.body.velocity.x = -200;
        // recitne
            sprite.body.velocity.x = -150;

            if (facing != 'left')
            {
                sprite.animations.play('left');
                facing = 'left';
            }
        // reciente
    }
    else if (cursors.right.isDown)
    {
        sprite.body.velocity.x = 200;
    }

    if (cursors.up.isDown)
    {
        sprite.body.velocity.y = -200;
    }
    else if (cursors.down.isDown)
    {
        sprite.body.velocity.y = 200;
    }

}

function processHandler (player, veg) {

    return true;

}

function collisionHandler (player, veg) {

    if (veg.frame == 17)
    {
        veg.kill();
    }

}
</script>