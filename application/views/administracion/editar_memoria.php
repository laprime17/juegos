<?php $this->load->view('administracion/layouts/header.php'); ?>

<?php //$this->load->view('layouts/navbar_administradidor');?>

<div class="row">

	<?php $this->load->view('administracion/layouts/side_juegos.php');?>
<div class="nuevojuego-panel">
<div class="nuevo_juego nuevo_memoria row" style= "padding-bottom: 5px;" data-equalizer>
	<div class="lbl2">Personalizar juego "Memoria"</div>
	<form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/memoria_guardarmemoria" method="post"class="form_datos">
	<div class="dib nuevo_juego-form columns large-4" data-equalizer-watch>
		
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Titulo de la Actividad:</label></div>
                    <div class=""> <input type="text"  placeholder="título" name="nombre" id="" value="<?php echo $juegomemoria->nombre;?>"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Duración(min):</label></div>
                    <div class=""><input type="number" name="tiempo" placeholder="minutos" value="<?php echo $juegomemoria->tiempo;?>"/></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Puntaje de actividad</label></div>
                    <div class=""><input type ="number" name="puntaje" value="<?php echo $juegomemoria->puntaje;?>"/></div>
                </div>
            </div>
            <div class="row">
                <div class="column large-6">
                    <div class=""><label for="">Materia:</label></div>
                    <div class=""><select name="curso" id="curso" >
                        <option value="Matematica">Matemática</option>
                        <option value="Comunicacion">Comunicación</option>
                        <option value="CTA">Ciencia y Ambiente</option>
                        <option value="Personal Social">Personal Social</option>
                    </select></div>
                </div>
                <div class="column large-6">
                    <div class=""><label for="">Edad sugerida:</label></div>
                    <div class="row">
                        <div class="columns large-5"><input type ="text" placeholder="06" name="edad_minima" value="<?php echo $juegomemoria->edad_minima;?>"/></div>
                        <div class="columns large-2">-</div>
                        <div class="columns large-5"><input type ="text" placeholder="09" name="edad_maxima" value="<?php echo $juegomemoria->edad_maxima;?>"/></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns large-12">
                    <div class=""><label for="">Descripción:</label></div>
                    <div class=""><textarea name="descripcion" id="descripcion" cols="30" rows="s"></textarea></div>
                </div>
            </div>
		
		<label for="">Explicacion del juego</label>
		<div class="memoria_explicacion">
           <span class="gris">Este juego consiste en encontrar pares de imagenes pequeñas y reconocer de que imagen se trata.</span> 
           <label class="gris" for="">Pasos</label>
           <span class="gris"> - Cargar una imagen (debido a que esta imagen principal necesita edicion, es proporcionada en un banco de imagenes)</span>
           <span class="gris">   La imagen tiene la siguiente forma:</span>
            <table id = "tablamaqueta" BORDER="3">
                <?php
                $texto = 1;
                 //Iniciamos el bucle de las filas
                 for($t=0;$t<6;$t++){
                  echo "<tr>";
                  //Iniciamos el bucle de las columnas
                  for($y=0;$y<6;$y++){
                    //Pintamos el cuadro
                    if($texto > 18)
                    {
                        echo "<td style=padding:7px;
                        background: #EDF1EF;>-</td>";
                        //El próximo no será pintado
                        $texto++;
                    }
                    else
                    {
                        echo "<td style=padding:7px;
                        background: #EDF1EF>".$texto."</td>";
                        //El próximo no será pintado
                        $texto++;
                    }
                   }
                   //Cerramos columna
                   echo "</tr>";
                  }
                ?>
            </table>
            <span class="gris">Los numeros representan pequeñas imagenes, después de cargar la imagen, usted debe asignar un nombre a cada una de ellas, esto en la parte derecha de esta misma ventana.</span>
        </div>

            <input type="hidden" name="imagen" id="memoria_src_imagen">
            <input type="hidden" name="idmemoria" id="memoria_id_memoria" value="-1">
            <input type="hidden" name="detallesmemoria" id="iddetallesmemoria" value="-1">
            
        
        </form>
	</div>
       
	<div class="dib nuevo_juego-vista_previa columns large-8" >
    <div class="label db" id="memoria_imagen_titulo">Cargar Imagen</div> 
        
        <form enctype="multipart/form-data" action="<?php echo base_url();?>juegos/subir_archivomemoria" method="post" class="form_imagen">
            <input type="file" name="memoriafile[]" multiple id="userfilememoria" >
        </form>

        <div class="memoria-prev_img" id="memoria-contenedorimg"></div>
        <div class= "memoria-ninoayuda_img" id="memoria-ninoayuda_img"></div>
        <div class="label db" id="memoria_imagen_titulo">Asignacion de nombres a las pequeñas imagenes</div>
        <div id="nombres_imagenes">
        </div>
    </div>
</div>
    <div id="aviso_guardado"></div>
    <section class="nuevo_memoria-botones">
        <div   class="button  guardar " id="guardardatos"> Guardar </div>
        <a  href="#" class="button success boton_previsualizacion"  id="previsualizar" data-reveal-id="modal_prev"> Previsualizar </a>
        <div   class="button  alert " id="cancelar_todo"> Cancelar </div>
    </section>        
</div>
</div>

<div id="modal_prev" class="reveal-modal large" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
  	<iframe  id="iframe_prev" width="1000" height="1000" src="" frameborder="0"></iframe>
</div>
<script>
    //var tiempo_actividad = '<?php $juegomemoria->tiempo;?>';
    //console.log("hola mundo", tiempo_actividad);
    $('#memoria_id_memoria').val('<?php echo $juegomemoria->idjuego;?>');
    $('#memoria_src_imagen').val('<?php echo $juegomemoria->ruta;?>');
    $('#curso').val('<?php echo $juegomemoria->curso_dirigido;?>');

    var descripcion_memoria = '<?php echo $juegomemoria->descripcion;?>';
    var imagen = '<?php echo $juegomemoria->ruta;?>';
    //console.log('llenar descripcion ',descripcion_memoria);
    $('textarea#descripcion').val(descripcion_memoria);
    $('.memoria-prev_img').html('<img class="memoria_imagen" src="'+imagen+'" width="350" height="240">');
    //$('.memoria-ninoayuda_img').html('<img class="memoria_imagen" src="'+public_url+'img/niño.jpg" width="350" height="240">');
    Asignando_nombres();
    function Asignando_nombres()
    {
        $( "#nombres_imagenes").empty();
        var contizq =1;
        var contder=2;
        for (var i = 1; i <= 9; i++) {
            divnom = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+contizq+"</div><div class='column large-5'><div class=''><input type='text' name='nombre_"+contizq+"' id='nombre_"+contizq+"' placeholder='Nombre'/></div></div><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+contder+"</div><div class='column large-5'><div class=''><input type='text' name='nombre_"+contder+"' id='nombre_"+contder+"' placeholder='Nombre'/></div></div></div>";
            $( "#nombres_imagenes").append(divnom);

            contizq=contizq+2;
            contder=contder+2;
        };
    }
    var cont=1;
    </script>
    <?php foreach ($memoriadetalles as $detalles ) { ?>
        <script> 
        $('#nombre_'+cont).val('<?php echo $detalles->nombre_imgfraccion; ?>');
        cont++;
        </script> <?php } ?>
    <script >

</script>
		
<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script src="<?php echo public_url();?>js/administracion/editar_memoria.js"></script>

 

