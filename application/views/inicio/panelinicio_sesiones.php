<?php $this->load->view('inicio/header_inicio.php'); ?>


	<div class="row" data-equalizer>
		<div class="columns large-3 sideprincipal" data-equalizer-watch>
			<?php $this->load->view('inicio/menu_inicio.php'); ?>
		</div>
		<div class="columns large-9 contenedor panelinicio" data-equalizer-watch>
			<h1>SESIONES PARA HOY</h1>
			<div class="panelinicio-sesiones">
				<?php 
				if (count($sesiones_hoy)==0) {
					echo '<div class="txt_thin"><span class=".icon-smile"> No hay juegos programados</div>';
				}
				else{
					foreach ($sesiones_hoy as $jh) {
						?>
							<a target="_blank" href="<?php echo base_url();?>sesion/index/<?php echo $jh->idsesion;?>">
								<div class="panel_inicio_juegos_item">
									<div class="panel_inicio_juegos_item_imagen">
									<?php 

									if ($jh->ruta_imagen==null){?>
									<?php echo'<img src="'.public_url().'img/sesion.jpg">';?>
									<?php }else {?>
									<?php echo'<img src="'.public_url().'uploads/'.$jh->ruta_imagen.'">';?>
									<?php } ?>
									</div>
									<div class="panel_inicio_juegos_item_des">
										<div class="titulo" data-sesion='<?php echo $jh->idsesion;?>'><?php echo $jh->nombre;?></div>
										<div class="fecha_prog"> Programado para el: <?php echo $jh->fecha_programada;?> </div>
										<div class="fecha_prog"> Grupo: <?php 
										$this->load->model('mdl_inicio');	
										echo $this->mdl_inicio->get_by_pk('grupo',$jh->idgrupo)->nombre;?> </div>
										

									</div>
								</div>
							</a>
						<?php
					} 
				}?>
			</div>

			


			
			<h1>MIS SESIONES</h1>
			<div class="panelinicio-sesiones">
				<?php 
				foreach ($sesiones as $jh) {
									?>
										<a target="_blank" href="<?php echo base_url();?>sesion/index/<?php echo $jh->idsesion;?>">
											<div class="panel_inicio_juegos_item">
												<div class="panel_inicio_juegos_item_imagen">
												<?php 

												if ($jh->ruta_imagen==null){?>
												<?php echo'<img src="'.public_url().'img/sesion.jpg">';?>
												<?php }else {?>
												<?php echo'<img src="'.public_url().'uploads/'.$jh->ruta_imagen.'">';?>
												<?php } ?>
												</div>
												<div class="panel_inicio_juegos_item_des">
													<div class="titulo" data-sesion='<?php echo $jh->idsesion;?>'><?php echo $jh->nombre;?></div>
													<div class="fecha_prog"> Programado para el: <?php echo $jh->fecha_programada;?> </div>
													<div class="fecha_prog"> Grupo: <?php 
													$this->load->model('mdl_inicio');	
													echo $this->mdl_inicio->get_by_pk('grupo',$jh->idgrupo)->nombre;?> </div>
													

												</div>
											</div>
										</a>
									
									<?php
								} ?>
			</div>

		</div>
			
		</div>
		
	</div>
</body>

<?php 
echo '<script>
public_url = "'.public_url().'";
</script>'; ?>
<script>
	$(document).foundation();

	$(window).load(function(){
      $("#sticker").sticky({ topSpacing: 0, center:true, className:"hey" });
      
      $("#header").sticky({ topSpacing: 0 });
    });
  
                
$(window).scroll(function(){
console.log($(window).scrollTop());
    
    
    if  ($(window).scrollTop() > 130 && $(window).scrollTop() < 150) {
    	
       $("#sticker").addClass('abierto');
       $("#header").addClass('abierto');

    }


    if  ($(window).scrollTop() < 130 ) {
    	
       $("#sticker").removeClass('abierto');
    }

});
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>

</html>