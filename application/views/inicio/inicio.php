<?php $this->load->view('inicio/header_inicio.php'); ?>


	<div class="row" data-equalizer>
		<div class="columns large-3 sideprincipal" data-equalizer-watch>
			<?php $this->load->view('inicio/menu_inicio.php'); ?>
		</div>
		<div class="columns large-9 contenedor" data-equalizer-watch>

			<div class="contenedor-panel abierto" class="panel_inicio">
				<!-- <h1>PANEL DE INICIO</h1> -->
				<div class="row">
					<div class="columns large-7">
						<h1>PARTICIPANTES</h1>
						<div class="div_blanco_rgba panel_inicio-participantes">
							<?php 
							foreach ($participantes as $participante) {

								if ($participante->foto_ruta==null) {
								 echo '<figure class="panel_inicio_participante" ><img  src="'.public_url().'fotos/default.jpg"><figcaption>'.$participante->nombres.' </figcaption></figure>';
								
								}
								else
								{
								 echo '<figure class="panel_inicio_participante" ><img  src="'.public_url().'fotos/'.$participante->foto_ruta.'"><figcaption>'.$participante->nombres.'</figcaption></figure>';
								 }

							}
							 ?>
							<!-- participantes
							juegos_hoy
							juegos
							grupos -->
							
						</div>
						<h1>JUEGOS</h1>
						<div class="div_blanco_rgba">
							<div class="txt_gris">¡Sesiones  para hoy!</div>
							<?php 
							if (count($sesiones_hoy)==0) {
								echo '<div class="txt_thin"><span class=".icon-smile"> No hay juegos programados</div>';
							}
							else{
								foreach ($sesiones_hoy as $jh) {
									?>
										<a target="_blank" href="<?php echo base_url();?>sesion/index/<?php echo $jh->idsesion;?>">
											<div class="panel_inicio_juegos_item">
												<div class="panel_inicio_juegos_item_imagen">
												<?php 

												if ($jh->ruta_imagen==null){?>
												<?php echo'<img src="'.public_url().'img/sesion.jpg">';?>
												<?php }else {?>
												<?php echo'<img src="'.public_url().'uploads/'.$jh->ruta_imagen.'">';?>
												<?php } ?>
												</div>
												<div class="panel_inicio_juegos_item_des">
													<div class="titulo" data-sesion='<?php echo $jh->idsesion;?>'><?php echo $jh->nombre;?></div>
													<div class="fecha_prog"> Programado para el: <?php echo $jh->fecha_programada;?> </div>
													<div class="fecha_prog"> Grupo: <?php 
													$this->load->model('mdl_inicio');	
													echo $this->mdl_inicio->get_by_pk('grupo',$jh->idgrupo)->nombre;?> </div>
													

												</div>
											</div>
										</a>
									<?php
								} 
							}?>

							<div class="txt_gris">¡Sesiones programadas!</div>
							<?php 
							if (count($sesiones)==0) {
								echo '<i><span class=".icon-smile"> No hay juegos programados</i>';
							}
							else{
								echo '<div class="panel_inicio_juegos">';
								foreach ($sesiones as $jh) {
									?>
										<a target="_blank" href="<?php echo base_url();?>sesion/index/<?php echo $jh->idsesion;?>">
											<div class="panel_inicio_juegos_item">
												<div class="panel_inicio_juegos_item_imagen">
												<?php 

												if ($jh->ruta_imagen==null){?>
												<?php echo'<img src="'.public_url().'img/sesion.jpg">';?>
												<?php }else {?>
												<?php echo'<img src="'.public_url().'uploads/'.$jh->ruta_imagen.'">';?>
												<?php } ?>
												</div>
												<div class="panel_inicio_juegos_item_des">
													<div class="titulo" data-sesion='<?php echo $jh->idsesion;?>'><?php echo $jh->nombre;?></div>
													<div class="fecha_prog"> Programado para el: <?php echo $jh->fecha_programada;?> </div>
													<div class="fecha_prog"> Grupo: <?php 
													$this->load->model('mdl_inicio');	
													echo $this->mdl_inicio->get_by_pk('grupo',$jh->idgrupo)->nombre;?> </div>
													

												</div>
											</div>
										</a>
									
									<?php
								}
								echo '</div>' ;
							}?>


							
						</div>
					</div>
					<div class="columns large-5 panel_inicio_grupos">
						<h1>Grupos</h1>
						<article class="panel_inicio_grupos-grupo row">
							<?php 
							foreach ($grupos as $grupo ) {
								
								?>
								<div class="columns large-7 panel_inicio_grupos-grupo-nombre ">
									<div  class="panel_inicio_grupos-grupo-titulo"><?php echo $grupo['grupo']->nombre;?></div>
									<span>
										<?php 
											if (count($grupo['docentes'])>0) {
												$grupo['docentes'][0]->nombres.' '.$grupo['docentes'][0]->ap;
											}
										 ?> 
									</span>
								</div>
								<div class="columns large-5">
									<a href="#" class="panel_inicio_grupos-grupo-link">Mis compañeros</a>
									<a href="#" class="panel_inicio_grupos-grupo-link">Mis sesiones</a>
								</div>
								<?php

							}
							 ?>
						</article >
					</div>
				</div>
			</div>
			<div class="contenedor-panel" id="panel_grupos">
		</div>
			
		</div>
		
	</div>
</body>

<?php 
echo '<script>
public_url = "'.public_url().'";
</script>'; ?>
<script>
	$(document).foundation();

	$(window).load(function(){
      $("#sticker").sticky({ topSpacing: 0, center:true, className:"hey" });
      
      $("#header").sticky({ topSpacing: 0 });
    });
  
                
$(window).scroll(function(){
console.log($(window).scrollTop());
    
    
    if  ($(window).scrollTop() > 130 && $(window).scrollTop() < 150) {
    	
       $("#sticker").addClass('abierto');
       $("#header").addClass('abierto');

    }


    if  ($(window).scrollTop() < 130 ) {
    	
       $("#sticker").removeClass('abierto');
    }

});
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>

</html>