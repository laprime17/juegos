<ul class="sideprincipal-menusuperior">
				
			</ul>
			<figure class="sideprincipal-logo">
				<img src="<?php echo public_url();?>img/logo.png" alt="Pukllay">
			</figure>
			<div id="sticker">
				

				<figure class="sideprincipal-foto">
					<?php 
					if ($usuario['foto_ruta']=='') {
					 	$usuario['foto_ruta'] = 'default.jpg';
					 } ?>
					<img src="<?php echo public_url().'fotos/'.$usuario['foto_ruta'];?>" alt="<?php echo $usuario['nombre_completo'];?>">
				</figure>
				<figcaption class="sideprincipal-foto-nombres">
					<div class="username"><?php echo  $usuario['nombre_usuario']?></div>
					<div class="nombrecompleto"><?php echo  $usuario['nombre_completo'];?></div>
				</figcaption>
				<ul class="sideprincipal-menuinferior">
					<li class="sideprincipal-menuinferior-item"><a href="<?php echo base_url();?>inicio">Panel de Inicio	</a></li>
					<li class="sideprincipal-menuinferior-item"><a href="<?php echo base_url();?>inicio/sesiones">Mis sesiones <div class="numero naranja"><?php echo count($sesiones); ?></div> </a></li>
					<li class="sideprincipal-menuinferior-item"><a href="<?php echo base_url();?>inicio/resultados">Mis resultados</a></li>
					<li class="sideprincipal-menuinferior-item"><a href="<?php echo base_url();?>inicio/grupos">Mis grupos</a></li>
				</ul>
			</div>