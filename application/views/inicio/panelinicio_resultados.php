<?php $this->load->view('inicio/header_inicio.php'); ?>


	<div class="row" data-equalizer>
		<div class="columns large-3 sideprincipal" data-equalizer-watch>
			<?php $this->load->view('inicio/menu_inicio.php'); ?>
		</div>
		<div class="columns large-9 contenedor panelinicio" data-equalizer-watch>
			<h1>RESULTADOS</h1>
				<?php 
							$nroimg=0;
							foreach ($resultados as $resultado ) {
								?>
								<article class="panel_inicio_resultados row">
								
								<h4  class="panel_inicio_resultados-titulo">Sesión: <?php echo $resultado['sesion']->nombre;?></h4>
								<div  class="fecha_prog"><?php echo $resultado['sesion']->fecha_programada;?></div>	
								
								<div class="panelinicio-grupos-item-desc row">
									<table class="table" style="margin-left: 14px;">
										<tr>
										<th>Juego </th>
										<th>Máximo puntaje </th>
										<th>Mis puntos</th>										
										<th>Mi tiempo</th>
										<th>Mi rendimiento</th>
										</tr>
										<?php
											$sumpuntos =0;
											$summinutos =0;
											$sumsegundos=0;
											$puntajemaximo=0;
											$min=0;
											$seg=0;
											$cont=0; 
											foreach ($resultado['juegos'] as $juego) {
												$sumpuntos = $sumpuntos + $juego->puntaje;
												$tiempo = $juego->duracion; 
												$minutos = floor($tiempo / 60);
												$segundos = $tiempo%60;
												$summinutos=$summinutos+$minutos;
												$sumsegundos= $sumsegundos+$segundos;

												//para la sumatoria final
												$mas_min = floor($sumsegundos / 60);
												$seg = $sumsegundos%60;
												$min = $summinutos+$mas_min;


												if($cont==0)
												{
													echo '<tr><td>'.$juego->nombre.'</td><td style="color:blue;">'.intval($resultado['juegoespecifico'][$cont]).'</td><td>'.$juego->puntaje.'</td><td>'.$minutos.'min '.$segundos.'seg</td><td rowspan="4"><img src="" id="imgrendimiento'.$nroimg.'"/></td></tr>';
												}
												else
												{
													echo '<tr><td>'.$juego->nombre.'</td><td style="color:blue;">'.intval($resultado['juegoespecifico'][$cont]).'</td><td>'.$juego->puntaje.'</td><td>'.$minutos.'min '.$segundos.'seg</td></tr>';
												}
												
												$puntajemaximo=$puntajemaximo+$resultado['juegoespecifico'][$cont];
												$cont++;
											}

										?> 
										<tr>
										<th>Total </th>
										<th style="color:blue;" id="puntajemax<?php echo $nroimg;?>"><?php echo $puntajemaximo;?></th>
										<th id="puntajehecho<?php echo $nroimg;?>"><?php echo $sumpuntos;?></th>
										<th><?php echo $min;?>min <?php echo $seg;?>seg</th>
										</tr>
									</table>
									
										
								</div>
							</article >
								<?php
 								$nroimg++;
							}
							 ?>

		</div>
		
	</div>
</body>

<?php 
echo '<script> public_url = "'.public_url().'"; </script>'; ?>
<script>
	$(document).on('ready',ini);
    function ini()
    {
    	var nrosesiones = '<?php echo count($resultados);?>'; 
    	for (var i = 0; i < nrosesiones; i++) {
    		var rendimiento = Math.round(parseInt($('#puntajehecho'+i).html())*100/parseInt($('#puntajemax'+i).html()));
	    	console.log("suma: ",rendimiento);
	    	if(rendimiento<50)//
	    	{
	    		//bajo rendimiento
	    		$("#imgrendimiento"+i).attr("src", '<?php echo public_url()."img/mal.jpg"?>'); 
	    	}
	    	else if(rendimiento>=50 && rendimiento<70)
	    	{
	    		$("#imgrendimiento"+i).attr("src", '<?php echo public_url()."img/regular.jpg"?>'); 	
	    	}
	    	else if(rendimiento>=70 && rendimiento<85)
	    	{
	    		$("#imgrendimiento"+i).attr("src", '<?php echo public_url()."img/bien.jpg"?>'); 
	    	}
	    	else if(rendimiento>=85)
	    	{
	    		$("#imgrendimiento"+i).attr("src", '<?php echo public_url()."img/muybien.jpg"?>'); 
	    	}
    	};
    	
        
    }	

	$(document).foundation();

	$(window).load(function(){
      $("#sticker").sticky({ topSpacing: 0, center:true, className:"hey" });
      
      $("#header").sticky({ topSpacing: 0 });
    });
  
                
$(window).scroll(function(){
console.log($(window).scrollTop());
    
    
    if  ($(window).scrollTop() > 130 && $(window).scrollTop() < 150) {
    	
       $("#sticker").addClass('abierto');
       $("#header").addClass('abierto');

    }


    if  ($(window).scrollTop() < 130 ) {
    	
       $("#sticker").removeClass('abierto');
    }

});
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>

</html>