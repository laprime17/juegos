<?php $this->load->view('inicio/header_inicio.php'); ?>


	<div class="row" data-equalizer>
		<div class="columns large-3 sideprincipal" data-equalizer-watch>
			<?php $this->load->view('inicio/menu_inicio.php'); ?>
		</div>
		<div class="columns large-9 contenedor panelinicio-grupos" data-equalizer-watch>
			<h1>GRUPOS</h1>
							<?php 
							foreach ($grupos as $grupo ) {
								
								?>
								<article class="panel_inicio_grupos-grupo row">
								
								<h4  class="panel_inicio_grupos-grupo-titulo"><?php echo $grupo['grupo']->nombre;?></h4>
									
								
								<div class="panelinicio-grupos-item-desc row">
									<div class="columns large-4 div_borde">
										<div  class=" label">Profesores
										</div>
										<ul class="db">
																			
											<?php 
											foreach ($grupo['docentes'] as $docente) {
												echo '<li>'.$docente->nombres.' '.$docente->ap.' '.$docente->am.'</li>';
											}
											 ?> 
										</ul>
									</div>
									<div class="columns large-4 div_borde">
										<div  class=" label">Compañeros
										</div>
										<ul class="db">
																			
											<?php
											$cont = 1; 
											foreach ($grupo['alumnos'] as $alumnos) {
												if($cont <=2)
												{
													echo '<li>'.$alumnos->nombres.' '.$alumnos->ap.' '.$alumnos->am.'</li>';
												}
											    $cont++;
											}
											 ?> 
										</ul>
									</div>
								</div>
							</article >
								<?php

							}
							 ?>

		</div>
		
		
	</div>
</body>

<?php 
echo '<script>
public_url = "'.public_url().'";
</script>'; ?>
<script>
	$(document).foundation();

	$(window).load(function(){
      $("#sticker").sticky({ topSpacing: 0, center:true, className:"hey" });
      
      $("#header").sticky({ topSpacing: 0 });
    });
  
                
$(window).scroll(function(){
console.log($(window).scrollTop());
    
    
    if  ($(window).scrollTop() > 130 && $(window).scrollTop() < 150) {
    	
       $("#sticker").addClass('abierto');
       $("#header").addClass('abierto');

    }


    if  ($(window).scrollTop() < 130 ) {
    	
       $("#sticker").removeClass('abierto');
    }

});
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>

</html>