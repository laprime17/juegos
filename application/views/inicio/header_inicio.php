<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico"/>
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
</head>
<body class="juego">
	<header class="" id="header">
	
		<a href="<?php echo base_url();?>inicio"><img class="blubird" src="<?php echo public_url();?>img/logo_pk.png" alt="pukllay"></a>
	
		<?php 
		if ($this->session->userdata('rol') ==null) {
			echo "Error. Por favor debe 
			<a href='".public_url()."'>iniciar sesión.</a>";
			die;
		}
		$navbar = 'layouts/navbar_'.$this->session->userdata('rol');
		 ?>
		<?php $this->load->view($navbar);?>
		
	</header>