<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   <title>Pukllay: Aprende Jugando</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
    <link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
    <link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
    <script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
    <script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body class="ejecutar_juego">
<div class="prev">
    <input type="hidden" id="idsesion" value="<?php echo $idsesion;?>">
	<div class="prev-content" style="background: #065D79;">
        <div class="informacion_juego">
            <div class="prueba_ejecucion_titulo"><?php echo $juegoinvasores->nombre;?> </div >
            <div class="prueba_ejecucion_instruccion">Para ganar en este juego primero lee el texto y luego responde a las preguntas</div>
            <div class="prueba_ejecucion_texto" id="textocompleto1">Texto</div>
            <div id="texto_expandido" style="width: 350%;"><?php echo $juegoinvasores->texto_comprension;?></div>
            <div class="" id ="enunciado"></div>

            <div class="puntos_conteo">
                <div class="puntaje_memoria">
                <div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> 
                </div>
            </div>
            
            <div class="mt03m prev-content-titulo-otros" id="tiempo_actividad">
                <div id="hour">00</div>
                <div class="divideruno">:</div>
                <div id="minute">00</div>
                <div class="dividerdos">:</div>
                <div id="second">00</div>
            </div>
            <a class="prev-content-titulo-otros btn_siguiente_juego" id="btn_siguienteActividad" style="display: none;">
                <div class="boton amarillo dib"><span class="icon-arrow-right"></span> Siguiente Actividad</div>
            </a>

        </div>
		<figure class="prev-img" id="prev-img" style="left: 135px;">
		  <div class="memoria_prev_final"></div>
		</figure>
		
	</div>	
</div>
<script> 
    $(document).on('ready',ini);
    function ini()
    {
        $('.btn_siguiente_juego').click(function(){
        var idjuego_sesion = <?php echo $juego_sesion->idjuego_sesion;?>;
        var puntaj = parseInt($('#puntaje_acumulado').html());  
        puntaje_final = Math.round(puntaj);
        //-------- tiempo exprasado en segundo
        var minutos = parseInt($('#minute').html());
        var segundos = parseInt($('#second').html());
        var tiempo_seg= (minutos*60) + segundos;

        $.ajax({
               type: "POST",
               url: base_url+'sesion/guardar_puntaje/',
               dataType:"json", 
               data:'idjuego_sesion='+idjuego_sesion+'&puntaje='+puntaje_final+'&duracion='+tiempo_seg,
               cache: false,
               // contentType: false, -- evita en´vio por post
               processData: false,
               success: function(data)
               {

               }, 
                 error: function(data) {

                }
            });
        });
    }
</script>
<script src="<?php echo public_url();?>js/ejecutar_juego.js"></script>
<script >
    $("#textocompleto1").click(function(){
        game.paused = true;
        juegoTerminadoYN = 'N';
        var te_nro_caracteres = $('#texto_expandido').html().length;
        var te_height = (te_nro_caracteres/110)*70;
        $('#texto_expandido').css('height',te_height+'px');
        $("#texto_expandido").toggleClass('abierto');
            if($('#texto_expandido').hasClass('abierto')==false)
            {
               $('#texto_expandido').css('height','0px');
               game.paused = false;
               corriendoJuegoYN = 'Y';
            }
        });

    var puntaje_actividad = <?php echo $juegoinvasores->puntaje;?>;
    var tiempo_actividad = <?php echo $juegoinvasores->tiempo;?>;
    var soles_array = new Array();
    var centimos_array = new Array();
    var enunciado_array = new Array();
    var figura_array = new Array();
    var enunciadofig_array = new Array();
    var tipoejercicio_array = new Array();
    var preguntas_array = new Array();
    var respuestas_array = new Array();
    var primerDistractor_array = new Array();
    var segundoDistractor_array = new Array();
    var nroejercicio = 0;
    var tipoejercicio;
    var nroejer_dinero =0;
    var nroejer_figura =0;
    var nroejer_comprension =0;
    var puntajeximagen;
    var puntajexpregunta;
</script>
<?php foreach ($invasoresdetalles as $detalles ) { 
        if($detalles->tipo == 'dinero'){?>
    <script>     
            soles_array.push('<?php echo $detalles->soles;?>');
            centimos_array.push('<?php echo $detalles->centimos;?>');
            enunciado_array.push('<?php echo $detalles->enunciado;?>');
            tipoejercicio_array.push('<?php echo $detalles->tipo;?>');
    </script> <?php }
        else if($detalles->tipo == 'figuras')
        { ?> 
        <script>
            figura_array.push('<?php echo $detalles->tipofigura;?>');
            enunciadofig_array.push('<?php echo $detalles->enunciadofig;?>');
            tipoejercicio_array.push('<?php echo $detalles->tipo;?>');
        </script> <?php }
        else if($detalles->tipo == 'comprension')
        { ?> 
        <script>
            console.log('tipo comprension');
            preguntas_array.push('<?php echo $detalles->pregunta;?>');
            respuestas_array.push('<?php echo $detalles->respuesta;?>');
            primerDistractor_array.push('<?php echo $detalles->primerDistractor;?>');
            segundoDistractor_array.push('<?php echo $detalles->segundoDistractor;?>');
            tipoejercicio_array.push('<?php echo $detalles->tipo;?>');
        </script> <?php }
}?>
<script>
    tipoejercicio = tipoejercicio_array[nroejercicio];
    if(tipoejercicio=='dinero')
    {
        puntajeximagen = puntaje_actividad / soles_array.length;
    }
    else if(tipoejercicio=='figuras')
    {
        console.log('falta llenar puntaje de figuras');
    }
    else if (tipoejercicio=='comprension') 
    {
        puntajexpregunta = puntaje_actividad / respuestas_array.length;
    }
    cargar_enunciado_array();

    var tiempo = {hora:0, minuto:0, segundo:0};
    var tiempo_corriendo = null;
    var texto_tiempo;
    correr_tiempo();
    function correr_tiempo()
    {
        
            tiempo_corriendo = setInterval(function(){

                // Segundos
                tiempo.segundo++;
                if(tiempo.segundo >= 60)
                {
                    tiempo.segundo = 0;
                    tiempo.minuto++;
                }

                if(tiempo.minuto>=tiempo_actividad)
                {
                    console.log('se acabó el tiempo');
                    texto_tiempo = game.add.text(400, 250, '¡Se terminó el tiempo! \n pasa a la siguiente actividad' , {fill: '#fff' });
                    texto_tiempo.fontWeight = 'bold';
                    texto_tiempo.fontSize = 40;
                    texto_tiempo.font = 'Arial Black';
                    texto_tiempo.anchor.setTo(0.5, 0.5);
                    texto_tiempo.stroke = "#008CBA";
                    texto_tiempo.strokeThickness = 16;
                    texto_tiempo.setShadow(2, 2, "#333333", 2, true, false);

                    //parar_tiempo();
                    clearInterval(tiempo_corriendo);
                    $('#btn_siguienteActividad').show();

                    game.paused=true; 
                    corriendoJuegoYN = 'N';

                }
                // Minutos
                if(tiempo.minuto >= 60)
                {
                    tiempo.minuto = 0;
                    tiempo.hora++;
                }

                $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
                $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
                $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);

            },1000);
        
    }

    function cargar_enunciado_array()
    {   
        if(tipoejercicio=='dinero')
        {
            $('#enunciado').text('La nave debe : '+enunciado_array[nroejer_dinero]);
            nroejer_dinero++;
        }
        else if (tipoejercicio=='figuras')
        {
            $('#enunciado').text('La nave debe : '+enunciadofig_array[nroejer_figura]);
            nroejer_figura++;
        }
        else if (tipoejercicio=='comprension')
        {
            $('#enunciado').text('Pregunta '+(nroejer_comprension+1)+': '+preguntas_array[nroejer_comprension]);
        }
        
    }
    function Agregar_puntaje(puntaj)
    {
        console.log('entró agregar puntos');
        if ($.isNumeric($('#puntaje_acumulado').html()))
        {
            var tmp = parseInt($('#puntaje_acumulado').html()); 
            tmp = tmp + puntaj; 
            $('#puntaje_acumulado').html(tmp);
        }
        
    }
</script>
<?php $this->load->view('administracion/layouts/footer.php'); ?>

<script>
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'prev-img', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('bullet', public_url+'sprites/invasores/bullet.png');
    game.load.image('bala2', public_url+'sprites/invasores/bala2.png');
    game.load.image('enemyBullet', public_url+'sprites/invasores/enemy-bullet.png');
    game.load.spritesheet('invader', public_url+'sprites/invasores/invader32x32x4.png', 32, 32);
    game.load.image('ship', public_url+'sprites/invasores/player.png');
    game.load.spritesheet('kaboom', public_url+'sprites/invasores/explode.png', 128, 128);
    game.load.image('starfield', public_url+'sprites/invasores/starfield.png');
    game.load.image('background', public_url+'sprites/invasores/background2.png');
    game.load.image('menu', public_url+'sprites/invasores/reiniciar_continuar.png', 270, 180);
    game.load.image('menu_terminado', public_url+'sprites/invasores/opciones_terminar.png', 270, 180);
    game.load.spritesheet('boton', public_url+'sprites/invasores/button_sprite_sheet.png', 193, 71);

    //cargar monedas
    game.load.image('10centimos', public_url+'sprites/invasores/10centimos.png');
    game.load.image('20centimos', public_url+'sprites/invasores/20centimos.png');
    game.load.image('50centimos', public_url+'sprites/invasores/50centimos.png');
    game.load.image('1sol', public_url+'sprites/invasores/1sol.png');
    game.load.image('2soles', public_url+'sprites/invasores/2soles.png');
    game.load.image('5soles', public_url+'sprites/invasores/5soles.png');
    game.load.image('10soles', public_url+'sprites/invasores/10soles.png');
    game.load.image('20soles', public_url+'sprites/invasores/20soles.png');
    game.load.image('50soles', public_url+'sprites/invasores/50soles.png');
    game.load.image('100soles', public_url+'sprites/invasores/100soles.png');
    game.load.image('200soles', public_url+'sprites/invasores/200soles.png'); 

    //cargar figuras
    game.load.image('triangulo', public_url+'sprites/invasores/figuras/triangulo.png'); 
    game.load.image('circulo', public_url+'sprites/invasores/figuras/circulo.png');
    game.load.image('cuadrado', public_url+'sprites/invasores/figuras/cuadrado.png');
    game.load.image('estrella', public_url+'sprites/invasores/figuras/estrella.png');
    game.load.image('hexagono', public_url+'sprites/invasores/figuras/hexagono.png');
    game.load.image('luna', public_url+'sprites/invasores/figuras/luna.png');
    game.load.image('octogono', public_url+'sprites/invasores/figuras/octogono.png');
    game.load.image('pentagono', public_url+'sprites/invasores/figuras/pentagono.png');
    game.load.image('poligono5irr', public_url+'sprites/invasores/figuras/poligono5irr.png');
    game.load.image('poligono6irr', public_url+'sprites/invasores/figuras/poligono6irr.png');
    game.load.image('rectangulo', public_url+'sprites/invasores/figuras/rectangulo.png');
    game.load.image('rombo', public_url+'sprites/invasores/figuras/rombo.png');
    game.load.image('semicirculo', public_url+'sprites/invasores/figuras/semicirculo.png');
    game.load.image('trapecio', public_url+'sprites/invasores/figuras/trapecio.png');
    game.load.image('trapecioIrr', public_url+'sprites/invasores/figuras/trapecioIrr.png');

    //
    //fondo para las palabras
    game.load.image('fondorojo', public_url+'sprites/invasores/fondorojo.png');
    game.load.image('fondoazul', public_url+'sprites/invasores/fondoazul.png');
    game.load.image('fondoverde', public_url+'sprites/invasores/fondoverde.png');
    game.load.image('fondonaranja', public_url+'sprites/invasores/fondonaranja.png');
    game.load.image('fondomostaza', public_url+'sprites/invasores/fondomostaza.png');        

    //vidas con monedas
    game.load.spritesheet('coin', public_url+'sprites/invasores/coin.png', 32, 32);

    // sonido
    game.load.audio('blaster', public_url+'audio/invasores/squit.mp3');
    game.load.audio('sonidobala', public_url+'audio/invasores/blaster.mp3');
    game.load.audio('explosion', public_url+'audio/invasores/explosion.mp3');
    game.load.audio('sonmoneda', public_url+'audio/invasores/ping.mp3');
    //game.load.audio('sonfondo', public_url+'audio/invasores/instrumental.mp3');
    game.load.audio('sonfondo', public_url+'audio/invasores/top1.mp3');

    //game.load.binary('macrocosm', public_url+'audio/invasores/macrocosm.mod', modLoaded, this);

}

var player;
var aliens;
var bullets, balasobjetivo;
var bulletTime = 0;
var bala2tiempo = 0;
var cursors;
var fireButton;
var objetivo;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var centimos = 0;
var centimosString = '';
var centimosText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var uno,dos,tres;
var monedas;
var w=800;
var h=600;
var posicion_navesy;
var opcionesYN, menu, choiseLabel;
var textodentro1,textodentro2,textodentro3;
var fondopos;

//Variables comprension lectora
var grupotextos, untexto;
var coins, coin;
var primero, segundo, tercero;
var menu_terminado;
var juegoTerminadoYN='N';
var boton;
var textoinformativo;
var corriendoJuegoYN='Y';
var dentroYN;

//variables de sonido
var balaAsonido;
var balaDsonido;
var explota, sonmoneda,sonfondo;
var teclas;
var sounds;

function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bala2');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    //  grupo de balas de objetivo
    balasobjetivo = game.add.group();
    balasobjetivo.enableBody = true;
    balasobjetivo.physicsBodyType = Phaser.Physics.ARCADE;
    balasobjetivo.createMultiple(30, 'bullet');
    balasobjetivo.setAll('anchor.x', 0.5);
    balasobjetivo.setAll('anchor.y', 1);
    balasobjetivo.setAll('outOfBoundsKill', true);
    balasobjetivo.setAll('checkWorldBounds', true);

    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    player = game.add.sprite(400, 570, 'ship');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);

    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    coins = game.add.group();
    coins.enableBody = true;
    coins.physicsBodyType = Phaser.Physics.ARCADE;

    //
    monedas = game.add.group();
    monedas.enableBody = true;
    monedas.physicsBodyType = Phaser.Physics.ARCADE;

    //
    grupotextos = game.add.group();
    grupotextos.enableBody = true;
    grupotextos.physicsBodyType = Phaser.Physics.ARCADE;

    createAliens();

    if (tipoejercicio == 'dinero')
    {
        //  Cantidad de Soles
        scoreString = 'Soles : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

        //  Cantidad de céntimos
        centimosString = 'Céntimos : ';
        centimosText = game.add.text(10, 50, centimosString + centimos, { font: '25px Arial', fill: '#fff' });

        lives = game.add.group();
        //game.add.text(game.world.width - 100, 10, 'Vidas: ', { font: '34px Arial', fill: '#fff' });
        game.add.text(10, 90, 'Vidas: ', { font: '25px Arial', fill: '#fff' });

        posicion_navesy= 140;
    }
    else if (tipoejercicio == 'figuras')
    {
        //  Número de figuras
        scoreString = 'Correctos: ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '30px Arial', fill: '#fff' });

        //  Lives    
        lives = game.add.group();
        game.add.text(10, 60, 'Vidas: ', { font: '25px Arial', fill: '#fff' });

        posicion_navesy= 110;
    }
    else if (tipoejercicio == 'comprension')
    {
        var coin_estatico = game.add.sprite(10, 10, 'coin');
        coin_estatico.alpha = 0.8;
        scoreText = game.add.text(45, 9, score, { font: '34px Arial', fill: '#fff' });
        //  Lives    
        lives = game.add.group();
        game.add.text(10, 60, 'Vidas: ', { font: '25px Arial', fill: '#fff' });

        posicion_navesy= 110;

        //crear_coins();
    }
        
    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;
    var j=0,ship;
    for (var i = 0; i < 5; i++) 
    {
        if(i>=3)
        {
            posicion_navesy = 140;
            ship = lives.create(20 + (30 * j), posicion_navesy, 'ship');
            j++;
        }
        else
        {
            ship = lives.create(20 + (30 * i), posicion_navesy, 'ship');
        }
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.8;
    }

    //sonido
    balaAsonido = game.add.audio('sonidobala',0.2);
    balaDsonido = game.add.audio('blaster',0.2);
    explota = game.add.audio('explosion',0.3);
    sonmoneda = game.add.audio('sonmoneda',0.3);
    sonfondo = game.add.audio('sonfondo');

    //game.sound.setDecodedCallback(sounds, start, this);
    //game.sound.setDecodedCallback([ balaAsonido, balaDsonido ], empezar, this);


    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.D);
    objetivo = game.input.keyboard.addKey(Phaser.Keyboard.A);

    //
    pause_label = game.add.text(w - 140, 20, 'Opciones', { font: '26px Arial Black', fill: '#fff' });
    pause_label.inputEnabled = true;
    pause_label.events.onInputUp.add(opciones);

    // Add a input listener that can help us return from being paused
    game.input.onDown.add(unpause_opciones, self);
    
}
function start() {

    sounds.shift();

    sonfondo.loopFull(0.6);
    //bass.onLoop.add(hasLooped, this);

    //text.text = 'bass';

}
function opciones()
{
    // When the paus button is pressed, we pause the game
    game.paused = true;

    //desabilitar el boton de texto
    $("#textocompleto1").css("pointer-events", "none");

    // Then add the menu
    menu = game.add.sprite(w/2, h/2, 'menu');
    menu.anchor.setTo(0.5, 0.5);

    if(tipoejercicio != 'comprension')
    {
        // And a label to illustrate which menu item was chosen. (This is not necessary)
        choiseLabel = game.add.text(w/2, h-150, 'Haz click fuera del cuadro para continuar', { font: '30px Arial', fill: '#fff' });
        choiseLabel.anchor.setTo(0.5, 0.5);
    }
    opcionesYN = 'Y';
    corriendoJuegoYN='N';
}
function unpause_opciones(event){
        // Only act if paused
        if(game.paused && corriendoJuegoYN=='N'){

                // Calculate the corners of the menu
                var x1 = w/2 - 270/2, x2 = w/2 + 270/2,
                    y1 = h/2 - 180/2, y2 = h/2 + 180/2;

                // Check if the click was inside the menu
                if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
                    // The choicemap is an array that will help us see which item was clicked
                    var choisemap = ['Uno', 'Dossss'];

                    // Get menu local coordinates for the click
                    var x = event.x - x1,
                        y = event.y - y1;

                    // Calculate the choice 
                    var choise = Math.floor(x / 270) + Math.floor(y / 90);

                    // Display the choice
                    if(choise == 0)
                    {
                        console.log('primera opcion');
                        if(tipoejercicio=='comprension')
                        {
                            if(juegoTerminadoYN=='Y')
                            {
                                menu_terminado.destroy();
                                textoinformativo.destroy();
                                opcionesYN='N';

                                // Unpause the game
                                game.paused = false;

                                restart();  
                            }
                            else
                            {
                                menu.destroy();
                                opcionesYN='N';
                                // Unpause the game
                                game.paused = false;
                                restart();
                            }          
                        }
                        else
                        {
                            // Remove the menu and the label
                            menu.destroy();
                            choiseLabel.destroy();

                            opcionesYN='N';

                            // Unpause the game
                            game.paused = false;

                            restart();
                        }
                    }
                    else
                    {
                        console.log('segunda opcion');
                        if(tipoejercicio=='comprension')
                        {
                            if (juegoTerminadoYN=='Y')
                            {
                                console.log('por ahora no hacer nada');

                                //parar_tiempo();
                                clearInterval(tiempo_corriendo);


                                menu_terminado.destroy();

                            }
                            else
                            {
                                // Remove the menu and the label
                                menu.destroy();

                                opcionesYN='N';

                                // Unpause the game
                                game.paused = false;
                            }    
                        }
                        else
                        {
                            // Remove the menu and the label
                            menu.destroy();
                            choiseLabel.destroy();

                            opcionesYN='N';

                            // Unpause the game
                            game.paused = false;
                            
                            siguiente_actividad();
                        }
                    }
                    dentroYN = 'Y';
                    //despues elegir cualquier opcion, habilitamos el boton de texto
                    $("#textocompleto1").css("pointer-events", "auto");
                    
                }
                else{ // cuando el click es fuera del cuadro
                    if(tipoejercicio!='comprension')
                    {
                        if(opcionesYN == 'Y')
                        {
                            // Remove the menu and the label
                            menu.destroy();
                            choiseLabel.destroy();

                            opcionesYN='N';

                            // Unpause the game
                            game.paused = false;
                        }  
                    }
                    dentroYN = 'N';      
                }
                if(dentroYN=='Y')
                corriendoJuegoYN='Y';
            
        }
};
function createAliens () {

    if (tipoejercicio == 'dinero')
    {
    var alien, cent10, cent20, cent50, soles200, soles100, soles50, soles20, soles10, soles5, soles2, soles1;
    for (var y = 12; y > 0; y--)
    {
        aleatorios();
        for (var x = 14; x > 0; x--)
        {
            if(y<3)
            {   
                if (uno == x)
                {                   
                    cent20 = monedas.create(x * 45, y * 50, '20centimos');
                    cent20.anchor.setTo(0.5, 0.5);
                }
                else if(dos== x)
                {
                    cent10 = monedas.create(x * 45, y * 50, '10centimos');
                    cent10.anchor.setTo(0.5, 0.5);
                }
                else
                {
                    alien = aliens.create(x * 45, y * 50, 'invader');
                }
                
            }
            else if(y>=3 && y<6)
            {
                if (uno == x)
                {                   
                    soles2 = monedas.create(x * 45, y * 50, '2soles');
                    soles2.anchor.setTo(0.5, 0.5);
                }
                else if(dos== x)
                {
                    soles1 = monedas.create(x * 45, y * 50, '1sol');
                    soles1.anchor.setTo(0.5, 0.5);
                }
                 else if(tres== x)
                {
                    cent50 = monedas.create(x * 45, y * 50, '50centimos');
                    cent50.anchor.setTo(0.5, 0.5);
                }
                else
                {
                    alien = aliens.create(x * 45, y * 50, 'invader');
                }

            }
            else if(y>=6 && y<9)
            {
                if (uno == x)
                {                   
                    soles20 = monedas.create(x * 45, y * 50, '20soles');
                    soles20.anchor.setTo(0.5, 0.5);
                }
                else if(dos== x)
                {
                    soles10 = monedas.create(x * 45, y * 50, '10soles');
                    soles10.anchor.setTo(0.5, 0.5);
                }
                 else if(tres== x)
                {
                    soles5 = monedas.create(x * 45, y * 50, '5soles');
                    soles5.anchor.setTo(0.5, 0.5);
                }
                else
                {
                    alien = aliens.create(x * 45, y * 50, 'invader');
                }

            } 
            else if(y>=9 && y<12)
            {
                if (uno == x)
                {                   
                    soles200 = monedas.create(x * 45, y * 50, '200soles');
                    soles200.anchor.setTo(0.5, 0.5);
                }
                else if(dos== x)
                {
                    soles100 = monedas.create(x * 45, y * 50, '100soles');
                    soles100.anchor.setTo(0.5, 0.5);
                }
                 else if(tres== x)
                {
                    soles50 = monedas.create(x * 45, y * 50, '50soles');
                    soles50.anchor.setTo(0.5, 0.5);
                }
                else
                {
                    alien = aliens.create(x * 45, y * 50, 'invader');
                }
            }
            else
            {
                alien = aliens.create(x * 45, y * 50, 'invader');
            }
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            alien.play('fly');
            alien.body.moves = false;  
            
        }
    }

    aliens.x = 20;
    aliens.y = -550;

    monedas.x = 20;
    monedas.y = -550;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(aliens).to( { x: 100 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    var tween = game.add.tween(monedas).to( { x: 100 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);

    }
    else if (tipoejercicio == 'figuras')
    {
        var alien, triangulo;
        
        for (var y = 12; y > 0; y--)
        {
            aleatorios();
            for (var x = 14; x > 0; x--)
            {
                
                if(y==1 || y==6 || y==11)
                {   
                    if (uno == x)
                    {                   
                        alien = monedas.create(x * 45, y * 50, 'triangulo');
                    }
                    else if(dos== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'estrella');
                        //cent10.anchor.setTo(0.5, 0.5);
                    }
                    else if(tres== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'poligono5irr');
                    }
                    else
                    {
                        alien = aliens.create(x * 45, y * 50, 'invader');
                    }                        

                }
                else if(y==2 || y==5 || y==10)
                {
                    if (uno == x)
                    {                   
                        alien = monedas.create(x * 45, y * 50, 'cuadrado');
                    }
                    else if(dos== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'poligono6irr');
                        //cent10.anchor.setTo(0.5, 0.5);
                    }
                    else if(tres== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'octogono');
                    }
                    else
                    {
                        alien = aliens.create(x * 45, y * 50, 'invader');
                    }

                }
                else if(y==4 || y==7 || y==12)
                {
                    if (uno == x)
                    {                   
                        alien = monedas.create(x * 45, y * 50, 'rombo');
                    }
                    else if(dos== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'hexagono');
                        //cent10.anchor.setTo(0.5, 0.5);
                    }
                    else if(tres== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'luna');
                    }
                    else
                    {
                        alien = aliens.create(x * 45, y * 50, 'invader');
                    }

                }
                else if(y==3 || y==8 || y==9)
                {
                    if (uno == x)
                    {                   
                        alien = monedas.create(x * 45, y * 50, 'pentagono');
                    }
                    else if(dos== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'trapecio');
                        //cent10.anchor.setTo(0.5, 0.5);
                    }
                    else if(tres== x)
                    {
                        alien = monedas.create(x * 45, y * 50, 'circulo');
                    }
                    else
                    {
                        alien = aliens.create(x * 45, y * 50, 'invader');
                    }      

                }
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.moves = false;
            }
        }

        aliens.x = 20;
        aliens.y = -350;

        monedas.x = 20;
        monedas.y = -350;
         
        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to( { x: 70 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        var tween2 = game.add.tween(monedas).to( { x: 70 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        //  When the tween loops it calls descend
        tween.onLoop.add(descend, this);

    }
    else if (tipoejercicio == 'comprension')
    {
        var contadorpreg=0;
        var contadorcoin=0;
        var stylacho = { font: "18px Arial", fill: "#D6D5C8", wordWrap: true, wordWrapWidth: 140, align: "center", backgroundColor: "#ffff00" };
        var pos_respuestas = [80,300,520];
        var pos_img = [70,288,510];
        var tmpnro = nroejer_comprension;
        var listafondo = ['fondorojo','fondoazul','fondomostaza','fondoverde','fondonaranja'];
        var aleatorio_fondo;
        var plata;
        
        contadorpreg =preguntas_array.length; 
        contadorcoin =preguntas_array.length; 
        var filas = ( contadorpreg* 5) +3;
        //console.log('nro filas', filas);

        for (var y = filas; y > 0; y--)
        {
            aleatorios_por_pregunta();
            aleatorios();
            aleatorio_fondo = Math.floor((Math.random() * 5) + 1);
            
            for (var x = 15; x > 0; x--)
            {
                if(y==contadorpreg*5)
                {
                    //poner la fila de la respuesta y las otras alternativas
                    if(x==2)
                    {
                        fondopos = monedas.create(pos_img[primero-1], y*50-85, listafondo[aleatorio_fondo-1]);
                        fondopos.name=respuestas_array[tmpnro];
                        untexto = game.add.text(pos_respuestas[primero-1],y*50-70,respuestas_array[tmpnro],stylacho,grupotextos);
                    }
                    else if(x==7)
                    {
                        fondopos = monedas.create(pos_img[segundo-1], y*50-85, listafondo[aleatorio_fondo-1]);
                        fondopos.name=primerDistractor_array[tmpnro];
                        untexto = game.add.text(pos_respuestas[segundo-1],y*50-70,primerDistractor_array[tmpnro],stylacho,grupotextos);                    }
                    else if(x==12)
                    {
                        fondopos = monedas.create(pos_img[tercero-1], y*50-85, listafondo[aleatorio_fondo-1]);
                        fondopos.name=segundoDistractor_array[tmpnro];
                        untexto = game.add.text(pos_respuestas[tercero-1],y*50-70,segundoDistractor_array[tmpnro],stylacho,grupotextos);
                    }
                    else if(x==1||x==5||x==6||x==10||x==11||x==15)
                    {
                            alien = aliens.create(x * 45, y * 50, 'invader'); 
                    }
                }
                else if(y==contadorcoin*5+2)
                {
                    if(x==uno || x==dos || x==3)
                    {
                        plata = coins.create(x * 45, y * 50, 'coin');
                        plata.anchor.setTo(0.5, 0.5);
                    }
                    else
                    {
                        alien = aliens.create(x * 45, y * 50, 'invader');
                    }

                }
                else
                {
                    if(y==contadorpreg*5-1)//FALTA CORREGIR ESTO
                    {
                        if(x==1||x==5||x==6||x==10||x==11||x==15)
                        {
                            alien = aliens.create(x * 45, y * 50, 'invader'); 
                        }
                    }
                    else
                    {
                        alien = aliens.create(x * 45, y * 50, 'invader'); 
                    }
                    
                }
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.moves = false;

                 //Add animations to all of the coin sprites
                
            }
            if(y==contadorpreg*5-1)
            {   
                contadorpreg--;
                tmpnro++;
            }
            if(y==contadorcoin*5+2)
            {
                contadorcoin--;
            }
        }

        aliens.x = 20;
        //aliens.y = -140;
        aliens.y = -(250*preguntas_array.length);

        monedas.x = 20;
        //monedas.y = -140;
        monedas.y = -(250*preguntas_array.length);

        grupotextos.x =20;
        //grupotextos.y = -140;
        grupotextos.y = -(250*preguntas_array.length);

        coins.x =20;
        //coins.y = -140;
        coins.y = -(250*preguntas_array.length);
        coins.callAll('animations.add', 'animations', 'spin', [0, 1, 2, 3, 4, 5], 10, true);
        coins.callAll('animations.play', 'animations', 'spin');
       
         
        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to( { x: 70 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        var tween2 = game.add.tween(monedas).to( { x: 70 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        var tweentext = game.add.tween(grupotextos).to( { x: 70 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        var tweencoin = game.add.tween(coins).to( { x: 70 }, 5000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        //  When the tween loops it calls descend
        tween.onLoop.add(descend, this);
    }

    
}
function aleatorios()
{
    uno = Math.floor((Math.random() * 13) + 1);
    dos = Math.floor((Math.random() * 13) + 1);
    var bandera =true;
    while (uno == dos)
    {
        {
            dos = Math.floor((Math.random() * 13) + 1);
        }
        
    }
    while(bandera==true)
    {
        tres = Math.floor((Math.random() * 13) + 1);
        if (tres!=uno)
        {
            if (tres !=dos) {
                bandera =false;
            }
            
        }
    } 
}
function aleatorios_por_pregunta()
{
    primero = Math.floor((Math.random() * 3) + 1);
    segundo = Math.floor((Math.random() * 3) + 1);
    var bandera =true;
    while (primero == segundo)
    {
        {
            segundo = Math.floor((Math.random() * 3) + 1);
        }
    }
    while(bandera==true)
    {
        tercero = Math.floor((Math.random() * 3) + 1);
        if (tercero!=primero)
        {
            if (tercero !=segundo) {
                bandera =false;
            }
            
        }
    }
}
function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 12;
    monedas.y += 12;

    if(tipoejercicio=='comprension')
    {
       grupotextos.y +=12; 
       coins.y +=12;
    }

    var alienvivo;
    var monedaviva;
    if (monedas.countLiving()>0) 
    {
        monedaviva = monedas.getFirstAlive();
        if (monedaviva.world.y >=535)
        {
            terminar();
        }
    }
    if (aliens.countLiving()>0)
    {
       alienvivo = aliens.getFirstAlive();
       if(alienvivo.world.y >= 535)
       {
            terminar();
       } 
    }
    
    function terminar()
    {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER \n Click para reiniciar";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }
}

function update() {
  
    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -200;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 200;
        }

        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }

        if (objetivo.isDown)
        {
            bala_objetivo();
        }
        
        if (game.time.now > firingTimer)
        {
            enemyFires();
        }

        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);

        game.physics.arcade.overlap(balasobjetivo, aliens, objetivo_alien, null, this);
        game.physics.arcade.overlap(balasobjetivo, monedas, objetivo_moneda, null, this);
        game.physics.arcade.overlap(bullets, monedas, bullet_moneda, null, this);

        if(tipoejercicio=='comprension')
        {
            game.physics.arcade.overlap(bullets, coins, bullet_coin, null, this);
            game.physics.arcade.overlap(balasobjetivo, coins, balasobjetivo_coin, null, this);
        }

        

        //game.physics.arcade.collide(balasobjetivo, grupotextos, bullet_texto, null, this);
    }

}

function render() {

    // for (var i = 0; i < aliens.length; i++)
    // {
    //     game.debug.body(aliens.children[i]);
    // }

}
function objetivo_alien (balaobjetivo, alien)
{   
    balaobjetivo.kill();
}
function bullet_moneda (bullet, moneda)
{
    bullet.kill();
    if(tipoejercicio!='comprension')
    {
        moneda.kill(); 

        var explosion = explosions.getFirstExists(false);
        explosion.reset(moneda.body.x, moneda.body.y);
        explosion.play('kaboom', 30, false, true);   
    }
    

    //
}
function objetivo_moneda(balaobjetivo,moneda)
{
    sonmoneda.play();
    if (tipoejercicio=='dinero') 
    {
        balaobjetivo.kill();
        moneda.kill();

        switch(moneda.key)
        {
            case '200soles':
                  score += 200;
                  break;
            case '100soles':
                  score += 100;
                  break;
            case '50soles':
                score += 50;
                break;
            case '20soles':
                score += 20;
                break;
            case '10soles':
                score += 10;
                break;
            case '5soles':
                score += 5;
                break;
            case '2soles':
                score += 2;
                break;
            case '1sol':
                score += 1;
                break;
            case '50centimos':
                centimos += 50;
                break;
            case '20centimos':
                centimos += 20;
                break;
            case '10centimos':
                centimos += 10;
                break;
        }
        scoreText.text = scoreString + score;

        if(centimos >=100)
        {
            centimos = centimos-100;
            centimosText.text = centimosString + centimos;
            score +=1;
            scoreText.text = scoreString + score;        
        }
        else
        {
            centimosText.text = centimosString + centimos;
        }
    }
    else if(tipoejercicio=='figuras')
    {
        console.log('bala impacto a la figura', figura_array[nroejer_figura-1]);

        balaobjetivo.kill();
        moneda.kill();

        switch(moneda.key)
        {
            case 'triangulo':
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'triangulo':
                            score += 1;
                            break;
                        case 'poligono':
                            score +=1;
                            break;
                        case 'poligono_r':
                            score +=1;
                            break;                        
                        default:
                            score -= 1;                                  
                    } 
                break;
            case 'circulo':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'circulo':
                            score += 1;
                            break;
                        case 'nopoligono':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'cuadrado':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'cuadrilatero':
                            score += 1;
                            break;
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'estrella':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'hexagono':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'luna':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'nopoligono':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'octogono':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'pentagono':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'poligono5irr':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_i':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'poligono6irr':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_i':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'rectangulo':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'cuadrilatero':
                            score += 1;
                            break;
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'rombo':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'cuadrilatero':
                            score += 1;
                            break;
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'semicirculo':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'nopoligono':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'trapecio':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'cuadrilatero':
                            score += 1;
                            break;
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_r':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
            case 'trapecioIrr':
                {
                    switch(figura_array[nroejer_figura-1])
                    {
                        case 'cuadrilatero':
                            score += 1;
                            break;
                        case 'poligono':
                            score += 1;
                            break;
                        case 'poligono_i':
                            score += 1;
                            break;
                        default:
                            score -= 1;                                  
                    }
                }
                break;
        }
        scoreText.text = scoreString + score;
    }
    else if (tipoejercicio == 'comprension')
    {
        balaobjetivo.kill();
        //var indice= monedas.getIndex(moneda);
        //var texto_unitario = grupotextos.getChildAt(indice); 
        //console.log('indice ',indice);
        console.log('numero ejercicio ',nroejer_comprension);

        //destruir las fila de respuestas 
        if(moneda.name==respuestas_array[nroejer_comprension])
        {
            //sumar puntos
            Agregar_puntaje(puntajexpregunta);
        }
        var rta1 = monedas.getChildAt(nroejer_comprension*3+0);
        var rta2 = monedas.getChildAt(nroejer_comprension*3+1);
        var rta3 = monedas.getChildAt(nroejer_comprension*3+2);

        var txt1 = grupotextos.getChildAt(0);
        var txt2 = grupotextos.getChildAt(1);
        var txt3 = grupotextos.getChildAt(2);

        rta1.kill();
        rta2.kill();
        rta3.kill();

        txt1.destroy();
        txt2.destroy();
        txt3.destroy();

        nroejer_comprension++;
        if(nroejer_comprension+1>respuestas_array.length)
        {
            //respondió todas las preguntas
            $('#enunciado').text('No hay mas preguntas, destruye todos los invasores y gana');
        }
        else
        {
            cargar_enunciado_array();    
        }      
        //moneda.kill();

    }
}
function bullet_coin(bala,coin)
{
    bala.kill();
    coin.kill();
}
function balasobjetivo_coin(bala,coin)
{
    sonmoneda.play();
    bala.kill();
    coin.kill();
    score += 1;
    scoreText.text = score;
}
function collisionHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (aliens.countLiving() == 0)
    {
        if(tipoejercicio='comprension')
        {
            juegoTerminadoYN='Y';
            enemyBullets.callAll('kill',this);
            game.paused = true;
            corriendoJuegoYN='N';
            menu_terminado = game.add.sprite(400, 300, 'menu_terminado');
            menu_terminado.anchor.setTo(0.5,0.5);

            textoinformativo = game.add.text(400, 150, '¡TERMINASTE!', {fill: '#fff' });
            textoinformativo.fontWeight = 'bold';
            textoinformativo.fontSize = 50;
            textoinformativo.font = 'Arial Black';
            textoinformativo.anchor.setTo(0.5, 0.5);
            textoinformativo.stroke = "#008CBA";
            textoinformativo.strokeThickness = 16;
            textoinformativo.setShadow(2, 2, "#333333", 2, true, false); 
        }
        else
        {
            enemyBullets.callAll('kill',this);
            stateText.text = " COMPLETADO, \n siguiente";
            stateText.visible = true;

            //the "click to restart" handler
            game.input.onTap.addOnce(siguiente_actividad,this);
        }
        
    }

}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    explota.play();

    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER \n Click para reiniciar";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}
function over() {
    console.log('button over');
}
function out() {
    console.log('button out');
}
function actionOnClick()
{
    console.log('click');
}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(enemyBullet,player,120);
        firingTimer = game.time.now + 6000;//estaba en 2000
    }

}

function fireBullet () {

    
    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        balaAsonido.play();
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
        }
    }
}
function bala_objetivo() {

    
    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bala2tiempo)
    {
        balaDsonido.play();
        //  Grab the first bullet we can from the pool
        balaobjetivo = balasobjetivo.getFirstExists(false);

        if (balaobjetivo)
        {
            //  And fire it
            balaobjetivo.reset(player.x, player.y + 8);
            balaobjetivo.body.velocity.y = -400;
            bala2tiempo = game.time.now + 200;
        }
    }
}

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}
function restart () {

    //  A new level starts
    
    //resets the life count
    lives.callAll('revive');
    
    if(tipoejercicio!='comprension')
    {
        //borrar el score
        score=0;
        scoreText.text = '0';
    }
    else
    {      
        if(juegoTerminadoYN=='N')
        {
            grupotextos.removeAll();
            coins.removeAll();

            //momendas cogidas debe bajar
        }

        //reiniciar el numero de ejercicio
        nroejer_comprension=0;
        //reinicial las preguntas
        cargar_enunciado_array();
        //reiniciar el puntaje
        $('#puntaje_acumulado').html(0);
        scoreText.destroy();
        scoreText = game.add.text(45, 9, 0, { font: '34px Arial', fill: '#fff' });
        juegoTerminadoYN='N';

    }
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    monedas.removeAll();
    createAliens();


    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
}
function siguiente_actividad () {

    //  A new level starts
    console.log('siguiente actividad');
    //resets the life count

    
    lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    monedas.removeAll();
    nroejercicio++;
    tipoejercicio = tipoejercicio_array[nroejercicio];

    //reiniciar el contador de soles y centimos
    score=0;
    scoreText.text = '0';

    if(tipoejercicio =='dinero')
    {
        centimos=0;
        centimosText.text = '0';
    }
    cargar_enunciado_array();
    
    create();
    //createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;

}
</script>
