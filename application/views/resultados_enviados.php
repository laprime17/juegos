<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body class="ejecutar_juego">
	<div class="ejecutar_juego-inicio">

		<div class="ejecutar_sesion-contenedor-enviado">
			Terminaste la sesión
		</div>
		<div class="ejecutar_sesion-contenedor-enviado-desc">
			¡Enviaste con éxito tus resultados!	
		</div>

		<?php $this->load->view('administracion/layouts/footer.php'); ?>

	</div>
</body>
</html>