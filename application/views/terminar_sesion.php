<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body class="ejecutar_juego">
	<div class="ejecutar_juego-inicio">
		
		<input type="hidden" value="<?php echo $sesion['sesion']->idsesion;?>"  id="idsesion" placeholder="IDSESIONNNNNNNNNN">

<div class="ejecutar_sesion-contenedor-titulo">
	Terminaste la sesión:  <?php 
	echo $sesion['sesion']->nombre;?>
</div>
<div class="ejecutar_sesion-contenedor-desc">
	¡Estos fueron los resultados que obtuviste!

	<table class="table"  style="margin-left: 3em; height: 10em; font-family: cursive;">
		<tr>
		<th>Juego </th>
		<th>Máximo puntaje </th>
		<th>Mis puntos</th>										
		<th>Mi tiempo</th>
		<th>Mi rendimiento de esta actividad</th>
		</tr>
		<?php
			$sumpuntos =0;
			$summinutos =0;
			$sumsegundos=0;
			$puntajemaximo=0;
			$min=0;
			$seg=0;
			$cont=0; 
			foreach ($resultados['juegos'] as $juego) {
				$sumpuntos = $sumpuntos + $juego->puntaje;
				$tiempo = $juego->duracion; 
				$minutos = floor($tiempo / 60);
				$segundos = $tiempo%60;
				$summinutos=$summinutos+$minutos;
				$sumsegundos= $sumsegundos+$segundos;

				//para la sumatoria final
				$mas_min = floor($sumsegundos / 60);
				$seg = $sumsegundos%60;
				$min = $summinutos+$mas_min;


				if($cont==0)
				{
					echo '<tr style="text-shadow: 0px 0px;"><td>'.$juego->nombre.'</td><td style="color:blue;">'.intval($resultados['juegoespecifico'][$cont]).'</td><td>'.$juego->puntaje.'</td><td>'.$minutos.'min '.$segundos.'seg</td><td rowspan="4"><img src="" id="imgrendimiento"/></td></tr>';
				}
				else
				{
					echo '<tr style="text-shadow: 0px 0px;"><td>'.$juego->nombre.'</td><td style="color:blue;">'.intval($resultados['juegoespecifico'][$cont]).'</td><td>'.$juego->puntaje.'</td><td>'.$minutos.'min '.$segundos.'seg</td></tr>';
				}
				
				$puntajemaximo=$puntajemaximo+$resultados['juegoespecifico'][$cont];
				$cont++;
			}

		?> 
		<tr>
		<th>Total </th>
		<th style="color:blue;" id="puntajemax"><?php echo $puntajemaximo;?></th>
		<th id="puntajehecho"><?php echo $sumpuntos;?></th>
		<th><?php echo $min;?>min <?php echo $seg;?>seg</th>
		</tr>
	</table>
		
</div>
<a class="boton amarillo btn_enviar_resultados" href="<?php echo base_url();?>sesion/resultados_enviados"> Enviar resultados</a>


<?php $this->load->view('administracion/layouts/footer.php'); ?>

<script>
	$(document).on('ready',ini);
    function ini()
    {
        $('.btn_enviar_resultados').click(function(){

        	var idsesion = $('#idsesion').val();
        	console.log('idsesion ', idsesion);
        $.ajax({
               type: "POST",
               url: base_url+'sesion/enviar_resultados_sesion/'+idsesion,
               cache: false,
               processData: false,
               async: false,
               success: function(data)
               {

               }, 
               error: function(data) {

               }
            });
        });

    	
    		var rendimiento = Math.round(parseInt($('#puntajehecho').html())*100/parseInt($('#puntajemax').html()));
	    	console.log("suma: ",rendimiento);
	    	if(rendimiento<50)//
	    	{
	    		//bajo rendimiento
	    		$("#imgrendimiento").attr("src", '<?php echo public_url()."img/mal.jpg"?>'); 
	    	}
	    	else if(rendimiento>=50 && rendimiento<70)
	    	{
	    		$("#imgrendimiento").attr("src", '<?php echo public_url()."img/regular.jpg"?>'); 	
	    	}
	    	else if(rendimiento>=70 && rendimiento<85)
	    	{
	    		$("#imgrendimiento").attr("src", '<?php echo public_url()."img/bien.jpg"?>'); 
	    	}
	    	else if(rendimiento>=85)
	    	{
	    		$("#imgrendimiento").attr("src", '<?php echo public_url()."img/muybien.jpg"?>'); 
	    	}
    	
    }
</script>
<script src="<?php echo public_url();?>js/jquery.timeout.interval.idle.js"></script>
<script src="<?php echo public_url();?>js/jquery.countdown.counter.js"></script>

</div>
</body>
</html>