<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico"/>
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body class="ejecutar_juego">
	<div class="ejecutar_juego-inicio">
		
		<input type="hidden" value="<?php echo $sesion['sesion']->idsesion;?>"  id="idsesion" placeholder="IDSESIONNNNNNNNNN">



<div class="ejecutar_sesion-contenedor-titulo">
	Bienvenido a la sesión bla bla: <?php 
	echo $sesion['sesion']->nombre;?>
</div>
<div class="ejecutar_sesion-contenedor-desc">
	¡Juega, aprende y gana puntos!</div>
<a class="boton amarillo btn_siguiente_juego" id="ir_primer_juego" > Empezar ahora</a>
<div class="ejecutar_sesion-contenedor-participantes">
	<div class="titulo">Participantes</div>
	<div class="div_blanco_rgba">
		<?php 
		foreach ($participantes as $participante) {

			if ($participante->foto_ruta==null) {
			 echo '<figure class="panel_inicio_participante" ><img  src="'.public_url().'fotos/default.jpg"><figcaption>'.$participante->nombres.' </figcaption></figure>';
			
			}
			else
			{
			 echo '<figure class="panel_inicio_participante" ><img  src="'.public_url().'fotos/'.$participante->foto_ruta.'"><figcaption>'.$participante->nombres.'</figcaption></figure>';
			 }

		}
		 ?>
		<!-- participantes
		juegos_hoy
		juegos
		grupos -->
		
	</div>
</div>
<?php $this->load->view('administracion/layouts/footer.php'); ?>



<script>

 	localStorage.setItem('juego_actual',0);

</script>
<script src="<?php echo public_url();?>js/ejecutar_juego.js"></script>
<script src="<?php echo public_url();?>js/jquery.timeout.interval.idle.js"></script>
<script src="<?php echo public_url();?>js/jquery.countdown.counter.js"></script>

</div>
</body>
</html>