<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<title>Pukllay: Aprende Jugando</title>
<!--  -->
<!-- estilos -->
<link rel="stylesheet" type="text/css" href="<?php echo public_url();?>css/estilo_pukllay2.css">

<!-- js´s -->
<script type="text/javascript" src="<?php echo public_url();?>pukllay/js/jquery1-11-2.min.js"></script>
</head>
<body>

<img src="<?php echo public_url();?>img/engranaje.png " class="engranaje"/>
<img src="<?php echo public_url();?>img/logo.png " alt="Pukllay"  class="logo"/>
<div class="contenedor">
    <div class="error">
	
    <?php 
    
    if (isset($error['usuario_incorrecto'])) {
       echo 'Tu usuario es incorrecto </br>';
    }
      if (isset($error['clave_incorrecto'])) {
       echo 'Tu contraseña es incorrecta. Vuelve a intentarlo.';
    }
   

     ?>
     </div>

<form class="login" method="post" action="<?php echo base_url();?>login/verificar">

	<div class="controls mg-t">
	    <div id="form_txtSesion" class="input_form">
         	<div class="ico_login ico_usuario dib"></div>	    
         	<input name="usuario" type="text" id="txtSesion" style="border:none" placeholder="Escribe tu nombre de usuario" autocomplete="off">
	    </div>
 	</div>
 	<div id="form_txtclave" class="input_form">
      	<div class="ico_login ico_pass dib"></div>
     	<input name="clave" type="password" id="txtClave" style="border:none" placeholder="¿Cuál es tu contraseña?" autocomplete="off">
    </div>
    <hr />
    <i><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste vero aperiam illum blanditiis ducimus.</p></i>
    <hr />

	<input type="submit" class="boton_entrar" value="Entrar"/>

</form>
</div>

	



	
</body>

</html>


