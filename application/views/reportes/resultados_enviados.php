<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body class="ejecutar_juego">
	<div class="ejecutar_juego-inicio">

<div class="ejecutar_sesion-contenedor-titulo">
	Terminaste la sesión: 
</div>
<div class="ejecutar_sesion-contenedor-desc">
	¡Enviaste con exito tus resultados!

	
</div>
<a class="boton amarillo " id="enviar_resultados"> Volver a inicio</a>


<?php $this->load->view('administracion/layouts/footer.php'); ?>

<script>
	
</script>
<script src="<?php echo public_url();?>js/jquery.timeout.interval.idle.js"></script>
<script src="<?php echo public_url();?>js/jquery.countdown.counter.js"></script>

</div>
</body>
</html>