<!-- estrucutura de alumno siempre -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pukllay: Aprende Jugando</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo public_url();?>iconos/ico_pukllay.ico" />
	<link rel="stylesheet" href="<?php echo public_url();?>css/estilo.css">
	<link rel="stylesheet" href="<?php echo public_url();?>foundation/css/foundation.css">
	<script src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
	<script src="<?php echo public_url();?>foundation/js/foundation.min.js"></script>
	<script type="text/javascript" src="<?php echo public_url();?>js/jquery.sticky.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body class="ejecutar_juego">

<div class="prev">
<input type="hidden" id="idsesion" value="<?php echo $idsesion;?>">
	<div class="prev-content">
		<div class="prev-content-titulo row">
			<div class="mt03m prev-content-titulo-titulo columns small-5 medium-5 large-5"><?php echo $juego->nombre;?> </div >
			<div class="mt03m prev-content-titulo-otros columns small-2 medium-2 large-2">0 intentos</div>
			<div class="mt03m prev-content-titulo-otros columns small-2 medium-2 large-2" id="tiempo_restante">06:25</div>
			<a class="prev-content-titulo-otros columns small-3 medium-3 large-3 btn_siguiente_juego">
				<div class="boton amarillo  dib" ><span class="icon-arrow-right"></span> Siguiente</div>
			</a>
		</div>
		<div class="prev-content-datos ">
			<div class="dib prev-content-datos-enunciado">Haz click en la hoja</div>
			
			<div class="prev-content-datos-puntos"><div id="puntaje_acumulado" class="fc_black">0</div><label>puntos</label> </div>
			
		</div>
		<figure class="prev-img">
		<?php echo $imagen;?>
		<div class="mapa_prev_final"></div>
		</figure>
	
	</div>

<a  class="link_siguiente" href=""></a>
</div>

</body>
</html>
<?php echo '<script> Puntos = '.json_encode($puntos).';
					pu = '.$puntaje_unitario.';
					tiempo_total = '.$mapa->tiempo.';
					idmapa='.$mapa->idmapa.';
					juego_general = '.json_encode($juego).'
					idjuego_sesion='.$juego_sesion->idjuego_sesion.';
					juego_sesion_estado= "'.$juego_sesion->estado.'";
					</script>'; ?>

<?php $this->load->view('administracion/layouts/footer.php'); ?>
<script>
	
	$(document).on('ready',ini);
	function ini()
	{
		punto_actual = 0;
		boton_actual = null;
		puntaje = 0;
		siguiente_pregunta();
		dibujar_puntos();
		updateReloj();

		$('.btn_siguiente_juego').click(function(){
		puntaje_final = Math.round(puntaje);
    	$.ajax({
		       type: "POST",
		       url: base_url+'sesion/guardar_puntaje/',
		       dataType:"json", 
		       data:'idjuego_sesion='+idjuego_sesion+'&puntaje='+puntaje_final+'&duracion=235',
		       cache: false,
               // contentType: false, -- evita en´vio por post
	     	   processData: false,
		       success: function(data)
		       {

		       }, 
		    	 error: function(data) {

                }
		    });
		});
	}
	function dibujar_puntos()
	{
		for (var i = Puntos.length - 1; i >= 0; i--) {

			py_ = parseInt(Puntos[i].py);
			px_ = parseInt(Puntos[i].px);
			px_-=36;
			py_-=36;

			boton_actual = document.createElement('div');
			$(boton_actual).addClass("prev-boton");
			$(boton_actual).attr("data-nrodepunto",i);
			$(boton_actual).css('position','absolute');
			$(boton_actual).css('top',py_+'px');
			$(boton_actual).css('left',px_+'px');
			$(boton_actual).attr('onclick','Procesar_respuesta(this)');
			$('.prev-img').append(boton_actual);
		};
		
	}
	function Procesar_respuesta(boton)
	{
		punto_elegido = $(boton).data('nrodepunto');
		if (punto_elegido==punto_actual) {
			// es correcto
			console.log('correcto');
			// transformar boton
			$(boton).addClass('correcto');
			$(boton).html('<img src="'+public_url+'img/mapa_correcto.png"/>');
			// aumentar puntaje

			puntaje+=pu;
			
			$('#puntaje_acumulado').html(Math.round(puntaje));
			// continuar
			punto_actual++;
			siguiente_pregunta('Correcto');
		}
		else
		{
			console.log('No es correcto');
			$(boton).addClass('correcto');
			$(boton).html('<img src="'+public_url+'img/cross.png"/>');
		}
	}
	function siguiente_pregunta()
	{
		if (punto_actual<Puntos.length) 
		{
			var texto = 'Haz click en '+Puntos[punto_actual].descripcion;
			$('.prev-content-datos-enunciado').html(texto);
		}else
		{
			$('.mapa_prev_final').addClass('show');
			$('.mapa_prev_final').html('<div class="content">Felicidades</div>');
		}
	}
	 function updateReloj()
    {
        if(tiempo_total==1)
        {
            console.log('Se terminó');
            $('#tiempo_restante').html('00:00');
        }
        else{
            
        	tiempo_total-=1;
            // convertir el tiempo total a min y segundos
            trestante_sec = tiempo_total % 60;

            trestante_min = tiempo_total/60;
            trestante_min = Math.floor(trestante_min);

            $('#tiempo_restante').html(trestante_min+':'+ajustar('1',trestante_sec));
            //
            setTimeout("updateReloj()",1000);
        }
    }
    function ajustar(tam, num) {

	    if (num.toString().length <= tam) return ajustar(tam, "0" + num)
	    else return num;
    }
   


</script>

<script src="<?php echo public_url();?>js/ejecutar_juego.js"></script>
<!--<script src="<?php echo public_url();?>js/administracion/mapa_prev.js"></script>-->
