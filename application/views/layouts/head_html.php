	<!doctype html>
	<html>
	<head>
		<title></title>
		<meta charset="utf-8">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="<?php echo public_url();?>bootstrap/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="<?php echo public_url();?>bootstrap/css/bootstrap-theme.min.css">

		<!-- upload imagen -->
		<link href="<?php echo public_url();?>bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
		<!-- estilos propios -->
		<link rel="stylesheet" type="text/css" href="<?php echo public_url();?>css/estilo.css">

		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="<?php echo public_url();?>js/jquery-2.0.3.min.js"></script>
		<script src="<?php echo public_url();?>bootstrap/js/bootstrap.min.js"></script>
		<!-- upload imagen -->
		<script src="<?php echo public_url();?>bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>

	</head>
	<body>