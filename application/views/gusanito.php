 
<?php $this->load->view('layouts/navbar_administrador');?>
<?php $this->load->view('layouts/header_juego.php'); ?>

<!-- <canvas id="phaser-example"> el navegador no soporta canvas</canvas> -->
<?php
echo '<script type="text/javascript">
var public_url = "'.public_url().'";
</script>';
 ?>


</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
    console.log(public_url);
 

var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {
    
    game.load.image('arrow', public_url+'sprites/arrow.png');
    game.load.image('bola', public_url+'sprites/bola.png')
}
var sprite;
function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.stage.backgroundColor = '#0072bc';
    sprite = game.add.sprite(400, 300, 'arrow');
    sprite.anchor.setTo(0.5, 0.5);

    sprite_bola = game.add.sprite(400, 300, 'bola');
    sprite_bola.name =  'bola';

    //	Enable Arcade Physics for the sprite
    game.physics.enable(sprite, Phaser.Physics.ARCADE);
    game.physics.enable(sprite_bola, Phaser.Physics.ARCADE);

    //	Tell it we don't want physics to manage the rotation
    sprite.body.allowRotation = false;
    sprite_bola.body.immovable = true;

}

function update() {

    sprite.rotation = game.physics.arcade.moveToPointer(sprite, 60, game.input.activePointer, 500);
    //game.physics.arcade.collide(sprite1, sprite_bola, collisionHandler, null, this);

}

function render() {

    
    // game.debug.body(sprite1);
    // game.debug.body(sprite2);
}
});
</script>