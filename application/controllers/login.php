<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function construct()
	{
		parent::__construct();
	}
	public function index($data_error=false)
	{
		$data['error'] = $data_error;
		$this->load->view('pukllay',$data);
		/*$this->load->view('pukllay2',$data);*/
	}
	public function verificar()
	{


		$usuario = $this->input->post('usuario');
		$clave = $this->input->post('clave');

 		$this->load->model('mdl_persona');
 		$recurso = $this->mdl_persona->get_by_usuario($usuario);
 		$data_error_user=array();

 		// var_dump(md5($clave));
 		// var_dump($recurso->row()->clave);
 		if ($recurso->num_rows()==1) {
 			
 			
 				if ($recurso->row()->clave == md5($clave) ) {
 				//correcto
 					
 				
 				$persona = $this->mdl_persona->obtener_persona_usuario($recurso->row());
 				$foto = $persona->foto_ruta;
 				if ($foto =='') {
 					$foto = public_url().'fotos/default.jpg';
 				}

 				$nuevo_usuario=array(
 					'nombres'=> $persona->nombres,
 					'id'=> $persona->idusuario,
 					'ap'=> $persona->ap,
 					'am'=> $persona->am,
 					'nombre_usuario'=>$usuario,
 					'rol'=> $persona->rol,
 					'foto_ruta'=>$persona->foto_ruta,
 					'nombre_completo'=>$persona->nombres.' '.$persona->ap.' '.$persona->am,
 					'logged_in'=>true,
 					'tipo_usuario'=>$persona->idtipo_usuario


 					);
 				

 				$this->session->set_userdata($nuevo_usuario);
 			
 				redirect('inicio');
 			}
 			else
 			{
 				$data_error_user['clave_incorrecto'] =true;
 				$this->index($data_error_user);	
 			}
 		}
 		else
 		{
 			$data_error_user['usuario_incorrecto'] =true;
 			$this->index($data_error_user);	
 		}
	}
	public function cerrar_sesion()
	{
		$this->session->sess_destroy();
		$this->index();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */