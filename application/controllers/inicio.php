<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function construct()
	{
		parent::__construct();
	}
	public function index($data_error=false)
	{
		parent::__construct();

		$this->load->model('mdl_inicio');
		$this->load->model('mdl_persona');
		$usuario = $this->session->all_userdata();


		$data = array('usuario' => $usuario,
					  'participantes'=>$this->mdl_inicio->participantes($usuario['id']),
					  'juegos_hoy'=>$this->mdl_inicio->juegos($usuario['id'],hoy_a_m_d()),
					  'juegos'=>$this->mdl_inicio->juegos($usuario['id']),
					  'sesiones_hoy'=>$this->mdl_inicio->sesiones($usuario['id'],hoy_a_m_d()),
					  'sesiones'=>$this->mdl_inicio->sesiones($usuario['id']),
					  'grupos'=>$this->mdl_persona->grupos_x_persona($usuario['id'])
					  );
		$this->load->view('inicio/inicio',$data);

		
	}
	public function resultados()
	{
		$this->load->model('mdl_inicio');
		$this->load->model('mdl_persona');
		$usuario = $this->session->all_userdata();
	

		$data = array('usuario' => $usuario,
					  'sesiones'=>$this->mdl_inicio->sesiones($usuario['id']),
					  'resultados'=>$this->mdl_persona->resultados_x_persona($usuario['id'])
					  );
		$this->load->view('inicio/panelinicio_resultados.php',$data);
	}
	public function grupos()
	{
		$this->load->model('mdl_inicio');
		$this->load->model('mdl_persona');
		$usuario = $this->session->all_userdata();
	


		$data = array('usuario' => $usuario,
					  'sesiones'=>$this->mdl_inicio->sesiones($usuario['id']),
					  'grupos'=>$this->mdl_persona->grupos_x_persona($usuario['id'])
					  );
		$this->load->view('inicio/panelinicio_grupos.php',$data);
	}
	public function sesiones()
	{
		$this->load->model('mdl_inicio');
		$this->load->model('mdl_persona');
		$usuario = $this->session->all_userdata();
		$data = array('usuario' => $usuario,
					  'sesiones_hoy'=>$this->mdl_inicio->sesiones($usuario['id'],hoy_a_m_d()),
					  'sesiones'=>$this->mdl_inicio->sesiones($usuario['id']),
					  );
		$this->load->view('inicio/panelinicio_sesiones.php',$data);

	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */