<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Administrador extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function construct()
	{
		parent::__construct();
	}
	public function index()
	{
		
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GRUPOS Y SESIONES' => base_url().'administrador/juegos');


		$data = array('uusuario' => $usuario = $this->session->all_userdata()
			);
		$data['breadcumb'] = $this->breadcumb($links);
		$this->load->view('administracion/inicio',$data);
	}
	public function get_usuarios_ajax($tipo)
	{
		$this->load->model('mdl_persona');
		$usuarios = $this->mdl_persona->get_usuarios_by_tipo($tipo);
		echo json_encode($usuarios);
	}
	public function eliminar_grupo($idgrupo)
	{
		$this->load->model('mdl_administrador');
		$rows = $this->mdl_administrador->eliminar_grupo($idgrupo);
		$result = array('result' => $rows);
		echo json_encode($result);
	}
	public function buscar_usuario_ajax()
	{
		$coin_nombres = $this->input->post('nombres');
		$this->load->model('mdl_persona');
		$usuarios = $this->mdl_persona->buscar_coincidencia_usuario($coin_nombres);
		echo json_encode($usuarios);

	}
	public function editar_usuario($idusuario)
	{
		$uusuario  = $this->session->all_userdata();
			$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GESTIÓN DE USUARIOS' => base_url().'administrador',
					   
					   strtoupper('editar usuario: '.$uusuario['nombre_completo']) =>'');
		$data = array('uusuario' => $uusuario ,
			'usuario' => $this->db->query('select * from persona where idusuario = '.$idusuario)->result()[0],
			'breadcumb' => $this->breadcumb($links));
		$this->load->view('administracion/editar_usuario',$data);
	}
	public function panel()
	{
		
	}
	public function form_nuevo_usuario()
	{
		// $this->load->view('administracion/paneles/form_nuevo_usuario');
		$cadena = '<form action="" enctype="multipart/form-data" method="post" id="form_nuevousuario">

									<label for="nombres">Nombre</label>
									<input type="text" name="nombres" placeholder="Nombres" id="nombres">
									<div class="row">
										<div class="columns large-6">
											<label for="">Apellido paterno</label>
											<input type="text" placeholder="Apellido Paterno" name="ap">
										</div>
										<div class="columns large-6">
											<label for="">Apellido Materno</label>
											<input type="text" placeholder="Apellido Materno" name="am">
										</div>
									</div>
									<div class="row">
										<div class="columns large-6">
											<label for="">Fecha de nacimiento</label>
											<input type="date" placeholder="Apellido Paterno" name="fecha_nacimiento">
										</div>
										<div class="columns large-6">
											<label for="">Tipo de Usuario</label>
											<select name="" id="" name="idtipo_usuario">
												<option value="3">Alumno</option>
												<option value="2">Profesor</option>
												<option value="1">Administrador</option>

											</select>
										</div>
									</div>
									<div class="row">
										<div class="columns large-6">
											<label for="">Nombre de usuario</label>
											<input type="date" placeholder="Apellido Paterno" name="usuario">
										</div>
										<div class="columns large-6">
											<label for="">Clave</label>
											<input type="password" placeholder="Apellido Materno" name="clave">
										</div>
									</div>
									<div class="button submit" >Guardar</div>
									
								</form>';
			echo  json_encode($cadena);
	}
	public function creartanque()
	{
		$this->load->view('administracion/nuevo_tanque.php');
	}
	public function crearabeja()
	{
		$this->load->view('administracion/nueva_abeja.php');
	}
	public function crearsnake()
	{
		$this->load->view('administracion/nuevo_snake.php');
	}
	public function crearmemoria()
	{
		$this->load->view('administracion/nuevo_memoria.php');
	}
	public function crearinvasores()
	{
		$this->load->view('administracion/nuevo_invasores.php');
	}
	public function crearmapa()
	{
		$this->load->view('administracion/nuevo_mapa.php');
	}
	public function crearrecolector()
	{
		$this->load->view('administracion/nuevo_recolector.php'); 
	}
	public function crearinvasorescomprension()
	{
		$this->load->view('administracion/nuevo_invasorescomprension.php');
	}
	public function detallessnake($idjuego)
	{
		$this->load->model('mdl_juegos');
		$data = array();
		$data['juegosnake'] = $this->mdl_juegos->snake_datos($idjuego);
		$data['snakedetalles'] = $this->mdl_juegos->snakedetalles_datos($idjuego);

		$this->load->view('administracion/editar_snake.php',$data);
	}
	public function detallesmemoria($idjuego)
	{
		$this->load->model('mdl_juegos');
		$data = array();
		$data['juegomemoria'] = $this->mdl_juegos->memoria_datos($idjuego);
		$data['memoriadetalles'] = $this->mdl_juegos->memoriadetalles_datos($idjuego);
		
		$this->load->view('administracion/editar_memoria.php',$data);
	}
	public function detallesinvasores($idjuego)
	{
		$this->load->model('mdl_juegos');
		$data = array();
		$data['juegoinvasores'] = $this->mdl_juegos->invasores_datos($idjuego);
		$data['invasoresdetalles'] = $this->mdl_juegos->invasoresdetalles_datos($idjuego);

		$this->load->view('administracion/editar_invasores.php',$data);
	}
	public function detallesrecolector($idjuego)
	{
		$this->load->model('mdl_juegos');
		$data = array();
		$data['juegorecolector'] = $this->mdl_juegos->recolector_datos($idjuego);
		$data['recolectordetalles'] = $this->mdl_juegos->recolectordetalles_datos($idjuego);

		$this->load->view('administracion/editar_recolector.php',$data); 
	}
	public function Guardar_usuario($idusuario)
	{	
		
		$this->load->model('mdl_persona');
		$data = $this->input->post();
		$data['rol'] = '';

		switch ($data['idtipo_usuario']) {
			case '1':
				$data['rol'] = 'administrador';
				break;
			case '2':
				$data['rol'] = 'profesor';
				break;
			case '3':
				$data['rol'] = 'alumno';
				break;

			default:
				$data['rol'] = 'alumno';
				break;
		}
		if (isset($data['clave'])) {
			$data['clave'] = md5($data['clave']);
		}
		$result = $this->mdl_persona->guardar_usuario($data,$idusuario);
		
		echo json_encode($result);
		


	}
	public function juegos()
	{
		// Muestra los juegos predefinidos 
		$this->load->model('mdl_administrador');
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'JUEGOS' => base_url().'administrador/juegos');
		

		$data = array('juegos' => $this->mdl_administrador->get_juegos_x_creador($this->session->userdata('id')),//juegos x administrador o profesor
				      'breadcumb' => $this->breadcumb($links),
				      'uusuario' => $usuario = $this->session->all_userdata());

		$this->load->view('administracion/juegos.php',$data);

	}
	public function simulacion($tipojuego,$idjuego)
	{
		// $data = array();
		$this->load->view('administracion/simulacion.php');
	}
	public function breadcumb($links)
	{
		$cadena =  '<ul class="breadcrumbs">';
		$i = 0;
		foreach ($links as $key => $value) {
			$class = '';
			if ($i == count($links)-1) {
				$class ='class="current"';
			}
			$cadena.='<li '.$class.'><a href="'.$value.'">'.$key.'</a></li>';
			$i++;
		}
		$cadena .=  '</ul>';
				
		return $cadena;
				
	}
	public function detalle_juego($idjuego)
	{
		$this->load->model('mdl_administrador');
		
		

		$juego =  $this->mdl_administrador->get_detalle_juego($idjuego);
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'JUEGOS' => base_url().'administrador/juegos',
					   $juego['juego_derivado']->nombre => base_url().'administrador/detalle_juego' );

		$data = array('breadcumb' =>$this->breadcumb($links),
					  'detalle'=>$juego);
		$this->load->view('administracion/detalle_juego',$data);
	}
	public function gruposysesiones( $idgrupo =null)
	{
		if ($idgrupo!=null) {
			$this->detalle_grupo($idgrupo);
		}
		else
		{
					$this->load->model('mdl_administrador');
		$this->load->model('mdl_persona');

		$idpersona = $this->session->userdata('id');
		// lista de grupos
		$data = array();
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GRUPOS Y SESIONES' => base_url().'administrador/juegos');


		$data['breadcumb'] = $this->breadcumb($links);
		$data['grupos'] = $this->mdl_administrador->grupos_x_creador($idpersona);

		$data['creador'] = $this->session->userdata('nombres').' '.$this->session->userdata('ap');
		$data['sesiones'] = $this->mdl_administrador->sesiones_x_creador($idpersona);
		$data['ruta_formnuevogrupo'] = base_url().'administrador/form_nuevo_grupo';
		$data['alumnos'] = $this->mdl_persona->get_todos();
		$data['uusuario'] = $usuario = $this->session->all_userdata();
		$this->load->view('administracion/gruposysesiones',$data);
		}

		// grupos y sesiones de la persona actual
	}
	public function grupo($idgrupo)
	{
		$this->load->model('mdl_administrador');
		$grupo = $this->mdl_administrador->by_id('grupo',$idgrupo);
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GRUPOS Y SESIONES' => base_url().'administrador/gruposysesiones',
					   'GRUPO '.strtoupper($grupo->nombre)=> base_url().'administrador/juegos');
		$alumnos = $this->db->query('select * from persona where idusuario in(select idpersona from persona_grupo where idgrupo = '.$idgrupo.')')->result();;
		$data = array('alumnos' =>$alumnos,'breadcumb' =>$this->breadcumb($links));

		$this->load->view('administracion/detalle_grupo',$data);


	}
	public function get_sesiones_x_grupo($idgrupo)
	{

		$query = "select * from sesion where idgrupo =".$idgrupo;
		$result = $this->db->query($query);
		return $result;
	}
	public function form_nuevo_grupo()
	{
		$this->load->model('mdl_persona');
		$data['alumnos'] = $this->mdl_persona->get_alumnos();
		$this->load->view('administracion/paneles/form_nuevo_grupo',$data);
	}
	public function guardar_grupo()
	{
		$this->load->model('mdl_administrador');
		$data = $this->input->post();

		$alumnos = json_decode($data['lista_alumnos']);



		unset($data['lista_alumnos']);
		unset($data['alumnos']);
		$data['idpersona_creador'] = $this->session->userdata('id');
		$data['fecha_creacion'] = hoy_a_m_d();
		$idgrupo =$this->mdl_administrador->guardar_grupo($data);



		$result = $this->mdl_administrador->asignar_alumnos_grupo($alumnos,$idgrupo);
		echo json_encode($result);

	}
	public function get_sesionesp_ajax($idgrupo)
	{
		$this->load->model('mdl_administrador');
		$sesiones = $this->mdl_administrador->sesiones_x_grupo($idgrupo,'programado');
		echo json_encode($sesiones);
	}
	public function detalle_grupo($idgrupo)
	{


		$this->load->model('mdl_administrador');
		$grupo = $this->mdl_administrador->by_id('grupo',$idgrupo);

		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GRUPOS Y SESIONES' => base_url().'administrador/gruposysesiones',
					   'GRUPO '.strtoupper($grupo->nombre)=> base_url().'administrador/juegos');

		$data = array('sesionesp' => $this->mdl_administrador->sesiones_x_grupo($idgrupo,'programado'),
					  'grupo_actual'=>$grupo,
					  'nro_sesiones'=>count($this->mdl_administrador->sesiones_x_grupo($idgrupo)),
					  'sesionesj'=>$this->mdl_administrador->sesiones_x_grupo($idgrupo,'ejecutado'),
					  'sesionesa'=>$this->mdl_administrador->sesiones_x_grupo($idgrupo,'anulado'),
					  'participantes'=>$this->mdl_administrador->participantes_x_grupo($idgrupo),
					  'breadcumb'=>$this->breadcumb($links),
					  'uusuario' => $usuario = $this->session->all_userdata());
		$data['grupos'] = $this->mdl_administrador->grupos_x_creador($this->session->userdata('id'));
		$data['grupo'] = $grupo;
		$data['juegos'] = $this->mdl_administrador->get_juegos_x_creador($this->session->userdata('id'));



		$this->load->view('administracion/detalle_sesiones_x_grupo',$data);
		
	}
	public function get_juegos_x_creador()
	{
		$this->load->model('mdl_administrador');
		$result = $this->mdl_administrador->get_juegos_x_creador($this->session->userdata('id'));
		echo json_encode($result);
	}
	public function nueva_sesion($idgrupo)
	{
		$this->load->model('mdl_administrador');
		$data = array('grupo' => $this->mdl_administrador->by_id('grupo',$idgrupo),
					  'juegos'=>$this->mdl_administrador->get_juegos_x_creador($this->session->userdata('id')));
		
		$this->load->view('administracion/modals/nueva_sesion',$data);
	}
	
	public function usuario($usuario)
	{
		$this->load->model('mdl_administrador');

		$this->load->model('mdl_persona');
		$persona = $this->mdl_persona->get_by_usuario($usuario)->row();
		
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GESTIÓN DE USUARIOS' => base_url().'administrador',
					   'usuario  '.strtoupper($persona->usuario)=> base_url().'administrador/usuario/'.$usuario);

		$data = array('breadcumb'=>$this->breadcumb($links),
					  'persona'=>$persona);

		$this->load->view('administracion/usuario_detalle.php',$data);

	}
	public function get_usuarios() //todos los usuario
	{
		$texto = $this->input->post()['nombres'];
		$this->load->model('mdl_persona');
		$result = $this->mdl_persona->lista_usuario_coincidencia($texto);
		echo json_encode($result);
	}
	public function detalle_juego_ajax($idjuego)
	{
		$this->load->model('mdl_administrador');
		$juego = $this->mdl_administrador->get_detalle_juego($idjuego);
		echo json_encode($juego);
	}	
	public function guardar_sesion()
	{
		$data = $this->input->post();

		$juegos = $data['juegos'];
		$juegos = json_decode($juegos);
			$a = array('nombre' => $data['nombre'],
						'fecha_creacion' => hoy_a_m_d() ,
						'idpersona_creador' => $this->session->userdata['id'] ,
						'idgrupo' => $data['idgrupo'] ,
						'fecha_programada' =>$data['fecprog'],
						'estado'=>'programado'
						 );
		$this->db->insert('sesion',$a);
		$idsesion = $this->db->insert_id();
		$j = 1;
		foreach ($juegos as $juego) {
			$d = array('fk_idjuego' => $juego,
					   'fk_idsesion'=>$idsesion,
					   'fecha'=> hoy_d_m_a(),
					   'orden'=>$j++);
			$d2 = array('idgrupo' => $data['idgrupo'],
					   'idsesion'=>$idsesion);
			$this->db->insert('juego_sesion',$d);
			$this->db->insert('sesion_grupo',$d2);

		}
		
		echo json_encode(1);

	}
	public function nuevo_usuario()
	{
		$links = array('ADMINISTRACIÓN' => base_url().'administrador',
					   'GESTIÓN DE USUARIOS' => base_url().'administrador',
					   'NUEVO USUARIO' =>'');
	

		$data = array('uusuario' => $usuario = $this->session->all_userdata(),
			'breadcumb' => $this->breadcumb($links));
		$this->load->view('administracion/nuevo_usuario',$data);
	}
	public function resetear_clave($idusuario)
	{

		$this->load->model('mdl_administrador');
		$result = $this->mdl_administrador->reset_clave($idusuario,$this->input->post());
		echo json_encode($result);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */