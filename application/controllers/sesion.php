<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sesion extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function construct()
	{
		parent::__construct();
	}
	public function index($idsesion)
	{
		if ($idsesion) {
			$this->load->model('mdl_juegos');
			$this->load->model('mdl_inicio');
			$usuario = $this->session->all_userdata();

			$data = array('usuario' => $usuario,
						  'sesion'=>$this->mdl_juegos->get_sesion($idsesion),
						  'participantes'=>$this->mdl_inicio->participantes($usuario['id']));
			$this->load->view('ejecutar_sesion.php',$data);
		}

	}
	public function guardar_puntaje()
	{
		$data=$this->input->post();
		$data['idusuario'] = $this->session->userdata('id');
		$data['stsreg'] = 'REG';
		$this->db->insert('partida',$data);
	}

	public function enviar_resultados_sesion($idsesion)
	{
		$this->load->model('mdl_juegos');
		$this->mdl_juegos->Guardar_resultados_sesion($idsesion);

	}
	public function resultados_enviados()
	{
		$this->load->view('resultados_enviados.php');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */