<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Juegos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = array('panel' =>$this->panel());
		
		$this->load->view('admin_view',$data);
	}
	public function inicio_sesion($idsesion)
	{
		$this->load->model('mdl_juegos');
			$this->load->model('mdl_inicio');
			$usuario = $this->session->all_userdata();

			$data = array('usuario' => $usuario,
						  'sesion'=>$this->mdl_juegos->get_sesion($idsesion),
						  'participantes'=>$this->mdl_inicio->participantes($usuario['id']));
		$this->load->view('inicio_sesion',$data);

	}
	public function mapa_subir_imagen()
	{
		
		$post = $this->input->post();

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		

		
		if ( !$this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
			exit();
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

		}
		

	}
	function mapa_datosprevisulizacion($idmapa)
	{

		// obetner puntos d eun mapa
		$this->load->model('mdl_juegos');
		$puntos = $this->mdl_juegos->mapa_puntosxmapa($idmapa);
		$mapa = $this->mdl_juegos->mapa_getmapa($idmapa);
		$data = array('puntos' => $puntos ,
					  'mapa'=>$mapa );
		echo json_encode($data);

	}
	function subir_archivo()
	{		 	
		    //obtenemos el archivo a subir
		    $file = $_FILES['userfile'];
		    //comprobamos si existe un directorio para subir el archivo
		    //si no es así, lo creamos
		    // modificamos el nombre 
		    $nuevo_nombre = microtime();
		   		// eliminar espacios y puntos
	   		$nuevo_nombre = str_replace(' ', '', $nuevo_nombre);
	   		$nuevo_nombre = str_replace('.', '', $nuevo_nombre);
	   		// sleep(1);
	   		$extension_ = explode(".", $file['name']);
	   		$extension_ = explode(".", $file['name']);
	   		/*var_dump($extension_);*/
	   		$extension = $extension_[1];
	   		$nuevo_nombre .= '.'.$extension;
		    $ruta_destino =  public_url(). 'uploads/'; 
		    $archivo_final = $ruta_destino.$nuevo_nombre;
		    $ruta_fisica = './assets/uploads/' . $nuevo_nombre;
		    //comprobamos si el archivo ha subido
		    if ($file && move_uploaded_file($_FILES['userfile']['tmp_name'], $ruta_fisica))
		    {
		       // sleep(3);//retrasamos la petición 3 segundos
		       echo  json_encode($archivo_final);//devolvemos el nombre del archivo para pintar la imagen
		    }
	}
	function subir_archivo_unitario($carpeta='uploads')
	{
		
		    //obtenemos el archivo a subir
		    $file = $_FILES['userfile'];

		    //comprobamos si existe un directorio para subir el archivo
		    //si no es así, lo creamos
		    // modificamos el nombre 
		    $nuevo_nombre = microtime();
		   		// eliminar espacios y puntos
	   		$nuevo_nombre = str_replace(' ', '', $nuevo_nombre);
	   		$nuevo_nombre = str_replace('.', '', $nuevo_nombre);
	   		// sleep(1);
	   		$extension_ = explode(".", $file['name']);
	   		$extension = $extension_[1];
	   		$nuevo_nombre .= '.'.$extension;
		    $ruta_destino =  public_url(). $carpeta.'/'; 
		    $archivo_final = $ruta_destino.$nuevo_nombre;
		    /*$ruta_fisica = './assets/'.$carpeta.'/' . $nuevo_nombre;*/
		    $ruta_fisica = '/var/www/pukllay.org/public_html/assets/'.$carpeta.'/' . $nuevo_nombre;
		    $ruta_fisica = upload_url().$carpeta.'/' . $nuevo_nombre;
		    //comprobamos si el archivo ha subido
		    $move = move_uploaded_file($_FILES['userfile']['tmp_name'], $ruta_fisica);
		 
		    if ($file && $move)
		    {
		       // sleep(3);//retrasamos la petición 3 segundos
		    	$A = array('ruta_archivo' =>$archivo_final,
		    			   'nombre_archivo'=> $nuevo_nombre);
		       echo  json_encode($A);//devolvemos el nombre del archivo para pintar la imagen
		    }
		    else
		    {
		    	
		       echo  json_encode($ruta_fisica);//devolvemos el nombre del archivo para pintar la imagen
		       
		    }

	}
	function mapa_guardarmapa($idmapa = null)
	{
		// edicion o nuevo?, esta restricción se maneja en el modelo
		
		$post = $this->input->post();
		$this->load->model('mdl_juegos');
		$id =  $this->mdl_juegos->Guardar_mapa($post,$idmapa);
		echo json_encode($id);
	}
	function mapa_nuevopunto($px,$py,$idmapa,$des)
	{
		//recibimos datos po ajax
		// var_dump($_POST);

		// var_dump($post);
		$this->load->model('mdl_juegos');
		$data = array('px' =>$px ,
					  'py'=>$py,
					  'idmapa'=>$idmapa,
					  'descripcion'=>$des);
		$id =  $this->mdl_juegos->Guardar_punto($data);
		echo json_encode($id);
	}
	public function mapa_guardar_escala($escala,$idmapa)
	{
		$this->load->model('mdl_juegos');
		$result = $this->mdl_juegos->mapa_guardar_escala($escala,$idmapa);
		echo json_encode("lala");

	}
	public function mapa_cancelar($idmapa)
	{
		$this->load->model('mdl_juegos');

		// borrar puntos
		$this->mdl_juegos->mapa_eliminar_registro($idmapa);
		$this->mdl_juegos->mapa_eliminar_puntos($idmapa);
		//borrar mapa
	}
	public function mapa_previsualizacion($idmapa)
	{
		$this->load->model('mdl_juegos');
		$mapa = $this->mdl_juegos->mapa_datos($idmapa);
		$puntos= $this->mdl_juegos->mapa_puntos($idmapa);
		
		$args_imagen= getimagesize($mapa->imagen);
		$args_w = $args_imagen[0] * $mapa->escala;
		$args_h = $args_imagen[1] * $mapa->escala;

		$imagen = '<img class="prev-content-img" src="'.$mapa->imagen.'" style="width:'.$args_w.'px; height:'.$args_h.'px;">';
		$puntaje_unitario = $mapa->puntaje;
		
		$puntaje_unitario /=count($puntos);
		// enviar

		$data = array('mapa' => $mapa ,
					  'puntos'=>$puntos,
					  'imagen'=>$imagen,
					  
					  'puntaje_unitario'=>$puntaje_unitario);
		$this->load->view('administracion/mapa_previsualizacion',$data);

	}
	public function get_juego_especifico($idjuego)
	{

		$query = 'select fk_idjuegoespecifico from juego where idjuego='.$idjuego;
		$result = $this->db->query($query)->result()[0];
		return $result;
	}
	public function get_juegosesion_js($idjuego,$idsesion)
	{
		$result =  $this->db->query('select idjuego_sesion,estado from juego_sesion where fk_idsesion= '.$idsesion.' and fk_idjuego = '.$idjuego)->result()[0];
		// var_dump($this->db->last_query());
		return $result;
	}
	public function get_datos_juegogeneral($idjuego)
	{
		$query = 'select * from juego where idjuego='.$idjuego;
		$result = $this->db->query($query)->result()[0];
		return $result;
	}
	public function mapa_jugar($idjuego,$idsesion)
	{
		$idmapa = $this->get_juego_especifico($idjuego)->fk_idjuegoespecifico;
		$this->load->model('mdl_juegos');
		$mapa = $this->mdl_juegos->mapa_datos($idmapa);
		$puntos= $this->mdl_juegos->mapa_puntos($idmapa);
		
		$args_imagen= getimagesize($mapa->imagen);
		$args_w = $args_imagen[0] * $mapa->escala;
		$args_h = $args_imagen[1] * $mapa->escala;
		$imagen = '<img class="prev-content-img" src="'.$mapa->imagen.'" style="width:'.$args_w.'px; height:'.$args_h.'px;">';
		$puntaje_unitario = $mapa->puntaje;
		
		$puntaje_unitario /=count($puntos);
		// enviar

		$data = array('mapa' => $mapa ,
					  'puntos'=>$puntos,
					  'imagen'=>$imagen,
					  'juego'=>$this->get_datos_juegogeneral($idjuego),
					  'juego_sesion'=>$this->get_juegosesion_js($idjuego,$idsesion), //****
					  'idsesion'=>$idsesion, //****
					  'puntaje_unitario'=>$puntaje_unitario);
		$this->load->view('mapa_jugar',$data);

	}
	public function previsualizacion($tipo=null,$idtipo=null)
	{
		switch ($tipo) {
			case 'mapa':
				$this->mapa_previsualizacion($idtipo);
				break;
			case 'abeja':
				$this->abeja_previsualizacion();
				break;
			case 'snake':
				$this->snake_previsualizacion();
				break;
			
			default:
				# code...
				break;
		}

	}

	public function abeja_uploadimgs()
	{
		$files = $_FILES['abeja_img'];
	    // $fecha = new DateTime();
	   	$F = array();
		   for ($i=0; $i < count($files['name']); $i++) { 
		   			
		   		$nuevo_nombre = microtime();
		   		// eliminar espacios y puntos
		   		 $nuevo_nombre = str_replace(' ', '', $nuevo_nombre);
		   		$nuevo_nombre = str_replace('.', '', $nuevo_nombre);
		   		// sleep(1);
		   		$extension_ = explode(".", $files['name'][$i]);
		   		$extension = $extension_[1];
		   		$nombre_real = $extension_[0];
		   		// $nuevo_nombre = $nombre_real;

	   			$ruta_destino =  public_url(). 'uploads/';
	   			$archivo_final = $ruta_destino.$nuevo_nombre.'.'.$extension;
	   			$ruta_fisica = './assets/uploads/' .$nuevo_nombre.'.'.$extension;

	   			move_uploaded_file($files['tmp_name'][$i], $ruta_fisica);

	   			// array de datos de img
	   			$img = array('ruta' => $archivo_final,
	   						 'nombre'=>$nombre_real);
	   			array_push($F, $img);
		   }
		echo  json_encode($F);//devolvemos el nombre del archivo para pintar la imagen


	}
	public function snake_uploadimgs()
	{
		$files = $_FILES['snake_img'];
	    // $fecha = new DateTime();
	   	$F = array();
		   for ($i=0; $i < count($files['name']); $i++) { 
		   			
		   		$nuevo_nombre = microtime();
		   		// eliminar espacios y puntos
		   		 $nuevo_nombre = str_replace(' ', '', $nuevo_nombre);
		   		$nuevo_nombre = str_replace('.', '', $nuevo_nombre);
		   		// sleep(1);
		   		$extension_ = explode(".", $files['name'][$i]);
		   		$extension = $extension_[1];
		   		$nombre_real = $extension_[0];
		   		// $nuevo_nombre = $nombre_real;

	   			$ruta_destino =  public_url(). 'uploads/';
	   			$archivo_final = $ruta_destino.$nuevo_nombre.'.'.$extension;
	   			$ruta_fisica = './assets/uploads/' .$nuevo_nombre.'.'.$extension;

	   			move_uploaded_file($files['tmp_name'][$i], $ruta_fisica);

	   			// array de datos de img
	   			$img = array('ruta' => $archivo_final,
	   						 'nombre'=>$nombre_real);
	   			array_push($F, $img);
		   }
		echo  json_encode($F);//devolvemos el nombre del archivo para pintar la imagen
	}
	public function snake_guardarsnake()
	{
		// edicion o nuevo?, esta restricción se maneja en el modelo
		
		$post = $this->input->post();
		$this->load->model('mdl_juegos');
		$idjuego =  $this->mdl_juegos->Guardar_snake($post);
		echo json_encode($idjuego);
	}
	public function snake_previsualizacion($idjuego)
	{
		$this->load->model('mdl_juegos');
		//$juegosnake = $this->mdl_juegos->snake_datos($idjuego);
		//$snakedetalles = $this->mdl_juegos->snakedetalles_datos($idjuego);
		$data = array();
		$data['juegosnake'] = $this->mdl_juegos->snake_datos($idjuego);
		$data['snakedetalles'] = $this->mdl_juegos->snakedetalles_datos($idjuego);
		// $data = array('juegosnake' => $juegosnake ,
		// 			  'snakedetalles'=>$snakedetalles);

		$this->load->view('administracion/snake_previsualizacion',$data);

	}

	function abeja_previsualizacion()
	{
		$this->load->view('administracion/abeja_previsualizacion');

	}

	function abeja_guardar()
	{
		$data = $this->input->post();
		$this->db->insert('abeja',$data);

	}
	function abeja_vincular()
	{
		// $data = $this->input->post();

		// $data_img = array('url' => ,
		// 				  'idjuego' => ,
		// 				  'tabla' => ,
		// 				  'estado'=> );
		// $data['idabeja'];
		// AKI ME KEDE
	}

	
	//--------------JUEGO MEMORIA --------------------------
	function subir_archivomemoria()
	{
		
		    //obtenemos el archivo a subir
		    $file = $_FILES['memoriafile'];
		    
		    //comprobamos si existe un directorio para subir el archivo
		    //si no es así, lo creamos
		    // modificamos el nombre 
		    $nuevo_nombre = microtime();
		   		// eliminar espacios y puntos
	   		$nuevo_nombre = str_replace(' ', '', $nuevo_nombre);
	   		$nuevo_nombre = str_replace('.', '', $nuevo_nombre);
	   		// sleep(1);
	   		$extension_ = explode(".", $file['name'][0]);
	   		$extension = $extension_[1];
	   		$nuevo_nombre .= '.'.$extension;


		    $ruta_destino =  public_url(). 'uploads/'; 
		    $archivo_final = $ruta_destino.$nuevo_nombre;
		    $ruta_fisica = './assets/uploads/' . $nuevo_nombre;
		    
		     
		    //comprobamos si el archivo ha subido
		    if ($file && move_uploaded_file($_FILES['memoriafile']['tmp_name'][0], $ruta_fisica))
		    {
		       // sleep(3);//retrasamos la petición 3 segundos
		       echo  json_encode($archivo_final);//devolvemos el nombre del archivo para pintar la imagen
		    }

	}
	function memoria_guardarmemoria()
	{
		// edicion o nuevo?, esta restricción se maneja en el modelo
		$post = $this->input->post();
		$this->load->model('mdl_juegos');
		$id =  $this->mdl_juegos->Guardar_memoria($post);
		echo json_encode($id);
	}

	public function memoria_previsualizacion($idjuego)
	{
		$this->load->model('mdl_juegos');
		//$juegosnake = $this->mdl_juegos->snake_datos($idjuego);
		//$snakedetalles = $this->mdl_juegos->snakedetalles_datos($idjuego);
		$data = array();
		$data['juegomemoria'] = $this->mdl_juegos->memoria_datos($idjuego);
		$data['memoriadetalles'] = $this->mdl_juegos->memoriadetalles_datos($idjuego);
		// $data = array('juegosnake' => $juegosnake ,
		// 			  'snakedetalles'=>$snakedetalles);

		$this->load->view('administracion/memoria_previsualizacion',$data);
	}

	//Juego Invasores
	function invasores_guardarinvasores()
	{
		// edicion o nuevo?, esta restricción se maneja en el modelo
		$post = $this->input->post();
		$this->load->model('mdl_juegos');
		$id =  $this->mdl_juegos->Guardar_invasores($post);
		echo json_encode($id);
	}
	public function invasores_previsualizacion($idjuego)
	{
		$this->load->model('mdl_juegos');
		$data = array();
		$data['juegoinvasores'] = $this->mdl_juegos->invasores_datos($idjuego);
		$data['invasoresdetalles'] = $this->mdl_juegos->invasoresdetalles_datos($idjuego);
		// $data = array('juegosnake' => $juegosnake ,
		// 			  'snakedetalles'=>$snakedetalles);

		$this->load->view('administracion/invasores_previsualizacion',$data);
	}
	//Juego Invasores comprension
	function invasores_guardarinvasorescomprension()
	{
		// edicion o nuevo?, esta restricción se maneja en el modelo
		$post = $this->input->post();
		$this->load->model('mdl_juegos');
		$id =  $this->mdl_juegos->Guardar_invasorescomprension($post);
		echo json_encode($id);
	}
	public function get_sesion($idsesion)
	{
		$this->load->model('mdl_juegos');
		$Sesion = $this->mdl_juegos->get_sesion($idsesion);
		echo json_encode($Sesion);
	}
	public function get_juego_sesion($idsesion,$orden)
	{
	$result = $this->db->query('select tipo,fk_idjuegoespecifico,idjuego
					 	  from juego 
					 	  where idjuego = (select fk_idjuego 
					 	  				  from juego_sesion 
					 	  				  where fk_idsesion = '.$idsesion.' 
					 	  				  and orden ='.$orden.')');

		if ($result->num_rows($result)==1) {
			echo json_encode($result->result()[0]);
		}
		else
			echo json_encode('-1');
		
	}
	public function terminar_sesion($idsesion)
	{
		$this->load->model('mdl_juegos');
		$this->load->model('mdl_inicio');
		$usuario = $this->session->all_userdata();

		$data = array('usuario' => $usuario,
					  'sesion'=>$this->mdl_juegos->get_sesion($idsesion),
					  'resultados'=>$this->mdl_juegos->resultados_juegos_sesion($idsesion)
					  );

		$this->load->view('terminar_sesion',$data);
	}
	//------------ JUEGO RECOLECTOR -------
	public function recolector_uploadimgs()
	{
		$files = $_FILES['recolector_img'];
	    // $fecha = new DateTime();
	   	$F = array();
		   for ($i=0; $i < count($files['name']); $i++) { 
		   			
		   		$nuevo_nombre = microtime();
		   		// eliminar espacios y puntos
		   		 $nuevo_nombre = str_replace(' ', '', $nuevo_nombre);
		   		$nuevo_nombre = str_replace('.', '', $nuevo_nombre);
		   		// sleep(1);
		   		$extension_ = explode(".", $files['name'][$i]);
		   		$extension = $extension_[1];
		   		$nombre_real = $extension_[0];
		   		// $nuevo_nombre = $nombre_real;

	   			$ruta_destino =  public_url(). 'uploads/';
	   			$archivo_final = $ruta_destino.$nuevo_nombre.'.'.$extension;
	   			$ruta_fisica = './assets/uploads/' .$nuevo_nombre.'.'.$extension;

	   			move_uploaded_file($files['tmp_name'][$i], $ruta_fisica);

	   			// array de datos de img
	   			$img = array('ruta' => $archivo_final,
	   						 'nombre'=>$nombre_real);
	   			array_push($F, $img);
		   }
		echo  json_encode($F);//devolvemos el nombre del archivo para pintar la imagen
	}
	function recolector_guardarrecolector()
	{
		// edicion o nuevo?, esta restricción se maneja en el modelo
		$post = $this->input->post();
		$this->load->model('mdl_juegos');
		$idjuego =  $this->mdl_juegos->Guardar_recolector($post);
		echo json_encode($idjuego);
	}
	public function recolector_previsualizacion($idjuego)
	{
		$this->load->model('mdl_juegos');
		//$juegorecolector = $this->mdl_juegos->recolector_datos($idjuego);
		//$recolectordetalles = $this->mdl_juegos->recolectordetalles_datos($idjuego);
		$data = array();
		$data['juegorecolector'] = $this->mdl_juegos->recolector_datos($idjuego);
		$data['recolectordetalles'] = $this->mdl_juegos->recolectordetalles_datos($idjuego);
		// $data = array('juegorecolector' => $juegorecolector ,
		// 			  'recolectordetalles'=>$recolectordetalles);

		$this->load->view('administracion/recolector_previsualizacion',$data);

	}
	//--------- ejecución de los juegos
	public function snake_jugar($idjuego,$idsesion)
	{
		$idsnake = $this->get_juego_especifico($idjuego)->fk_idjuegoespecifico;

		$this->load->model('mdl_juegos');
		$juegosnake = $this->mdl_juegos->snake_datos_byidsnake($idsnake);
		$snakedetalles = $this->mdl_juegos->snakedetalles_datos_byidsnake($idsnake);
		$data = array('juegosnake' => $juegosnake,
		 			  'snakedetalles'=>$snakedetalles,
		 			  'juego_sesion'=>$this->get_juegosesion_js($idjuego,$idsesion),
		 			  'idsesion'=>$idsesion);

		$this->load->view('snake_jugar',$data);

	}
	public function memoria_jugar($idjuego,$idsesion)
	{
		$idmemoria = $this->get_juego_especifico($idjuego)->fk_idjuegoespecifico;

		$this->load->model('mdl_juegos');
		$juegomemoria = $this->mdl_juegos->memoria_datos_byidmemoria($idmemoria);
		$memoriadetalles = $this->mdl_juegos->memoriadetalles_datos_byidmemoria($idmemoria);

		$data = array('juegomemoria' => $juegomemoria,
		 			  'memoriadetalles'=>$memoriadetalles,
		 			  'juego_sesion'=>$this->get_juegosesion_js($idjuego,$idsesion),
		 			  'idsesion'=>$idsesion);

		$this->load->view('memoria_jugar',$data);
	}
	public function recolector_jugar($idjuego,$idsesion)
	{
		$idrecolector = $this->get_juego_especifico($idjuego)->fk_idjuegoespecifico;

		$this->load->model('mdl_juegos');
		$juegorecolector = $this->mdl_juegos->recolector_datos_byidrecolector($idrecolector);
		$recolectordetalles = $this->mdl_juegos->recolectordetalles_datos_byidrecolector($idrecolector);

		$data = array('juegorecolector' => $juegorecolector,
		 			  'recolectordetalles'=>$recolectordetalles,
		 			  'juego_sesion'=>$this->get_juegosesion_js($idjuego,$idsesion),
		 			  'idsesion'=>$idsesion);

		$this->load->view('recolector_jugar',$data);
	}
	public function invasores_jugar($idjuego,$idsesion)
	{
		$idinvasores = $this->get_juego_especifico($idjuego)->fk_idjuegoespecifico;

		$this->load->model('mdl_juegos');
		$juegoinvasores = $this->mdl_juegos->invasores_datos_byidinvasores($idinvasores);
		$invasoresdetalles = $this->mdl_juegos->invasoresdetalles_datos_byidinvasores($idinvasores);

		$data = array('juegoinvasores' => $juegoinvasores,
		 			  'invasoresdetalles'=>$invasoresdetalles,
		 			  'juego_sesion'=>$this->get_juegosesion_js($idjuego,$idsesion),
		 			  'idsesion'=>$idsesion);

		$this->load->view('invasores_jugar',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */