<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mantenimiento extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->model('mdl_persona');
		$data = array();
		$data['alumnos']= $this->mdl_persona->lista('alumno');
		
		$this->load->view('administracion/mantenimiento',$data);
		
	}
	public function form(){
		$this->load->model('mdl_profesor');
		$post = $this->input->post();
		if ($post) {

			$this->mdl_profesor->save(null,$post);
			redirect('mantenimiento_profesor/index');
			
		}
		else
		{
			$this->load->view('administracion/profesor_form');
		}
		


	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */