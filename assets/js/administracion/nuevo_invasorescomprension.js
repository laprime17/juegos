$(document).ready(function(){
	//$('#userfilememoria').change(Enviar_imagen);
	// url = '';
   initSample();
	 idjuego=-1;
	 nroejer_dinero = 0;
   nroejer_fig = 0;
   nropreg_comprension = 0;
   mayor=0;
	 localStorage.clear();
  $('#nroejercicios_comprension').click(Cargar_preguntasComprension);
  
	$('#guardardatos').click(Guardar_invasores);

	$('.boton_previsualizacion').click(prev);
    //$('#cancelar_todo').click(Cancelar_mapa);
});
function prev()
{
    idjuego= $('#invasores_id_invasores').val();
    console.log('id del juego ',idjuego);
    $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/invasores_previsualizacion/'+idjuego);
}
function Cargar_preguntasComprension()
{
  $('#actividad_comprension').empty();
  nropreg_comprension = $('#nroejercicios_comprension1').val();
  //console.log('nro comprension: ',nropreg_comprension);
  for (var i = 1; i <= nropreg_comprension; i++) {
    var divpreg = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+i+"</div><div class='column large-11'><input type='text' name='pregunta_"+i+"' id='pregunta_"+i+"' placeholder='Pregunta'/></div><div class='column large-2'>Respuesta:</div><div class='column large-10'><input type='text' name='respuesta_"+i+"' id='respuesta_"+i+"' placeholder='Respuesta'/></div><div class='column large-2'>Distractor1:</div><div class='column large-10'><input type='text' name='primerDistractor_"+i+"' id='primerDistractor_"+i+"' placeholder='Primer distractor'/></div><div class='column large-2'>Distractor2:</div><div class='column large-10'><input type='text' name='segundoDistractor_"+i+"' id='segundoDistractor_"+i+"' placeholder='Segundo distractor'/></div></div>";
    $( "#actividad_comprension").append(divpreg);
  };
}
function Guardar_invasores()
{
  Agregar_allocalstorage_comprension();
  var infocomprension = Array();
  infocomprension = localStorage;
  var detallescomprension = JSON.stringify(infocomprension);
  $('#iddetallesinvasores_texto').attr('value',detallescomprension);
  //console.log('detalles de figuras',detallescomprension);

  //localStorage.clear();
  //Agregar_allocalstorage_texto();
  //var textoprinc = Array();
  var textoprinc = CKEDITOR.instances['editor'].getData();
  var detallestexto = JSON.stringify(textoprinc);
  $('#idinvasores_textoprincipal').attr('value',detallestexto);
  console.log('texto : ',detallestexto);
  

	formData = new FormData($(".form_datos")[0]);
	console.log("FORM DATA",FormData);
    $.ajax({
               type: "POST",
               url: base_url+'juegos/invasores_guardarinvasorescomprension',
               data: formData,
               dataType:"json", 
               cache: false,
               contentType: false,
               processData: false,
               success: function(data)
               {
                   console.log('idjuego',data);
                   $('#invasores_id_invasores').attr('value',data);
                   idjuego = data;
                   
                   $('#aviso_guardado').html('<p id="guardado_exito">Se guardó exitosamente</p>');
                   $('#iframe_prev').attr('src',base_url+'juegos/invasores_previsualizacion/'+idjuego);

               }, //fin success
                 error: function(data) {

                    $('#aviso_guardado').html('<p id="guardado_exito">No se guardaron los cambios</p>');
                    console.log(data);
                }
            });  //fin ajax  
}
function Agregar_allocalstorage_comprension()
{
    for (var i = 1; i <= nropreg_comprension; i++) {
      
      localStorage.setItem('pregunta'+i,$('#pregunta_'+i).val());
      localStorage.setItem('respuesta'+i,$('#respuesta_'+i).val());
      localStorage.setItem('primerDistractor'+i,$('#primerDistractor_'+i).val());
      localStorage.setItem('segundoDistractor'+i,$('#segundoDistractor_'+i).val());
    };
}
function Agregar_allocalstorage_texto()
{
  var contenido_editor = CKEDITOR.instances['editor'].getData();
  localStorage.setItem('texto_comprension',contenido_editor);
}