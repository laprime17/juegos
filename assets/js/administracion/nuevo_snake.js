$(document).on('ready',ini);

function ini(){
    //console.log("hola snake");
    //localStorage.clear();
    idjuego = -1;
    Imagenes = Array();
    Correctos = Array();
    imagenes_correctas_abierto = false;
    var figura;
    var numero; 
    contpalabra = 1;
    contayuda = 1;
    contimg = 1;
    $('#file_imgs').change(Enviar_modelos);

    $('.aceptar_img').click(agregar_imagen_correcta);

    $('#guardardatos').click(Guardar_snake);

    $('.boton_previsualizacion').click(prev);
}
 function Enviar_modelos()
  {
    formData = new FormData($(".form_imgs")[0]);
    $.ajax({
             type: "POST",
             url: base_url+'juegos/snake_uploadimgs',
             data: formData,
             dataType:"json", 
             cache: false,
              contentType: false,
             processData: false,
             success: function(data)
             {
                
                var j=0;
                var cont_modelo = '';
                console.log(data);
                 $.each(data,function(index,img){
                    console.log("data");
                    Imagenes.push(new Elemento(j,img));                       
                    cont_modelo += '<figure  id="item-'+j+'" data-id="'+j+'" class="snake-galeria-todos-item"><img alt="imagen_'+j+'" src="'+img.ruta+'"><figcaption><input type="text" value="'+img.nombre+'" class="snake-galeria-itemdes"></figcaption><article class="sombra"> <span class="icon-arrow-right" data-item="'+j+'" onclick="seleccionar_imagen(this);" ></span></article></figure>';
                    j++;
                    
                 });

                 $('.snake-galeria-todos').append(cont_modelo);

                 
             }, //fin success
             error: function(data) {
                  }
          });  //fin ajax  
  }
  function seleccionar_imagen(figure)
  {
        figura = figure;
        var nro = $(figure).data('item');
        numero = nro;
        div = '<div class="nueva_palabra_imagen" id="imgguia_'+nro+'"><input type="text" id="palabra_'+nro+'" placeholder="Nombre"><input type="text" id="ayuda_'+nro+'" placeholder="Texto de ayuda"><a class="boton verde aceptar_img" onclick="agregar_imagen_correcta('+nro+');" id="aceptar_'+nro+'"><span class="icon-checkmark" ;></span> </a> <div class="boton rojo" onclick="Cancelar_accion('+nro+');"><span class="icon-cross cancelar_punto" id="cancelar_'+nro+'"></span></div> </div>';
        $('.snake-galeria').append(div);

  }
  function agregar_imagen_correcta(nro_imagen)
  {
      var nro = numero;
      localStorage.setItem('palabra'+contpalabra, $('#palabra_'+nro_imagen).val());
      localStorage.setItem('ayuda'+contayuda, $('#ayuda_'+nro_imagen).val());
      //texto_ayuda.push($('#ayuda_'+nro_imagen).val());
      //nombre_imagen.push();
      var tmppalabra =  localStorage.getItem('palabra'+contpalabra);  
      var tmpayuda = localStorage.getItem('ayuda'+contayuda); 

      var src = $('img[alt="imagen_'+nro+'"]').attr('src');
      localStorage.setItem('imagen'+contimg,src);

      contpalabra++;
      contayuda++;
      contimg++;

      var aaa = localStorage.getItem('imagen'+contimg);
      
      
      id = '#item-'+nro;
       $(id).css('border-color','#037fa2');
       //Correctos.push();
       // trasladar a panel derecho 

       if (!imagenes_correctas_abierto) {

        $('.snake-galeria-correctos').addClass('ampliado');
        $('.snake-galeria-todos').addClass('reducido');
        imagenes_correctas_abierto = true;

        // mover de padre
       }
  
        var figure = $(id).detach();
        $('.snake-galeria-correctos').append(figure);

        //borrar nuevo formulario
        $('.nueva_palabra_imagen').remove();

        divdatosimagen = '<div class = "snake_explicacion_imagen"><label class ="gris" for>'+tmppalabra+'</label><span class ="negro">'+tmpayuda+'</span></div>';
        $('.snake-galeria-correctos').append(divdatosimagen);
        $(figure).addClass('datosimagen'); // no está funcionando como deberia

        //console.log('todo ',localStorage);

  }
  function Cancelar_accion(nro)
  {
    console.log('cancelar accion');
    $('.nueva_palabra_imagen').remove();
  }

    // function Elemento_by_clave_()
    // {
    //     for (var i = Imagenes.length - 1; i >= 0; i--) {
    //         if(Imagenes)
    //         {

    //         }
    //     };
    // }
    function Elemento(clave_, valor_)
    {
        this.clave = clave_;
        this.valor = valor_;
        this.toLowerCase = function () { return this.valor.toLowerCase(); };
        this.indexOf = function (texto_) { return this.valor.indexOf(texto_); };
        this.replace = function (old_, new_) { return this.valor.replace(old_, new_); };
        this.toString = function(){ return this.clave.toString() +'-'+ this.valor.toString();};            
    }
//Funcion previsualizar
function prev()
{
    idjuego= $('#snake_id_snake').val();
    //console.log('idjuego-previsualizacion',idjuego); 
    $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/snake_previsualizacion/'+idjuego);

}
function Guardar_snake()
{
    var almacenado = Array();
    almacenado =  localStorage;
    var detalles = JSON.stringify(almacenado);
    $('#iddetalles').attr('value',detalles);
    console.log('guardaremos el snake',detalles);
    formData = new FormData($(".form_datos")[0]);

    $.ajax({
               type: "POST",
               url: base_url+'juegos/snake_guardarsnake',
               data: formData,
               dataType:"json", 
               cache: false,
               contentType: false,
               processData: false,
               success: function(data)
               {
                   console.log('idjuego',data);
                   // errores , esribir id de juegos
                   $('#snake_id_snake').attr('value',data);
                   idjuego = data;
                   $('#aviso_guardado').html('<p id="guardado_exito">Se guardó exitosamente</p>');
                   $('#iframe_prev').attr('src',base_url+'juegos/snake_previsualizacion/'+idjuego);
                   


               }, //fin success
                 error: function(data) {

                    console.log(data);
                    $('#aviso_guardado').html('<p id="guardado_exito">NO se logró guardar</p>');
                }
            });  //fin ajax  
}