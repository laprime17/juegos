$(document).ready(function(){
	//$('#userfilememoria').change(Enviar_imagen);
	// url = '';
  initSample();
	idjuego=-1;
	nropreguntas = 0;
	Imagenes = Array();
	Correctos = Array();
  imagenes_correctas_abierto = false;
    var figura;
    var numero; 
    contpregunta = 1;
    contrespuesta = 1;
    contimg = 1;

	//$('#nropreguntas').click(Cargar_preguntas);  
	//$('#guardardatos').click(Guardar_recolector);

	$('#file_imgs').change(Enviar_modelos);
  $('#guardardatos').click(Guardar_recolector); 

  $('.boton_previsualizacion').click(prev);
  //$('#cancelar_todo').click(Cancelar_mapa);

});
function prev()
{
    idjuego= $('#recolector_id_recolector').val();
    //console.log('id del juego ',idjuego);
    $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/recolector_previsualizacion/'+idjuego);
}
// function Cargar_preguntas()
// {
// 	$('#preguntas_texto').empty();
// 	$('#preguntas_imagen').empty();
// 	nropreguntas = $('#nropreguntas').val();
// 	for (var i = 1; i <= nropreguntas; i++) {
// 		divpreg = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+i+"</div><div class='column large-11'><div class=''><textarea name='texto' id='' cols='30' rows='4' placeholder='Pregunta'></textarea></div></div></div><div class='row' id='rpta_"+i+"'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'></div><div class='column large-11'><div class=''><input type='text' name='respuesta_"+i+"' id='respuesta_"+i+"' placeholder='Respuesta'/></div></div>";
// 		$( "#preguntas_texto").append(divpreg);

// 		divpreg_img = "<div class='row' style='height: 143px;'><form enctype='multipart/form-data' action='<?php echo base_url();?>juegos/subir_archivorecolector' method='post' class='form_imagen'><input type='file' name='recolectorfile[]' multiple id='userfilerecolector' style='margin-bottom: 2px; margin-left: -10px;' ></form> <div class='img_respuesta'></div></div>";
// 		$( "#preguntas_imagen").append(divpreg_img);
// 	};
// }
function Enviar_modelos()
{
    console.log("dentro de funcion enviar modelos");
    formData = new FormData($(".form_imgs")[0]);
    $.ajax({
             type: "POST",
             url: base_url+'juegos/recolector_uploadimgs',
             data: formData,
             dataType:"json", 
             cache: false,
              contentType: false,
             processData: false,
             success: function(data)
             {
                var j=0;
                var cont_modelo = '';
                console.log(data);
                 $.each(data,function(index,img){
                    console.log("data");
                    Imagenes.push(new Elemento(j,img));                       
                    cont_modelo += '<figure  id="item-'+j+'" data-id="'+j+'" class="recolector-galeria-todos-item"><img alt="imagen_'+j+'" src="'+img.ruta+'"><figcaption><input type="text" value="'+img.nombre+'" class="recolector-galeria-itemdes"></figcaption><article class="sombra"> <span class="icon-arrow-right" data-item="'+j+'" onclick="seleccionar_imagen(this);" ></span></article></figure>';
                    j++;
                    
                 });

                 $('.recolector-galeria-todos').append(cont_modelo);

                 
             }, //fin success
             error: function(data) {
                  }
          });  //fin ajax  
}
function seleccionar_imagen(figure)
  {
        figura = figure;
        var nro = $(figure).data('item');
        numero = nro;
        div = '<div class="nueva_palabra_imagen" id="imgguia_'+nro+'"><input type="text" id="pregunta_'+nro+'" placeholder="Pregunta"><input type="text" id="respuesta_'+nro+'" placeholder="Respuesta"><a class="boton verde aceptar_img" onclick="agregar_imagen_correcta('+nro+');" id="aceptar_'+nro+'"><span class="icon-checkmark" ;></span> </a> <div class="boton rojo"><span class="icon-cross cancelar_punto" id="cancelar_'+nro+'"></span></div> </div>';
        $('.recolector-galeria').append(div);

  }
function Elemento(clave_, valor_)
    {
        this.clave = clave_;
        this.valor = valor_;
        this.toLowerCase = function () { return this.valor.toLowerCase(); };
        this.indexOf = function (texto_) { return this.valor.indexOf(texto_); };
        this.replace = function (old_, new_) { return this.valor.replace(old_, new_); };
        this.toString = function(){ return this.clave.toString() +'-'+ this.valor.toString();};            
    }

function agregar_imagen_correcta(nro_imagen)
{
      var nro = numero;
      localStorage.setItem('pregunta'+contpregunta, $('#pregunta_'+nro_imagen).val());
      localStorage.setItem('respuesta'+contrespuesta, $('#respuesta_'+nro_imagen).val());

      var tmppregunta =  localStorage.getItem('pregunta'+contpregunta);  
      var tmprespuesta = localStorage.getItem('respuesta'+contrespuesta); 

      var src = $('img[alt="imagen_'+nro+'"]').attr('src');
      localStorage.setItem('imagen'+contimg,src);

      contpregunta++;
      contrespuesta++;
      contimg++;

      var aaa = localStorage.getItem('imagen'+contimg);
      //console.log("localStorage", aaa);   
      
      
      id = '#item-'+nro;
       $(id).css('border-color','#037fa2');
       Correctos.push();
       // trasladar a panel derecho 

       if (!imagenes_correctas_abierto) {

        $('.recolector-galeria-correctos').addClass('ampliado');
        $('.recolector-galeria-todos').addClass('reducido');
        imagenes_correctas_abierto = true;

        // mover de padre
       }
  
        var figure = $(id).detach();
        $('.recolector-galeria-correctos').append(figure);

        //borrar nuevo formulario
        $('.nueva_palabra_imagen').remove();

        divdatosimagen = '<div class = "recolector_explicacion_imagen"><label class ="gris" for>'+tmppregunta+'</label><span class ="negro">'+tmprespuesta+'</span></div>';
        $('.recolector-galeria-correctos').append(divdatosimagen);
        $(figure).addClass('datosimagen'); // no está funcionando como deberia

        console.log('todo ',localStorage);
}
function Guardar_recolector()
{
    var almacenado = Array();
    almacenado =  localStorage;
    var detalles = JSON.stringify(almacenado);
    $('#iddetallesrecolector').attr('value',detalles);
    console.log('imagenes : ',detalles);

    //localStorage.clear();
    //Agregar_allocalstorage_texto();
    //var textoprinc = Array();
    var textoprinc= CKEDITOR.instances['editor'].getData();
    //textoprinc = localStorage;
    var detallestexto = JSON.stringify(textoprinc);
    $('#idrecolectortexto').attr('value',detallestexto);
    console.log('texto : ',detallestexto);
    formData = new FormData($(".form_datos")[0]);
    //console.log("FORM DATA",FormData);

    $.ajax({
               type: "POST",
               url: base_url+'juegos/recolector_guardarrecolector',
               data: formData,
               dataType:"json", 
               cache: false,
               contentType: false,
               processData: false,
               success: function(data)
               {
                   console.log('idjuego',data);
                   // errores , esribir id de juegos
                   $('#recolector_id_recolector').attr('value',data);
                   idjuego = data;
                   $('#aviso_guardado').html('<p id="guardado_exito">Se guardó exitosamente</p>');
                   $('#iframe_prev').attr('src',base_url+'juegos/recolector_previsualizacion/'+idjuego);


               }, //fin success
                 error: function(data) {
                    $('#aviso_guardado').html('<p id="guardado_exito">No se guardaron los cambios</p>');
                    console.log(data);
                }
            });  //fin ajax  
}
function Agregar_allocalstorage_texto()
{
  var contenido_editor = CKEDITOR.instances['editor'].getData();
  //localStorage.setItem('textoprincipal',contenido_editor);
}