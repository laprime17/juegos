bandera_panel = 1;
usuario_res = 0;
$(document).ready(function(){
if (rol=='administrador') {
	get_usuarios(1);
}
else
{
	get_usuarios(2);
}
$('#form_buscar_usuario').keypress(function (e) {
	  if (e.which == 13) {   
	  	formData = new FormData($("#form_buscar_usuario")[0]);
		console.log(formData);
		$.ajax({
		       type: "POST",
		       url: base_url+'administrador/buscar_usuario_ajax/',
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		       		var cadena='' ;
		       		var i = 1;
		       		$.each(data,function(index,persona){
						cadena  +='<tr><td>'+ ++i +'</td><td><a href="'+base_url+'administrador/usuario/'+persona.usuario+'">'+persona.nombres+' '+ persona.ap+' '+persona.am+'</a></td><td>'+persona.usuario+'</td><td><a class="btn_default small" href="'+base_url+'administrador/editar_usuario/'+persona.idusuario+'"></span> Editar</a></td></tr>';
			    		})
		       		
		       		
		       		var alto = i *38;
		       		alto = alto+'px';
		       		$('#t_user_found').html(cadena);
		       		$('#cont_coincidencias').removeClass('cerrado');
		       		$('#cont_coincidencias').height('cerrado',alto);



		       }, //fin success
		    	 error: function(data) {alert('Error al buscar usuario. Recague la página.');}
		    });	 
			return false;
	  }
});
$('#userfile').change(Enviar_imagen);

	$('#form_nuevousuario').keypress(function (e) {
	  if (e.which == 13) {
	    Guardar_usuario();
	  	$('#form_nuevousuario > .submit').toggleClass('pulsado');
	    return false;    //<---- Add this lineest
	  }

	});
	$('#form_nuevousuario > .submit').click(function(){

		Guardar_usuario();

		
	});
	$('#btn_confirmar_cambio_clave').click(function(){

		Guardar_nueva_clave();

	});
	
});
function prep_res_clave(idusuario)
{
	console.log('kka');
	 usuario_res = idusuario;
}

function validar_nuevousuario()
{
	var A= $('#form_nuevousuario .obligatorio');
	var b=true;
	$.each(A,function(i,item)
	{
		if ($(item).val()=='') {
			$(item).css('border-color','salmon');
			b &=false;
		}
		else
		{
			$(item).css('border-color','#cccccc');
			
		}
	});
	// verificar si el nombre de usuario ya existe

	
	
	return b;
}

function Guardar_usuario()
{
	console.log(validar_nuevousuario());
	if (!validar_nuevousuario()) {
		return;
	}
	else {
	
	formData = new FormData($("#form_nuevousuario")[0]);
	console.log(formData);
	$.ajax({
		       type: "POST",
		       url: base_url+'administrador/Guardar_usuario/'+$('#idusr_edicion').val(),
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		       		if (data =='Error. El usuario ya existe.') {
		       			$('.alert_incorrecto_user').addClass('dis_block');
		       		}
		           else
		           	{
		           		//inserion correcta->borrar
		           		$('#form_nuevousuario')[0].reset();
		           		$('.alert_correcto').addClass('dis_block');
		           		$('.alert_incorrecto').removeClass('dis_block');
		           		$('.alert_incorrecto_user').removeClass('dis_block');

		           	};

		       }, //fin success
		    	 error: function(data) {

		           	$('.alert_incorrecto').addClass('dis_block');
                    console.log(data);
                }
		    });	 //fin ajax	 
	}
}
function Confirmacion_nuevo_usuario(idpersona)
{
	

}

function ejecutar(action, parametro) {
		console.log(action);
		act = action +'('+parametro+')';
		eval(act);
}
function  get_form()
{


}
function get_usuarios(tipo)
{
	$.ajax({
       type: "POST",
       url: base_url+'administrador/get_usuarios_ajax/'+tipo,
       dataType:"json", 
       cache: false,
       contentType: false,
 	   processData: false,
       success: function(data)
       {
       	var rol='';
       		if (tipo ==1) {
       			rol='Administradorres';
       		}
       		else if(tipo ==2) {
       			rol='Profesores';
       		}
       		else if(tipo ==3) {
       			rol='Alumnos';
       		}
    		cadena = '<h5>Lista de '+rol+'</h5><table class="tabla_small w100"><thead><tr><th>N°</th><th>Nombre Completo</th><th>Usuario</th><th>Opciones</th></tr></thead><tbody>';
    		var i=0;
    		$.each(data,function(index,persona){
			cadena  +='<tr><td>'+ ++i +'</td><td><a href="'+base_url+'administrador/usuario/'+persona.usuario+'">'+persona.nombres+' '+ persona.ap+' '+persona.am+'</a></td><td>'+persona.usuario+'</td><td><a class="btn_default small" href="'+base_url+'administrador/editar_usuario/'+persona.idusuario+'"></span> Editar</a><a data-reveal-id="modal_resetear_clave" class="btn_default small" onclick="prep_res_clave('+persona.idusuario+')" href="'+base_url+'administrador/editar_usuario/'+persona.idusuario+'"></span> Resetear clave</a></td></tr>';

    		})


    		cadena +='</tbody></table>';
    		
    	
			$('#panel'+tipo).html(cadena);
		
    		// return cadena;

       }, //fin success
    	 error: function(data) {

            console.log("error get usuarios");
        }
    });
}

 function Enviar_imagen()
{
$('.nuevo_usuario-contfoto figure').addClass('load');
	$('#nuevo_usuario-foto').addClass('imgload');
	$('#nuevo_usuario-foto').attr('src',public_url+'img/loading.gif');
	formData = new FormData($(".form_imagen")[0]);
	
	
	console	.log(formData);


	$.ajax({
		       type: "POST",
		       url: base_url+'juegos/subir_archivo_unitario/fotos',
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
               async:false,
	     	   processData: false,
		       success: function(data)
		       {
		       		$('.nuevo_usuario-contfoto figure').removeClass('load');
					$('#nuevo_usuario-foto').removeClass('imgload');
		            $('#nuevo_usuario-foto').attr('src',data.ruta_archivo);
		            $('#label_subir_foto').html('Cambiar foto');
		            // obtener solo el nombre 
		            $('#ruta_foto').attr('value',data.nombre_archivo);

		       }, //fin success
		    	 error: function(data) {

                    console.log("ERROR AL GUARDAR FOTO. CONSULTE AL ADMINISTRADOR.");
                }
		    });	 
}

function Guardar_nueva_clave()
{
	formData = new FormData($(".form_reset")[0]);
	$.ajax({
		       type: "POST",
		       url: base_url+'administrador/resetear_clave/'+usuario_res,
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
               async:false,
	     	   processData: false,
		       success: function(data)
		       {
		       		if (data == 1) {
		       			$('.alert_correcto').addClass('dis_block');
		       			$('.alert_incorrecto').removeClass('dis_block');
		       		}
		       		else
		       		{
		       			$('.alert_incorrecto').addClass('dis_block');
		       			$('.alert_correcto').removeClass('dis_block');

		       		}
		       }, //fin success
		    	 error: function(data) {
                    $('.alert_incorrecto').addClass('dis_block');
                }
		    });	 
}


