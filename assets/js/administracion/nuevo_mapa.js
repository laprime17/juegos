	
// inciio ******************

// fin*********************
$(document).ready(function(){
	$('#userfile').change(Enviar_imagen);
	$('#btn_guardar_mapa').click(Guardar_mapa);
	url = '';
	game ='';
	nro_punto = 0;
	punto_activo = false;
	fondo ='';
	escala ='';
	idmapa = -1;
	punto_actual = { px: "-1", py: "-1", descripcion: "", idmapa: "-1"};
	inicio_sprite_back = 0;

	var sprite;
	var bounds;
	$( ".mapa-prev_img").dblclick(function() {
	  nuevo_punto();
	});	

	$('.aceptar_punto').click(Guardar_punto);
	$('.cancelar_punto').click(Cancelar_punto);
	// $('#previsualizar').click(previsualizacion);
	url_punto = public_url+'sprites/cursor2.png';
	imagen_puntero = new Image();
    imagen_puntero.src=url_punto;
    $('#cancelar_todo').click(Cancelar_mapa);
    $('.boton_previsualizacion').click(prev);

});
function prev()
{
    $('#frame_prev').attr('src',base_url+'juegos/mapa_previsualizacion/'+idmapa);
    $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/mapa_previsualizacion/'+idmapa);

}
function Cancelar_mapa()
{
	$.get( base_url+"juegos/mapa_cancelar/"+idmapa, function(data) {
	  Limpiar_todo();
	});
}
function Limpiar_todo()
{
	$('canvas').remove();
	$('.nuevo_mapa-contenedor_respuestas').html('');
	$('#tb_nombre').clear();
	$('#ta_descripcion').clear();

}

function Guardar_punto(nro_punto)
{
	punto_actual['descripcion'] = $('#text_'+nro_punto).val();
	jsonString = JSON.stringify(punto_actual);

	$.ajax({
		       type: "POST",
		       url: base_url+'juegos/mapa_nuevopunto/'+(punto_actual['px']-inicio_sprite_back)+'/'+punto_actual['py']+'/'+punto_actual['idmapa']+'/'+punto_actual['descripcion'],
		       data:  'data='+$('#mapa_id_mapa').val(),
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
		       success: function(data)
		       {
		    		// Eliminar caja de punto

		    		px = $('#punto_'+nro_punto).css('left');
		    		py = $('#punto_'+nro_punto).css('top');
		    		puntohecho ="<div class='punto_hecho' id='puntohecho_"+nro_punto+"' style='top:"+py+";left:"+px+";'>"+(nro_punto+1)+"</div>";
		    		$( ".mapa-prev_img").append(puntohecho);
		    		
		    		
		    		$('#punto_'+nro_punto).remove();
		    		punto_activo=false;

		    		// escribirlo en el tblero de puntos
		    		$('.nuevo_mapa-contenedor_respuestas').append('<div class="nuevo_mapa-contenedor_respuestas-item reciente"><div class="punto_hecho pr dib_med">'+(nro_punto+1)+'</div><div class="nombre_punto dib_med">'+punto_actual['descripcion']+'</div></div>');
		    		$('.nuevo_mapa-contenedor_respuestas-item.reciente').removeClass('reciente');


		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	

}
function Cancelar_punto(nro_punto)
{
	 punto = $('#punto_'+nro_punto);
	 punto_activo=false;
	 $(punto).remove();

}
function nuevo_punto()
{

// nuevo punto =false
	console.log("nuevo punto = FALSE");

	if (!punto_activo) //hya un punto activ pendiente de cancelacion o activacion
	{
		console.log("nuevo punto = true");
		// nuevo punto = true
		div = '<div class="nuevo_punto" id="punto_'+nro_punto+'"><input type="text" id="text_'+nro_punto+'" placeholder="Descripción"> <a class="boton verde aceptar_punto" onclick="Guardar_punto('+nro_punto+');" id="aceptar_'+nro_punto+'"><span class="icon-checkmark"></span>	</a> <div class="boton rojo" onclick="Cancelar_punto('+nro_punto+');"><span class="icon-cross cancelar_punto" id="cancelar_'+nro_punto+'"></span></div>  </div>';
		// deactivar function double click para evitar puntos juntos
		$( ".mapa-prev_img").append(div);


		$('#punto_'+nro_punto).css('top',sprite.position.y -imagen_puntero.height+10);
		$('#punto_'+nro_punto).css('left',sprite.position.x -imagen_puntero.width+10);
		$('#punto_'+nro_punto).focus();
		
		punto_actual['px'] = sprite.position.x;
		punto_actual['py']=sprite.position.y;
		punto_actual['idmapa']=$('#mapa_id_mapa').val();
		punto_activo = true;
		nro_punto++;
	}
	
}

 function Enviar_imagen()
{

	formData = new FormData($(".form_imagen")[0]);
	console.log("fomr",formData);
	
	$('.mapa-prev_img').html('<img class="cont-load" src="'+public_url+'img/loading.gif">');
	$.ajax({
		       type: "POST",
		       url: base_url+'juegos/subir_archivo',
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
               async:false,
	     	   processData: false,
		       success: function(data)
		       {
		       		// console.log('entro a satisfactorio',data);
		            $('#mapa_src_imagen').attr('value',data);
		           	url = data;
		           	fondo =  new Image();
		            fondo.src = url


		           	game = new Phaser.Game($('.mapa-prev_img').width(), $('.mapa-prev_img').height(), Phaser.AUTO, 'mapa-contenedorimg', { preload: preload, create: create },true);

		           // $('.mapa-prev_img').html('<img class="mapa_imagen" src="'+data+'">');
		          
		           ;

				// $('.mapa-prev_img').css('height',fondo.height);
				// $('.mapa-prev_img').css('width',fondo.width);

		           // crear juego en db
		           Guardar_mapa();



		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 
}


function Guardar_mapa()
{
	console.log('escala en guarda mapa',escala);
	formData = new FormData($(".form_datos")[0]);
	console.log("FORM DATA",FormData);
	$.ajax({
		       type: "POST",
		       url: base_url+'juegos/mapa_guardarmapa/'+idmapa,
		       data: formData,
		       dataType:"json", 
		       cache: false,
               contentType: false,
	     	   processData: false,
	     	   async:false,
		       success: function(data)
		       {
		           console.log('IDMAPA',data);
		           // errores , esribir id de juegos
		           $('#mapa_id_mapa').attr('value',data);
		           idmapa = data;
		           
		           


		       }, //fin success
		    	 error: function(data) {

                    console.log(data);
                }
		    });	 //fin ajax	 
}
function Guardar_escala()
{ 
	$.ajax({
       type: "POST",
       url: base_url+'juegos/mapa_guardar_escala/'+ escala+'/'+idmapa,
       data: formData,
       dataType:"json", 
       cache: false,
       contentType: false,
 	   processData: false,
 	   async:false,
       success: function(data)
       {


       }, //fin success
    	 error: function(data) {

            console.log(data);
        }
    });

}
function preload() {

    game.load.image('punto', url_punto);
    game.load.image("background", url);


}


function create() {

	$('canvas').addClass('oscuro');
    game.stage.backgroundColor = '#2d2d2d';
    escala =  $('.mapa-prev_img').height()/fondo.height;
    console.log('la escala es',escala);
    console.log('la idmapa es',idmapa);
    Guardar_escala();

    
    
    //  Create a graphic so you can see the bounds
   

  
    // obtener la posicion central para ubicar sprite "background"
    inicio_sprite_back =  ($('canvas').width()-fondo.width*escala)/2;

    var background = game.add.sprite(inicio_sprite_back,0, 'background');
     // obtener la escala de la imagen respecto al canvas 
    
    background.scale.setTo(escala,escala);
    sprite = game.add.sprite(inicio_sprite_back+imagen_puntero.width/2.1, imagen_puntero.height/2, 'punto');
    bounds = new Phaser.Rectangle(inicio_sprite_back+imagen_puntero.width/2.1, sprite.height/2, fondo.width*escala+sprite.width-5, fondo.height*escala+sprite.height);
     var graphics = game.add.graphics(bounds.x, bounds.y);
    // graphics.beginFill(0x000077);
    graphics.drawRect(0, 0, bounds.width, bounds.height);
    sprite.inputEnabled = true;
    sprite.anchor.set(0.5);
    sprite.input.enableDrag();
    sprite.input.boundsRect = bounds;
}
// juego en phaser

// fin phaser