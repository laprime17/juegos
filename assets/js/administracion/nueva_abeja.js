$(document).on('ready',ini);

function ini(){
    

    Imagenes = Array();
    Correctos = Array();
    imagenes_correctas_abierto = false;
    $('#guardar').click(guardar_abeja);
    $('#file_imgs').change(enviar_modelos);
    $('#btn_previsualizar').click(previsualizar);
    $('#premisa_img').change(actualizar_premisa);
}
function actualizar_premisa()
{
    $('#premisa').val($('#premisa_img').val());
}
function enviar_modelos()
  {
    console.log("lala");
    formData = new FormData($(".form_imgs")[0]);
    $.ajax({
             type: "POST",
             url: base_url+'juegos/abeja_uploadimgs',
             data: formData,
             dataType:"json", 
             cache: false,
              contentType: false,
             processData: false,
             success: function(data)
             {
                
                var j=0;
                var cont_modelo = '';
                console.log(data);
                 $.each(data,function(index,img){
                    console.log("data");
                    Imagenes.push(new Elemento(j,img));                        
                    cont_modelo += '<div class="abeja_img" id="item-'+j+'" data-id="'+j+'"><figure   class="abeja-galeria-todos-item"><img src="'+img.ruta+'"><article class="sombra"> <span class="icon-arrow-right" data-item="'+j+'" data-lista="incorrecto" onclick="seleccionar_imagen(this);" ></span></article></figure><figcaption><input type="text" value="'+img.nombre+'" class="abeja-galeria-itemdes"></figcaption></div>';
                    j++;
                 });
                 $('.abeja-galeria-todos').append(cont_modelo);
                 
             }, //fin success
             error: function(data) {
                  }
          });  //fin ajax  
  }
  function seleccionar_imagen(figure)
  {
        var span = figure;
        var nro = $(figure).data('item');
        id = '#item-'+nro;
       $(id).css('border-color','#037fa2');
       Correctos.push();
       // trasladar a panel derecho 
       $(figure).toggleClass('icon-arrow-right');
       $(figure).toggleClass('icon-arrow-left');


       if (!imagenes_correctas_abierto) {

        $('.abeja-galeria-correctos').addClass('ampliado');
        $('.abeja-galeria-todos').addClass('reducido');
        imagenes_correctas_abierto = true;

        // mover de padre
       

       }

        if ($(figure).data('lista')=="incorrecto") { 
            var figure = $(id).detach();
            $('.abeja-galeria-correctos').append(figure);
            console.log("ANTES");
            console.log($(figure).attr('data-lista'));
            console.log($(figure));
            $(span).data('lista','correcto');
            // agregar al array
            Correctos.push(Elemento_by_clave($(span).data('item')));                        

            console.log("DESPUES");
        }
        else
        {
            var figure = $(id).detach();
            $('.abeja-galeria-todos').append(figure);
            $(span).data('lista','incorrecto');
        }


  }

    function Elemento_by_clave(j)
    {
        console.log("JOTA",j);
        for (var i = Imagenes.length - 1; i >= 0; i--) {
            console.log("CLAVES",Imagenes[i].clave);
            if(Imagenes[i].clave==j)
            {
                return Imagenes[i];
            }
           
        }
        return null;
    }
    function Elemento(clave_, valor_)
    {
        this.clave = clave_;
        this.valor = valor_;
        this.toLowerCase = function () { return this.valor.toLowerCase(); };
        this.indexOf = function (texto_) { return this.valor.indexOf(texto_); };
        this.replace = function (old_, new_) { return this.valor.replace(old_, new_); };
        this.toString = function(){ return this.clave.toString() +'-'+ this.valor.toString();};            
    }
    function previsualizar()
    {
        // setear datos a localStorage
        console.log('VLIDACION',validar());
       if (validar())
       {

            console.log('RAYOS',validar());
            localStorage.setItem('abeja_titulo',$('#titulo').val());
            localStorage.setItem('abeja_duracion',$('#tiempo').val());
            localStorage.setItem('abeja_puntaje',$('#puntaje').val());
            localStorage.setItem('abeja_premisa',$('#premisa').val());
            localStorage.setItem('abeja_imagenes',JSON.stringify(Imagenes));
            localStorage.setItem('abeja_correctos',JSON.stringify(Correctos));
            $('.lanzar_modal_prev').trigger('click');
            $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/previsualizacion/abeja/');
       }



       
    }
    function validar()
    {
        var valido = true;
        
        $('.requerido').each(function(){
            if ($(this).val()=='') {
                console.log($(this));
                $(this).addClass('input_required');
                // console.log('&&&',valido);
                valido = valido && false;
            }
        });
        // console.log('DESPUES',valido);
        return valido;
    }

function guardar_abeja()
{

    formData = new FormData($(".form_datos")[0]);
    // guardar datos de juego y juego_abeja
    $.ajax({
             type: "POST",
             url: base_url+'juegos/abeja_guardar',
             data: formData,
             dataType:"json", 
             cache: false,
              contentType: false,
             processData: false,
             success: function(idabeja)
             {
                if (data!=1) {
                    alert('Error al guardar actividad, por favor recargue la página');
                }
                else
                {
                    vincular_imagenes(idabeja);
                }

             }, //fin success
             error: function(data) {
                  }
          });
    // vincular imágenes

}
function vincular_imagenes(idabeja)
{
    // vincular imagenes a una actividad tipo abeja
    $.ajax({
             type: "POST",
             url: base_url+'juegos/abeja_vincular/'+idabeja,
             data: 'idabeja='+idabeja+'&=imgs'+JSON.toString(Imagenes)+'&=imgs_correctas'+JSON.toString(Correctos),
             dataType:"json", 
             cache: false,
              contentType: false,
             processData: false,
             success: function(data)
             {
                if (data!=1) {
                    alert('Error al guardar actividad, por favor recargue la página');
                }
             }, //fin success
             error: function(data) {
                  }
          });

}


// ********************************************************

