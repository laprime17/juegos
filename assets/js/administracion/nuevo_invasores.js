$(document).ready(function(){
	//$('#userfilememoria').change(Enviar_imagen);
	// url = '';
   initSample();
	 idinvasores=-1;
	 nroejer_dinero = 0;
   nroejer_fig = 0;
   nropreg_comprension = 0;
   mayor=0;
	 localStorage.clear();
	$('#nroejercicios').click(Cargar_ejercicioDinero);
  $('#nroejercicios_figura').click(Cargar_ejercicioFigura);
  $('#nroejercicios_comprension').click(Cargar_preguntasComprension);
  
	$('#guardardatos').click(Guardar_invasores);

	$('.boton_previsualizacion').click(prev);
    //$('#cancelar_todo').click(Cancelar_mapa);
});
function prev()
{
    $('#modal_prev').foundation('reveal', 'open', base_url+'juegos/invasores_previsualizacion/'+idinvasores);
}
function Cargar_ejercicioDinero()
{
	$('#actividad_dinero').empty();
	nroejer_dinero = $('#nroejercicios_dinero').val();
	for (var i = 1; i <= nroejer_dinero; i++) {
		divnom = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+i+"</div><div class='column large-2'><div class=''><input type='number' name='soles_"+i+"' id='soles_"+i+"' placeholder='Monto soles'/></div></div><div class='column large-2'><div class=''><input type='number' name='centimos_"+i+"' id='centimos_"+i+"' placeholder='Céntimos'/></div></div><div class='column large-7'><div class=''><input type='text' name='enunciado_"+i+"' id='enunciado_"+i+"' placeholder='Enunciado'/></div></div></div>";
		$( "#actividad_dinero").append(divnom);
	};
}
function Cargar_ejercicioFigura()
{
  $('#actividad_figura').empty();
  nroejer_fig = $('#nroejercicios_fig').val();
  console.log('nro',nroejer_fig);
  for (var i = 1; i <= nroejer_fig; i++) {
    divnom = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+i+"</div><div class='column large-4'><div class=''><select name='figura_"+i+"' id='figura_"+i+"'><option value='triangulo'>Triángulos</option><option value='cuadrilatero'>Cuadriláteros</option><option value='circulo'>Círculos</option><option value='poligono'>Poligonos</option><option value='nopoligono'>No polígonos</option><option value='poligono_r'>Polígonos regulares</option><option value='poligono_i'>Polígonos irregulares</option></select></div></div><div class='column large-7'><div class=''><input type='text' name='enunciadofig_"+i+"' id='enunciadofig_"+i+"' placeholder='Enunciado'/></div></div></div>";
    $( "#actividad_figura").append(divnom);
  };
}
function Cargar_preguntasComprension()
{
  $('#actividad_comprension').empty();
  nropreg_comprension = $('#nroejercicios_comprension').val();
  //console.log('nro comprension: ',nropreg_comprension);
  for (var i = 1; i <= nropreg_comprension; i++) {
    var divpreg = "<div class='row'><div class='column large-1' style ='background: #B6C9CA; border-radius: 12px; text-align: center;'>"+i+"</div><div class='column large-11'><input type='text' name='pregunta_"+i+"' id='pregunta_"+i+"' placeholder='Pregunta'/></div><div class='column large-2'>Respuesta:</div><div class='column large-10'><input type='text' name='respuesta_"+i+"' id='respuesta_"+i+"' placeholder='Respuesta'/></div><div class='column large-2'>Distractor1:</div><div class='column large-10'><input type='text' name='primerDistractor_"+i+"' id='primerDistractor_"+i+"' placeholder='Primer distractor'/></div><div class='column large-2'>Distractor2:</div><div class='column large-10'><input type='text' name='segundoDistractor_"+i+"' id='segundoDistractor_"+i+"' placeholder='Segundo distractor'/></div></div>";
    $( "#actividad_comprension").append(divpreg);
  };
}
function Guardar_invasores()
{
	Agregar_allocalstorage();
  var almacenado = Array();
  almacenado =  localStorage;
  var detalles = JSON.stringify(almacenado);
  $('#iddetallesinvasores').attr('value',detalles);
  //console.log('guardaremos invasores',detalles);
  localStorage.clear();
  Agregar_allocalstorage_fig();
  var infofiguras = Array();
  infofiguras = localStorage;
  var detallesfiguras = JSON.stringify(infofiguras);
  $('#iddetallesinvasores_fig').attr('value',detallesfiguras);
  //console.log('detalles de figuras',detallesfiguras);

  localStorage.clear();
  Agregar_allocalstorage_comprension();
  var infocomprension = Array();
  infocomprension = localStorage;
  var detallescomprension = JSON.stringify(infocomprension);
  $('#iddetallesinvasores_texto').attr('value',detallescomprension);
  //console.log('detalles de figuras',detallescomprension);

  localStorage.clear();
  Agregar_allocalstorage_texto();
  var textoprinc = Array();
  textoprinc = localStorage;
  var detallestexto = JSON.stringify(textoprinc);
  $('#idinvasores_textoprincipal').attr('value',detallestexto);
  console.log('texto : ',detallestexto);
  

	formData = new FormData($(".form_datos")[0]);
	console.log("FORM DATA",FormData);
    $.ajax({
               type: "POST",
               url: base_url+'juegos/invasores_guardarinvasores',
               data: formData,
               dataType:"json", 
               cache: false,
               contentType: false,
               processData: false,
               success: function(data)
               {
                   console.log('idjuego',data);
                   $('#invasores_id_invasores').attr('value',data);
                   idinvasores = data;
                   
                   $('#aviso_guardado').html('<p id="guardado_exito">Se guardó exitosamente</p>');
                   $('#iframe_prev').attr('src',base_url+'juegos/invasores_previsualizacion/'+idinvasores);

               }, //fin success
                 error: function(data) {

                    $('#aviso_guardado').html('<p id="guardado_exito">No se guardaron los cambios</p>');
                    console.log(data);
                }
            });  //fin ajax  
}
function Agregar_allocalstorage()
{
  
	for (var i = 1; i <= nroejer_dinero; i++) {
  
      localStorage.setItem('montosoles'+i, $('#soles_'+i).val());
      localStorage.setItem('montocentimos'+i, $('#centimos_'+i).val());
      localStorage.setItem('enunciado'+i, $('#enunciado_'+i).val()); 
		
	};
}
function Agregar_allocalstorage_fig()
{
    for (var i = 1; i <= nroejer_fig; i++) {
      
      localStorage.setItem('tipofigura'+i,$('#figura_'+i).val());
      localStorage.setItem('enunciadofig'+i,$('#enunciadofig_'+i).val());
    
    };
}
function Agregar_allocalstorage_comprension()
{
    for (var i = 1; i <= nropreg_comprension; i++) {
      
      localStorage.setItem('pregunta'+i,$('#pregunta_'+i).val());
      localStorage.setItem('respuesta'+i,$('#respuesta_'+i).val());
      localStorage.setItem('primerDistractor'+i,$('#primerDistractor_'+i).val());
      localStorage.setItem('segundoDistractor'+i,$('#segundoDistractor_'+i).val());
    };
}
function Agregar_allocalstorage_texto()
{
  var contenido_editor = CKEDITOR.instances['editor'].getData();
  localStorage.setItem('texto_comprension',contenido_editor);
  // var contenido_editor = $('#editor').val();
  // localStorage.setItem('texto_comprension',contenido_editor);
}
